#!/bin/python

import sys
import pandas as pd

# Reading and merging csv files
df = pd.read_csv(sys.argv[1])
df.rename(columns = {'ratio_cycles':'ratio_cycles1', 'ratio_time':'ratio_time1'}, inplace = True)
for i in range(2, len(sys.argv)):
    sdf = pd.read_csv(sys.argv[i])
    sdf.rename(columns = {'ratio_cycles':('ratio_cycles'+str(i)), 'ratio_time':('ratio_time'+str(i))}, inplace = True)
    df = df.merge(sdf, on="test_name", how="inner")

indices=[]
for idx, row in df.iterrows():
    brow = row[1:].map(lambda x: x==0)
    if brow.all():
        indices.append(idx)
df.drop(indices, inplace=True)

df.to_csv("merged.csv")
