#!/bin/bash
# @author gourdinl
# Script to profile several CompCert functions
# Args: $1=jq program; $2=json file; $3=list of functions

if [ $# -ne 3 ]; then
    echo "Usage: ./prof_function.sh <jq_program> <json_file> <fct1;fct2;...>"
    echo $#
    exit 1
fi

JQ=$1
JSON=$2
FCTS=$3

convert_to_bc () {
  printf "%.20f" $1
}

test_name=$($JQ '.label' $JSON)
total_cycles=$(convert_to_bc $($JQ '.nodes | .[] | select(.kind=="root") | .time' $JSON))
total_time=$(convert_to_bc $($JQ '.nodes | .[] | select(.kind=="root") | .sys_time' $JSON))

IFS=';'
read -ra arr_FCTS <<< "$FCTS"
sum_fct_time=0
sum_fct_cycles=0
for fct in ${arr_FCTS[@]}; do
  echo $fct
  fct_cycles=$(convert_to_bc $($JQ '.nodes | .[] | select(.name | test("'$fct'")) | .time' $JSON))
  fct_time=$(convert_to_bc $($JQ '.nodes | .[] | select(.name | test("'$fct'")) | .sys_time' $JSON))
  sum_fct_time=$(bc -l <<< "$sum_fct_time + $fct_time")
  sum_fct_cycles=$(bc -l <<< "$sum_fct_cycles + $fct_cycles")
done

echo "total_cycles,total_time"
echo "$total_cycles,$total_time"
echo "sum_fct_cycles,sum_fct_time"
echo "$sum_fct_cycles,$sum_fct_time"
if (( $(bc -l <<< "$sum_fct_cycles > 0") )) && (( $(bc -l <<< "$sum_fct_time > 0") )); then
  ratio_cycles=$(bc -l <<< "($sum_fct_cycles / $total_cycles) * 100")
  ratio_time=$(bc -l <<< "($sum_fct_time / $total_time) * 100")
  echo "test_name,ratio_cycles,ratio_time"
  echo "$test_name,$ratio_cycles,$ratio_time"
else
  echo "$test_name,0,0"
fi
