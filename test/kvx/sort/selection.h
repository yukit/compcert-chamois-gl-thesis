#ifndef __SELECTION_H__
#define __SELECTION_H__

int select_sort(uint64_t *res, const uint64_t *T);

#endif // __SELECTION_H__
