#ifndef __MERGE_H__
#define __MERGE_H__

int merge_sort(uint64_t *res, const uint64_t *T);

#endif // __MERGE_H__

