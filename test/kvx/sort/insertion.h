#ifndef __INSERTION_H__
#define __INSERTION_H__

int insert_sort(uint64_t *res, const uint64_t *T);

#endif // __INSERTION_H__
