int main(int n)
{
  int x = 1, i;

  /* for positive values */
  if (n > 0)
  {
    for (; x <= n >> 1;)
    {
      x = x << 1;
    }
    n = x;
  }
  /* for negative values */
  else
  {
    n = ~n;
    n = n + 1;
    for (; x <= n >> 1;)
    {
      x = x << 1;
    }
    x = x << 1;
    x = ~x;
    x = x + 1;
    n = x;
  }
  return n;
}
