int main()
{
  int n,sum,i,t,a,z;

  for(i = 1; i <= 500; i++)
  {
      t = i;  // as we need to retain the original number
      sum = 0;
      while(t != 0)
      {
          a = t%10;
          sum += a*a*a;
          t = t/10;
      }

      if(sum == i)
        z += i;
  }

  return 0;
}
