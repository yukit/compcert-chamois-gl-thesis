#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

typedef uint32_t data;

void quicksort(data x, data *A, int len);

int main (void) {
  int len = 1000;
  
  data *a = malloc(sizeof(data) * len);
  
  for (int i = 0; i < len; i++) {
    a[i] = i;
  }

  const data base = UINT32_C(0x81234563);
  for(data x=base; x<base+300000; x+=2) {
    quicksort(x, a, len);
  }
  quicksort(1, a, len);
  
  for (int i = 0; i < len; i++) {
    if(a[i] != i) return 1;
  }
  
  free(a);
  
  return 0;
}

void quicksort(data x, data *A, int len) {
  if (len < 2) return;

  data pivot = A[len / 2];

  int i, j;
  for (i = 0, j = len - 1; ; i++, j--) {
    while (x*A[i] < x*pivot) i++;
    while (x*A[j] > x*pivot) j--;

    if (i >= j) break;

    data temp = A[i];
    A[i]     = A[j];
    A[j]     = temp;
  }

  quicksort(x, A, i);
  quicksort(x, A + i, len - i);
}

