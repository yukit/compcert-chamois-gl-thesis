typedef struct {
  int b;
  int a;
} * CLAUSE;
__inline__ int g(CLAUSE c) { return c->b; }
__inline__ int d(CLAUSE c) { return c->a; }
__inline__ void clause_SetNumOfConsLits(CLAUSE c, int e) {
  c->b = e;
  c->a = e;
}
__inline__ int f(CLAUSE c) { return g(c) + d(c); }
__inline__ int clause_LastLitIndex(c) { return f(c); }
