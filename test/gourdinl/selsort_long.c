#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

typedef uint32_t data;

void selsort(data x, data *A, long len);

int main (void) {
  long len = 1000;
  
  data *a = malloc(sizeof(data) * len);
  
  for (long i = 0; i < len; i++) {
    a[i] = i;
  }

  const data base = UINT32_C(0x81234563);
  for(data x=base; x<base+10005; x+=2) {
    selsort(x, a, len);
  }
  selsort(1, a, len);
  
  for (long i = 0; i < len; i++) {
    if(a[i] != i) return 1;
  }
  
  free(a);
  
  return 0;
}

void selsort(data x, data *A, long len) {
  for (long i = 0; i < len; i++) {
    long m = i;
    for (long j = i+1; j < len; j++) {
      if (x*A[j] < x*A[m]) {
	m = j;
      }
    }
    data t;
    t = A[i];
    A[i] = A[m];
    A[m] = t;
  }
}
