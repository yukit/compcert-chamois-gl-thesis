int sum(int *t, int n) {
  int i = 0, s = 0;
  while (i < n) {
    s += t[i];
    i++;
  }
  return s;
}
