#!/bin/bash

# This script can be use to check non-reg between two clones of CompCert
# Example:
# ./compare_ccomps.sh $(pwd) /home/gourdinl/Work/VERIMAG/Compcert_three/ccomp /home/gourdinl/Work/VERIMAG/Compcert_one/ccomp "-flct -flct-trap -flct-sr -fstruct-passing -S"

dir=$1
ccomp1=$2
ccomp2=$3
args=$4

filter="sed -i '1,2d'"
tmp1="tmp1.s"
tmp2="tmp2.s"

for file in $dir/*.c; do
  rm -f $dir/$tmp1 $dir/$tmp2
  cmd1="$ccomp1 $args $file -o $dir/$tmp1 > /dev/null 2>&1"
  cmd2="$ccomp2 $args $file -o $dir/$tmp2 > /dev/null 2>&1"
  #echo "cmd1=$cmd1"
  #echo "cmd2=$cmd2"
  eval "$cmd1"
  eval "$cmd2"
  eval "$filter $dir/$tmp1"
  eval "$filter $dir/$tmp2"
  if cmp -s $dir/$tmp1 $dir/$tmp2; then
    echo "for $file => OK"
  else
    diff -y $dir/$tmp1 $dir/$tmp2
    echo "for $file => KO!"
    #exit 1
  fi
done

exit 0
