#!/bin/bash

../../ccomp -S clause2.c -o clause2.nopostpass.noph.s -fno-coalesce-mem -fno-postpass
../../ccomp -S clause2.c -o clause2.nopostpass.ph.s -fcoalesce-mem -fno-postpass
../../ccomp -S clause2.c -o clause2.noph.s -fno-coalesce-mem
../../ccomp -S clause2.c -o clause2.ph.s -fcoalesce-mem
