long main(long x, long n) {
  long i = 0;
  while (i < n) {
    x += i * 5;
    i += 3;
  }
  return x;
}
