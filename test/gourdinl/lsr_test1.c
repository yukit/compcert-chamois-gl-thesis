long main(long x, long n) {
  long l = 10;
  long y = 0;
  for(int i = 0; i < n; i++) {
    y += x * l;
    x += 3;
  }
  return x+y;
}
