double approx(double *a) {
  double r = 0;
  if (a[0] < 0) return 0;
  while (r < a[2])
    if (r >= a[1]) r -= a[0];
    else r *= 7;
  return r;
}
