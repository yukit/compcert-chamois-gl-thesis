int main(int x, int *y, int z) {
  int a = *y, b;
  while (a < 100) {
    b = z << 2;
    if (a > 35) {
      x += a * b;
      x *= *y;
    }
    a += x;
  }
  return a;
}
