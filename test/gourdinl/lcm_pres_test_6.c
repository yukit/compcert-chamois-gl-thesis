double approx(double *a) {
  double r = 2;
  if (a[0] < 2) return 2;
  while (r < a[1])
    if (r >= a[2]) r -= a[0];
    else r *= 7;
  return r;
}
