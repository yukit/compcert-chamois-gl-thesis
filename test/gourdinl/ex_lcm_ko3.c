#include <stdio.h>

long x;

long bar(long a, long *y){
  long s = x;
  do {
    s += x;
    *y = x+1;
    s += x;
    *y = x+1;
    s += x;
    *y = x+1;
  } while (x < a);
  return s;
}

int main() {
  long r;
  x=0;
  r=bar(100,&x);
  printf("%ld\n",r);
}
