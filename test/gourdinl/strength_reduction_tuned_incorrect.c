int sum(int *t, int n) {
  int *ti = t, *tn = t + n;
  int s = 0;
  while (ti < tn) {
    s += *ti;
    ti++;
  }
  return s;
}

/* Calling sum with (NULL, -1) will produce a segfault!
int main() {
  sum(NULL, -1);
}
*/
