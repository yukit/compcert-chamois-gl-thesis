typedef int b;
struct c {
  b d[5];
} static e(struct c *ctx) {
  int f;
  b a, g = ctx->d[0];
  for (; f; f++)
    g = a;
  a = g;
  ctx->d[0] += a;
}
void h(struct c *ctx) {
  if (ctx)
    e(ctx);
}
