int sum_until_max(int x, int *A, int max) {
  int sum = 0;

  for(int i = 0; A[i] < max; i++)
    sum += A[i];

  return sum;
}

