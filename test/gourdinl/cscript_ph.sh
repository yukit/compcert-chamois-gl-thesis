#!/bin/bash

/home/yuki/Work/VERIMAG/Compcert_neutral/ccomp -stdlib ../../runtime -dparse -dclight -S -fstruct-return   -c clause2.c > log 2>&1

b1=$(cat log | ack "LDP_CONSEC_PEEP_IMM_DEC_ldr64")
sb1=$?
b2=$(cat log | ack "LDP_BACK_SPACED_PEEP_IMM_DEC_ldr32")
sb2=$?
b3=$(cat log | ack "STP_FORW_SPACED_PEEP_IMM_INC_str32")
sb3=$?
b4=$(cat log | ack "STP_CONSEC_PEEP_IMM_INC_str64")
sb4=$?

#if [ "$sb1" == 0 ] && [ "$sb2" == 0 ] && [ "$sb3" == 0 ] && [ "$sb4" == 0 ]
if [ "$sb1" == 0 ] && [ "$sb2" == 0 ] && [ "$sb3" == 0 ] && [ "$sb4" == 0 ]
then
  exit 0
else
  exit 1
fi
