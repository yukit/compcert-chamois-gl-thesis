import sys
if len(sys.argv )< 2:
    sys.exit(1)
n=int(sys.argv[1])
if len(sys.argv) < 3:
    file=sys.stdout
else:
    file=open(sys.argv[2], 'w')

file.write("""#include <stdlib.h>
#include <stdint.h>
int main(int argc, char **argv) {
  if (argc < 2) return 2;
  uint32_t x = atoi(argv[1]);
""")
x = 0;
for i in range(n):
  file.write("""  x ^= UINT32_C(0xDEADBEEF);
  x += UINT32_C(0xABBA);
""")
  x = ((x ^ 0xDEADBEEF) + 0xABBA) & 0xFFFFFFFF;
file.write(f"""  return (x != UINT32_C({x}));
}}
""")
