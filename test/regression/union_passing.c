#include <stdint.h>
#include <stdio.h>
static int32_t bk;
union ba {
  int64_t bb;
};
static void dada(union ba);
void nothing(void);
void stuff(void) {
  union ba f = {5};
  int32_t i[1000];
  nothing();
  dada(f);
}
static void dada(union ba k) {
  bk = k.bb;
}
void nothing(void) {
}
int main() {
  stuff();
  printf("result = %d\n", bk);
}
