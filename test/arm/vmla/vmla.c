#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <inttypes.h>

union double_u64 {
  double f;
  uint64_t u; 
};

static uint64_t double_to_u64(double x) {
  union double_u64 z;
  z.f = x;
  return z.u;
}

static double u64_to_double(uint64_t x) {
  union double_u64 z;
  z.u = x;
  return z.f;
}

double mul_add(double a, double b, double c) {
  return a + b * c;
}
double mul_add2(double a, double b, double c) {
  return b * c + a;
}

int main() {
  double x = u64_to_double(UINT64_C(0x7FF8000000000000));
  double y = u64_to_double(UINT64_C(0x7FFFFFFFFFFFFFFF));
  double z = u64_to_double(UINT64_C(0xFFFFFFFFFFFFFFFF));
  uint64_t px = double_to_u64(x);
  uint64_t py = double_to_u64(y);
  uint64_t pz = double_to_u64(z);
  printf("x=%" PRIx64 " y=%" PRIx64 " z=%" PRIx64 "\n", px, py, pz);
  double a = mul_add(x, y, z);
  double b = mul_add2(x, y, z);
  uint64_t pa = double_to_u64(a);
  uint64_t pb = double_to_u64(b);
  printf("a=%" PRIx64 " b=%" PRIx64 "\n", pa, pb);
}
