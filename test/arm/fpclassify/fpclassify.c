#include <math.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>

int isfinite_float(float x) {
  return isfinite(x);
}

int isnan_float(float x) {
  return isnan(x);
}

union float_uint32 {
  float f;
  uint32_t u;
};

static inline uint32_t float2uint32(float x) {
  union float_uint32 v;
  v.f = x;
  return v.u;
}

int isfinite_float2(float x) {
  return ((float2uint32(x) >> 23) & 0xFF) != 0xFF;
}

int isnan_float2(float x) {
  return x!=x;
}

#if 1
int main() {
  static float vals[] = {0, -0., 1, -1, 55, -67, 1./0., -1./0., 0./0. };
  for(int i=0; i<sizeof(vals)/sizeof(vals[0]); i++) {
    float x = vals[i];
    uint32_t u = float2uint32(x);
    /* printf("%f %08" PRIx32 " %08" PRIx32 "\n", x,
       float2uint32(x), __builtin_bits_of_float(x)); */
    if (__builtin_bits_of_float(__builtin_float_of_bits(__builtin_bits_of_float(x))) != u) {
      printf("error bits %f %08" PRIx32 "\n", x, u);
    }
    if (isfinite_float(x) != isfinite_float2(x)) {
      printf("error isfinite_float %f\n", x);
    }
    if (isnan_float(x) != isnan_float2(x)) {
      printf("error isnan_float %f\n", x);
    }
  }
  return 0;
}
#else
int main() {
  for(int i=0; i<1000000000; i++) {
    if (! isfinite_float2(i)) return 1;
  }
  return 0;
}
#endif
