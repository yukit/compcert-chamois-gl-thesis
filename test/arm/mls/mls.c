#include <stdlib.h>
#include <stdio.h>

int mul_subtract(int x, int y, int z) {
  return x - y*z;
}

int main(int argc, char **argv) {
  if (argc != 4) return 1;
  printf("%d\n", mul_subtract(atoi(argv[1]), atoi(argv[2]), atoi(argv[3])));
  return 0;
}
