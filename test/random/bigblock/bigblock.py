import sys, random
file=sys.stdout
n=200
seed=33

gen=random.Random()
gen.seed(seed)
file.write("""#include <stdlib.h>
#include <stdint.h>
int main(int argc, char **argv) {
  if (argc < 2) return 1;
  uint32_t x = atoi(argv[1]);
""")
x = 0;
for i in range(n):
  file.write("""  x ^= UINT32_C(0xDEADBEEF);
  x += UINT32_C(0xABBA);
""")
  x = ((x ^ 0xDEADBEEF) + 0xABBA) & 0xFFFFFFFF;
file.write(f"""  return (x != UINT32_C({x}));
}}
""")
