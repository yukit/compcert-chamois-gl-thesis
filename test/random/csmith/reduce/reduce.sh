#!/bin/bash
INCLUDE=-I$COMPCERT/test/monniaux/csmith/csmith/include/csmith-2.3.0
if kvx-cos-gcc $INCLUDE -Werror=uninitialized -Werror=implicit source.c -o source.gcc.target && kvx-cluster -- source.gcc.target >& source.gcc.log
then
    if $COMPCERT/ccomp $INCLUDE -fall -fno-cse -fno-cse2 -fno-cse3 source.c -o source.ccomp.target
    then
	kvx-cluster -- source.ccomp.target >& source.ccomp.log
	grep OPCODE source.ccomp.log
    else
	exit 2
    fi
else
    exit 1
fi
    
