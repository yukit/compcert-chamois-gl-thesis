#include <stdlib.h>
#include <stdio.h>

int main(int argc, char **argv) {
  if (argc < 2) return 1;
  int i = atoi(argv[1]);
  if (i > 0) {
    printf("positive\n");
  } else if (i==0) {
    printf("zero\n");
  } else {
    printf("negative\n");
  }
  return 0;
}
