if ! aarch64-linux-gnu-gcc -fno-sanitize-recover -fsanitize=address -fsanitize=undefined -Wall -Werror=format -Werror=implicit -Werror=uninitialized -Werror=return-type -Werror=main -Werror=missing-prototypes -Werror=int-conversion -I . source.c -o source.gcc-sanitized.aarch64 2> source.gcc-sanitized.err ;
then exit 1;
fi
if ! ASAN_OPTIONS='detect_leaks=0' qemu-aarch64 -L /usr/aarch64-linux-gnu ./source.gcc-sanitized.aarch64 2>&1 > source.gcc-sanitized.out ;
then exit 2 ;
fi
if ! grep -q checksum source.gcc-sanitized.out ;
then exit 3 ;
fi

if ! gcc source.c -o source.gcc.amd64 ;
then exit 5;
fi
if ! valgrind --error-exitcode=42 ./source.gcc.amd64 ;
then exit 6 ;
fi

if ! /local/monniaux/Kalray/kvx-work/ccomp -fstruct-passing -fbitfields -fno-cse2 -fno-cse -fno-cse3 -I . source.c -o source.ccomp.aarch64 ;
then exit 4 ;
fi
qemu-aarch64 -L /usr/aarch64-linux-gnu ./source.ccomp.aarch64 2>&1 > source.ccomp.out
! cmp source.gcc-sanitized.out source.ccomp.out
