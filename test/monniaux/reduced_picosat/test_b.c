#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>

extern uint64_t dummyload(void);

int main() {
  printf("%" PRIu64 "\n", dummyload());
}
