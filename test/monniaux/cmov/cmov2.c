#include <stdio.h>

long cmovl(int x, long y, long z) {
  return x ? y : z;
}

int cmovi(int x, int y, int z) {
  return x ? y : z;
}

double cmovd(int x, double y, double z) {
  return x ? y : z;
}

float cmovf(int x, float y, float z) {
  return x ? y : z;
}

int main() {
  printf("%ld\n", cmovl(1, 42, 65));
  printf("%ld\n", cmovl(0, 42, 65));
  printf("%d\n", cmovi(1, 42, 65));
  printf("%d\n", cmovi(0, 42, 65));
  printf("%f\n", cmovd(1, 42., 65.));
  printf("%f\n", cmovd(0, 42., 65.));
  printf("%f\n", cmovf(1, 42.f, 65.f));
  printf("%f\n", cmovf(0, 42.f, 65.f));
}
