extern _Bool get_out(void);
#define loop(T, T1)  \
T loop_##T1(T n) { \
  for(T i=0; i < n; i++) { \
     if (get_out()) return i; \
  } \
  return 0; \
}

loop(signed int, signed_int)
loop(unsigned int, unsigned_int)

loop(signed long, signed_long)
loop(unsigned long, unsigned_long)
  
