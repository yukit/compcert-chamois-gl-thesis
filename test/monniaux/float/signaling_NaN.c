#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>

#ifdef __COMPCERT__
#define float_of_bits(x) __builtin_float_of_bits(x)
#define bits_of_float(x) __builtin_bits_of_float(x)
#else
union float_bits {
  float f;
  uint32_t i32;
};

float float_of_bits(uint32_t x) {
  union float_bits u;
  u.i32 = x;
  return u.f;
}
uint32_t bits_of_float(float x) {
  union float_bits u;
  u.f = x;
  return u.i32;
}
#endif

extern float mul(float, float);

int main() {
  float s = float_of_bits(UINT32_C(0xFF8FFFFF));
#ifdef EXTERN_MUL
  float q = mul(s, 1.0f);
#else
  float q = s * 1.0f;
#endif
  uint32_t s1 = bits_of_float(s);
  uint32_t q1 = bits_of_float(q);
  printf("%d\t%" PRIx32 "\t%" PRIx32 "\n", s1 == q1, s1, q1);
  printf("FLT_EVAL_METHOD = %d\n", FLT_EVAL_METHOD);
  return 0;
}
