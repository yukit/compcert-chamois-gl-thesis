#include <stdint.h>
#include <limits.h>
#include <stdint.h>

#define LOOPC 3000
#if __WORDSIZE == 32
typedef uint32_t cycle_t;
#define PRcycle PRId32
#else
typedef uint64_t cycle_t;
#define PRcycle PRId64
#endif

void clock_prepare(void);
void clock_stop(void);
void clock_start(void);
cycle_t get_total_clock(void);
cycle_t get_current_cycle(void);
void print_total_clock(void);
void printerr_total_clock(void);
