#include <assert.h>

#if defined(PROTECT) && defined(__COMPCERT__)
#define copy(typ, x) __builtin_copy_##typ((x), __LINE__)
#else
#define copy(typ, x) ((int) (x))
#endif

long duplique(long x, long y) {
  long x2 = copy(long, x);
  long y2 = copy(long, y);
  long z = x * y + 4;
  long z2 = x2 * y2 + 4;
  assert(z == z2);
  return z;
}
