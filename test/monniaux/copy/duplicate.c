#include <assert.h>

#if defined(PROTECT) && defined(__COMPCERT__)
#define copy(typ, x) __builtin_copy_##typ((x), __LINE__)
#else
#define copy(typ, x) ((int) (x))
#endif

int duplique(int x, int y) {
  int x2 = copy(int, x);
  int y2 = copy(int, y);
  int z = x * y + 4;
  int z2 = x2 * y2 + 4;
  assert(z == z2);
  return z;
}
