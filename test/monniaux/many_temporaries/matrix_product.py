m=4
n=5
p=6
number_type='double'
with open('matrix_product.c', 'w') as cfile:
    cfile.write(f'void matrix_product({number_type} c[{m}][{p}], const {number_type} a[{m}][{n}], const {number_type} b[{n}][{p}]) {{\n')
    for i in range(m):
        for j in range(n):
            for k in range(p):
                cfile.write(f'  const {number_type} p_{i}_{j}_{k} = a[{i}][{j}] * b[{j}][{k}];\n')
    for i in range(m):
        for k in range(p):
            cfile.write(f'  c[{i}][{k}] = ')
            for j in range(n):
                if j>0:
                    cfile.write(' + ')
                cfile.write(f'p_{i}_{j}_{k}')
            cfile.write(';\n')
    cfile.write('}\n')
    
