	.align 8
	.global my_udiv32
	.type	my_udiv32, @function
my_udiv32:
	zxwd $r1 = $r1
	make $r3 = 0x3ff0000000000000 # 1.0
	zxwd $r0 = $r0
	;;
	floatud.rn $r5 = $r1, 0
	;;
	floatuw.rn $r2 = $r1, 0
	;;
	finvw $r2 = $r2
	;;

	fwidenlwd $r2 = $r2
	floatud.rn $r4 = $r0, 0
	;;
	ffmsd $r3 = $r2, $r5
	;;
	ffmad $r2 = $r2, $r3
	;;
	fmuld $r2 = $r2, $r4
	;;
	fixedud.rn $r2 = $r2, 0
	;;
	msbfw $r0 = $r2, $r1
	zxwd $r1 = $r2
	addw $r2 = $r2, -1
	;;
	cmoved.wltz $r0? $r1 = $r2
	;;
	copyd $r0 = $r1
	ret
	;;
	.size	my_udiv32, .-my_udiv32
