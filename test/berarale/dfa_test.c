#      define PCRE2_EXP_DECL       extern
#      define PCRE2_EXP_DEFN       PCRE2_EXP_DECL
#define PCRE2_CALL_CONVENTION
#include <stdlib.h>
#include <stdint.h>
#define PCRE2_ERROR_DFA_WSSIZE        0
typedef uint8_t  PCRE2_UCHAR8;
typedef const PCRE2_UCHAR8  *PCRE2_SPTR8;
#define PCRE2_SIZE            size_t
#define PCRE2_TYPES_LIST \
typedef struct pcre2_real_match_context pcre2_match_context; \
typedef struct pcre2_real_code pcre2_code; \
typedef struct pcre2_real_match_data pcre2_match_data; \

#define PCRE2_OTHER_FUNCTIONS \

#define PCRE2_JOIN(a,b) a ## b
#define PCRE2_GLUE(a,b) PCRE2_JOIN(a,b)
#define PCRE2_SUFFIX(a) PCRE2_GLUE(a,PCRE2_LOCAL_WIDTH)
#define PCRE2_SPTR                  PCRE2_SUFFIX(PCRE2_SPTR)
#define PCRE2_TYPES_STRUCTURES_AND_FUNCTIONS \
PCRE2_TYPES_LIST \
PCRE2_OTHER_FUNCTIONS
#define PCRE2_LOCAL_WIDTH 8
PCRE2_TYPES_STRUCTURES_AND_FUNCTIONS
enum { OP_CALLOUT, OP_CALLOUT_STR,  };
typedef struct pcre2_real_match_data {
    uint16_t         oveccount;
    PCRE2_SIZE       ovector;
}
pcre2_real_jit_stack;
typedef struct dfa_match_block {
    PCRE2_SPTR start_code;
    PCRE2_SPTR last_used_ptr;
}
dfa_match_block;
#define ADD_ACTIVE(x,y) \
  if (active_count++ < wscount) \
    ; \
  else return PCRE2_ERROR_DFA_WSSIZE
static int internal_dfa_match(   dfa_match_block *mb,   PCRE2_SPTR this_start_code,   PCRE2_SPTR current_subject,   PCRE2_SIZE start_offset,   PCRE2_SIZE *offsets,   uint32_t offsetcount,   int *workspace,   int wscount,   uint32_t rlevel,   int *RWS) {
    PCRE2_SPTR ptr;
    int active_count, new_count;
    wscount -= 2;
    wscount = (wscount - 0) /          0;
    for (;
            ;
        )   {
        int i, j;
        if (ptr > mb->last_used_ptr) mb->last_used_ptr = ptr;
        new_count = 0;
        for (i = 0;                      i < active_count;                      i++)     {
            uint32_t codevalue;
            int rrc;
        for (j = 0;                 j < i;                 j++)                 switch (codevalue)                  case OP_CALLOUT:
        case OP_CALLOUT_STR:
            if (rrc == 0)               ADD_ACTIVE(state_offset + 0callout_length, 0);
        }
        if (new_count <= 0)               break;
    }
}
PCRE2_EXP_DEFN int PCRE2_CALL_CONVENTION pcre2_dfa_match(const pcre2_code *code, PCRE2_SPTR subject, PCRE2_SIZE length,   PCRE2_SIZE start_offset, uint32_t options, pcre2_match_data *match_data,   pcre2_match_context *mcontext, int *workspace, PCRE2_SIZE wscount) {
    int rc;
    PCRE2_SPTR start_match;
    dfa_match_block actual_match_block;
    dfa_match_block *mb = &actual_match_block;
    int base_recursion_workspace;
    for (;
            ;
        )            rc = internal_dfa_match(     mb,                                mb->start_code,                    start_match,                       start_offset,                      match_data->ovector,               match_data->oveccount * 2,       workspace,                         wscount,                      0,                                 base_recursion_workspace);
}
