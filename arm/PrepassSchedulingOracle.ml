(* *************************************************************)
(*                                                             *)
(*             The Compcert verified compiler                  *)
(*                                                             *)
(*           Sylvain Boulmé     Grenoble-INP, VERIMAG          *)
(*           David Monniaux     CNRS, VERIMAG                  *)
(*           Cyril Six          Kalray                         *)
(*           Léo Gourdin        UGA, VERIMAG                   *)
(*           Nicolas Nardino    ENS-Lyon, VERIMAG              *)
(*                                                             *)
(*                                                             *)
(* *************************************************************)

(** Prepass scheduler backend specific implementation *)

open AST
open BTL
open Maps
open InstructionScheduler
open Registers
open PrepassSchedulingOracleDeps
open PrintBTL
open DebugPrint
open BTLtypes
open ValueDomain
open BTLcommonaux

open BinInt (* Z *)

type iflift_mode =
 | Default
 | Ignore_Liveness
 | Move_Stores

(* Alias value analysis *)
type relsymbol = int

type relvalue =
  | RAdd of relsymbol * Z.t
  | RConst of Z.t;;

let relvalue_add rv1 rv2 =
  match rv1, rv2 with
  | (RAdd(rs, ro1), RConst(ro2))
  | (RConst(ro2), RAdd(rs, ro1)) ->
     Some (RAdd(rs, (Z.add ro1 ro2)))
  | (RConst(ro1), RConst(ro2)) ->
     Some (RConst(Z.add ro1 ro2))
  | _ -> None;;

let relvalue_sub rv1 rv2 =
  match rv1, rv2 with
  | RAdd(rs1, ro1), RAdd(rs2, ro2) when rs1=rs2 ->
     Some (RConst(Z.sub ro1 ro2))
  | RAdd(rs1, ro1), RConst(ro2) ->
     Some (RAdd(rs1, Z.sub ro1 ro2))
  | RConst(ro1), RConst(ro2) ->
     Some (RConst(Z.sub ro1 ro2))
  | _ -> None;;

let relvalue_add_imm rv ofs =
  match rv with
  | RAdd(rs, ro) -> RAdd(rs, Z.add ro ofs)
  | RConst(ro) -> RConst(Z.add ro ofs);;

(* The relative analysis can use an abstract interpretation of some operators to
   relate different pointers computed using pointer arithmetic.
   This is not supported by the verifier but can be enabled using the following flag. *)
let rel_abstract_analysis = false

let rel_eval_op (op : Op.operation) (params : relvalue list) : relvalue option =
  if rel_abstract_analysis
  then
    match op, params with
    | Op.Oadd, [v1 ; v2] -> relvalue_add v1 v2
    | Op.Osub, [v1 ; v2] -> relvalue_sub v1 v2
    | (Op.Oaddimm(z)), [v1] -> Some (relvalue_add_imm v1 z)
    | Op.Omove, [v] -> Some v
    | _ -> None
  else
    match op, params with
    | Op.Omove, [v] -> Some v
    | _ -> None

let eval_reladdr (addr : Op.addressing) (params : relvalue list) : relvalue option =
  match addr, params with
  | Op.Aindexed ofs, [v1] -> Some (relvalue_add_imm v1 ofs)
  | _ -> None;;

let use_alias_analysis () =
  !Clflags.option_fprepass_alias_rel ||
  !Clflags.option_fprepass_alias_abs ||
  !Clflags.option_fprepass_alias_trivial

type rel_loc =
  { rel_loc_base : int;
    rel_loc_ofs : Z.t;
    rel_loc_len : Z.t };;

let overlap al ar bl br =
  Z.compare (Z.max al bl) (Z.min ar br) = Datatypes.Lt

let can_interfere_rel loc1 loc2 =
  if loc1.rel_loc_base = loc2.rel_loc_base
  then
    (overlap loc1.rel_loc_ofs (Z.add loc1.rel_loc_ofs loc1.rel_loc_len)
       loc2.rel_loc_ofs (Z.add loc2.rel_loc_ofs loc2.rel_loc_len))
  else true;;

let can_interfere_rel_opt loc1 loc2 =
  match loc1, loc2 with
  | (Some l1), (Some l2) -> can_interfere_rel l1 l2
  | None, _ | _, None -> true;;

let rel_loc_of_rv_chunk (rv : relvalue option) (chunk : memory_chunk) : rel_loc option =
  match rv with
  | Some (RAdd(rs, ro)) ->
     Some { rel_loc_base = rs;
            rel_loc_ofs = ro;
            rel_loc_len = Memdata.size_chunk chunk }
  | _ -> None;;

type abs_loc = {
    abs_loc_base : aval;
    abs_loc_len : Z.t };;

let abs_loc_of_av_chunk (av : aval) (chunk : memory_chunk) : abs_loc =
  { abs_loc_base = av;
    abs_loc_len = Memdata.size_chunk chunk };;

let can_interfere_abs loc1 loc2 =
  not (ValueDomain.vdisjoint
         loc1.abs_loc_base loc1.abs_loc_len
         loc2.abs_loc_base loc2.abs_loc_len);;

let can_interfere_abs_opt loc1 loc2 =
  match loc1, loc2 with
  | (Some l1), (Some l2) -> can_interfere_abs l1 l2
  | None, _ | _, None -> true;;

type loc = {
    abs_loc : abs_loc option;
    rel_loc_opt : rel_loc option };;

(* Since some abstract values are disjoints with [Vtop] (for instance [Vbot]),
   using [Vtop] does not prevent all instruction commutations.
   Even if this behaviour is correct (since a [Vbot] abstract value means that the source location is
   unreachable or raises an error), this yields program transformations that are not expected by the verifier,
   notably, some stores could be moved below branches.
   In order to avoid this, [default_loc] has [None] as abstract location, which prevents all commutations. *)
let default_loc = {
    abs_loc = None;
    rel_loc_opt = None };;

let can_interfere loc1 loc2 =
  !Clflags.option_fprepass_alias_trivial ||
  (if !Clflags.option_fprepass_alias_rel
   then can_interfere_rel_opt loc1.rel_loc_opt loc2.rel_loc_opt
   else true) &&
  (if !Clflags.option_fprepass_alias_abs
   then can_interfere_abs_opt loc1.abs_loc loc2.abs_loc
   else true);;
    
(* [va] should be true if the program contains meaningful abstract value annotations,
   that is, if a BTL_ValueAnalysisAnnotate pass has been executed.
   It must be false otherwise. *)
let build_constraints (opweights : opweights) seqa btl ifliftopt (va : bool) =
  let last_reg_reads : int list PTree.t ref = ref PTree.empty
  and last_reg_write : (int * int) PTree.t ref = ref PTree.empty
  and last_mem_reads : int list ref = ref []
  and last_mem_write : int option ref = ref None
  and last_rel_mem_reads : (int * loc) list ref = ref []
  and last_rel_mem_writes : (int * loc) list ref = ref [] 
  and last_branch : int option ref = ref None
  and last_non_pipelined_op : int array =
    Array.make opweights.nr_non_pipelined_units (-1)
  and latency_constraints : latency_constraint list ref = ref []
  and current_relvalues : relvalue PTree.t ref = ref PTree.empty
  and next_relsymbol_ctr = ref 0 in
  let next_relsymbol () =
    let r = !next_relsymbol_ctr in
    next_relsymbol_ctr := r + 1;
    r
  and add_constraint instr_from instr_to latency =
    assert (instr_from <= instr_to);
    assert (latency >= 0);
    if instr_from = instr_to then
      if latency = 0 then ()
      else
        (debug_flag := true;
        debug "negative self-loop on: %d %d %d\n\n" instr_from instr_to latency;
        debug_flag := false; 
        failwith "PrepassSchedulingOracle.get_dependencies: negative self-loop")
    else
      latency_constraints :=
        { instr_from; instr_to; latency } :: !latency_constraints
  and get_last_reads reg =
    match PTree.get reg !last_reg_reads with Some l -> l | None -> [] in

  let get_new_relvalue () : relvalue =
    RAdd (next_relsymbol (), BinNums.Z0) in
  let set_relvalue (r : reg) (rv : relvalue) =
    current_relvalues := PTree.set r rv !current_relvalues in
  let get_relvalue (r : reg) : relvalue =
    match PTree.get r !current_relvalues with
    | None ->
       let v = get_new_relvalue () in
       set_relvalue r v;
       v
    | Some x -> x in

  let rel_assign_op rd op rs =
    let res =
      match rel_eval_op op (List.map get_relvalue rs) with
      | None -> get_new_relvalue ()
      | Some x -> x in
    set_relvalue rd res in

  let assign_op rd op rs =
    (if !Clflags.option_fprepass_alias_rel
    then rel_assign_op rd op rs) in

  let store chk addr args src = () in

  let condition cond rs = () in

  let get_reladdr addr rs =
    eval_reladdr addr (List.map get_relvalue rs) in
  
  let rel_load rd =
    set_relvalue rd (get_new_relvalue ()) in

  let load dst =
    (if !Clflags.option_fprepass_alias_rel
    then rel_load dst) in

  let loc_of_access addr lr (aaddr : aval option) chk =
    { rel_loc_opt = rel_loc_of_rv_chunk (get_reladdr addr lr) chk;
      abs_loc = Option.map (fun aa -> abs_loc_of_av_chunk aa chk)
                           (if va then aaddr else None) }

  and add_input_mem_l i loc latency =
    if use_alias_analysis () then
      begin
        List.iter (fun (j, prev_wr_loc) ->
            if can_interfere loc prev_wr_loc
            then add_constraint j i latency)
          !last_rel_mem_writes;
        last_rel_mem_reads := (i, loc) :: !last_rel_mem_reads
      end
    else
      begin
        (* Read after write *)
        (match !last_mem_write with
         | None -> ()
         | Some j -> add_constraint j i latency);
        last_mem_reads := i :: !last_mem_reads
      end in
  let add_input_mem i loc = add_input_mem_l i loc 1
  and add_input_mem_formality i = add_input_mem_l i default_loc 0 in
  
  let add_output_mem_l i loc latency =
    if use_alias_analysis () then
      begin
        List.iter (fun (j, prev_wr_loc) ->
            if can_interfere loc prev_wr_loc
            then add_constraint j i latency)
          !last_rel_mem_writes;
        List.iter (fun (j, prev_rd_loc) ->
            if can_interfere loc prev_rd_loc
            then add_constraint j i 0)
          !last_rel_mem_reads;
        last_rel_mem_writes := (i, loc) :: !last_rel_mem_writes
      end
    else
      begin
        (* Write after write *)
        (match !last_mem_write with
         | None -> ()
         | Some j -> add_constraint j i latency);
        (* Write after read *)
        List.iter (fun j -> add_constraint j i 0) !last_mem_reads;
        last_mem_write := Some i;
        last_mem_reads := []
      end in

  let add_output_mem i loc = add_output_mem_l i loc 1
  and add_output_mem_formality i = add_output_mem_l i default_loc 0

  and add_input_reg i reg =
    (* Read after write *)
    (match PTree.get reg !last_reg_write with
    | None -> ()
    | Some (j, latency) -> add_constraint j i latency);
    last_reg_reads := PTree.set reg (i :: get_last_reads reg) !last_reg_reads

  and add_output_reg i latency reg =
    (* Write after write *)
    (match PTree.get reg !last_reg_write with
    | None -> ()
    | Some (j, _) -> add_constraint j i 1);
    (* Write after read *)
    List.iter (fun j -> add_constraint j i 0) (get_last_reads reg);
    last_reg_write := PTree.set reg (i, latency) !last_reg_write;
    last_reg_reads := PTree.remove reg !last_reg_reads
  in
  let add_input_regs i regs = List.iter (add_input_reg i) regs in
  let rec add_builtin_res i (res : reg builtin_res) =
    match res with
    | BR r -> add_output_reg i 10 r
    | BR_none -> ()
    | BR_splitlong (hi, lo) ->
        add_builtin_res i hi;
        add_builtin_res i lo
  in
  let rec add_builtin_arg i (ba : reg builtin_arg) =
    match ba with
    | BA r -> add_input_reg i r
    | BA_int _ | BA_long _ | BA_float _ | BA_single _ -> ()
    | BA_loadstack (_, _) -> add_input_mem i default_loc (* todo *)
    | BA_addrstack _ -> ()
    | BA_loadglobal (_, _, _) -> add_input_mem i default_loc (* todo *)
    | BA_addrglobal _ -> ()
    | BA_splitlong (hi, lo) ->
        add_builtin_arg i hi;
        add_builtin_arg i lo
    | BA_addptr (a1, a2) ->
        add_builtin_arg i a1;
        add_builtin_arg i a2
  and irreversible_action i =
    match !last_branch with None -> () | Some j -> add_constraint j i 1
  in
  let set_branch i =
    irreversible_action i;
    last_branch := Some i
  and add_non_pipelined_resources i resources =
    Array.iter2
      (fun latency last ->
        if latency >= 0 && last >= 0 then add_constraint last i latency)
      resources last_non_pipelined_op;
    Array.iteri
      (fun rsc latency -> if latency >= 0 then last_non_pipelined_op.(rsc) <- i)
      resources
  in
  let apply_constraints inst i =
    let rec ibso_input_regs ib i =
      match ib with
      | Bop (_, lr, _, _) | Bload (_, _, _, lr, _, _) ->
          lr
      | Bstore (_, _, lr, src, _) ->
          (src :: lr)
      | Bseq (ib1, ib2) ->
          (ibso_input_regs ib1 i @ ibso_input_regs ib2 i)
      | _ -> [] in
    let rec ibso_output_regs ib i =
      match ib with
      | Bop (_, _, rd, _)
      | Bload (_, _, _, _, rd, _) ->
          [rd]
      | Bseq (ib1, ib2) ->
          (ibso_output_regs ib1 i @ ibso_output_regs ib2 i)
      | _ -> [] in
    let rec ibso_output_mem ib i =
      match ib with
      | Bstore (_, _, _, _, _) ->
          add_output_mem_formality i
      | Bseq (ib1, ib2) ->
          ibso_output_mem ib1 i;
          ibso_output_mem ib2 i
      | _ -> ()
    in
    match inst with
    | Bnop _ -> ()
    | Bop (op, lr, rd, _) ->
        add_non_pipelined_resources i
          (opweights.non_pipelined_resources_of_op op (List.length lr));
        if Op.is_trapping_op op then irreversible_action i;
        add_input_regs i lr;
        add_output_reg i (opweights.latency_of_op op (List.length lr)) rd;
        assign_op rd op lr
    | Bload (trap, chk, addr, lr, rd, iinfo) ->
        (if trap = TRAP then irreversible_action i);
        load rd;
        add_input_mem i (loc_of_access addr lr None chk);
        add_input_regs i lr;
        add_output_reg i
          (opweights.latency_of_load trap chk addr (List.length lr))
          rd
    | Bstore (chk, addr, lr, src, iinfo) ->
       irreversible_action i;
        store chk addr lr src;
        add_input_regs i lr;
        add_input_reg i src;
        add_output_mem i (loc_of_access addr lr None chk)
    | Bcond (cond, lr, ibso, ibnot, iinfo) ->
       condition cond lr;
        (* useless to irreversible action here, branch is set at the same place *)
        set_branch i; 
        let in_r = ibso_input_regs ibso i in
        let out_r = ibso_output_regs ibso i in
        (match ifliftopt, iinfo.visited with
        | Ignore_Liveness, false -> add_input_mem_formality i
        | Move_Stores, false -> ()
        | _ -> add_input_mem i default_loc);
        add_input_regs i (lr @ in_r);
        List.iter (fun reg -> add_output_reg i 0 reg) out_r;
        ibso_output_mem ibso i
    | BF (Bcall (signature, ef, lr, rd, _), _) ->
        set_branch i;
        (match ef with
        | Datatypes.Coq_inl r -> add_input_reg i r
        | Datatypes.Coq_inr symbol -> ());
        add_input_mem i default_loc;
        add_input_regs i lr;
        add_output_reg i (opweights.latency_of_call signature ef) rd;
        add_output_mem i default_loc;
        failwith "build_constraints: invalid Bcall"
    | BF (Btailcall (signature, ef, lr), _) ->
        set_branch i;
        (match ef with
        | Datatypes.Coq_inl r -> add_input_reg i r
        | Datatypes.Coq_inr symbol -> ());
        add_input_mem i default_loc;
        add_input_regs i lr;
        failwith "build_constraints: invalid Btailcall"
    | BF (Bbuiltin (ef, lr, rd, _), _) ->
        set_branch i;
        add_input_mem i default_loc;
        List.iter (add_builtin_arg i) lr;
        add_builtin_res i rd;
        add_output_mem i default_loc;
        failwith "build_constraints: invalid Bbuiltin"
    | BF (Bjumptable (lr, _), _) ->
        set_branch i;
        add_input_reg i lr;
        failwith "build_constraints: invalid Bjumptable"
    | BF (Breturn (Some r), _) ->
        set_branch i;
        add_input_reg i r;
        failwith "build_constraints: invalid Breturn Some"
    | BF (Breturn None, _) ->
        set_branch i;
        failwith "build_constraints: invalid Breturn None"
    | BF (Bgoto _, _) ->
        failwith "build_constraints: invalid Bgoto"
    | Bseq (_, _) -> failwith "build_constraints: Bseq"
  in
  Array.iteri
    (fun i (inst, other_uses) ->
      (match ifliftopt, get_visited inst with
      (* ignoring liveness of outer blocks *)
      | Ignore_Liveness, false | Move_Stores, false ->
        List.iter
          (fun use ->
            last_reg_reads := PTree.set use 
                              (i :: get_last_reads use) 
                              !last_reg_reads)
          (Regset.elements other_uses)
      | _ ->
          List.iter (fun use -> add_input_reg i use) (Regset.elements other_uses));
      apply_constraints inst i)
    seqa;
  !latency_constraints

let get_inum seqa_elem =
  let (inst, _) = seqa_elem in
  match inst with
  | Bnop (Some iinfo)
  | Bop (_, _, _, iinfo)
  | Bload (_, _, _, _, _, iinfo)
  | Bstore (_, _, _, _, iinfo)
  | Bcond (_, _, _, _, iinfo)
  | BF (_, iinfo) ->
    iinfo.inumb
  | _ -> -2

let print_deps deps seqa =
  debug "Printing dependencies:\n";
  List.iter
    (fun {InstructionScheduler.instr_from; instr_to; latency} ->
      debug "(%2d,%2d) depends on (%2d,%2d), latency: %2d\n"
          (get_inum seqa.(instr_from)) instr_from  (get_inum seqa.(instr_to)) instr_to latency)
    deps

let resources_of_instruction (opweights : opweights) = function
  | Bnop _ -> Array.map (fun _ -> 0) opweights.pipelined_resource_bounds
  | Bop (op, inputs, output, _) ->
      opweights.resources_of_op op (List.length inputs)
  | Bload (trap, chunk, addressing, addr_regs, output, _) ->
      opweights.resources_of_load trap chunk addressing (List.length addr_regs)
  | Bstore (chunk, addressing, addr_regs, input, _) ->
      opweights.resources_of_store chunk addressing (List.length addr_regs)
  | BF (Bcall (signature, ef, inputs, output, _), _) ->
      opweights.resources_of_call signature ef
  | BF (Bbuiltin (ef, builtin_inputs, builtin_output, _), _) ->
      opweights.resources_of_builtin ef
  | Bcond (cond, args, _, _, _) ->
      opweights.resources_of_cond cond (List.length args)
  | BF (Btailcall _, _) | BF (Bjumptable _, _) | BF (Breturn _, _) ->
      opweights.pipelined_resource_bounds
  | BF (Bgoto _, _) | Bseq (_, _) ->
      failwith "resources_of_instruction: invalid btl instruction"

let print_sequence pp seqa =
  Array.iteri
    (fun i (inst, other_uses) ->
      debug "i=%d\n inst = " i;
      print_btl_inst pp inst;
      debug "\n other_uses=";
      print_regset other_uses;
      debug "\n")
    seqa

let length_of_chunk = function
  | Mint8signed | Mint8unsigned -> 1
  | Mint16signed | Mint16unsigned -> 2
  | Mint32 | Mfloat32 | Many32 -> 4
  | Mint64 | Mfloat64 | Many64 -> 8

let define_problem (opweights : opweights) (live_entry_regs : Regset.t)
      (typing : RTLtyping.regenv) (va : bool)
      reference_counting seqa btl ifliftopt =
  let simple_deps = build_constraints opweights seqa btl ifliftopt va in
  (*debug_flag := true;*)
  print_deps simple_deps seqa;
  (*debug_flag := false;*)
  {
    max_latency = -1;
    resource_bounds = opweights.pipelined_resource_bounds;
    live_regs_entry = live_entry_regs;
    typing;
    reference_counting = reference_counting;
    instruction_usages =
      Array.map (resources_of_instruction opweights) (Array.map fst seqa);
    latency_constraints = simple_deps;
  };;

let add_sequentiality_constraints n constraints0 =
  let tab = Hashtbl.create (2*n) in
  List.iter (fun ctr -> Hashtbl.add tab (ctr.instr_from, ctr.instr_to) ())
            constraints0 ;
  let constraints = ref constraints0 in
  for i=0 to n-2
  do
    if not (Hashtbl.mem tab (i, i+1))
    then
      constraints := { instr_from = i;
                       instr_to = i+1;
                       latency = 0 } :: !constraints
  done;
  !constraints;;

let define_sequential_problem (opweights : opweights) (live_entry_regs : Regset.t)
      (typing : RTLtyping.regenv) (va : bool)
      reference_counting seqa btl ifliftopt =
  let usual_problem =  define_problem (opweights : opweights) (live_entry_regs : Regset.t)
      (typing : RTLtyping.regenv) va
      reference_counting seqa btl ifliftopt in
  { usual_problem with latency_constraints =
                         add_sequentiality_constraints (Array.length seqa)
      usual_problem.latency_constraints };;

let zigzag_scheduler problem early_ones =
  let nr_instructions = get_nr_instructions problem in
  assert (nr_instructions = Array.length early_ones);
  match list_scheduler problem with
  | Some fwd_schedule ->
      let fwd_makespan = fwd_schedule.(Array.length fwd_schedule - 1) in
      let constraints' = ref problem.latency_constraints in
      Array.iteri
        (fun i is_early ->
          if is_early then
            constraints' :=
              {
                instr_from = i;
                instr_to = nr_instructions;
                latency = fwd_makespan - fwd_schedule.(i);
              }
              :: !constraints')
        early_ones;
      validated_scheduler reverse_list_scheduler
        { problem with latency_constraints = !constraints' }
  | None -> None

let prepass_scheduler_by_name name problem seqa =
  match name with
  | "zigzag" ->
      let early_ones =
        Array.map
          (fun (inst, _) ->
            match inst with Bcond (_, _, _, _, _) -> true | _ -> false)
          seqa
      in
      zigzag_scheduler problem early_ones
  | _ -> scheduler_by_name name problem

let makespan_of_solution sol = sol.((Array.length sol) - 1);;

let print_solution sol =
  Array.iteri (fun i time ->
      Printf.printf "time[%d] = %d\n" i time) sol;;

let solution_to_position solution =
  let positions = Array.init ((Array.length solution)-1) (fun i -> i) in
  Array.sort
    (fun i j ->
              let si = solution.(i) and sj = solution.(j) in
              if si < sj then -1 else if si > sj then 1 else i - j)
    positions;
  positions;;

let compute_greedy_makespan opweights seqa btl =
  let seq_problem = define_sequential_problem opweights Regset.empty (fun _ -> AST.coq_Tptr) false None seqa btl Default in
  match list_scheduler seq_problem with
  | None -> failwith "compute_makespan fails?!"
  | Some sol -> makespan_of_solution sol;;
     
let schedule_sequence seqa btl (live_regs_entry : Registers.Regset.t)
      (typing : RTLtyping.regenv) (va : bool)
      reference ifliftopt =
  let opweights = OpWeights.get_opweights () in
  try
    if Array.length seqa <= 1 then None
    else
      let nr_instructions = Array.length seqa in
      if !Clflags.option_debug_compcert > 6 then
        Printf.printf "prepass scheduling length = %d\n" nr_instructions;
      let problem =
        define_problem opweights live_regs_entry typing va (Some reference) seqa btl ifliftopt
      in
      if !Clflags.option_debug_compcert > 7 then (
        print_sequence stdout seqa;
        print_problem stdout problem);
      match
        prepass_scheduler_by_name !Clflags.option_fprepass_sched problem seqa
      with
      | None ->
          Printf.printf "no solution in prepass scheduling\n";
          None
      | Some solution ->
         let new_positions = solution_to_position solution in
          (*debug_flag := true;*)
          debug "solution:\n";
          Array.iteri (fun i e -> debug "%d: %d\n" i e) solution;
          (*debug_flag := false;*)
          (if !Clflags.option_fprepass_print_makespan then
             let seq_makespan = compute_greedy_makespan opweights seqa btl
             and sched_makespan = compute_greedy_makespan opweights
                         (Array.map (fun i -> seqa.(i)) new_positions) btl in
                 Printf.printf "prepass_sched: len=%d seq_makespan=%d sched_makespan=%d sched_ok=%b sched_reduces=%b block=%s\n"
                   (Array.length seqa) seq_makespan sched_makespan
                   (sched_makespan <= seq_makespan)
                   (sched_makespan < seq_makespan)
              (Marshal.to_bytes seqa [] |> Digest.bytes |> Digest.to_hex)
          );
          Some (new_positions, solution.(nr_instructions))
  with Failure s ->
    Printf.printf "failure in prepass scheduling: %s\n" s;
    None

let dubious_compute_greedy_makespan seqa btl =
  if Array.length seqa > 0 then (
    let opweights = OpWeights.get_opweights () in
    let problem = define_problem opweights Regset.empty (fun _ -> AST.coq_Tptr) false None seqa btl Default in
    match greedy_scheduler problem with
    | Some solution -> (
        let positions = Array.init (Array.length seqa) (fun i -> i) in
        Array.sort
          (fun i j ->
            let si = solution.(i) and sj = solution.(j) in
            if si < sj then -1 else if si > sj then 1 else i - j)
          positions;
        match (recompute_makespan problem solution) with
        | Some ms -> ms
        | None -> failwith "compute_makespan: recompute_makespan failed")
  | None -> failwith "compute_makespan: greedy scheduler failed") else 0
