(* *************************************************************)
(*                                                             *)
(*             The Compcert verified compiler                  *)
(*                                                             *)
(*           David Monniaux     CNRS, VERIMAG                  *)
(*                                                             *)
(*  Copyright Kalray. Copyright VERIMAG. All rights reserved.  *)
(*  This file is distributed under the terms of the INRIA      *)
(*  Non-Commercial License Agreement.                          *)
(*                                                             *)
(* *************************************************************)

Require Import Coqlib.
Require Import Integers.
Require Import Values.
Require Import Lia.

Open Scope Z_scope.

Fixpoint pos_highest_bit (x : positive) (already : nat) : nat :=
  match x with
  | xH => already
  | xI x' | xO x' => pos_highest_bit x' (S already)
  end.

Lemma pos_highest_bit_range :
  forall x p (RANGE : (x < Pos.of_nat (2^p)%nat)%positive) already,
    (already <= pos_highest_bit x already < already + p)%nat.
Proof.
  induction x; intros; cbn.
  all: destruct p; cbn in RANGE; [lia|idtac].
  3: lia.
  all: rewrite Nat.add_0_r in RANGE.
  all: replace (2^p + 2^p)%nat with (2 * (2^p))%nat in RANGE by lia.
  {
    assert (S already <= pos_highest_bit x (S already) < S already + p)%nat.
    { apply IHx. lia. }
    lia.
  }
  {
    assert (S already <= pos_highest_bit x (S already) < S already + p)%nat.
    { apply IHx. lia. }
    lia.
  }
Qed.

Definition highest_bit x :=
  match x with
  | Zpos x => Z.of_nat (pos_highest_bit x O)
  | _ => 0%Z
  end.

Definition int_highest_bit (x : int) : Z :=
  highest_bit (Int.unsigned x).

Opaque Pos.pow Nat.pow.

Lemma int_highest_bit_range :
  forall x, 0 <= int_highest_bit x <= 31.
Proof.
  unfold int_highest_bit, highest_bit.
  destruct x; cbn.
  destruct intval; try lia.
  assert (RANGE : (p < Pos.of_nat (2^32)%nat)%positive).
  { rewrite Nat2Pos.inj_pow by lia.
    change Int.modulus with (2^32) in *.
    change (Pos.of_nat 2) with 2%positive.
    change (Pos.of_nat 32) with 32%positive.
    lia.
  }
  pose proof (pos_highest_bit_range p 32%nat RANGE O).
  lia.
Qed.

Fixpoint pos_lowest_bit (x : positive) (already : nat) : nat :=
  match x with
  | xH | xI _ => already
  | xO x' => pos_lowest_bit x' (S already)
  end.

Transparent Pos.pow Nat.pow.

Lemma pos_lowest_bit_range :
  forall x p (RANGE : (x < Pos.of_nat (2^p)%nat)%positive) already,
    (already <= pos_lowest_bit x already < already + p)%nat.
Proof.
  induction x; intros; cbn.
  all: destruct p; cbn in RANGE; [lia|idtac].
  1,3: lia.
  rewrite Nat.add_0_r in RANGE.
  replace (2^p + 2^p)%nat with (2 * (2^p))%nat in RANGE by lia.
  assert (S already <= pos_lowest_bit x (S already) < S already + p)%nat.
  { apply IHx. lia. }
  lia.
Qed.

Definition lowest_bit x :=
  match x with
  | Zpos x => Z.of_nat (pos_lowest_bit x O)
  | _ => 0%Z
  end.

Definition int_lowest_bit (x : int) : Z :=
  lowest_bit (Int.unsigned x).

Opaque Pos.pow Nat.pow.

Lemma int_lowest_bit_range :
  forall x, 0 <= int_lowest_bit x <= 31.
Proof.
  unfold int_lowest_bit, lowest_bit.
  destruct x; cbn.
  destruct intval; try lia.
  assert (RANGE : (p < Pos.of_nat (2^32)%nat)%positive).
  { rewrite Nat2Pos.inj_pow by lia.
    change Int.modulus with (2^32) in *.
    change (Pos.of_nat 2) with 2%positive.
    change (Pos.of_nat 32) with 32%positive.
    lia.
  }
  pose proof (pos_lowest_bit_range p 32%nat RANGE O).
  lia.
Qed.

Lemma pos_highest_bit_bigger:
  forall x a, (a <= pos_highest_bit x a)%nat.
Proof.
  induction x; cbn; intros.
  1,2: pose proof (IHx (S a)); lia.
  lia.
Qed.

Lemma pos_lowest_highest_bit:
  forall x a, (pos_lowest_bit x a <= pos_highest_bit x a)%nat.
Proof.
  induction x; cbn; intros.
  - pose proof (pos_highest_bit_bigger x (S a)). lia.
  - apply IHx.
  - lia.
Qed.

Lemma lowest_highest_bit:
  forall n, lowest_bit n <= highest_bit n.
Proof.
  destruct n; cbn; try lia.
  pose proof (pos_lowest_highest_bit p 0).
  lia.
Qed.

Lemma int_lowest_highest_bit:
  forall n, ExtValues.int_lowest_bit n <= ExtValues.int_highest_bit n.
Proof.
  intro. apply lowest_highest_bit.
Qed.

Definition is_bitfield lsb sz :=
  (Int.unsigned lsb) + (Int.unsigned sz) <=? 32.

Definition insf lsb sz prev fld :=
  let mask :=  Vint (Int.shl (Int.repr (Z.ones (Int.unsigned sz))) lsb) in
  if is_bitfield lsb sz
  then
    Val.or (Val.and prev (Val.notint mask))
           (Val.and (Val.shl fld (Vint lsb)) mask)
  else Vundef.

Definition clearf lsb sz prev :=
  let mask := Vint (Int.shl (Int.repr (Z.ones (Int.unsigned sz))) lsb) in
  if is_bitfield lsb sz
  then Val.and prev (Val.notint mask)
  else Vundef.

Definition zbitfield_mask zstop zstart :=
  Z.shiftl (Z.ones (Z.succ (zstop - zstart))) zstart.
