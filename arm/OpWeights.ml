(* *************************************************************)
(*                                                             *)
(*             The Compcert verified compiler                  *)
(*                                                             *)
(*           David Monniaux     CNRS, VERIMAG                  *)
(*                                                             *)
(*  Copyright Kalray. Copyright VERIMAG. All rights reserved.  *)
(*  This file is distributed under the terms of the INRIA      *)
(*  Non-Commercial License Agreement.                          *)
(*                                                             *)
(* *************************************************************)

(** Latencies & resources information for prepass scheduling *)

open Op;;
open PrepassSchedulingOracleDeps;;

(**
Derived from my understanding of
_Cortex-R5 Technical Reference Manual_
https://developer.arm.com/documentation/ddi0460/latest/
-- D. Monniaux *)

module Cortex_R5=
  struct
    let latency_of_op (op : operation) (nargs : int) =
      match op with
      | Ointconst n ->
         List.length (Asmgen.loadimm Asm.IR3 n []) 
      | Omove
      | Ocopy _ | Ocopyimm _ -> 1
      | Ofloatconst _ -> 4 (* TODO *)
      | Osingleconst _ -> 4 (* TODO *)
      | Oaddrsymbol(_, _) -> 4 (* TODO *)
      | Oaddrstack _
      | Ocast8signed
      | Ocast16signed
      | Oubfx(_, _) | Osbfx(_, _) | Obfc(_, _) | Obfi(_, _)
      | Oadd
      | Oaddshift _ 
      | Oaddimm _
      | Osub
      | Osubshift _
      | Orsubshift _
      | Orsubimm _ -> 1
      | Omul
      | Omla | Omls
      | Omulhs
      | Omulhu -> 3
      | Odiv -> 12
      | Odivu -> 11
      | Oand -> 1
      | Oandshift _ -> 1
      | Oandimm _ -> 1
      | Oor -> 1
      | Oorshift _
      | Oorimm _
      | Oxor
      | Oxorshift _
      | Oxorimm _
      | Obic
      | Obicshift _
      | Onot
      | Onotshift _
      | Oshl
      | Oshr
      | Oshru
      | Oshift _ -> 1
      | Oshrximm n -> if Camlcoq.Z.to_int n <= 1 then 2 else 3
      | Onegf
      | Oabsf -> 1
      | Oaddf
      | Osubf -> 9
      | Omulf | Omlaf | Omlsf -> 19
      | Odivf -> 63
      | Onegfs 
      | Oabsfs -> 1
      | Oaddfs
      | Osubfs -> 2
      | Omulfs | Omlafs | Omlsfs -> 5
      | Odivfs -> 16
      | Osingleoffloat -> 7
      | Ofloatofsingle -> 5
      | Ointoffloat
      | Ointuoffloat
      | Ofloatofint
      | Ofloatofintu -> 7
      | Ointofsingle
      | Ointuofsingle
      | Osingleofint
      | Osingleofintu -> 5
      | Omakelong
      | Olowlong
      | Ohighlong -> 1
      | Ocmp cmp ->
         (match cmp with
         | Ccompf _
         | Cnotcompf _
         | Ccompfzero _
         | Cnotcompfzero _ -> 2
         | Ccompfs _
         | Cnotcompfs _
         | Ccompfszero _
         | Cnotcompfszero _ -> 1
         | _ -> 1)

      | Osel(cmp, _) -> (* TODO CHECK *)
         (match cmp with
         | Ccompf _
         | Cnotcompf _
         | Ccompfzero _
         | Cnotcompfzero _ -> 2
         | Ccompfs _
         | Cnotcompfs _
         | Ccompfszero _
         | Cnotcompfszero _ -> 1
         | _ -> 1)

      | Osingle_of_bits
      | Obits_of_single -> 1;;

    let resource_bounds = [| 2; 1; 1 |];; (* instr ; ALU ; load/store *)
    let res_no_alu = [| 1; 0; 0 |]
    and res_alu = [| 1; 1; 0 |]
    and res_load_store = [| 1; 0; 1 |];;
    let resources_of_op (op : operation) (nargs : int) =
      match op with
      | Omove
      | Ocopy _ | Ocopyimm _
      | Ointconst _ -> res_no_alu
      | Ofloatconst _
      | Osingleconst _
      | Oaddrsymbol(_, _) 
      | Oaddrstack _
      | Ocast8signed
      | Ocast16signed
      | Oubfx(_, _) | Osbfx(_, _) | Obfc(_, _) | Obfi(_, _)
      | Oadd
      | Oaddshift _
      | Oaddimm _
      | Osub
      | Osubshift _
      | Orsubshift _
      | Orsubimm _
      | Omul
      | Omla | Omls
      | Omulhs
      | Omulhu
      | Odiv
      | Odivu
      | Oand
      | Oandshift _
      | Oandimm _
      | Oor
      | Oorshift _
      | Oorimm _
      | Oxor
      | Oxorshift _
      | Oxorimm _
      | Obic
      | Obicshift _
      | Onot
      | Onotshift _
      | Oshl
      | Oshr
      | Oshru
      | Oshift _
      | Oshrximm _
      | Onegf
      | Oabsf
      | Oaddf
      | Osubf
      | Omulf
      | Omlaf
      | Omlsf
      | Odivf
      | Onegfs
      | Oabsfs
      | Oaddfs
      | Osubfs
      | Omulfs
      | Omlafs
      | Omlsfs
      | Odivfs
      | Osingleoffloat
      | Ofloatofsingle
      | Ointoffloat
      | Ointuoffloat
      | Ofloatofint
      | Ofloatofintu
      | Ointofsingle
      | Ointuofsingle
      | Osingleofint
      | Osingleofintu
      | Omakelong
      | Olowlong
      | Ohighlong
      | Ocmp _
      | Osel(_, _)
    
      | Osingle_of_bits
      | Obits_of_single -> res_alu;;

    let nr_non_pipelined_units = 1;; (* Divider unit *)
    let non_pipelined_resources_of_op (op : operation) (nargs : int) =
      match op with
      | Odiv -> [| 12 |]
      | Odivu -> [| 11 |]
      | Odivf-> [| 63 |]
      | Odivfs -> [| 16 |]
      | _ -> [| -1 |];;
    
    let resources_of_cond (cmp : condition) (nargs : int) = res_alu;;

    let latency_of_load trap chunk (addr : addressing) (nargs : int) = 3;;
    let latency_of_call _ _ = 6;;
    
    let resources_of_load trap chunk addressing nargs = res_load_store;;
    
    let resources_of_store chunk addressing nargs = res_load_store;;
    
    let resources_of_call _ _ = resource_bounds;;
    let resources_of_builtin _ = resource_bounds;;
  end;;

let get_opweights () : opweights =
  match !Clflags.option_mtune with
  | "cortex-r5" | "" ->
     {
       pipelined_resource_bounds = Cortex_R5.resource_bounds;
       nr_non_pipelined_units = Cortex_R5.nr_non_pipelined_units;
       latency_of_op = Cortex_R5.latency_of_op;
       resources_of_op = Cortex_R5.resources_of_op;
       non_pipelined_resources_of_op = Cortex_R5.non_pipelined_resources_of_op;
       latency_of_load = Cortex_R5.latency_of_load;
       resources_of_load = Cortex_R5.resources_of_load;
       resources_of_store = Cortex_R5.resources_of_store;
       resources_of_cond = Cortex_R5.resources_of_cond;
       latency_of_call = Cortex_R5.latency_of_call;
       resources_of_call = Cortex_R5.resources_of_call;
       resources_of_builtin = Cortex_R5.resources_of_builtin
     }
  | xxx -> failwith (Printf.sprintf "unknown -mtune: %s" xxx);;
