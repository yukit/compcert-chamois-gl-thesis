(* *************************************************************)
(*                                                             *)
(*             The Compcert verified compiler                  *)
(*                                                             *)
(*           David Monniaux     CNRS, VERIMAG                  *)
(*                                                             *)
(*  Copyright VERIMAG. All rights reserved.                    *)
(*  This file is distributed under the terms of the INRIA      *)
(*  Non-Commercial License Agreement.                          *)
(*                                                             *)
(* *************************************************************)

Require Import BoolEqual Coqlib.
Require Import AST Integers Floats.
Require Import Values Memory Globalenvs Events.
Require Import Op.


Definition can_swap_accesses_ofs ofsr chunkr ofsw chunkw :=
     (0 <=? ofsw) && (ofsw <=? (Ptrofs.modulus - largest_size_chunk))
  && (0 <=? ofsr) && (ofsr <=? (Ptrofs.modulus - largest_size_chunk))
  && ((ofsw + size_chunk chunkw <=? ofsr) ||
      (ofsr + size_chunk chunkr <=? ofsw)).

Definition may_overlap chunk addr args chunk' addr' args' :=
  match addr, addr', args, args' with
  | (Aindexed ofs), (Aindexed ofs'),
    (base :: nil), (base' :: nil) =>
    if peq base base'
    then negb (can_swap_accesses_ofs (Int.unsigned ofs') chunk' (Int.unsigned ofs) chunk)
    else true
  | (Ainstack ofs), (Ainstack ofs'), _, _ =>
    negb (can_swap_accesses_ofs (Ptrofs.unsigned ofs') chunk' (Ptrofs.unsigned ofs) chunk)
  | _, _, _, _ => true
  end.

Definition is_condition_reflexive (cond: condition) : bool :=
  match cond with
  | Ccomp (Ceq | Cle | Cge)
  | Ccompu (Ceq | Cle | Cge) => true
  | _ => false
  end.
           
Lemma is_condition_reflexive_sound:
  forall (cond: condition) (REFL : is_condition_reflexive cond = true)
         (v: val) (m: mem),
    eval_condition cond (v :: v :: nil) m <> Some false.
Proof.
  destruct cond; cbn; try discriminate.
  all: destruct c ; try discriminate.
  all: intro ZZ; clear ZZ.
  all: destruct v; cbn; try congruence; intros.
  all: try rewrite Int.eq_true.
  all: try rewrite Int64.eq_true.
  all: try rewrite Int.lt_false.
  all: try rewrite Int64.lt_false.
  all: try rewrite Int.ltu_false.
  all: try rewrite Int64.ltu_false.
  all: cbn; try congruence.
  all: destruct Archi.ptr64.
  all: cbn; try congruence.
  all: destruct eq_block as [EQ | NEQ]; [clear EQ | try congruence].
  all: try rewrite Ptrofs.eq_true.
  all: try rewrite Ptrofs.ltu_false.
  all: destruct (_ && _); cbn; congruence.
Qed.
