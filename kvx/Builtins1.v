(* *************************************************************)
(*                                                             *)
(*             The Compcert verified compiler                  *)
(*                                                             *)
(*           Sylvain Boulmé     Grenoble-INP, VERIMAG          *)
(*           Xavier Leroy       INRIA Paris-Rocquencourt       *)
(*           David Monniaux     CNRS, VERIMAG                  *)
(*           Cyril Six          Kalray                         *)
(*                                                             *)
(*  Copyright Kalray. Copyright VERIMAG. All rights reserved.  *)
(*  This file is distributed under the terms of the INRIA      *)
(*  Non-Commercial License Agreement.                          *)
(*                                                             *)
(* *************************************************************)

(** Platform-specific built-in functions *)

Require Import String Coqlib.
Require Import AST Integers Floats Values ExtFloats ExtValues.
Require Import Builtins0.

Inductive platform_builtin : Type :=
| BI_fmin
| BI_fmax
| BI_fminf
| BI_fmaxf
| BI_fma
| BI_fmaf
| BI_lround_ne
| BI_luround_ne
| BI_fp_udiv32
| BI_fp_udiv64
| BI_fp_umod32
| BI_fp_umod64
| BI_fp_sdiv32
| BI_fp_sdiv64
| BI_fp_smod32
| BI_fp_smod64
| BI_abs
| BI_absl.

Local Open Scope string_scope.

Definition platform_builtin_table : list (string * platform_builtin) :=
     ("__builtin_fmin", BI_fmin)
  :: ("__builtin_fmax", BI_fmax)
  :: ("__builtin_fminf", BI_fminf)
  :: ("__builtin_fmaxf", BI_fmaxf)
  :: ("__builtin_fma", BI_fma)
  :: ("__builtin_fmaf", BI_fmaf)
  :: ("__builtin_lround_ne", BI_lround_ne)
  :: ("__builtin_luround_ne", BI_luround_ne)
  :: ("__builtin_fp_udiv32", BI_fp_udiv32)
  :: ("__builtin_fp_udiv64", BI_fp_udiv64)
  :: ("__builtin_fp_umod32", BI_fp_umod32)
  :: ("__builtin_fp_umod64", BI_fp_umod64)
  :: ("__builtin_fp_sdiv32", BI_fp_sdiv32)
  :: ("__builtin_fp_sdiv64", BI_fp_sdiv64)
  :: ("__builtin_fp_smod32", BI_fp_smod32)
  :: ("__builtin_fp_smod64", BI_fp_smod64)
  :: ("__builtin_abs", BI_abs)
  :: ("__builtin_absl", BI_absl)
  :: nil.

Definition platform_builtin_sig (b: platform_builtin) : signature :=
  match b with
  | BI_fmin | BI_fmax =>
      mksignature (Tfloat :: Tfloat :: nil) Tfloat cc_default
  | BI_fminf | BI_fmaxf =>
      mksignature (Tsingle :: Tsingle :: nil) Tsingle cc_default
  | BI_fma =>
      mksignature (Tfloat :: Tfloat :: Tfloat :: nil) Tfloat cc_default
  | BI_fmaf =>
      mksignature (Tsingle :: Tsingle :: Tsingle :: nil) Tsingle cc_default
  | BI_lround_ne | BI_luround_ne =>
      mksignature (Tfloat :: nil) Tlong cc_default
  | BI_fp_udiv32 =>
      mksignature (Tint :: Tint :: nil) Tint cc_default
  | BI_fp_udiv64 =>
      mksignature (Tlong :: Tlong :: nil) Tlong cc_default
  | BI_fp_umod32 =>
      mksignature (Tint :: Tint :: nil) Tint cc_default
  | BI_fp_umod64 =>
      mksignature (Tlong :: Tlong :: nil) Tlong cc_default
  | BI_fp_sdiv32 =>
      mksignature (Tint :: Tint :: nil) Tint cc_default
  | BI_fp_sdiv64 =>
      mksignature (Tlong :: Tlong :: nil) Tlong cc_default
  | BI_fp_smod32 =>
      mksignature (Tint :: Tint :: nil) Tint cc_default
  | BI_fp_smod64 =>
      mksignature (Tlong :: Tlong :: nil) Tlong cc_default
  | BI_abs =>
      mksignature (Tint :: nil) Tint cc_default
  | BI_absl =>
      mksignature (Tlong :: nil) Tlong cc_default
  end.

Definition platform_builtin_sem (b: platform_builtin) : builtin_sem (sig_res (platform_builtin_sig b)) :=
  match b with
  | BI_fmin => mkbuiltin_n2t Tfloat Tfloat Tfloat ExtFloat.min
  | BI_fmax => mkbuiltin_n2t Tfloat Tfloat Tfloat ExtFloat.max
  | BI_fminf => mkbuiltin_n2t Tsingle Tsingle Tsingle ExtFloat32.min
  | BI_fmaxf => mkbuiltin_n2t Tsingle Tsingle Tsingle ExtFloat32.max
  | BI_fma => mkbuiltin_n3t Tfloat Tfloat Tfloat Tfloat Float.fma
  | BI_fmaf => mkbuiltin_n3t Tsingle Tsingle Tsingle Tsingle Float32.fma
  | BI_lround_ne => mkbuiltin_n1p Tfloat Tlong Float.to_long_ne
  | BI_luround_ne => mkbuiltin_n1p Tfloat Tlong Float.to_longu_ne
  | BI_fp_udiv32 => mkbuiltin_n2p Tint Tint Tint
       (fun n1 n2 => if Int.eq n2 Int.zero
                     then None
                     else Some (Int.divu n1 n2))
  | BI_fp_udiv64 => mkbuiltin_n2p Tlong Tlong Tlong
       (fun n1 n2 => if Int64.eq n2 Int64.zero
                     then None
                     else Some (Int64.divu n1 n2))
  | BI_fp_umod32 => mkbuiltin_n2p Tint Tint Tint
       (fun n1 n2 => if Int.eq n2 Int.zero
                     then None
                     else Some (Int.modu n1 n2))
  | BI_fp_umod64 => mkbuiltin_n2p Tlong Tlong Tlong
       (fun n1 n2 => if Int64.eq n2 Int64.zero
                     then None
                     else Some (Int64.modu n1 n2))
  | BI_fp_sdiv32 => mkbuiltin_n2p Tint Tint Tint
       (fun n1 n2 => if Int.eq n2 Int.zero
                     then None
                     else Some (Int.divs n1 n2))
  | BI_fp_sdiv64 => mkbuiltin_n2p Tlong Tlong Tlong
       (fun n1 n2 => if Int64.eq n2 Int64.zero
                     then None
                     else Some (Int64.divs n1 n2))
  | BI_fp_smod32 => mkbuiltin_n2p Tint Tint Tint
       (fun n1 n2 => if Int.eq n2 Int.zero
                     then None
                     else Some (Int.mods n1 n2))
  | BI_fp_smod64 => mkbuiltin_n2p Tlong Tlong Tlong
       (fun n1 n2 => if Int64.eq n2 Int64.zero
                     then None
                     else Some (Int64.mods n1 n2))
  | BI_abs => mkbuiltin_n1t Tint Tint ExtValues.int_abs
  | BI_absl => mkbuiltin_n1t Tlong Tlong ExtValues.long_abs
  end.
