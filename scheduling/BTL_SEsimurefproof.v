(** * Intermediate refinement of invariant application
      /!\ Proofs defined here are optional, and may be commented.
      The intermediate proof of refinement is not complete but can help
      when adding new features to decompose the proof outside the Impure library.
*)

Require Import Coqlib Maps OptionMonad Registers.
Require Import BTL_SEtheory BTL_SEsimuref.
Import SvalNotations.

Local Open Scope option_monad_scope.
Local Open Scope lazy_bool_scope.

Definition ris_ir_get ris ris_old ir: option sval :=
  if force_input ir then ris_sreg_get ris_old (regof ir) else
  ris_sreg_get ris (regof ir).

Lemma ris_ir_get_subst ctx ris ris_old sis (si: fpasv) s ir
  (ROK: ris_ok ctx ris_old)
  (RR: ris_refines ctx ris_old sis)
  (GETIR: ris_ir_get ris ris_old ir = Some s)
  (REG_SUBST: reg_subst ris sis si ctx)
  :sval_equiv ctx s (sv_subst (sis_smem sis) sis (ir_subst (ext si) ir)).
Proof.
  inv RR; generalize GETIR; clear GETIR.
  unfold ris_ir_get, ir_subst.
  repeat autodestruct; intros FINPUT GETIR;
  fold (eval_osv ctx (Some s)); eauto.
  - rewrite <- GETIR, REG_EQ; auto; simpl.
    autodestruct; rewrite <- ALIVE_EQ; auto.
    congruence.
  - rewrite <- GETIR; auto.
Qed.

Fixpoint ris_lir_get ris ris_old (lir: list ireg): option list_sval :=
  match lir with
  | nil => Some fSnil
  | ir::lir =>
      SOME lsv <- ris_lir_get ris ris_old lir IN
      SOME sv <- ris_ir_get ris ris_old ir IN
      Some (fScons sv lsv)
  end.

Lemma ris_lir_get_subst args: forall ctx ris ris_old sis (si: fpasv) l
  (ROK: ris_ok ctx ris_old)
  (RR: ris_refines ctx ris_old sis)
  (GETLIR: ris_lir_get ris ris_old args = Some l)
  (REG_SUBST: reg_subst ris sis si ctx),
  list_sval_equiv ctx l (lsv_subst (sis_smem sis) sis (lsvof (ext si) args)).
Proof.
  induction args; simpl; intros.
  - inv GETLIR; simpl; auto.
  - generalize GETLIR; clear GETLIR; do 2 autodestruct.
    intros; inv GETLIR; simpl.
    erewrite ris_ir_get_subst; eauto. autodestruct; intros EVAL.
    erewrite IHargs; eauto.
Qed.

Lemma ris_lir_get_lmap_sv args: forall ctx ris ris_old sis (si: fpasv) l
  (ROK: ris_ok ctx ris)
  (ROK_OLD: ris_ok ctx ris_old)
  (RR: ris_refines ctx ris_old sis)
  (GETLIR: ris_lir_get ris ris_old args = Some l)
  (OK_SUBST: si_ok_subst sis si ctx)
  (REG_SUBST: reg_subst ris sis si ctx),
  lmap_sv (ir_subst_si sis si) args = Some (lsv_subst (sis_smem sis) sis (lsvof (ext si) args))
  /\ eval_list_sval ctx (lsv_subst (sis_smem sis) sis (lsvof (ext si) args)) <> None.
Proof.
  induction args; simpl; intros.
  - intuition congruence.
  - generalize GETLIR; clear GETLIR; do 2 autodestruct.
    inversion RR. apply OK_EQUIV in ROK_OLD as SOK; inv SOK.
    intros; inv GETLIR; simpl.
    exploit ris_ir_get_subst; eauto; intros.
    unfold ir_subst_si at 1; unfold ris_ir_get in EQ. autodestruct.
    + autodestruct.
      * intros; exploit (IHargs ctx ris ris_old); eauto; unfold ir_subst.
        intros (LMAP & LSEVAL). rewrite LMAP.
        rewrite EQ2; simpl; rewrite EQ1. split; [ reflexivity |].
        apply OK_SREG in EQ1. repeat autodestruct.
      * rewrite <- ALIVE_EQ; congruence.
    + intros; exploit (IHargs ctx ris ris_old); eauto; unfold ir_subst.
      intros (LMAP & LSEVAL). rewrite LMAP.
      rewrite EQ1; split; [ reflexivity |]; repeat autodestruct.
      unfold ext; autodestruct; simpl.
      * intros; destruct (is_input_dec s0).
        { inv i; simpl in *; destruct (sis r) eqn:EQSIS;
          [ apply OK_SREG in EQSIS; auto | simpl in *; congruence ]. }
        apply si_wf in EQ2; auto.
        apply OK_SUBST in EQ2; congruence.
      * autodestruct; simpl; try congruence.
        intros; apply OK_SREG in EQ2; auto.
Qed.

Definition root_rapply (rsv: root_op) (lir: list ireg) (ris ris_old: ristate): option sval :=
  SOME lsv <- ris_lir_get ris ris_old lir IN
  match rsv with
  | Rop op => Some (fSop op lsv)
  | Rload trap chunk addr => Some (fSload ris trap chunk addr lsv)
  end.

Fixpoint lsv_in_ref (sv: sval) lsv: bool :=
  match lsv with
  | nil => false
  | sv'::lsv =>
      if sveq_dec sv sv' then true
      else lsv_in_ref sv lsv
  end.

Lemma lsv_in_ref_correct lsv: forall sv,
  lsv_in_ref sv lsv = true ->
  In sv lsv.
Proof.
  induction lsv; simpl.
  - congruence.
  - intros sv; autodestruct; subst; auto.
Qed.

Definition ris_rsv_set r (args: list ireg) (rsv: root_op)
  (ris ris_old: ristate) : option ristate :=
  SOME sv <- root_rapply rsv args ris ris_old IN
  SOME ok_lsv <- if may_trap rsv args then (
      if (negb ris.(si_mode)) ||| (lsv_in_ref sv (ok_rsval ris)) then
        Some (sv::(ok_rsval ris)) else None
    ) else (
      Some (ok_rsval ris))
  IN
  Some {| ris_smem := ris.(ris_smem);
          ris_input_init := ris.(ris_input_init);
          si_mode := ris.(si_mode);
          ok_rsval := ok_lsv;
          ris_sreg:= PTree.set r sv ris.(ris_sreg) |}.

Definition ris_ival_set (ris ris_old: ristate) (r: reg) (iv: ival): option ristate :=
  match iv with
  | Ireg ir =>
      SOME sv <- ris_ir_get ris ris_old ir IN
      Some (ris_sreg_set ris (PTree.set r sv (ris_sreg ris)))
  | Iop rop args => ris_rsv_set r args rop ris ris_old
  end.

Lemma ris_ival_set_preserv_rinit (ris ris_old ris': ristate) (r: reg) (iv: ival)
  (RINIT: ris_input_init ris = true)
  (SIVAL: ris_ival_set ris ris_old r iv = Some ris')
  :ris_input_init ris' = true.
Proof.
  destruct iv; simpl in SIVAL;
  unfold ris_rsv_set in SIVAL;
  generalize SIVAL; clear SIVAL; repeat autodestruct;
  intros; inv SIVAL; simpl; auto.
Qed.

Lemma ris_ival_set_preserv_sim (ris ris_old ris': ristate) (r: reg) (iv: ival)
  (SIM: si_mode ris = true)
  (SIVAL: ris_ival_set ris ris_old r iv = Some ris')
  :si_mode ris' = true.
Proof.
  destruct iv; simpl in SIVAL;
  unfold ris_rsv_set in SIVAL;
  generalize SIVAL; clear SIVAL; repeat autodestruct;
  intros; inv SIVAL; simpl; auto.
Qed.

Lemma ris_ival_set_incl_rev ris ris_old ris' r iv
  (SIM: si_mode ris = true)
  (SIVAL: ris_ival_set ris ris_old r iv = Some ris')
  :incl (ok_rsval ris') (ok_rsval ris).
Proof.
  destruct iv; simpl in SIVAL;
  unfold ris_rsv_set, ris_sreg_set in SIVAL;
  generalize SIVAL; clear SIVAL; repeat autodestruct;
  intros; inv SIVAL; simpl; try apply incl_refl.
  - rewrite SIM in EQ; discriminate.
  - apply lsv_in_ref_correct in EQ.
    apply incl_cons; auto. apply incl_refl.
Qed.

Lemma ris_ival_set_preserv_smem (ris ris_old ris': ristate) (r: reg) (iv: ival)
  (MEM_EQ: (ris_smem ris) = (ris_smem ris_old))
  (SIVAL: ris_ival_set ris ris_old r iv = Some ris')
  :(ris_smem ris_old) = (ris_smem ris').
Proof.
  destruct iv; simpl in SIVAL;
  unfold ris_rsv_set in SIVAL;
  generalize SIVAL; clear SIVAL; repeat autodestruct;
  intros; inv SIVAL; simpl; auto.
Qed.

Lemma ris_ival_set_preserv_rok ctx (ris ris_old ris': ristate) r iv
  (ROK: ris_ok ctx ris)
  (SIM: si_mode ris = true)
  (SIVAL: ris_ival_set ris ris_old r iv = Some ris')
  :ris_ok ctx ris'.
Proof.
  destruct iv; simpl in *.
  - generalize SIVAL; clear SIVAL; autodestruct; intros.
    inv SIVAL; inv ROK; constructor; auto.
  - generalize SIVAL; clear SIVAL; unfold ris_rsv_set;
    repeat autodestruct; intros; inv SIVAL; inv ROK; constructor; auto.
    + rewrite SIM in EQ; discriminate.
    + simpl; intros sv [CUR|HIN]; auto; subst.
      apply lsv_in_ref_correct in EQ; auto.
Qed.

Ltac destruct_sreg_si_set r r0 :=
  unfold ris_sreg_set, ris_sreg_get, ext in *; simpl;
  destruct (Pos.eq_dec r r0); subst;
  rewrite PTree.gss || rewrite PTree.gso; auto;
  rewrite si_gss || rewrite si_gso; auto.

Lemma ris_ival_set_sv_subst ctx ris ris_old ris' sis (si: fpasv) r0 iv
  (ROK: ris_ok ctx ris_old)
  (RR: ris_refines ctx ris_old sis)
  (MEM_EQ: (ris_smem ris) = (ris_smem ris_old))
  (SIVAL: ris_ival_set ris ris_old r0 iv = Some ris')
  (REG_SUBST: reg_subst ris sis si ctx)
  :reg_subst ris' sis (si_set r0 (iv_subst (ext si) iv) si) ctx.
Proof.
  unfold reg_subst in *.
  inversion RR; destruct iv; subst; simpl;
  generalize SIVAL; clear SIVAL; simpl.
  - autodestruct; intros; inv SIVAL.
    destruct_sreg_si_set r r0.
    eapply ris_ir_get_subst; eauto.
  - unfold ris_rsv_set, root_rapply; repeat autodestruct;
    intros; inv SIVAL; destruct_sreg_si_set r r0;
    simpl; erewrite ris_lir_get_subst; eauto; try reflexivity;
    rewrite MEM_EQ, MEM_EQ0; auto.
Qed.

Lemma ris_ival_set_tr_sis_ok ctx ris ris_old ris' sis (si: fpasv) r iv
  (SOK: sis_ok ctx sis)
  (ROK_OUT: ris_ok ctx ris')
  (ROK_OLD: ris_ok ctx ris_old)
  (RR: ris_refines ctx ris_old sis)
  (MEM_EQ: (ris_smem ris) = (ris_smem ris_old))
  (SIM: si_mode ris = true)
  (SIVAL: ris_ival_set ris ris_old r iv = Some ris')
  (OK_SUBST: si_ok_subst sis si ctx)
  (REG_SUBST: reg_subst ris sis si ctx)
  :si_ok_subst sis (si_set r (iv_subst (ext si) iv) si) ctx.
Proof.
  unfold si_ok_subst in *.
  inv SOK; destruct iv; subst; simpl;
  generalize SIVAL; clear SIVAL; simpl.
   -intros; destruct H as [CUR|HIN]; auto.
    generalize CUR; clear CUR; unfold ir_subst.
    autodestruct; intros FINPUT H; inv H; simpl.
    + autodestruct; simpl; try congruence; apply OK_SREG.
    + destruct ((fpa_reg si)!(regof ir)) eqn:HSI;
      unfold ext, si_apply; rewrite HSI.
      1: destruct (s); try (apply fpa_wf in HSI; auto; intros CONTRA; inv CONTRA; fail).
      all: simpl; autodestruct; simpl; try congruence; apply OK_SREG.
  - inversion ROK_OUT; unfold ris_rsv_set, root_rapply; do 2 autodestruct;
    intros; destruct H as [CUR|HIN]; auto; generalize SIVAL; clear SIVAL;
    repeat autodestruct.
    1,4: rewrite SIM; discriminate.
    2,4:
      intros; inv SIVAL;
      eapply ris_lir_get_lmap_sv in EQ0 as LMAP; eauto;
      [| inv ROK_OUT; simpl in *; constructor; auto ];
      eapply may_trap_correct; intuition eauto;
      simpl; unfold root_sapply; rewrite H; reflexivity.
      all: intros; inv SIVAL; simpl; inversion RR.
      * erewrite <- ris_lir_get_subst; eauto; intros.
        specialize (OK_RREG (fSop op l)); simpl in OK_RREG;
        apply OK_RREG; auto.
      * erewrite <- ris_lir_get_subst, <- MEM_EQ0, <- MEM_EQ; eauto; intros;
        specialize (OK_RREG (fSload ris trap chunk addr l)); simpl in OK_RREG;
        apply OK_RREG; auto.
Qed.

Fixpoint exec_seq_ref (ris ris_old: ristate) (l: list (reg*ival)): option ristate :=
  match l with
  | nil => Some ris
  | (r,iv)::l =>
      SOME ris' <- ris_ival_set ris ris_old r iv IN
      exec_seq_ref ris' ris_old l
  end.

Lemma exec_seq_ref_tr_sis_ok lseq: forall ctx (ris ris_old ris': ristate) (sis: sistate) (si: fpasv)
  (SOK: sis_ok ctx sis)
  (ROK: ris_ok ctx ris)
  (ROK_OLD: ris_ok ctx ris_old)
  (RR: ris_refines ctx ris_old sis)
  (SIM: si_mode ris = true)
  (MEM_EQ: (ris_smem ris) = (ris_smem ris_old))
  (OK_SUBST: si_ok_subst sis si ctx)
  (REG_SUBST: reg_subst ris sis si ctx)
  (EXSEQ: exec_seq_ref ris ris_old lseq = Some ris'),
  tr_sis_ok ctx sis (exec_seq lseq si).
Proof.
  induction lseq; simpl; intros.
  - intros _ sv HIN; apply OK_SUBST; auto.
  - generalize EXSEQ; clear EXSEQ; repeat autodestruct.
    intros; subst.
    apply (ris_ival_set_preserv_rok ctx ris ris_old r0) in EQ as ROK_OUT; auto.
    apply (ris_ival_set_preserv_sim ris ris_old r0) in EQ as SIM_OUT; auto.
    apply (ris_ival_set_preserv_smem ris ris_old r0) in EQ as MEM_EQ_OUT; auto.
    eapply (IHlseq ctx r0 ris_old ris'); eauto; intros.
    + eapply (ris_ival_set_tr_sis_ok ctx ris ris_old r0); eauto.
    + eapply ris_ival_set_sv_subst in EQ; eauto.
Qed.

Lemma exec_seq_preserv_sim_smem lseq: forall (ris ris_old ris': ristate)
  (SIM: si_mode ris = true)
  (MEM_EQ: (ris_smem ris) = (ris_smem ris_old))
  (EXSEQ: exec_seq_ref ris ris_old lseq = Some ris'),
  si_mode ris' = true /\ (ris_smem ris) = (ris_smem ris').
Proof.
  induction lseq; simpl.
  - intros; inv EXSEQ; auto.
  - intros until ris';  repeat autodestruct; intros; subst.
    apply ris_ival_set_preserv_smem in EQ as MEM_EQ_OUT; auto.
    apply ris_ival_set_preserv_sim in EQ as SIM_OUT; auto.
    rewrite MEM_EQ, MEM_EQ_OUT.
    apply (IHlseq r0 ris_old ris'); auto.
Qed.

Lemma exec_seq_ref_incl_rev lseq: forall (ris ris_old ris': ristate)
  (SIM: si_mode ris = true)
  (EXSEQ: exec_seq_ref ris ris_old lseq = Some ris'),
  incl (ok_rsval ris') (ok_rsval ris).
Proof.
  induction lseq; simpl.
  - intros; inv EXSEQ. apply incl_refl.
  - intros until ris';  repeat autodestruct; intros; subst.
    apply ris_ival_set_incl_rev in EQ as INCL; auto.
    eapply incl_tran with (l:=(ok_rsval ris')); eauto.
    apply ris_ival_set_preserv_sim in EQ as SIM_OUT; auto.
    apply (IHlseq r0 ris_old ris'); auto.
Qed.

Lemma exec_seq_ref_sv_subst lseq: forall ctx (ris ris_old ris': ristate) (sis: sistate) (si: fpasv)
  (ROK_OLD: ris_ok ctx ris_old)
  (RR_OLD: ris_refines ctx ris_old sis)
  (MEM_EQ: (ris_smem ris) = (ris_smem ris_old))
  (REG_SUBST: reg_subst ris sis si ctx)
  (RINIT: ris_input_init ris = true)
  (SIM: si_mode ris = true)
  (EXSEQ: exec_seq_ref ris ris_old lseq = Some ris'),
  reg_subst ris' sis (exec_seq lseq si) ctx.
Proof.
  induction lseq; simpl; intros; inversion RR_OLD.
  - inv EXSEQ; eauto.
  - generalize EXSEQ; clear EXSEQ; repeat autodestruct.
    unfold reg_subst in *; intros; subst.
    apply (ris_ival_set_preserv_rinit ris ris_old r0) in EQ as RINIT_OUT; auto.
    apply (ris_ival_set_preserv_sim ris ris_old r0) in EQ as SIM_OUT; auto.
    apply (ris_ival_set_preserv_smem ris ris_old r0) in EQ as MEM_EQ_OUT; auto.
    eapply IHlseq with (ris:=r0); eauto.
    eapply ris_ival_set_sv_subst; eauto.
Qed.

Fixpoint build_alive_ref (loutputs: list reg) (ris: ristate): option (PTree.t sval) :=
  match loutputs with
  | nil => Some (PTree.empty sval)
  | r :: tl =>
      SOME sreg <- build_alive_ref tl ris IN
      SOME hsv <- ris_sreg_get ris r IN
      Some (PTree.set r hsv sreg)
  end.

Lemma build_alive_ref_sv_subst loutputs: forall ctx (ris: ristate) ris' sis lseq r sv1 sv2
  (GETR: ris'!r = sv1)
  (REG_SUBST: reg_subst ris sis (exec_seq lseq si_empty) ctx)
  (BA: (build_alive (ext (exec_seq lseq si_empty)) loutputs) ! r = Some sv2)
  (BA_REF: build_alive_ref loutputs ris = Some ris'),
  eval_osv ctx ris'!r = eval_sval ctx (sv_subst (sis_smem sis) sis sv2).
Proof.
  induction loutputs; simpl; intros.
  - rewrite PTree.gEmpty in BA; congruence.
  - generalize BA_REF; clear BA_REF; repeat autodestruct; intros; inv BA_REF.
    destruct (Pos.eq_dec a r); subst.
    + rewrite PTree.gss in *; inv BA.
      rewrite <- REG_SUBST, EQ; reflexivity.
    + rewrite PTree.gso in *; auto.
      eapply IHloutputs; eauto.
Qed.

Lemma build_alive_ref_alive_eq lseq loutputs: forall (ris: ristate) hsreg r
  (BA_REF: build_alive_ref loutputs ris = Some hsreg),
  hsreg!r = None <-> (build_alive (ext (exec_seq lseq si_empty)) loutputs)!r = None.
Proof.
  induction loutputs; simpl; intros.
  - inv BA_REF; intuition auto.
  - generalize BA_REF; clear BA_REF; repeat autodestruct.
    intros; inv BA_REF.
    destruct (Pos.eq_dec a r); subst; intros.
    + rewrite !PTree.gss. split; congruence.
    + rewrite !PTree.gso; eauto.
Qed.

Lemma build_alive_ref_nofail loutputs sreg_subst: forall ris sreg r sv
  (BA_REF: (build_alive_ref loutputs ris = Some sreg))
  (BA: (sreg ! r = Some sv
       /\ (build_alive sreg_subst loutputs) ! r = None)
    \/ (sreg ! r = None
       /\ (build_alive sreg_subst loutputs) ! r = Some sv)),
  False.
Proof.
  induction loutputs; simpl.
  - intros; intuition; congruence.
  - intros until sv; repeat autodestruct; intros;
    destruct (Pos.eq_dec a r); subst;
    destruct BA as [[HL1 HL2]|[HR1 HR2]];
    try (try_simplify_someHyps; rewrite !PTree.gss in *; congruence);
    try (try_simplify_someHyps; rewrite !PTree.gso in *; eauto).
Qed.

Definition tr_ris_single (ris: ristate) (csi: csasv): option ristate :=
  SOME ris' <- exec_seq_ref ris ris csi.(aseq) IN
  SOME sreg <- build_alive_ref (Regset.elements csi.(outputs)) ris' IN
  Some (ris_sreg_set ris' sreg).

(** Grouping together properties about the consistency of invariants application. *)
Record tr_ris_coherent ctx (ris: ristate) (sis: sistate) (si: fpasv) : Prop := {
  ALIVE: forall r : positive, ris r = None -> si r = None;
  REG: forall r sv1 sv2, ris r = Some sv1 -> si r = Some sv2 ->
       sval_equiv ctx sv1 (sv_subst (sis_smem sis) sis sv2);
  TRSOK: tr_sis_ok ctx sis si;
  RR: ris_refines ctx ris (tr_sis sis si false) }.

Theorem tr_ris_single_correct ctx ris ris' sis csi
  (RINIT: ris_input_init ris = true)
  (SIM: si_mode ris = true)
  (SOK: sis_ok ctx sis)
  (RR: ris_refines ctx ris sis)
  (TR: tr_ris_single ris csi = Some ris')
  :tr_ris_coherent ctx (ris_input_false ris') sis csi.
Proof.
  inversion SOK; inversion RR; subst.
  apply OK_EQUIV in SOK as ROK. unfold ris_input_false, ris_sreg_set.
  generalize TR; clear TR; unfold tr_ris_single; repeat autodestruct.
  intros BA EXSEQ H; inv H.
  assert (TRSOK: tr_sis_ok ctx sis csi) by
  (eapply (exec_seq_ref_tr_sis_ok (aseq csi) ctx ris ris r); unfold si_ok_subst; eauto with sempty).
  constructor; simpl; auto.
  - unfold ris_sreg_get, si_apply; simpl.
    intros r0; autodestruct; intros GETR _.
    erewrite <- build_alive_ref_alive_eq; eauto.
  - unfold ris_sreg_get, si_apply; simpl; intros r0 sv1 sv2; autodestruct; intros.
    inv H; fold (eval_osv ctx (Some sv1)). rewrite <- EQ.
    eapply build_alive_ref_sv_subst with (ris':=t0); unfold reg_subst; eauto.
    eapply exec_seq_ref_sv_subst; eauto with sempty.
  - apply exec_seq_preserv_sim_smem in EXSEQ as PRES; auto.
    destruct PRES as [SIM_OUT MEM_EQ_OUT]. constructor.
    + set (ris' := {| ris_smem := r;
                      ris_input_init := false;
                      si_mode := si_mode r;
                      ok_rsval := ok_rsval r;
                      ris_sreg := t0 |}).
      apply (exec_seq_ref_incl_rev (aseq csi) ris ris r) in EXSEQ as INCL; auto.
      assert(OK_TR_HRS: ris_ok ctx ris' <-> ris_ok ctx ris) by
      (split; intros OK; inv OK; inv ROK; econstructor; eauto; simpl;
        rewrite <- MEM_EQ_OUT; auto).
      rewrite OK_TR_HRS, <- OK_EQUIV; eauto.
      apply ok_tr_sis; auto. 
    + simpl; rewrite <- MEM_EQ_OUT; auto.
    + intros; unfold ris_sreg_get, tr_sis, siof.
      exploit (build_alive_ref_alive_eq (aseq csi) (Regset.elements (outputs csi))); eauto; simpl.
      replace (SOME sv <- t0 ! r0 IN Some sv) with (t0 ! r0) by autodestruct.
      intros ALIVE_EQ'; rewrite ALIVE_EQ'.
      split; autodestruct; intros BA1 BA2.
      exploit build_alive_nofail; eauto;
      contradiction.
    + intros; unfold ris_sreg_get, tr_sis, siof; simpl.
      replace (SOME sv <- t0 ! r0 IN Some sv) with (t0 ! r0) by autodestruct.
      unfold eval_osv. repeat autodestruct; unfold si_apply; simpl.
      2,3: intros; exploit build_alive_ref_nofail; eauto; contradiction.
      intros BA_OUT GETR. fold (eval_osv ctx (Some s)); rewrite <- GETR.
      eapply build_alive_ref_sv_subst; eauto.
      eapply (exec_seq_ref_sv_subst (aseq csi) ctx ris ris r); eauto with sempty.
Qed.
