(* *************************************************************)
(*                                                             *)
(*             The Compcert verified compiler                  *)
(*                                                             *)
(*           Léo Gourdin        VERIMAG, UGA                   *)
(*           Sylvain Boulmé     Grenoble-INP, VERIMAG          *)
(*                                                             *)
(*  Copyright VERIMAG. All rights reserved.                    *)
(*  This file is distributed under the terms of the INRIA      *)
(*  Non-Commercial License Agreement.                          *)
(*                                                             *)
(* *************************************************************)

Require Import Coqlib AST Registers Maps.
Require Import Op OptionMonad.
Require Import BTL_SEtheory BTL_SEsimuref.
Require Import BTL_SEsimplify BTL_SEsimplifyproof.
Import Notations.
Import HConsing.
Import SvalNotations.
Import ListNotations.

Local Open Scope list_scope.
Local Open Scope option_monad_scope.
Local Open Scope impure.

(** Debug printer *)
Definition XDEBUG {A} (x:A) (k: A -> ?? pstring): ?? unit := RET tt. (* TO REMOVE DEBUG INFO *)
(*Definition XDEBUG {A} (x:A) (k: A -> ?? pstring): ?? unit := DO s <~ k x;; println ("DEBUG simu_check:" +; s). (* TO INSERT DEBUG INFO *)*)

Definition DEBUG (s: pstring): ?? unit := XDEBUG tt (fun _ => RET s).

(** * Implementation of Data-structure use in Hash-consing *)

(** Now, we build the hash-Cons value from a "hash_eq".

  Informal specification: 
    [hash_eq] must be consistent with the "hashed" constructors defined above.

  We expect that hashinfo values in the code of these "hashed" constructors verify:
    (hash_eq (hdata x) (hdata y) ~> true) <-> (hcodes x)=(hcodes y)
*)

Definition sval_hash_eq (sv1 sv2: sval): ?? bool :=
  match sv1, sv2 with
  | Sinput r1 _, Sinput r2 _ => struct_eq r1 r2 (* NB: really need a struct_eq here ? *)
  | Sop op1 lsv1 _, Sop op2 lsv2 _  =>
     DO b1 <~ phys_eq lsv1 lsv2;;
     if b1
     then struct_eq op1 op2 (* NB: really need a struct_eq here ? *)
     else RET false
  | Sload sm1 trap1 chk1 addr1 lsv1 _, Sload sm2 trap2 chk2 addr2 lsv2 _ =>
     DO b1 <~ phys_eq lsv1 lsv2;;
     DO b2 <~ phys_eq sm1 sm2;;
     DO b3 <~ struct_eq trap1 trap2;;
     DO b4 <~ struct_eq chk1 chk2;;
     if b1 && b2 && b3 && b4
     then struct_eq addr1 addr2
     else RET false
  | Sfoldr op1 lsv1 sv0_1 _, Sfoldr op2 lsv2 sv0_2 _ =>
      DO b1 <~ phys_eq lsv1 lsv2;;
      if b1 then
        DO b2 <~ phys_eq sv0_1 sv0_2;;
        if b2 then
          struct_eq op1 op2
        else RET false
      else RET false
  | _,_ => RET false
  end.

Lemma and_true_split a b: a && b = true <-> a = true /\ b = true.
Proof.
  destruct a; simpl; intuition.
Qed.

Lemma sval_hash_eq_correct x y:
  WHEN sval_hash_eq x y ~> b THEN 
  b = true -> sval_set_hid x unknown_hid = sval_set_hid y unknown_hid.
Proof.
  destruct x, y; wlp_simplify; try ( rewrite !and_true_split in * ); intuition; subst; try congruence.
Qed.
Global Opaque sval_hash_eq.
Global Hint Resolve sval_hash_eq_correct: wlp.

Definition list_sval_hash_eq (lsv1 lsv2: list_sval): ?? bool :=
  match lsv1, lsv2 with
  | Snil _, Snil _ => RET true
  | Scons sv1 lsv1' _, Scons sv2 lsv2' _  =>
     DO b <~ phys_eq lsv1' lsv2';;
     if b 
     then phys_eq sv1 sv2
     else RET false
  | _,_ => RET false
  end.

Lemma list_sval_hash_eq_correct x y:
  WHEN list_sval_hash_eq x y ~> b THEN 
  b = true -> list_sval_set_hid x unknown_hid = list_sval_set_hid y unknown_hid.
Proof.
  destruct x, y; wlp_simplify; try ( rewrite !and_true_split in * ); intuition; subst; try congruence.
Qed.
Global Opaque list_sval_hash_eq.
Global Hint Resolve list_sval_hash_eq_correct: wlp.

Definition smem_hash_eq (sm1 sm2: smem): ?? bool :=
  match sm1, sm2 with
  | Sinit _, Sinit _ => RET true
  | Sstore sm1 chk1 addr1 lsv1 sv1 _, Sstore sm2 chk2 addr2 lsv2 sv2 _ =>
     DO b1 <~ phys_eq lsv1 lsv2;;
     DO b2 <~ phys_eq sm1 sm2;;
     DO b3 <~ phys_eq sv1 sv2;;
     DO b4 <~ struct_eq chk1 chk2;;
     if b1 && b2 && b3 && b4
     then struct_eq addr1 addr2
     else RET false
  | _,_ => RET false
  end.

Lemma smem_hash_eq_correct x y:
  WHEN smem_hash_eq x y ~> b THEN 
  b = true -> smem_set_hid x unknown_hid = smem_set_hid y unknown_hid.
Proof.
  destruct x, y; wlp_simplify; try ( rewrite !and_true_split in * ); intuition; subst; try congruence.
Qed.
Global Opaque smem_hash_eq.
Global Hint Resolve smem_hash_eq_correct: wlp.

Definition hSVAL: hashP sval := {| hash_eq := sval_hash_eq; get_hid:=sval_get_hid; set_hid:=sval_set_hid |}. 
Definition hLSVAL: hashP list_sval := {| hash_eq := list_sval_hash_eq; get_hid:= list_sval_get_hid; set_hid:= list_sval_set_hid |}.
Definition hSMEM: hashP smem := {| hash_eq := smem_hash_eq; get_hid:= smem_get_hid; set_hid:= smem_set_hid |}.

Program Definition mk_hash_params: Dict.hash_params sval :=
 {|
    Dict.test_eq := phys_eq;
    Dict.hashing := fun (sv: sval) => RET (sval_get_hid sv);
    Dict.log := fun sv =>
         DO sv_name <~ string_of_hashcode (sval_get_hid sv);;
         println ("unexpected undef behavior of hashcode:" +; (CamlStr sv_name)) |}.
Next Obligation.
  wlp_simplify.
Qed.

Definition red_PTree_set (r: reg) (sv: sval) (ris: ristate): PTree.t sval :=
  match (ris_input_init ris), sv with
  | true, Sinput r' _ =>
      if Pos.eq_dec r r' 
      then PTree.remove r' ris.(ris_sreg)
      else PTree.set r sv ris.(ris_sreg)
  | _, _ => PTree.set r sv ris.(ris_sreg)
  end.

Lemma red_PTree_set_correct (r r0:reg) sv usv usv' (ris: ristate) ctx: forall
  (RED: (ris_sreg_set ris (red_PTree_set r sv ris)) r0 = Some usv)
  (SET: (ris_sreg_set ris (PTree.set r sv ris.(ris_sreg))) r0 = Some usv'),
  sval_equiv ctx usv usv'.
Proof.
  unfold red_PTree_set, ris_sreg_set, ris_sreg_get; simpl.
  destruct (ris_input_init ris) eqn:Hinit, sv; simpl; auto.
  1: destruct (Pos.eq_dec r r1); auto; subst; [ rename r1 into r |].
  all: destruct (Pos.eq_dec r r0); auto;
       [ subst; rewrite PTree.gss; try rewrite PTree.grs; try_simplify_someHyps |
         rewrite PTree.gso; try rewrite PTree.gro; simpl; auto; autodestruct ].
Qed.

Lemma red_PTree_set_refines_none (r r0:reg) ris sv:
  ris_sreg_set ris (red_PTree_set r sv ris) r0 = None <->
  ris_sreg_set ris (PTree.set r sv ris.(ris_sreg)) r0 = None.
Proof.
  split; intros HUSV1;
  generalize HUSV1; clear HUSV1; unfold red_PTree_set, ris_sreg_set, ris_sreg_get;
  repeat autodestruct; subst; intros; simpl in EQ0; discriminate.
Qed.

Lemma red_PTree_set_refines_sval (r r0:reg) ctx ris sis sv1 sv2 ousv
  (ESVEQ: sval_equiv ctx sv1 sv2)
  (ROK: ris_ok ctx ris)
  (RR: ris_refines ctx ris sis)
  (RED: ris_sreg_set ris (red_PTree_set r sv1 ris) r0 = ousv)
  :osval_equiv ctx ousv (if Pos.eq_dec r r0 then Some sv2 else sis r0).
Proof.
  destruct ousv.
  - assert (SET: exists usv', (ris_sreg_set ris (PTree.set r sv1 ris.(ris_sreg))) r0 = Some usv').
    { generalize RED; clear RED. unfold red_PTree_set, ris_sreg_set, ris_sreg_get.
      simplify_option. }
    destruct SET as (usv & SET). unfold eval_osv.
    erewrite red_PTree_set_correct with (usv':=usv); eauto.
    generalize SET; clear SET. unfold ris_sreg_set, ris_sreg_get.
    destruct (Pos.eq_dec r r0).
    + subst; simpl. rewrite PTree.gss; simpl; auto.
      intro SSV; inv SSV; assumption.
    + inv RR; simpl. rewrite PTree.gso; simpl; auto.
      fold (ris_sreg_get ris r0); intros HSREG.
      fold (eval_osv ctx (Some usv)).
      fold (eval_osv ctx (sis r0)).
      rewrite <- HSREG, <- REG_EQ; auto.
  - apply red_PTree_set_refines_none in RED as SET.
    generalize SET; clear SET. unfold ris_sreg_set, ris_sreg_get.
    repeat autodestruct; subst; simpl.
    + rewrite PTree.gss. congruence.
    + rewrite PTree.gso; auto. inv RR. intros.
      assert (HN: ris r0 = None) by (unfold ris_sreg_get; rewrite EQ0, EQ1; auto).
      apply ALIVE_EQ in HN; auto. rewrite HN; simpl; reflexivity.
Qed.

Lemma red_PTree_set_alive_eq ctx ris sis r r0 sv res
  (ROK: ris_ok ctx ris)
  (RR: ris_refines ctx ris sis)
  :ris_sreg_set ris (red_PTree_set r res ris) r0 = None <->
  set_sreg r sv sis r0 = None.
Proof.
  rewrite red_PTree_set_refines_none.
   unfold ris_sreg_get, set_sreg; simpl.
   destruct (Pos.eq_dec r r0); subst; simpl.
   - rewrite PTree.gss; split; congruence.
   - rewrite PTree.gso; auto. 
     intros. inv RR; autodestruct.
     all: 
       rewrite <- ALIVE_EQ; auto; unfold ris_sreg_get;
       intros EQ; rewrite EQ; split; congruence.
Qed.

Ltac simpl_ptree_fcts :=
  try (rewrite !PTree.grs; auto; fail);
  try (rewrite !PTree.gro; auto; fail);
  try (rewrite !PTree.gss; auto; fail);
  try (rewrite !PTree.gso; auto; fail).

Lemma red_PTree_set_ir_sv_subst (r:reg) ctx (ris: ristate)
  sis (si: fpasv) ir sv
  (ESVEQ: sval_equiv ctx sv (sv_subst (sis_smem sis) sis (ir_subst (ext si) ir)))
  (REG_SUBST: reg_subst ris sis si ctx)
  : reg_subst (ris_sreg_set ris (red_PTree_set r sv ris)) sis
    (si_set r (ir_subst (ext si) ir) si) ctx.
Proof.
  unfold reg_subst, ris_sreg_set, ris_sreg_get, red_PTree_set, ir_subst, ext in *; simpl.
  intros; destruct (Pos.eq_dec r r0); subst; rewrite si_gss || rewrite si_gso; auto;
  repeat (autodestruct; subst; simpl_ptree_fcts).
Qed.

Lemma red_PTree_set_rop_sv_subst sv sv' (r r0: reg) ctx (ris: ristate)
  sis (si: fpasv) args (rop: root_op) lsv l
  (ESVEQ: sval_equiv ctx sv sv')
  (ELSVEQ: eval_list_sval ctx l = eval_list_sval ctx lsv)
  (LRSUBST: list_sval_equiv ctx l (lsv_subst (sis_smem sis) sis (lsvof (ext si) args)))
  (LMAP: lmap_sv (ir_subst_si sis si) args = Some lsv)
  (RSV: rop args sis si = Some sv)
  (REG_SUBST: reg_subst ris sis si ctx)
  :eval_osv ctx (ris_sreg_set ris (red_PTree_set r sv' ris) r0) =
  eval_sval ctx (sv_subst (sis_smem sis) sis (ext (si_set r (rop_subst (ext si) rop args) si) r0)).
Proof.
  unfold reg_subst, ris_sreg_set, ris_sreg_get, red_PTree_set, rop_subst, root_apply, ext in *; simpl.
  revert RSV; unfold root_sapply; rewrite LMAP; intros HROP.
  destruct (Pos.eq_dec r r0); subst; rewrite si_gss || rewrite si_gso; auto.
  - repeat (autodestruct; subst;
      try (rewrite PTree.grs || rewrite PTree.gss;
           autodestruct; inv HROP; simpl in *;
           rewrite <- ESVEQ; unfold ext in *;
           rewrite <- ELSVEQ, LRSUBST; reflexivity; fail)).
  - repeat (autodestruct; subst;
      try (rewrite PTree.gro || rewrite PTree.gso; auto; eauto; fail)).
Qed.

Lemma rsv_set_alive_eq ctx sis hrs sv_sis sv_ref r:
  ris_refines ctx hrs sis ->
  sis_ok ctx sis /\ eval_sval ctx sv_sis <> None ->
  forall r0 rsvals,
  ris_sreg_ok_set hrs (red_PTree_set r sv_ref hrs) rsvals r0 =
  None <-> set_sreg r sv_sis sis r0 = None.
Proof.
  intros RR (SOK & SEVAL) r0 rsvals; inversion RR.
  apply OK_EQUIV in SOK as ROK.
  rewrite ris_sreg_set_access.
  rewrite red_PTree_set_alive_eq; eauto.
  reflexivity.
Qed.

(** * Implementation of symbolic execution *)
Record HashConsingFcts :=
  {
    hC_sval: hashinfo sval -> ?? sval;
    hC_list_sval: hashinfo list_sval -> ?? list_sval;
    hC_smem: hashinfo smem -> ?? smem
  }.

Class HashConsingHyps HCF :=
  {
    hC_sval_correct: forall s,
      WHEN HCF.(hC_sval) s ~> s' THEN forall ctx,
      sval_equiv ctx (hdata s) s';

    hC_list_sval_correct: forall lh,
      WHEN HCF.(hC_list_sval) lh ~> lh' THEN forall ctx,
      list_sval_equiv ctx (hdata lh) lh';

    hC_smem_correct: forall hm,
      WHEN HCF.(hC_smem) hm ~> hm' THEN forall ctx,
      smem_equiv ctx (hdata hm) hm'
  }.

Global Hint Resolve hC_sval_correct hC_list_sval_correct hC_smem_correct: wlp.

(* Useful Ltac when an impure lemma conclude to a contradiction. *)
Ltac wlp_exploit hint := try intros; exploit hint; eauto; try contradiction.

Section SymbolicCommon.

  Context `{HCF : HashConsingFcts}.
  Context `{HC : HashConsingHyps HCF}.
  Context `{RRULES: rrules_set}.

(** Hash consing of symbolic values *)

Definition reg_hcode := 1.
Definition op_hcode := 2.
Definition foldr_hcode := 3.
Definition load_hcode := 4.
Definition undef_code := 5.

Definition hSinput_hcodes (r: reg) :=
  DO hc <~ hash reg_hcode;;
  DO sv <~ hash r;;
  RET [hc;sv].
Extraction Inline hSinput_hcodes.

Definition hSop_hcodes (op:operation) (lsv: list_sval) :=
  DO hc <~ hash op_hcode;;
  DO sv <~ hash op;;
  RET [hc;sv;list_sval_get_hid lsv].
Extraction Inline hSop_hcodes.

Definition hSfoldr_hcodes (op: operation) (lsv: list_sval) (sv0: sval) :=
  DO hc <~ hash foldr_hcode;;
  DO sv <~ hash op;;
  RET [hc;sv;list_sval_get_hid lsv; sval_get_hid sv0].
Extraction Inline hSfoldr_hcodes.

Definition hSload_hcodes (sm: smem) (trap: trapping_mode) (chunk: memory_chunk)
  (addr: addressing) (lsv: list_sval):=
  DO hc <~ hash load_hcode;;
  DO sv1 <~ hash trap;;
  DO sv2 <~ hash chunk;;
  DO sv3 <~ hash addr;;
  RET [hc; smem_get_hid sm; sv1; sv2; sv3; list_sval_get_hid lsv].
Extraction Inline hSload_hcodes.

Definition hSstore_hcodes (sm: smem) (chunk: memory_chunk)
  (addr: addressing) (lsv: list_sval) (srce: sval) :=
  DO sv1 <~ hash chunk;;
  DO sv2 <~ hash addr;;
  RET [smem_get_hid sm; sv1; sv2; list_sval_get_hid lsv; sval_get_hid srce].
Extraction Inline hSstore_hcodes.

Definition hSnil (_: unit): ?? list_sval :=
  hC_list_sval HCF {| hdata := Snil unknown_hid; hcodes := nil |}.

Lemma hSnil_correct:
  WHEN hSnil() ~> sv THEN forall ctx,
  list_sval_equiv ctx sv (Snil unknown_hid).
Proof.
  unfold hSnil; wlp_simplify.
Qed.
Global Opaque hSnil.
Hint Resolve hSnil_correct: wlp.

Definition hScons (sv: sval) (lsv: list_sval): ?? list_sval :=
  hC_list_sval HCF
  {| hdata := Scons sv lsv unknown_hid; hcodes := [sval_get_hid sv; list_sval_get_hid lsv] |}.

Lemma hScons_correct sv1 lsv1:
  WHEN hScons sv1 lsv1 ~> lsv1' THEN forall ctx sv2 lsv2
  (ESVEQ: sval_equiv ctx sv1 sv2)
  (ELSVEQ: list_sval_equiv ctx lsv1 lsv2),
  list_sval_equiv ctx lsv1' (Scons sv2 lsv2 unknown_hid).
Proof.
  unfold hScons; wlp_simplify.
  rewrite <- ESVEQ, <- ELSVEQ.
  auto.
Qed.
Global Opaque hScons.
Hint Resolve hScons_correct: wlp.

Definition hSinput (r:reg): ?? sval :=
  DO sv <~ hSinput_hcodes r;;
  hC_sval HCF {| hdata:=Sinput r unknown_hid; hcodes :=sv; |}.

Lemma hSinput_correct r:
  WHEN hSinput r ~> sv THEN forall ctx,
  sval_equiv ctx sv (Sinput r unknown_hid).
Proof.
  wlp_simplify.
Qed.
Global Opaque hSinput.
Hint Resolve hSinput_correct: wlp.
  
Definition hSop (op:operation) (lsv: list_sval): ?? sval :=
  DO sv <~ hSop_hcodes op lsv;;
  hC_sval HCF {| hdata:=Sop op lsv unknown_hid; hcodes :=sv |}.

Lemma hSop_fSop_correct op lsv:
  WHEN hSop op lsv ~> sv THEN forall ctx,
  sval_equiv ctx sv (fSop op lsv).
Proof.
  wlp_simplify.
Qed.
Global Opaque hSop.
Hint Resolve hSop_fSop_correct: wlp_raw.

Lemma hSop_correct op lsv1:
  WHEN hSop op lsv1 ~> sv THEN forall ctx lsv2
  (ELSVEQ: list_sval_equiv ctx lsv1 lsv2),
  sval_equiv ctx sv (Sop op lsv2 unknown_hid).
Proof.
  wlp_xsimplify ltac:(intuition eauto with wlp wlp_raw).
  rewrite <- ELSVEQ. erewrite H; eauto.
Qed.
Hint Resolve hSop_correct: wlp.

Definition hSfoldr (op: operation) (lsv: list_sval) (sv0: sval): ?? sval :=
  DO sv <~ hSfoldr_hcodes op lsv sv0;;
  hC_sval HCF {| hdata:=Sfoldr op lsv sv0 unknown_hid; hcodes := sv |}.

Lemma hSfoldr_correct op lsv1 sv0_1:
  WHEN hSfoldr op lsv1 sv0_1 ~> sv THEN forall ctx lsv2 sv0_2
  (ELSVEQ: list_sval_equiv ctx lsv1 lsv2)
  (ESVEQ: sval_equiv ctx sv0_1 sv0_2),
  sval_equiv ctx sv (Sfoldr op lsv2 sv0_2 unknown_hid).
Proof.
  wlp_simplify.
  rewrite <- ELSVEQ, <- ESVEQ.
  auto.
Qed.
Global Opaque hSfoldr.
Hint Resolve hSfoldr_correct: wlp.
  
Definition hSload (sm: smem) (trap: trapping_mode) (chunk: memory_chunk)
  (addr: addressing) (lsv: list_sval): ?? sval :=
  DO sv <~ hSload_hcodes sm trap chunk addr lsv;;
  hC_sval HCF {| hdata := Sload sm trap chunk addr lsv unknown_hid; hcodes := sv |}.

Lemma hSload_correct sm1 trap chunk addr lsv1:
  WHEN hSload sm1 trap chunk addr lsv1 ~> sv THEN forall ctx lsv2 sm2
  (ELSVEQ: list_sval_equiv ctx lsv1 lsv2)
  (EMEQ: smem_equiv ctx sm1 sm2),
  sval_equiv ctx sv (Sload sm2 trap chunk addr lsv2 unknown_hid).
Proof.
  wlp_simplify.
  rewrite <- ELSVEQ, <- EMEQ.
  auto.
Qed.
Global Opaque hSload.
Hint Resolve hSload_correct: wlp.

Definition hSstore (sm: smem) (chunk: memory_chunk)
  (addr: addressing) (lsv: list_sval) (srce: sval): ?? smem :=
  DO sv <~ hSstore_hcodes sm chunk addr lsv srce;;
  hC_smem HCF {| hdata := Sstore sm chunk addr lsv srce unknown_hid; hcodes := sv |}.

Lemma hSstore_correct sm1 chunk addr lsv1 sv1:
  WHEN hSstore sm1 chunk addr lsv1 sv1 ~> sm1' THEN forall ctx lsv2 sm2 sv2
  (ELSVEQ: list_sval_equiv ctx lsv1 lsv2)
  (EMEQ: smem_equiv ctx sm1 sm2)
  (ESVEQ: sval_equiv ctx sv1 sv2),
  smem_equiv ctx sm1' (Sstore sm2 chunk addr lsv2 sv2 unknown_hid).
Proof.
  wlp_simplify.
  rewrite <- ELSVEQ, <- EMEQ, <- ESVEQ.
  auto.
Qed.
Global Opaque hSstore.
Hint Resolve hSstore_correct: wlp.

(* Convert a "fake" hash-consed term into a "real" hash-consed term *)

Fixpoint fsval_proj sv: ?? sval :=
  match sv with
  | Sinput r hc => 
      DO b <~ phys_eq hc unknown_hid;;
      if b then hSinput r (* was not yet really hash-consed *)
      else RET sv (* already hash-consed *)
  | Sop op hl hc => 
      DO b <~ phys_eq hc unknown_hid;;
      if b then (* was not yet really hash-consed *) 
        DO hl' <~ fsval_list_proj hl;;
        hSop op hl'
      else RET sv (* already hash-consed *)
  | Sfoldr op hl hv hc =>
      DO b <~ phys_eq hc unknown_hid;;
      if b then (* was not yet really hash-consed *)
        DO hl' <~ fsval_list_proj hl;;
        DO hv' <~ fsval_proj hv;;
        hSfoldr op hl' hv'
      else RET sv (* already hash-consed *)
  | Sload hm t chk addr hl _ => RET sv (* TODO *)
  end
with fsval_list_proj sl: ?? list_sval :=
  match sl with
  | Snil hc => 
      DO b <~ phys_eq hc unknown_hid;;
      if b then hSnil() (* was not yet really hash-consed *)
      else RET sl (* already hash-consed *)
  | Scons sv hl hc => 
      DO b <~ phys_eq hc unknown_hid;;
      if b then (* was not yet really hash-consed *)
        DO sv' <~ fsval_proj sv;;
        DO hl' <~ fsval_list_proj hl;;
        hScons sv' hl' 
      else RET sl (* already hash-consed *)
  end.

Lemma fsval_proj_correct sv:
  WHEN fsval_proj sv ~> sv' THEN forall ctx,
  sval_equiv ctx sv sv'.
Proof.
 induction sv using sval_mut 
 with (P0 := fun lsv => 
       WHEN fsval_list_proj lsv ~> lsv' THEN forall ctx,
         eval_list_sval ctx lsv = eval_list_sval ctx lsv')
       (P1 := fun sm => True); try (wlp_simplify; tauto).
  - (* Sop *)
    wlp_xsimplify ltac:(intuition eauto with wlp_raw wlp).
    rewrite H, H0; auto.
  - (* Sfoldr *)
    wlp_xsimplify ltac:(intuition eauto with wlp_raw wlp).
    rewrite H, H0; auto.
    erewrite H1; eauto.
  - (* Scons *)
    wlp_simplify; erewrite H0, H1; eauto.
Qed.
Global Opaque fsval_proj.
Hint Resolve fsval_proj_correct: wlp.

Lemma fsval_list_proj_correct lsv:
  WHEN fsval_list_proj lsv ~> lsv' THEN forall ctx,
  list_sval_equiv ctx lsv lsv'.
Proof.
  induction lsv; wlp_simplify.
  erewrite H0, H1; eauto.
Qed.
Global Opaque fsval_list_proj.
Hint Resolve fsval_list_proj_correct: wlp.

(** ** Access of registers *)

(** rem: this is a defensive check to assert
    that every value is already hash-consed. *)
(*
Definition is_hash_consed sv: ?? bool :=
  match sv with
  | Sinput _ hc
  | Sop _ _ hc
  | Sload _ _ _ _ _ hc =>
      DO b <~ phys_eq hc unknown_hid;;
      RET (negb b)
  end.
 *)

Definition hrs_sreg_get (hrs: ristate) r: ?? sval :=
   match hrs.(ris_sreg)!r with
   | None => if ris_input_init hrs then hSinput r
             else FAILWITH "hrs_sreg_get: dead variable"
   | Some sv =>
       (* DO is_hc <~ is_hash_consed sv;;*) RET sv
   end.

Lemma hrs_sreg_get_correct hrs r:
  WHEN hrs_sreg_get hrs r ~> hsv THEN forall ctx (sreg: reg -> option sval)
  (ESVEQ: forall r, osval_equiv ctx (hrs r) (sreg r)),
  osval_equiv ctx (Some hsv) (sreg r).
Proof.
  unfold ris_sreg_get; wlp_simplify; specialize ESVEQ with r;
  rewrite H in ESVEQ; try_simplify_someHyps;
  rewrite <- H1 in ESVEQ; rewrite <- ESVEQ; simpl; auto.
Qed.

Lemma hrs_sreg_get_nofail hrs r:
  WHEN hrs_sreg_get hrs r ~> hsv THEN forall ctx sis
  (ROK: ris_ok ctx hrs)
  (RR: ris_refines ctx hrs sis),
  sis r = None -> False.
Proof.
  wlp_simplify; inv RR;
  rewrite <- ALIVE_EQ in *; auto;
  unfold ris_sreg_get in *.
  - rewrite H in H0; congruence.
  - rewrite H, H0 in H2; congruence.
Qed.
Global Opaque hrs_sreg_get.
Hint Resolve hrs_sreg_get_correct: wlp.

(** ** Hash-consed operations on [ireg] *)

Definition hrs_ir_get hrs hrs_old ir: ?? sval :=
  if force_input ir then hrs_sreg_get hrs_old (regof ir) else
  hrs_sreg_get hrs (regof ir).

Lemma hrs_ir_get_subst ctx hrs hrs_old sis (si: fpasv) ir:
  WHEN hrs_ir_get hrs hrs_old ir ~> s THEN forall
  (ROK_OLD: ris_ok ctx hrs_old)
  (RR_OLD: ris_refines ctx hrs_old sis)
  (REG_SUBST: reg_subst hrs sis si ctx),
  sval_equiv ctx s (sv_subst (sis_smem sis) sis (ir_subst (ext si) ir)).
Proof.
  wlp_simplify; inversion RR_OLD; unfold ir_subst; rewrite H.
  - erewrite H0; eauto. rewrite REG_EQ; auto; simpl.
    autodestruct; try (rewrite <- ALIVE_EQ; auto; fail).
    wlp_exploit hrs_sreg_get_nofail.
  - erewrite H0; eauto.
Qed.

Fixpoint hrs_lir_get hrs hrs_old (lir: list ireg): ?? ((list sval) * list_sval) :=
  match lir with
  | nil => DO hlsv <~ hSnil();;
      RET (nil, hlsv)
  | ir::lir =>
      DO lsv_hlsv <~ hrs_lir_get hrs hrs_old lir;;
      DO sv <~ hrs_ir_get hrs hrs_old ir;;
      let (lsv, hlsv) := lsv_hlsv in
      DO hlsv <~ hScons sv hlsv;;
      RET ((sv :: lsv), hlsv)
  end.

Lemma hrs_lir_get_lmap_nofail hrs hrs_old lir
  :WHEN hrs_lir_get hrs hrs_old lir ~> _ THEN forall ctx sis si
  (ROK_OLD: ris_ok ctx hrs_old)
  (RR_OLD: ris_refines ctx hrs_old sis)
  (LMAP: lmap_sv (ir_subst_si sis si) lir  = None), False.
Proof.
  induction lir; wlp_simplify; try (inv LMAP; fail);
  revert LMAP; unfold ir_subst_si;
  repeat autodestruct; intros; eauto.
  exploit hrs_sreg_get_nofail; eauto.
Qed.

Lemma hrs_lir_get_exists_rop ctx lir hrs hrs_old (rop: root_op) sis si:
  WHEN hrs_lir_get hrs hrs_old lir ~> _ THEN forall
  (ROK_OLD: ris_ok ctx hrs_old)
  (RR_OLD: ris_refines ctx hrs_old sis),
  exists sv, rop lir sis si = Some sv.
Proof.
  wlp_simplify.
  destruct (rop lir sis si) eqn:RSV.
  - eexists; eauto.
  - revert RSV; unfold root_sapply.
    autodestruct; [ autodestruct |].
    wlp_exploit hrs_lir_get_lmap_nofail.
Qed.

Lemma hrs_lir_get_correct ctx (hrs hrs_old: ristate) lir (sis: sistate)
  :WHEN hrs_lir_get hrs hrs_old lir ~> lsv_hlsv THEN forall (si: fpasv) lsv'
  (ESVEQ: forall r, osval_equiv ctx (hrs_old r) (sis r))
  (LMAP: lmap_sv (ir_subst_si sis si) lir = Some lsv')
  (REG_SUBST: reg_subst hrs sis si ctx),
  list_sval_equiv ctx (snd lsv_hlsv) lsv'.
Proof.
  induction lir; wlp_simplify; try (inv LMAP; auto; fail);
  unfold ir_subst_si in *; revert LMAP; rewrite H0;
  repeat autodestruct; intros; inv LMAP; simpl;
  erewrite H2; eauto; erewrite H1; eauto.
  rewrite EQ0; simpl; reflexivity.
Qed.

Lemma hrs_lir_get_input_correct ctx (hrs hrs_old: ristate) lr (sis: sistate)
  :WHEN hrs_lir_get hrs hrs_old (ir_input_of lr) ~> lsv_hlsv THEN forall si lsv'
  (ESVEQ: forall r, osval_equiv ctx (hrs_old r) (sis r))
  (LMAP: lmap_sv (ir_subst_si sis si) (ir_input_of lr) = Some lsv'),
  list_sval_equiv ctx (snd lsv_hlsv) lsv'.
Proof.
  induction lr; wlp_simplify; try (inv LMAP; auto; fail); try (inv H0; fail).
  generalize LMAP; clear LMAP.
  repeat autodestruct; intros; inv LMAP; simpl.
  erewrite H2; eauto. erewrite H1; eauto.
  unfold ir_subst_si in EQ0; inv EQ0.
  fold (eval_osv ctx (Some s)); rewrite <- H4; eauto.
Qed.

Lemma hrs_lir_get_input_lmap_id_correct (hrs hrs_old: ristate) lr
  :WHEN hrs_lir_get hrs hrs_old (ir_input_of lr) ~> lsv_hlsv THEN
  forall ctx (sis: sistate) si lsv lsv'
  (ESVEQ: forall r, osval_equiv ctx (hrs_old r) (sis r))
  (ELSV: eval_list_sval ctx lsv = Some lsv')
  (LMAP: lmap_sv (ir_subst_si sis si) (ir_input_of lr) = Some lsv),
  exists l,
    lmap_sv (fun sv => Some sv) (fst lsv_hlsv) = Some l
    /\ list_sval_equiv ctx l lsv.
Proof.
  induction lr; wlp_simplify;
  revert LMAP; do 2 autodestruct; intros; inv LMAP.
  revert ELSV; simpl; do 2 autodestruct; intros; inv ELSV.
  exploit H; eauto. intros (l' & LMAP & ELSVEQ).
  rewrite LMAP. eexists; split; eauto; simpl.
  erewrite H1; eauto. revert EQ0; unfold ir_subst_si; simpl; intros.
  rewrite EQ0; simpl. rewrite EQ2, ELSVEQ, EQ1; reflexivity.
Qed.

Lemma hrs_lir_get_lmap_id_correct (hrs hrs_old: ristate) lir
  :WHEN hrs_lir_get hrs hrs_old lir ~> lsv_hlsv THEN
  forall ctx (sis: sistate) si lsv lsv'
  (ESVEQ: forall r, osval_equiv ctx (hrs_old r) (sis r))
  (ELSV: eval_list_sval ctx lsv = Some lsv')
  (LMAP: lmap_sv (ir_subst_si sis si) lir = Some lsv)
  (REG_SUBST: reg_subst hrs sis si ctx),
  exists l,
    lmap_sv (fun sv => Some sv) (fst lsv_hlsv) = Some l
    /\ list_sval_equiv ctx l lsv.
Proof.
  induction lir; wlp_simplify;
  revert LMAP; do 2 autodestruct; intros; inv LMAP;
  revert ELSV; simpl; do 2 autodestruct; intros; inv ELSV;
  exploit H; eauto; intros (l' & LMAP & ELSVEQ);
  rewrite LMAP; eexists; split; eauto; simpl;
  erewrite H1; eauto; revert EQ0; unfold ir_subst_si; simpl; intros;
  rewrite H0 in EQ0; simpl in EQ0.
  - rewrite EQ0; simpl; rewrite EQ2, ELSVEQ, EQ1; reflexivity.
  - inv EQ0; rewrite REG_SUBST, EQ2, ELSVEQ, EQ1; reflexivity.
Qed.

Lemma hrs_lir_get_eval_nofail lir: forall hrs hrs_old,
  WHEN hrs_lir_get hrs hrs_old lir ~> lsv_hlsv THEN forall ctx lsv' sis si
  (SOK: sis_ok ctx sis)
  (ROK_OLD: ris_ok ctx hrs_old)
  (RR_OLD: ris_refines ctx hrs_old sis)
  (LMAP: lmap_sv (ir_subst_si sis si) lir  = Some lsv')
  (OK_SUBST : si_ok_subst sis si ctx)
  (REG_SUBST: reg_subst hrs sis si ctx),
  eval_list_sval ctx (snd lsv_hlsv) <> None.
Proof.
  induction lir; wlp_simplify;
  [ inv LMAP; rewrite H in H0; congruence | idtac | idtac ].
  all:
    revert LMAP; unfold ir_subst_si;
    repeat autodestruct; intros; eauto;
    inv LMAP; erewrite H2 in H3; eauto; erewrite H1 in H3; eauto.
  - erewrite REG_EQ in H3; eauto. rewrite EQ0 in H3.
    eapply OK_SREG in EQ0 as NF; eauto.
    revert H3; simpl; repeat autodestruct.
    intros; exploit H; eauto.
  - erewrite REG_SUBST in H3; eauto. revert H3; autodestruct.
    + autodestruct; intros; exploit H; eauto.
    + unfold ext; destruct (si (regof a)) eqn:EQSI.
      * destruct s; simpl.
        2,3,4:
          apply si_wf in EQSI; [| intros ISINPUT; inv ISINPUT ];
          intros CONTRA _; apply OK_SUBST in EQSI; auto.
        autodestruct; simpl; try congruence; intros.
        eapply OK_SREG in EQ1; eauto.
      * simpl; autodestruct; simpl; try congruence; intros.
        eapply OK_SREG in EQ1; eauto.
Qed.

Lemma hrs_lir_get_subst args (hrs hrs_old: ristate)
  :WHEN hrs_lir_get hrs hrs_old args ~> lsv_hlsv THEN forall ctx sis (si: fpasv)
  (ROK_OLD: ris_ok ctx hrs_old)
  (RR_OLD: ris_refines ctx hrs_old sis)
  (REG_SUBST: reg_subst hrs sis si ctx),
  list_sval_equiv ctx (snd lsv_hlsv) (lsv_subst (sis_smem sis) sis (lsvof (ext si) args)).
Proof.
  induction args; wlp_simplify; unfold ir_subst; intros; rewrite H0;
  erewrite H2; eauto; erewrite H1; eauto.
  inversion RR_OLD; simpl; autodestruct; intros.
  - fold (eval_osv ctx (Some s)); rewrite <- EQ; eauto.
  - wlp_exploit hrs_sreg_get_nofail.
Qed.
Global Opaque hrs_ir_get hrs_lir_get.

(** ** Assignment of registers *)

Fixpoint lsv_in_imp (sv: sval) lsv :=
  match lsv with
  | nil => RET false
  | sv'::lsv =>
      DO b <~ phys_eq sv sv';;
      if b then RET true
      else lsv_in_imp sv lsv
  end.

Lemma lsv_in_imp_correct lsv: forall sv,
  WHEN lsv_in_imp sv lsv ~> b THEN b = true ->
  In sv lsv.
Proof.
  induction lsv; wlp_simplify; congruence.
Qed.
Local Hint Resolve lsv_in_imp_correct: wlp.

Definition root_happly (rsv: root_op) (lsv: list_sval) (sm: smem): ?? sval :=
  match rsv with
  | Rop op => hSop op lsv
  | Rload trap chunk addr => hSload sm trap chunk addr lsv
  end.

Lemma root_happly_correct (rsv: root_op) lir hrs hrs_old:
  WHEN hrs_lir_get hrs hrs_old lir ~> lsv_hlsv THEN
  WHEN root_happly rsv (snd lsv_hlsv) (ris_smem hrs) ~> sv THEN forall ctx sis si sv'
  (ROK_OLD: ris_ok ctx hrs_old)
  (RR_OLD: ris_refines ctx hrs_old sis)
  (MEM_EQ: (ris_smem hrs) = (ris_smem hrs_old))
  (RSV: rsv lir sis si = Some sv')
  (REG_SUBST: reg_subst hrs sis si ctx),
  sval_equiv ctx sv sv'.
Proof.
  unfold root_sapply, root_happly; destruct rsv; wlp_simplify;
  generalize RSV; clear RSV; autodestruct; intros; inv RSV; inversion RR_OLD.
  all:
    simpl; erewrite H; try rewrite MEM_EQ; eauto with wlp;
    eapply hrs_lir_get_correct; eauto.
Qed.

Lemma root_happly_input_correct (rsv: root_op) lr hrs hrs_old:
  WHEN hrs_lir_get hrs hrs_old (ir_input_of lr) ~> lsv_hlsv THEN
  WHEN root_happly rsv (snd lsv_hlsv) (ris_smem hrs) ~> sv THEN forall ctx sis si sv'
  (ROK_OLD: ris_ok ctx hrs_old)
  (RR_OLD: ris_refines ctx hrs_old sis)
  (MEM_EQ: (ris_smem hrs) = (ris_smem hrs_old))
  (RSV: rsv (ir_input_of lr) sis si = Some sv'),
  sval_equiv ctx sv sv'.
Proof.
  unfold root_sapply, root_happly; destruct rsv; wlp_simplify;
  generalize RSV; clear RSV; autodestruct; intros; inv RSV; inversion RR_OLD.
  all:
    simpl; erewrite H; try rewrite MEM_EQ; eauto with wlp;
    eapply hrs_lir_get_input_correct; eauto.
Qed.
Global Opaque root_happly.

Definition fst_lhsv_imp lhsv :=
  match lhsv with
  | Scons sv (Snil _) _ => RET sv
  | _ => FAILWITH "fst_lhsv_imp: move args bug - should never occur"
  end.

Lemma fst_lhsv_imp_correct (lhsv: list_sval): forall ctx l v,
  WHEN fst_lhsv_imp lhsv ~> sv THEN forall
  (LSEVAL: eval_list_sval ctx lhsv = eval_list_sval ctx l)
  (HEVAL: eval_list_sval ctx l = Some [v]),
  eval_list_sval ctx lhsv = Some [v]
  /\ eval_sval ctx sv = Some v.
Proof.
  induction lhsv.
  - wlp_simplify; rewrite LSEVAL; auto.
  - destruct lhsv; wlp_simplify;
    revert LSEVAL; repeat autodestruct; intros.
Qed.
Global Opaque fst_lhsv_imp.

(** Simplify a symbolic value before assignment to a register *)

Definition simplify (rsv: root_op) (lsv: list sval) (lhsv: list_sval) (sm: smem): ?? sval :=
  match rsv with
  | Rop op =>
      match is_move_operation op lsv with
      | Some arg => fst_lhsv_imp lhsv
      | None =>
        match rewrite_ops RRULES op lsv with
        | Some fsv => fsval_proj fsv
        | None => hSop op lhsv
        end
      end
  | Rload _ chunk addr => 
       hSload sm NOTRAP chunk addr lhsv
  end.

Lemma simplify_input_correct ctx (rsv: root_op) lr hrs hrs_old sis si sv
  (ROK_OLD: ris_ok ctx hrs_old)
  (RR_OLD: ris_refines ctx hrs_old sis)
  (RSV: rsv (ir_input_of lr) sis si = Some sv)
  :WHEN hrs_lir_get hrs hrs_old (ir_input_of lr) ~> lsv_hlsv THEN
  WHEN simplify rsv (fst lsv_hlsv) (snd lsv_hlsv) (ris_smem hrs) ~> sv' THEN forall
  (MEM_EQ: (ris_smem hrs) = (ris_smem hrs_old))
  (SEVAL: eval_sval ctx sv <> None),
  sval_equiv ctx sv sv'.
Proof.
  inversion RR_OLD; remember RSV as HRSV; clear HeqHRSV; revert HRSV;
  unfold root_sapply; autodestruct; intros.
  destruct rsv; inv HRSV.
  - (* Rop *)
    unfold simplify. wlp_step ltac:(eauto with wlp);
    destruct (is_move_operation _ _) eqn:Hmove.
    { wlp_simplify; exploit is_move_operation_correct; eauto.
      intros (Hop & Hlsv); subst; simpl in *.
      revert SEVAL; repeat autodestruct. intros _ _ LSEVAL _.
      exploit hrs_lir_get_input_correct; eauto; intros ELSVEQ.
      exploit fst_lhsv_imp_correct; eauto; intros (HLSEVAL & SEVAL).
      rewrite SEVAL; reflexivity. }
    destruct (rewrite_ops _ _ _) eqn:Htarget_op_simp; wlp_simplify.
    { autodestruct; rewrite <- H; intros.
      exploit hrs_lir_get_input_lmap_id_correct; eauto.
      intros (l' & LMAP_ID & ELSVEQ).
      exploit rewrite_ops_correct; eauto. }
    erewrite H; eauto; simpl.
    exploit hrs_lir_get_input_correct; eauto.
  - (* Rload *)
    destruct trap; wlp_simplify; intros.
    + revert SEVAL; simpl. repeat autodestruct.
      erewrite H; eauto. intros; erewrite hrs_lir_get_input_correct; eauto.
      simplify_option.
    + erewrite H; eauto.
      * erewrite hrs_lir_get_input_correct; eauto.
      * rewrite MEM_EQ; auto.
Qed.

Lemma simplify_correct ctx (rsv: root_op) lir hrs hrs_old sis si sv
  (ROK_OLD: ris_ok ctx hrs_old)
  (RR_OLD: ris_refines ctx hrs_old sis)
  (RSV: rsv lir sis si = Some sv)
  :WHEN hrs_lir_get hrs hrs_old lir ~> lsv_hlsv THEN
  WHEN simplify rsv (fst lsv_hlsv) (snd lsv_hlsv) (ris_smem hrs) ~> sv' THEN forall
  (MEM_EQ: (ris_smem hrs) = (ris_smem hrs_old))
  (SEVAL: eval_sval ctx sv <> None)
  (REG_SUBST: reg_subst hrs sis si ctx),
  sval_equiv ctx sv sv'.
Proof.
  inversion RR_OLD; remember RSV as HRSV; clear HeqHRSV; revert HRSV;
  unfold root_sapply; autodestruct; intros.
  destruct rsv; inv HRSV.
  - (* Rop *)
    unfold simplify. wlp_step ltac:(eauto with wlp);
    destruct (is_move_operation _ _) eqn:Hmove.
    { wlp_simplify; exploit is_move_operation_correct; eauto.
      intros (Hop & Hlsv); subst; simpl in *.
      revert SEVAL; repeat autodestruct. intros _ _ LSEVAL _.
      exploit hrs_lir_get_correct; eauto; intros ELSVEQ.
      exploit fst_lhsv_imp_correct; eauto; intros (HLSEVAL & SEVAL).
      rewrite SEVAL; reflexivity. }
    destruct (rewrite_ops _ _ _) eqn:Htarget_op_simp; wlp_simplify.
    { autodestruct; rewrite <- H; intros.
      exploit hrs_lir_get_lmap_id_correct; eauto.
      intros (l' & LMAP_ID & ELSVEQ).
      exploit rewrite_ops_correct; eauto. }
    erewrite H; eauto; simpl.
    exploit hrs_lir_get_correct; eauto.
  - (* Rload *)
    destruct trap; wlp_simplify; intros.
    + revert SEVAL; simpl. repeat autodestruct.
      erewrite H; eauto. intros; erewrite hrs_lir_get_correct; eauto.
      simplify_option.
    + erewrite H; eauto.
      * erewrite hrs_lir_get_correct; eauto.
      * rewrite MEM_EQ; auto.
Qed.
Local Hint Resolve simplify_input_correct: wlp.
Global Opaque simplify.

Lemma hrs_lir_get_reg_subst_rop ctx (si: fpasv) hrs hrs_old r r0 rop args sis
  :WHEN hrs_lir_get hrs hrs_old args ~> lsv_hlsv THEN
  WHEN simplify rop (fst lsv_hlsv) (snd lsv_hlsv) hrs ~> sv THEN forall
  (ROK: ris_ok ctx hrs_old)
  (RR: ris_refines ctx hrs_old sis)
  (MEM_EQ: ris_smem hrs = ris_smem hrs_old)
  (SMEM: sis_smem sis = fSinit)
  (OK_SUBST: si_ok_subst sis (si_set r0 (rop_subst (ext si) rop args) si) ctx)
  (REG_SUBST: reg_subst hrs sis si ctx),
  eval_osv ctx
    match (red_PTree_set r0 sv hrs) ! r with
    | Some sv => Some sv
    | None => ASSERT ris_input_init hrs IN Some (fSinput r)
    end = eval_sval ctx
      (sv_subst (sis_smem sis) sis (ext (si_set r0 (rop_subst (ext si) rop args) si) r)).
Proof.
  wlp_simplify.
  exploit (hrs_lir_get_exists_rop ctx args hrs hrs_old rop sis si); eauto; intros (sv & RSV).
  exploit root_sapply_rop_subst; eauto; intros ESVEQ.
  destruct (lmap_sv (ir_subst_si sis si) args) eqn:LMAP;
  [| wlp_exploit hrs_lir_get_lmap_nofail ].
  eapply red_PTree_set_rop_sv_subst with (sv:=sv) (lsv:=l); eauto.
  - eapply simplify_correct; eauto.
    rewrite ESVEQ; eapply OK_SUBST; simpl; eauto.
  - symmetry; apply lmap_sv_lsv_subst; auto.
Qed.

Lemma rsv_set_ok_equiv_trap_cons hrs lr rsv:
  WHEN hrs_lir_get hrs hrs (ir_input_of lr) ~> lsv_hlsv THEN
  WHEN root_happly rsv (snd lsv_hlsv) hrs ~> sv_ra THEN
  WHEN simplify rsv (fst lsv_hlsv) (snd lsv_hlsv) hrs ~> sv_simp THEN forall ctx sis r sv_sis
  (RSV: rsv (ir_input_of lr) sis si_empty = Some sv_sis)
  (RR: ris_refines ctx hrs sis),
  sis_ok ctx (set_sreg r sv_sis sis) <->
  ris_ok ctx (ris_sreg_ok_set hrs (red_PTree_set r sv_simp hrs) (sv_ra :: ok_rsval hrs)).
Proof.
  wlp_xsimplify ltac:(eauto with wlp); intros; inversion RR.
  rewrite ok_rsv_set_sreg, OK_EQUIV; eauto; split.
  - intros (ROK & SEVAL); inv ROK.
    assert (ROK: ris_ok ctx hrs) by (econstructor; eauto).
    econstructor; eauto; simpl.
    intuition (subst; eauto). remember RSV as RSV'; clear HeqRSV'.
    revert RSV; unfold root_sapply; autodestruct; intros; eauto.
    exploit hrs_lir_get_input_correct; eauto.
    intros EVAL_LIR.
    erewrite <- root_happly_input_correct with (sv':=sv_sis) in SEVAL; eauto.
  - intros (OK_RMEM & OK_RREG); simpl in *.
    assert (ROK: ris_ok ctx hrs) by (econstructor; eauto).
    erewrite <- root_happly_input_correct with (sv':=sv_sis); eauto.
Qed.

Lemma rsv_set_ok_equiv_trap_idok hrs lr rsv:
  WHEN hrs_lir_get hrs hrs (ir_input_of lr) ~> lsv_hlsv THEN
  WHEN root_happly rsv (snd lsv_hlsv) hrs ~> sv_ra THEN
  WHEN simplify rsv (fst lsv_hlsv) (snd lsv_hlsv) hrs ~> sv_simp THEN forall ctx sis r sv_sis
  (RSV: rsv (ir_input_of lr) sis si_empty = Some sv_sis)
  (RR: ris_refines ctx hrs sis)
  (HIN: In sv_ra (ok_rsval hrs)),
  sis_ok ctx (set_sreg r sv_sis sis) <->
  ris_ok ctx (ris_sreg_ok_set hrs (red_PTree_set r sv_simp hrs) (ok_rsval hrs)).
Proof.
  wlp_xsimplify ltac:(eauto with wlp); intros; inversion RR.
  rewrite ok_rsv_set_sreg, OK_EQUIV; eauto; split.
  - intros (ROK & SEVAL); inv ROK.
    assert (ROK: ris_ok ctx hrs) by (econstructor; eauto).
    econstructor; eauto; simpl.
  - intros (OK_RMEM & OK_RREG); simpl in *.
    assert (ROK: ris_ok ctx hrs) by (econstructor; eauto).
    erewrite <- root_happly_input_correct with (sv':=sv_sis); eauto.
Qed.

Lemma rsv_set_ok_equiv_notrap hrs lr rsv:
  WHEN hrs_lir_get hrs hrs (ir_input_of lr) ~> lsv_hlsv THEN
  WHEN simplify rsv (fst lsv_hlsv) (snd lsv_hlsv) hrs ~> sv_simp THEN forall ctx sis r sv_sis
  (RSV: rsv (ir_input_of lr) sis si_empty = Some sv_sis)
  (RR: ris_refines ctx hrs sis)
  (MAYT: may_trap rsv (ir_input_of lr) = false),
  sis_ok ctx (set_sreg r sv_sis sis) <->
  ris_ok ctx (ris_sreg_ok_set hrs (red_PTree_set r sv_simp hrs) (ok_rsval hrs)).
Proof.
  wlp_xsimplify ltac:(eauto with wlp); intros; inversion RR.
  rewrite ok_rsv_set_sreg, OK_EQUIV; eauto; split.
  - intros (ROK & SEVAL); inv ROK.
    econstructor; eauto.
  - intros (OK_RMEM & OK_RREG).
    assert (ROK: ris_ok ctx hrs) by (econstructor; eauto).
    split; auto.
    assert (HLM: exists lsv, lmap_sv (ir_subst_si sis si_empty) (ir_input_of lr) = Some lsv).
    { destruct lmap_sv eqn:EQLM; [ eexists; eauto |].
      wlp_exploit hrs_lir_get_lmap_nofail. }
    destruct HLM as (lsv & HLM).
    intros SNONE; exploit may_trap_correct; eauto.
    + intros LNONE; eapply eval_lmap_sv_input_nofail; eauto.
      intuition.
    + rewrite <- MEM_EQ; auto.
Qed.

Lemma rsv_set_reg_eq ctx sis rsv lr (hrs: ristate) sv_sis r:
  WHEN hrs_lir_get hrs hrs (ir_input_of lr) ~> lsv_hlsv THEN
  WHEN simplify rsv (fst lsv_hlsv) (snd lsv_hlsv) hrs ~> sv_ref THEN
  rsv (ir_input_of lr) sis si_empty = Some sv_sis ->
  ris_refines ctx hrs sis ->
  sis_ok ctx sis /\ eval_sval ctx sv_sis <> None ->
  forall r0 rsvals,
  eval_osv ctx (ris_sreg_ok_set hrs (red_PTree_set r sv_ref hrs) rsvals r0) =
  eval_osv ctx (set_sreg r sv_sis sis r0).
Proof.
  do 2 (intros; subst; wlp_step ltac:(eauto with wlp)).
  intros RSV RR (SOK & SEVAL) r0 rsvals; inversion RR.
  apply OK_EQUIV in SOK as ROK.
  rewrite ris_sreg_set_access.
  erewrite red_PTree_set_refines_sval at 1; eauto.
  unfold set_sreg; simpl; autodestruct; simpl.
  erewrite <- simplify_input_correct; eauto.
Qed.

Definition hrs_rsv_set_trap r (lsv: list sval) (hlsv: list_sval)
  rsv (hrs: ristate): ?? ristate :=
  DO sv <~ root_happly rsv hlsv (ris_smem hrs);;
  DO ok_lsv <~
    (if (negb hrs.(si_mode)) then (
       XDEBUG sv (fun sv => DO sv_name <~ string_of_hashcode (sval_get_hid sv);;
                  RET ("-- insert undef behavior of hashcode:" +; (CamlStr sv_name))%string);;
       RET (sv::(ok_rsval hrs)))
     else (
       DO bin <~ lsv_in_imp sv (ok_rsval hrs);;
       if bin then RET (ok_rsval hrs)
       else FAILWITH "hrs_rsv_set: adding potential failure"));;
  DO simp <~ simplify rsv lsv hlsv (ris_smem hrs);;
  RET (ris_sreg_ok_set hrs (red_PTree_set r simp hrs) ok_lsv).

Definition hrs_rsv_set_notrap r (lsv: list sval) (hlsv: list_sval)
  rsv (hrs: ristate): ?? ristate :=
  DO simp <~ simplify rsv lsv hlsv (ris_smem hrs);;
  RET (ris_sreg_ok_set hrs (red_PTree_set r simp hrs) (ok_rsval hrs)).

Definition hrs_rsv_set r (lir: list ireg) rsv (hrs hrs_old: ristate): ?? ristate :=
  DO lsv_hlsv <~ hrs_lir_get hrs hrs_old lir;;
  let (lsv, hlsv) := lsv_hlsv in
  let mayt := may_trap rsv lir in
  if mayt then hrs_rsv_set_trap r lsv hlsv rsv hrs
  else hrs_rsv_set_notrap r lsv hlsv rsv hrs.

Definition hrs_rsv_set_input r (args: list reg) rsv hrs :=
  hrs_rsv_set r (ir_input_of args) rsv hrs hrs.

Lemma hrs_rsv_set_preserv_rinit r lir rsv hrs hrs_old b:
  WHEN hrs_rsv_set r lir rsv hrs hrs_old ~> hrs' THEN
  ris_input_init hrs = b -> ris_input_init hrs' = b.
Proof.
  wlp_simplify.
Qed.

Lemma hrs_rsv_set_input_lmap_nofail dest args rop hrs:
  WHEN hrs_rsv_set_input dest args rop hrs ~> hsv THEN forall ctx sis
  (ROK: ris_ok ctx hrs)
  (RR: ris_refines ctx hrs sis)
  (LMAP: lmap_sv (ir_subst_si sis si_empty) (ir_input_of args) = None), False.
Proof.
  wlp_simplify; wlp_exploit hrs_lir_get_lmap_nofail.
Qed.

Lemma hrs_rsv_set_input_correct r lr rsv hrs:
  WHEN hrs_rsv_set_input r lr rsv hrs ~> hrs' THEN forall ctx sis sv
  (RR: ris_refines ctx hrs sis)
  (RSV: rsv (ir_input_of lr) sis si_empty = Some sv),
  ris_refines ctx hrs' (set_sreg r sv sis).
Proof.
  wlp_simplify; inversion RR;
  [ exploit rsv_set_ok_equiv_trap_cons; eauto; intros X_OK_EQUIV
  | exploit rsv_set_ok_equiv_trap_idok; eauto; intros X_OK_EQUIV
  | exploit rsv_set_ok_equiv_notrap; eauto; intros X_OK_EQUIV ];
  split; auto; rewrite <- X_OK_EQUIV, ok_rsv_set_sreg; eauto.
  1,5,9: reflexivity.
  1,4,7: intuition eauto.
  1,3,5: intros; eapply rsv_set_alive_eq; eauto.
  all: intros; eapply rsv_set_reg_eq; eauto.
Qed.

(** ** [ris_ok] preservation between implementation and refinement *)

Lemma hrs_ok_op_okpreserv ctx op args dest hrs:
  WHEN hrs_rsv_set_input dest args (Rop op) hrs ~> rst THEN
  ris_ok ctx rst -> ris_ok ctx hrs.
Proof.
  wlp_simplify; inv H0;
  try inv H1; simpl in *; constructor; eauto.
Qed.

Lemma hrs_ok_load_okpreserv ctx chunk addr args dest hrs trap:
  WHEN hrs_rsv_set_input dest args (Rload trap chunk addr) hrs ~> rst THEN
  ris_ok ctx rst -> ris_ok ctx hrs.
Proof.
  wlp_simplify; inv H0;
  try inv H1; simpl in *; econstructor; eauto.
Qed.
Global Opaque hrs_rsv_set.

End SymbolicCommon.
