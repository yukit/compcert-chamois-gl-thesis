(* *************************************************************)
(*                                                             *)
(*             The Compcert verified compiler                  *)
(*                                                             *)
(*           Léo Gourdin        VERIMAG, UGA                   *)
(*           Sylvain Boulmé     Grenoble-INP, VERIMAG          *)
(*                                                             *)
(*  Copyright VERIMAG. All rights reserved.                    *)
(*  This file is distributed under the terms of the INRIA      *)
(*  Non-Commercial License Agreement.                          *)
(*                                                             *)
(* *************************************************************)

(* This module implements the symbolic execution of BTL blocks: [sexec]. *)
Require Import Coqlib AST.
Require Import Op Registers.
Require Import BTL.
Require Import BTL_SEsimuref BTL_SEtheory OptionMonad.
Require Import BTL_SEsimplify BTL_SEsimplifyproof BTL_SEimpl_prelude.

Import Notations.
Import HConsing.
Import SvalNotations.

Import ListNotations.
Local Open Scope list_scope.
Local Open Scope lazy_bool_scope.
Local Open Scope option_monad_scope.
Local Open Scope impure.

Local Hint Resolve substitution_rule_si_empty: sempty.

Section SymbolicExecution.

  Context `{HCF : HashConsingFcts}.
  Context `{HC : HashConsingHyps HCF}.
  Context `{RRULES: rrules_set}.

  Hint Resolve hSnil_correct: wlp.
  Hint Resolve hScons_correct: wlp.
  Hint Resolve hSstore_correct: wlp.
  Hint Resolve hrs_sreg_get_correct: wlp.
  Hint Resolve fsval_list_proj_correct: wlp.
  Hint Resolve hrs_rsv_set_input_correct: wlp.

(** ** Hash-consed operations on [list_sval] *)

Fixpoint hlist_sreg_get (hrs: ristate) (l: list reg): ?? list_sval :=
  match l with
  | nil => @hSnil HCF ()
  | r::l =>
    DO v <~ @hrs_sreg_get HCF hrs r;;
    DO lsv <~ hlist_sreg_get hrs l;;
    @hScons HCF v lsv
  end.

Lemma hlist_sreg_get_nofail hrs l:
  WHEN hlist_sreg_get hrs l ~> lsv THEN forall ctx sis
  (ROK: ris_ok ctx hrs)
  (RR: ris_refines ctx hrs sis)
  (LMAP: lmap_sv sis l = None), False.
Proof.
  induction l; wlp_simplify.
  - inv LMAP.
  - generalize LMAP; clear LMAP.
    repeat autodestruct; intros; eauto.
    exploit hrs_sreg_get_nofail; eauto.
Qed.

Lemma hlist_sreg_get_correct hrs l:
  WHEN hlist_sreg_get hrs l ~> lsv THEN forall ctx (sreg: reg -> option sval) lsv'
  (ESVEQ: forall r, osval_equiv ctx (hrs r) (sreg r))
  (LMAP: lmap_sv sreg l = Some lsv'),
  list_sval_equiv ctx lsv lsv'.
Proof.
  unfold hlist_sreg_get; induction l; wlp_simplify.
  - inv LMAP; auto.
  - generalize LMAP; clear LMAP.
    repeat autodestruct; intros. inv LMAP; simpl.
    erewrite H1; eauto. erewrite H, EQ0; eauto.
Qed.
Global Opaque hlist_sreg_get. 
Local Hint Resolve hlist_sreg_get_correct: wlp.

(** ** Normalization and rewritings *)

Definition cbranch_expanse (prev: ristate) (cond: condition)
  (args: list reg): ?? (condition * list_sval) :=
  match rewrite_cbranches RRULES prev cond args with
  | Some (cond', vargs) =>
      DO vargs' <~ @fsval_list_proj HCF vargs;;
      RET (cond', vargs')
  | None =>
      DO vargs <~ hlist_sreg_get prev args ;;
      RET (cond, vargs)
  end.

Lemma cbranch_expanse_lmap_nofail hrs c l:
  WHEN cbranch_expanse hrs c l ~> r THEN forall ctx sis
  (ROK: ris_ok ctx hrs)
  (RR: ris_refines ctx hrs sis),
  lmap_sv sis l = None -> False.
Proof.
  unfold cbranch_expanse.
  destruct (rewrite_cbranches _ _ _ _) eqn: TARGET;
  wlp_simplify; [| wlp_exploit hlist_sreg_get_nofail ].
  destruct p; eapply rewrite_cbranches_nofail; eauto.
Qed.
  
Lemma cbranch_expanse_correct hrs c1 l1:
  WHEN cbranch_expanse hrs c1 l1 ~> r THEN forall ctx sis l3
  (ROK: ris_ok ctx hrs)
  (RR: ris_refines ctx hrs sis)
  (LMAP: lmap_sv sis l1 = Some l3),
  eval_scondition ctx (fst r) (snd r) =
  eval_scondition ctx c1 l3.
Proof.
  unfold cbranch_expanse.
  destruct (rewrite_cbranches _ _ _ _) eqn: TARGET;
  wlp_simplify; unfold eval_scondition; inversion RR.
  - destruct p as [c2 l2]; simpl.
    rewrite <- H; simpl.
    exploit rewrite_cbranches_correct; eauto.
  - erewrite H with (sreg:=sis); eauto.
Qed.
Local Hint Resolve cbranch_expanse_correct: wlp.
Global Opaque cbranch_expanse.

(** ** Assignment of memory *)

Definition hrexec_store chunk addr args src hrs: ?? ristate :=
  DO hargs <~ hlist_sreg_get hrs args;;
  DO hsrc <~ @hrs_sreg_get HCF hrs src;;
  DO hm <~ @hSstore HCF hrs chunk addr hargs hsrc;;
  RET (rset_smem hm hrs).

Lemma hrexec_store_preserv_rinit chunk addr args src hrs b:
  WHEN hrexec_store chunk addr args src hrs ~> hrs' THEN
  ris_input_init hrs = b ->
  ris_input_init hrs' = b.
Proof.
  wlp_simplify.
Qed.

Lemma hrexec_store_nofail chunk addr args src hrs:
  WHEN hrexec_store chunk addr args src hrs ~> hrs' THEN forall ctx sis
  (ROK: ris_ok ctx hrs)
  (RR: ris_refines ctx hrs sis),
  sis src = None -> False.
Proof.
  wlp_simplify. wlp_exploit hrs_sreg_get_nofail.
Qed.

Lemma hrexec_store_lmap_nofail chunk addr args src hrs:
  WHEN hrexec_store chunk addr args src hrs ~> hrs' THEN forall ctx sis
  (ROK: ris_ok ctx hrs)
  (RR: ris_refines ctx hrs sis),
  lmap_sv sis args = None -> False.
Proof.
  wlp_simplify. wlp_exploit hlist_sreg_get_nofail.
Qed.

Lemma hrexec_store_correct chunk addr args src hrs:
  WHEN hrexec_store chunk addr args src hrs ~> hrs' THEN forall ctx sis sis'
  (SEXEC: sexec_store chunk addr args src sis = Some sis')
  (RR: ris_refines ctx hrs sis),
  ris_refines ctx hrs' sis'.
Proof.
  wlp_simplify. generalize SEXEC; clear SEXEC.
  unfold sexec_store; repeat autodestruct; intros.
  inv SEXEC; eapply rset_mem_correct; simpl; eauto.
  - intros X; erewrite H1; eauto.
    simplify_option.
  - intros X; inversion RR.
    erewrite H1; eauto. erewrite H0 with (sreg:=sis), EQ; eauto.
Qed.
Local Hint Resolve hrexec_store_correct: wlp.

Lemma hrs_ok_store_okpreserv ctx chunk addr args src hrs:
  WHEN hrexec_store chunk addr args src hrs ~> rst THEN
  ris_ok ctx rst -> ris_ok ctx hrs.
Proof.
  unfold hrexec_store; wlp_simplify.
  generalize H2. erewrite ok_rset_mem; intuition.
  specialize H1 with (sm2 := hrs).
  erewrite H1; eauto. rewrite H3. repeat autodestruct.
Qed.
Global Opaque hrexec_store.

(** ** Execution of builtins *)

Fixpoint hbuiltin_arg (hrs: ristate) (arg : builtin_arg reg): ?? builtin_arg sval := 
  match arg with
  | BA r => 
         DO v <~ @hrs_sreg_get HCF hrs r;;
         RET (BA v)
  | BA_int n => RET (BA_int n)
  | BA_long n => RET (BA_long n)
  | BA_float f0 => RET (BA_float f0)
  | BA_single s => RET (BA_single s)
  | BA_loadstack chunk ptr => RET (BA_loadstack chunk ptr)
  | BA_addrstack ptr => RET (BA_addrstack ptr)
  | BA_loadglobal chunk id ptr => RET (BA_loadglobal chunk id ptr)
  | BA_addrglobal id ptr => RET (BA_addrglobal id ptr)
  | BA_splitlong ba1 ba2 => 
    DO v1 <~ hbuiltin_arg hrs ba1;;
    DO v2 <~ hbuiltin_arg hrs ba2;;
    RET (BA_splitlong v1 v2)
  | BA_addptr ba1 ba2 => 
    DO v1 <~ hbuiltin_arg hrs ba1;;
    DO v2 <~ hbuiltin_arg hrs ba2;;
    RET (BA_addptr v1 v2)
  end.

Lemma hbuiltin_arg_nofail hrs arg:
  WHEN hbuiltin_arg hrs arg ~> hargs THEN forall ctx sis
  (ROK: ris_ok ctx hrs)
  (RR: ris_refines ctx hrs sis),
  map_builtin_arg_opt sis arg = None -> False.
Proof.
  induction arg; wlp_simplify; try (inv H; fail);
  try (generalize H1; clear H1; repeat autodestruct; intros; eauto; fail).
  generalize H0; clear H0; autodestruct; intros.
  exploit hrs_sreg_get_nofail; eauto.
Qed.

Lemma hbuiltin_arg_correct hrs arg:
  WHEN hbuiltin_arg hrs arg ~> hargs THEN forall ctx (sreg: reg -> option sval)
  (RR: forall r, osval_equiv ctx (hrs r) (sreg r))
  sarg (SARG: map_builtin_arg_opt sreg arg = Some sarg),
  eval_builtin_sval ctx hargs = eval_builtin_sval ctx sarg.
Proof.
  induction arg; wlp_simplify; try (inv SARG; eauto; fail).
  + erewrite H; eauto.
    simplify_option.
  + generalize SARG; clear SARG.
    do 2 autodestruct; intros.
    erewrite H; eauto.
    erewrite H0; eauto.
    simplify_option.
  + generalize SARG; clear SARG.
    do 2 autodestruct; intros.
    erewrite H; eauto.
    erewrite H0; eauto.
    simplify_option.
Qed.
Global Opaque hbuiltin_arg.
Local Hint Resolve hbuiltin_arg_correct: wlp.

Fixpoint hbuiltin_args hrs (args: list (builtin_arg reg)): ?? list (builtin_arg sval) :=
  match args with
  | nil => RET nil
  | a::l =>
    DO ha <~ hbuiltin_arg hrs a;;
    DO hl <~ hbuiltin_args hrs l;;
    RET (ha::hl)
    end.

Lemma hbuiltin_args_nofail hrs args:
  WHEN hbuiltin_args hrs args ~> hargs THEN forall ctx sis
  (ROK: ris_ok ctx hrs)
  (RR: ris_refines ctx hrs sis),
  bamap_opt (map_builtin_arg_opt sis) args = None -> False.
Proof.
  unfold bargs_refines; induction args; wlp_simplify; try (inv H; fail).
  generalize H1; clear H1. repeat autodestruct; intros; inv H1; eauto.
  exploit hbuiltin_arg_nofail; eauto.
Qed.

Lemma hbuiltin_args_correct hrs args:
  WHEN hbuiltin_args hrs args ~> hargs THEN forall ctx (sreg: reg -> option sval)
  (EOSVEQ: forall r, osval_equiv ctx (hrs r) (sreg r))
  sargs (SARGS: bamap_opt (map_builtin_arg_opt sreg) args = Some sargs),
  bargs_refines ctx hargs sargs.
Proof.
  unfold bargs_refines; induction args; wlp_simplify.
  - inv SARGS; simpl; reflexivity.
  - generalize SARGS; clear SARGS. do 2 autodestruct; intros; inv SARGS; simpl.
    erewrite H; eauto. erewrite H0; eauto.
Qed.
Global Opaque hbuiltin_args.
Local Hint Resolve hbuiltin_args_correct: wlp.

(** ** Execution of final instructions *)

Definition hsum_left_optmap {A B C} (f: A -> ?? B) (x: A + C): ?? (B + C) :=
  match x with
  | inl r => DO hr <~ f r;; RET (inl hr)
  | inr s => RET (inr s)
  end.

Lemma hsum_left_optmap_nofail hrs (ros: reg + ident):
  WHEN hsum_left_optmap (@hrs_sreg_get HCF hrs) ros ~> hsi THEN forall ctx (sis: sistate)
  (ROK: ris_ok ctx hrs)
  (RR: ris_refines ctx hrs sis),
  sum_left_optmap sis ros = None -> False.
Proof.
  destruct ros; wlp_simplify; try congruence.
  destruct (sis r) eqn:EQ; inv H0.
  exploit hrs_sreg_get_nofail; eauto.
Qed.

Lemma hsum_left_optmap_correct hrs ros:
  WHEN hsum_left_optmap (@hrs_sreg_get HCF hrs) ros ~> hsi THEN
  forall ctx (sreg: reg -> option sval) (svi: sval + ident)
  (EOSVEQ: forall r, osval_equiv ctx (hrs r) (sreg r))
  (SUM: sum_left_optmap sreg ros = Some svi),
  rsvident_refines ctx hsi svi.
Proof.
  destruct ros; wlp_simplify;
  simplify_option; econstructor; eauto.
  erewrite H, EQ; eauto.
Qed.
Global Opaque hsum_left_optmap.
Local Hint Resolve hsum_left_optmap_correct: wlp.

Definition hrexec_final_sfv (i: final) hrs: ?? sfval := 
  match i with
  | Bgoto pc => RET (Sgoto pc)
  | Bcall sig ros args res pc => 
      DO svos <~ hsum_left_optmap (@hrs_sreg_get HCF hrs) ros;;
      DO sargs <~ hlist_sreg_get hrs args;;
      RET (Scall sig svos sargs res pc)
  | Btailcall sig ros args =>
      DO svos <~ hsum_left_optmap (@hrs_sreg_get HCF hrs) ros;;
      DO sargs <~ hlist_sreg_get hrs args;;
      RET (Stailcall sig svos sargs)
  | Bbuiltin ef args res pc =>
      DO sargs <~ hbuiltin_args hrs args;;
      RET (Sbuiltin ef sargs res pc)
  | Breturn or => 
      match or with
      | Some r => DO hr <~ @hrs_sreg_get HCF hrs r;; RET (Sreturn (Some hr))
      | None => RET (Sreturn None)
      end
  | Bjumptable reg tbl =>
      DO sv <~ @hrs_sreg_get HCF hrs reg;;
      RET (Sjumptable sv tbl)
  end.

Lemma hrexec_final_sfv_nofail ctx fi hrs sis:
  WHEN hrexec_final_sfv fi hrs ~> sfv THEN forall
  (ROK: ris_ok ctx hrs)
  (RR: ris_refines ctx hrs sis)
  (SEXEC: sexec_final_sfv fi sis = None),
  False.
Proof.
  destruct fi; simpl; wlp_simplify; inversion RR.
  1,3: simplify_option; repeat econstructor.
  all:
    generalize SEXEC; clear SEXEC; repeat autodestruct;
    try (wlp_exploit hrs_sreg_get_nofail; fail);
    try (wlp_exploit hlist_sreg_get_nofail; fail);
    try (wlp_exploit hsum_left_optmap_nofail; simpl;
         rewrite EQ; auto; fail);
    try (wlp_exploit hbuiltin_args_nofail; fail).
Qed.

Lemma hrexec_final_sfv_correct ctx fi hrs sis:
  WHEN hrexec_final_sfv fi hrs ~> sfv THEN forall sfv'
  (ROK: ris_ok ctx hrs)
  (RR: ris_refines ctx hrs sis)
  (SFV: sexec_final_sfv fi sis = Some sfv'),
  rfv_refines ctx sfv sfv'.
Proof.
  destruct fi; simpl; wlp_simplify; inv SFV; inversion RR.
  1,3: simplify_option; repeat econstructor.
  1,7: simplify_option; intros SREG; repeat econstructor;
       erewrite H; eauto; fold (eval_osv ctx (Some s));
       rewrite <- SREG; eauto.
  all: simplify_option; intros SREG; repeat econstructor;
       try (eapply H with (sreg:=sis); eauto; autodestruct);
       try (erewrite H0 with (sreg:=sis); eauto).
Qed.

(** ** Symbolic execution of the whole block *)

Fixpoint hrexec_rec ib hrs (k: ristate -> ?? rstate): ?? rstate := 
  match ib with
  | BF fin _ =>
      DO sfv <~ hrexec_final_sfv fin hrs;;
      RET (Rfinal hrs sfv)
  (* basic instructions *)
  | Bnop _ => k hrs
  | Bop op args dst _ =>
      DO next <~ @hrs_rsv_set_input HCF RRULES dst args (Rop op) hrs;;
      k next
  | Bload trap chunk addr args dst _ =>
      DO next <~ @hrs_rsv_set_input HCF RRULES dst args (Rload trap chunk addr) hrs;;
      k next
  | Bstore chunk addr args src _ =>
      DO next <~ hrexec_store chunk addr args src hrs;;
      k next
 (* composed instructions *)
  | Bseq ib1 ib2 =>
      hrexec_rec ib1 hrs (fun hrs2 => hrexec_rec ib2 hrs2 k)
  | Bcond cond args ifso ifnot _ =>
      DO res <~ cbranch_expanse hrs cond args;;
      let (cond, vargs) := res in
      DO ifso <~ hrexec_rec ifso hrs k;;
      DO ifnot <~ hrexec_rec ifnot hrs k;;
      RET (Rcond cond vargs ifso ifnot)
  end
  .

Definition hrexec ib hrinit :=
  hrexec_rec ib hrinit (fun _ => RET Rabort).

Local Hint Resolve exec_final_refpreserv: core.
Local Hint Constructors rst_refines: core.

Lemma hrexec_rec_correct1 ctx ib: 
  forall rk k
  (CONTh: forall sis lsis sfv,
    get_soutcome ctx (k sis) = Some (sout lsis sfv) ->
    sis_ok ctx lsis ->
    sis_ok ctx sis)
  (CONT: forall hrs sis lsis sfv ss,
    ris_refines ctx hrs sis ->
    k sis = ss ->
    get_soutcome ctx (k sis) = Some (sout lsis sfv) ->
    sis_ok ctx lsis ->
    ris_input_init hrs = true ->
    WHEN rk hrs ~> rst THEN
    rst_refines true ctx rst (k sis))
  hrs sis lsis sfv ss
  (RR: ris_refines ctx hrs sis)
  (EXEC: sexec_rec ib sis k = ss)
  (SOUT: get_soutcome ctx ss = Some (sout lsis sfv))
  (SOK: sis_ok ctx lsis)
  (RINIT: ris_input_init hrs = true),
  WHEN hrexec_rec ib hrs rk ~> rst THEN
  rst_refines true ctx rst ss.
Proof.
  induction ib; wlp_simplify; simpl in *;
  try (autodestruct; [| inv SOUT; fail ]); inversion RR.
  - (* BF *)
    inv SOUT. apply OK_EQUIV in SOK.
    intros; exploit hrexec_final_sfv_correct; eauto.
  - (* Bop *)
    intros; eapply CONT with (hrs:=exta); eauto.
    + generalize EQ; clear EQ; unfold sexec_op.
      autodestruct; intros. inv EQ0. eapply H; eauto.
      unfold root_sapply. rewrite <- lmap_sv_input_subst, EQ. reflexivity.
    + wlp_exploit (@hrs_rsv_set_preserv_rinit HCF).
  - (* Bload *)
    intros; eapply CONT with (hrs:=exta); eauto.
    + generalize EQ; clear EQ; unfold sexec_load.
    autodestruct; intros. inv EQ0. eapply H; eauto.
      unfold root_sapply. rewrite <- lmap_sv_input_subst, EQ. reflexivity.
    + wlp_exploit (@hrs_rsv_set_preserv_rinit HCF).
  - (* Bstore *)
    intros; eapply CONT; eauto.
    wlp_exploit hrexec_store_preserv_rinit.
  - (* Bseq *)
    eapply IHib1. 3-8: eauto.
    + simpl. eapply sexec_rec_okpreserv; eauto.
    + intros; eapply IHib2; eauto.
  - (* Bcond *)
    intros. assert (ROK': ris_ok ctx hrs).
    { erewrite <- OK_EQUIV.
      eapply sexec_rec_okpreserv with (ib:=(Bcond cond args ib1 ib2 iinfo)); simpl; eauto.
      rewrite EQ; eauto. }
    exploit (Refcond true ctx (fst exta) cond (snd exta) l exta0 exta1
            (sexec_rec ib1 sis k) (sexec_rec ib2 sis k));
    exploit H; eauto; intros SEVAL; auto.
    all:
      simpl in SOUT; generalize SOUT; clear SOUT;
      simplify_option.
    + intros; eapply IHib1; eauto.
    + intros; eapply IHib2; eauto.
Qed.

Lemma hrexec_correct1 ctx ib sis sfv sinit hrinit:
  WHEN hrexec ib hrinit ~> rst THEN forall
  (SOUT: get_soutcome ctx (sexec ib sinit) = Some (sout sis sfv))
  (SOK: sis_ok ctx sis)
  (RR: ris_refines ctx hrinit sinit)
  (RINIT: ris_input_init hrinit = true),
  rst_refines true ctx rst (sexec ib sinit).
Proof.
  unfold sexec; intros; wlp_simplify.
  eapply hrexec_rec_correct1; eauto; simpl;
  congruence.
Qed.

Lemma hrexec_rec_okpreserv ctx ib: 
  forall k
  (CONT: forall hrs lhrs rfv,
    WHEN k hrs ~> rst THEN
    get_routcome ctx rst = Some (rout lhrs rfv) ->
    ris_ok ctx lhrs ->
    ris_ok ctx hrs)
  hrs lhrs rfv,
  WHEN hrexec_rec ib hrs k ~> rst THEN forall
  (ROUT: get_routcome ctx rst = Some (rout lhrs rfv))
  (ROK: ris_ok ctx lhrs),
  ris_ok ctx hrs.
Proof.
  induction ib; simpl; try (wlp_simplify; try_simplify_someHyps; fail).
  - (* Bop *)
    wlp_simplify. exploit (@hrs_ok_op_okpreserv HCF); eauto.
  - (* Bload *)
    destruct trap; wlp_simplify; try_simplify_someHyps;
    exploit (@hrs_ok_load_okpreserv HCF); eauto.
  - (* Bstore *)
    wlp_simplify; exploit hrs_ok_store_okpreserv; eauto.
  - (* Bcond *)
    wlp_simplify; generalize ROUT; clear ROUT; simplify_option.
Qed.
Local Hint Resolve hrexec_rec_okpreserv: wlp.

Lemma hrexec_rec_correct2 ctx ib: 
  forall rk k
  (CONTh: forall hrs lhrs rfv,
    WHEN rk hrs ~> rst THEN
    get_routcome ctx rst = Some (rout lhrs rfv) ->
    ris_ok ctx lhrs ->
    ris_ok ctx hrs)
  (CONT: forall hrs sis lhrs rfv,
    ris_refines ctx hrs sis ->
    WHEN rk hrs ~> rst THEN
    get_routcome ctx rst = Some (rout lhrs rfv) ->
    ris_ok ctx lhrs ->
    ris_input_init hrs = false ->
    rst_refines false ctx rst (k sis))
  hrs sis lhrs rfv
  (RR: ris_refines ctx hrs sis),
  WHEN hrexec_rec ib hrs rk ~> rst THEN forall
  (ROUT: get_routcome ctx rst = Some (rout lhrs rfv))
  (ROK: ris_ok ctx lhrs)
  (RINIT: ris_input_init hrs = false),
  rst_refines false ctx rst (sexec_rec ib sis k).
Proof.
  induction ib; simpl; intros; wlp_simplify; eauto.
  - (* BF *)
    inv ROUT. autodestruct; intros.
    exploit hrexec_final_sfv_correct; eauto.
    exploit hrexec_final_sfv_nofail; eauto; contradiction.
  - (* Bnop *) eapply CONT; eauto.
  - (* Bop *)
    unfold sexec_op.
    repeat autodestruct.
    + intros; eapply CONT; eauto.
      * eapply H; eauto.
        unfold root_sapply. rewrite <- lmap_sv_input_subst, EQ. reflexivity.
      * wlp_exploit (@hrs_rsv_set_preserv_rinit HCF).
    + wlp_exploit (@hrs_rsv_set_input_lmap_nofail _ _ RRULES).
      exploit (@hrs_ok_op_okpreserv HCF); eauto.
      rewrite <- lmap_sv_input_subst; auto.
  - (* Bload *)
    unfold sexec_load.
    repeat autodestruct.
    + intros; eapply CONT; eauto.
      * eapply H; eauto.
        unfold root_sapply. rewrite <- lmap_sv_input_subst, EQ. reflexivity.
      * wlp_exploit (@hrs_rsv_set_preserv_rinit HCF).
    + wlp_exploit (@hrs_rsv_set_input_lmap_nofail _ _ RRULES).
      exploit (@hrs_ok_load_okpreserv HCF); eauto.
      rewrite <- lmap_sv_input_subst; auto.
  - (* Bstore *)
    unfold sexec_store in *.
    repeat autodestruct.
    + intros; eapply CONT; eauto.
      * eapply H; eauto.
        rewrite EQ, EQ0. reflexivity.
      * wlp_exploit hrexec_store_preserv_rinit.
    + wlp_exploit hrexec_store_nofail.
      exploit hrs_ok_store_okpreserv; eauto.
    + wlp_exploit hrexec_store_lmap_nofail.
      exploit hrs_ok_store_okpreserv; eauto.
  - (* Bseq *)
    eapply IHib1. 3-7: eauto.
    + simpl; eapply hrexec_rec_okpreserv; eauto.
    + intros; eapply IHib2; eauto.
  - (* Bcond *)
    autodestruct; [ intros | wlp_exploit cbranch_expanse_lmap_nofail;
        generalize H1; clear H1; simplify_option ].
    assert (ROK': ris_ok ctx hrs).
    { simpl in ROUT; generalize ROUT; simplify_option. }
    exploit (Refcond false ctx (fst exta) cond (snd exta) l exta0 exta1
            (sexec_rec ib1 sis k) (sexec_rec ib2 sis k));
    exploit H; eauto; intros SEVAL; auto.
    all:
      simpl in ROUT; generalize ROUT; clear ROUT;
      simplify_option.
    + intros; eapply IHib1; eauto.
    + intros; eapply IHib2; eauto.
    Unshelve. all: auto.
Qed.

Lemma hrexec_correct2 ctx ib hrs rfv sinit hrinit:
  WHEN hrexec ib hrinit ~> rst THEN forall
  (ROUT: get_routcome ctx rst = Some (rout hrs rfv))
  (ROK: ris_ok ctx hrs)
  (RR: ris_refines ctx hrinit sinit)
  (RINIT: ris_input_init hrinit = false),
  rst_refines false ctx rst (sexec ib sinit).
Proof.
  unfold hrexec; intros; wlp_simplify.
  eapply hrexec_rec_correct2; eauto; simpl;
  wlp_simplify; congruence.
Qed.

Global Opaque hrexec.

End SymbolicExecution.
