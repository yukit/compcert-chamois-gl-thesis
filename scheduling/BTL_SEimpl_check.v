(* *************************************************************)
(*                                                             *)
(*             The Compcert verified compiler                  *)
(*                                                             *)
(*           Léo Gourdin        VERIMAG, UGA                   *)
(*           Sylvain Boulmé     Grenoble-INP, VERIMAG          *)
(*                                                             *)
(*  Copyright VERIMAG. All rights reserved.                    *)
(*  This file is distributed under the terms of the INRIA      *)
(*  Non-Commercial License Agreement.                          *)
(*                                                             *)
(* *************************************************************)

Require Import Coqlib AST RTL BTL Errors.
Require Import Maps Registers OptionMonad.
Require Import BTL_Invariants BTL_SEtheory.
Require Import BTL_SEsimuref BTL_SEimpl_prelude.
Require Import BTL_SEimpl_SE BTL_SEimpl_SI.
Import Notations.
Import HConsing.
Local Open Scope impure.

(** * Implementing the simulation test with concrete hash-consed symbolic execution *)

Definition phys_check {A} (x y:A) (msg: pstring): ?? unit :=
  DO b <~ phys_eq x y;;
  assert_b b msg;;
  RET tt.

Definition struct_check {A} (x y: A) (msg: pstring): ?? unit :=
  DO b <~ struct_eq x y;;
  assert_b b msg;;
  RET tt.

Lemma struct_check_correct {A} (a b: A) msg:
  WHEN struct_check a b msg ~> _ THEN
  a = b.
Proof. wlp_simplify. Qed.
Global Opaque struct_check.
Local Hint Resolve struct_check_correct: wlp.

Definition option_eq_check {A} (o1 o2: option A): ?? unit :=
  match o1, o2 with
  | Some x1, Some x2 => phys_check x1 x2 "option_eq_check: data physically differ"
  | None, None => RET tt
  | _, _ => FAILWITH "option_eq_check: structure differs"
  end.

Lemma option_eq_check_correct A (o1 o2: option A): WHEN option_eq_check o1 o2 ~> _ THEN o1=o2.
Proof.
  wlp_simplify.
Qed.
Global Opaque option_eq_check.
Local Hint Resolve option_eq_check_correct:wlp.

Definition option_incl_check {A} (o1 o2: option A): ?? unit :=
  match o1, o2 with
  | Some x1, Some x2 => phys_check x1 x2 "option_incl_check: data physically differ"
  | None, _ => RET tt
  | _, _ => FAILWITH "option_incl_check: structure differs"
  end.

Lemma option_incl_check_correct A (o1 o2: option A): WHEN option_incl_check o1 o2 ~> _ THEN
  o1=o2 \/ o1 = None.
Proof.
  wlp_simplify.
Qed.
Global Opaque option_incl_check.
Local Hint Resolve option_incl_check_correct:wlp.

Import PTree.                           

Definition PTree_incl_check {A} (m1 m2: PTree.t A): ?? unit :=
  PTree.tree_rec2
    RET tt
    (fun t => RET tt)
    (fun t => FAILWITH "PTree_incl_check")
    (fun l1 o1 r1 l2 o2 r2 olrec orrec =>
      DO lrec <~ olrec;;
      DO rrec <~ orrec;;
      DO oc <~ option_incl_check o1 o2;;
      RET tt)
    m1 m2.

Lemma PTree_incl_check_correct A: forall (m1 m2: PTree.t A),
 WHEN PTree_incl_check m1 m2 ~> _ THEN forall x,
 m1!x = m2!x \/ m1!x = None.
Proof.
  unfold PTree_incl_check.
  induction m1 using PTree.tree_ind; induction m2 using PTree.tree_ind; intros;
  try (wlp_simplify; fail).
  - rewrite unroll_tree_rec2_NE by auto; wlp_simplify.
  - rewrite unroll_tree_rec2_NN by auto; wlp_simplify.
    all:
      rewrite !gNode; destruct x;
      [ eapply IHm0; eauto | eapply IHm1; eauto | intuition ].
Qed.
Local Hint Resolve PTree_incl_check_correct: wlp.

Definition hrs_simu_check (light_check: bool) (hrs1 hrs2: ristate) : ?? unit :=
  XDEBUG light_check (fun lc => DO slc <~ string_of_bool lc;; RET ("hrs_simu_check?:[light_check=" +; slc +; "]")%string);;
  phys_check (ris_smem hrs1) (ris_smem hrs2) "hrs_simu_check: ris_smem sets aren't equiv";;
  (if light_check then (
    phys_check (ris_input_init hrs1) false
      "hrs_simu_check (light): hrs1.(ris_input_init) bool isn't false")
  else (
    phys_check (ris_input_init hrs1) (ris_input_init hrs2)
      "hrs_simu_check (hard): ris_input_init bools aren't equiv";;
    phys_check (ris_input_init hrs2) false
      "hrs_simu_check (hard): ris_input_init bools aren't false";;
    Sets.assert_list_incl mk_hash_params (ok_rsval hrs2) (ok_rsval hrs1)));;
  PTree_incl_check (ris_sreg hrs1) (ris_sreg hrs2);;
  DEBUG("hrs_simu_check=>OK").

Lemma setoid_in {A: Type} (a: A): forall l,
  SetoidList.InA (fun x y => x = y) a l ->
  In a l.
Proof.
  induction l; intros; inv H.
  - constructor. reflexivity.
  - right. auto.
Qed.

Lemma regset_elements_in r rs:
  Regset.In r rs ->
  In r (Regset.elements rs).
Proof.
  intros. exploit Regset.elements_1; eauto. intro SIN.
  apply setoid_in. assumption.
Qed.
Local Hint Resolve regset_elements_in: core.

Lemma hrs_simu_check_light_correct hrs1 hrs2:
  WHEN hrs_simu_check true hrs1 hrs2 ~> _ THEN
  incl (ok_rsval hrs2) (ok_rsval hrs1) ->
  ris_simu hrs1 hrs2.
Proof.
  wlp_simplify; constructor; auto.
  intros r ALIVE; unfold ris_sreg_get in *.
  repeat autodestruct;
  specialize H with r; destruct H;
  try congruence.
Qed.

Lemma hrs_simu_check_hard_correct hrs1 hrs2:
  WHEN hrs_simu_check false hrs1 hrs2 ~> _ THEN
  ris_simu hrs1 hrs2.
Proof.
  wlp_simplify; constructor; auto.
  intros r ALIVE; unfold ris_sreg_get in *.
  repeat autodestruct;
  specialize H3 with r; destruct H3;
  try congruence.
Qed.
Local Hint Resolve hrs_simu_check_light_correct hrs_simu_check_hard_correct: wlp.
Global Opaque hrs_simu_check.

Definition svos_simu_check (svos1 svos2: sval + ident) :=
  match svos1, svos2 with
  | inl sv1, inl sv2 => phys_check sv1 sv2 "svos_simu_check: sval mismatch"
  | inr id1, inr id2 => phys_check id1 id2 "svos_simu_check: symbol mismatch"
  | _, _ => FAILWITH "svos_simu_check: type mismatch"
  end.

Lemma svos_simu_check_correct svos1 svos2:
  WHEN svos_simu_check svos1 svos2 ~> _ THEN
  svos1 = svos2.
Proof.
  destruct svos1; destruct svos2; wlp_simplify.
Qed.
Global Opaque svos_simu_check.
Local Hint Resolve svos_simu_check_correct: wlp.

Fixpoint builtin_arg_simu_check (bs bs': builtin_arg sval) :=
  match bs with
  | BA sv =>
    match bs' with
    | BA sv' => phys_check sv sv' "builtin_arg_simu_check: sval mismatch"
    | _ => FAILWITH "builtin_arg_simu_check: BA mismatch"
    end
  | BA_splitlong lo hi =>
    match bs' with
    | BA_splitlong lo' hi' =>
        builtin_arg_simu_check lo lo';;
        builtin_arg_simu_check hi hi'
    | _ => FAILWITH "builtin_arg_simu_check: BA_splitlong mismatch"
    end
  | BA_addptr b1 b2 =>
    match bs' with
    | BA_addptr b1' b2' =>
        builtin_arg_simu_check b1 b1';;
        builtin_arg_simu_check b2 b2'
    | _ => FAILWITH "builtin_arg_simu_check: BA_addptr mismatch"
    end
  | bs => struct_check bs bs' "builtin_arg_simu_check: basic mismatch"
  end.

Lemma builtin_arg_simu_check_correct: forall bs1 bs2,
  WHEN builtin_arg_simu_check bs1 bs2 ~> _ THEN
  bs1 = bs2.
Proof.
  induction bs1.
  all: try (wlp_simplify; subst; reflexivity).
  all: destruct bs2; wlp_simplify; congruence.
Qed.
Global Opaque builtin_arg_simu_check.
Local Hint Resolve builtin_arg_simu_check_correct: wlp.

Fixpoint list_builtin_arg_simu_check lbs1 lbs2 :=
  match lbs1, lbs2 with
  | nil, nil => RET tt
  | bs1::lbs1, bs2::lbs2 =>
    builtin_arg_simu_check bs1 bs2;;
    list_builtin_arg_simu_check lbs1 lbs2
  | _, _ => FAILWITH "list_builtin_arg_simu_check: length mismatch"
  end.

Lemma list_builtin_arg_simu_check_correct: forall lbs1 lbs2,
  WHEN list_builtin_arg_simu_check lbs1 lbs2 ~> _ THEN
  lbs1 = lbs2.
Proof.
  induction lbs1; destruct lbs2; wlp_simplify. congruence.
Qed.
Global Opaque list_builtin_arg_simu_check.
Local Hint Resolve list_builtin_arg_simu_check_correct: wlp.

Definition sfval_simu_check (sfv1 sfv2: sfval): ?? unit :=
  match sfv1, sfv2 with
  | Sgoto e1, Sgoto e2 =>
      phys_check e1 e2 "sfval_simu_check: Sgoto successors do not match"
  | Scall sig1 svos1 lsv1 res1 e1, Scall sig2 svos2 lsv2 res2 e2 =>
      phys_check e1 e2 "sfval_simu_check: Scall successors do not match";;
      phys_check sig1 sig2 "sfval_simu_check: Scall different signatures";;
      phys_check res1 res2 "sfval_simu_check: Scall res do not match";;
      svos_simu_check svos1 svos2;;
      phys_check lsv1 lsv2 "sfval_simu_check: Scall args do not match"
  | Stailcall sig1 svos1 lsv1, Stailcall sig2 svos2 lsv2 =>
      phys_check sig1 sig2 "sfval_simu_check: Stailcall different signatures";;
      svos_simu_check svos1 svos2;;
      phys_check lsv1 lsv2 "sfval_simu_check: Stailcall args do not match"
  | Sbuiltin ef1 lbs1 br1 e1, Sbuiltin ef2 lbs2 br2 e2 =>
      phys_check e1 e2 "sfval_simu_check: Sbuiltin successors do not match";;
      phys_check ef1 ef2 "sfval_simu_check: Sbuiltin ef do not match";;
      phys_check br1 br2 "sfval_simu_check: Sbuiltin br do not match";;
      list_builtin_arg_simu_check lbs1 lbs2
  | Sjumptable sv1 le1, Sjumptable sv2 le2 =>
      phys_check le1 le2 "sfval_simu_check: Sjumptable successors do not match";;
      phys_check sv1 sv2 "sfval_simu_check: Sjumptable sval do not match"
  | Sreturn osv1, Sreturn osv2 =>
      option_eq_check osv1 osv2
  | _, _ => FAILWITH "sfval_simu_check: structure mismatch"
  end.

Lemma sfval_simu_check_correct sfv1 sfv2:
  WHEN sfval_simu_check sfv1 sfv2 ~> _ THEN
  rfv_simu sfv1 sfv2.
Proof.
  destruct sfv1; destruct sfv2; simpl; wlp_simplify; try congruence.
Qed.
Local Hint Resolve sfval_simu_check_correct: wlp.
Global Opaque sfval_simu_check.

Fixpoint rst_simu_check (light_check: bool) (rst1 rst2: rstate) {struct rst1} :=
  match rst1, rst2 with
  | Rfinal hrs1 sfv1, Rfinal hrs2 sfv2 =>
      hrs_simu_check light_check hrs1 hrs2;;
      sfval_simu_check sfv1 sfv2
  | Rcond cond1 lsv1 rsL1 rsR1, Rcond cond2 lsv2 rsL2 rsR2 =>
      struct_check cond1 cond2 "rst_simu_check: conditions do not match";;
      phys_check lsv1 lsv2 "rst_simu_check: args do not match";;
      rst_simu_check light_check rsL1 rsL2;;
      rst_simu_check light_check rsR1 rsR2
  | _, _ => FAILWITH "rst_simu_check: simu_check failed"
  end.

Lemma rst_simu_check_correct rst1: forall ctx light_check rst2,
  WHEN rst_simu_check light_check rst1 rst2 ~> _ THEN
  (if light_check then rst_ok_in ctx rst2 rst1 else True) ->
  rst_simu ctx light_check rst1 rst2.
Proof.
  induction rst1; destruct rst2, light_check; wlp_simplify;
  inv H1; constructor; eauto.
Qed.
Global Opaque rst_simu_check.

Definition HashConsing :=
  DO hC_sval <~ hCons hSVAL;;
  DO hC_list_sval <~ hCons hLSVAL;;
  DO hC_smem <~ hCons hSMEM;;
  RET (hC_sval, hC_list_sval, hC_smem).

Lemma HashConsing_Instance_correct:
  WHEN HashConsing ~> HCs THEN
  HashConsingHyps {| hC_sval := (fst (fst HCs)).(hC);
                     hC_list_sval := (snd (fst HCs)).(hC);
                     hC_smem := (snd HCs).(hC) |}.
Proof.
  unfold HashConsing; wlp_simplify.
  constructor; simpl; wlp_simplify.
Qed.
Global Opaque HashConsing.
Global Hint Resolve HashConsing_Instance_correct: wlp.

Definition simu_check_single (RRULES: rrules_set) (f1 f2: function)
  (ibf1 ibf2: iblock_info) (gm: gluemap) pc: ?? unit :=
  (** creating the hash-consing tables *)
  DO HCs <~ HashConsing;;
  let HCF := {| hC_sval := (fst (fst HCs)).(hC);
                hC_list_sval := (snd (fst HCs)).(hC);
                hC_smem := (snd HCs).(hC) |} in
  let hrexec := @hrexec HCF RRULES in
  let tr_hrs_single := @tr_hrs_single HCF RRULES in
  let tr_hstate := @tr_hstate HCF RRULES in
  (** performing the hash-consed executions *)
  DO hst_H <~ tr_hrs_single ris_empty (history (gm pc));;
  DO hst_HI <~ tr_hrs_single hst_H (glue (gm pc));;
  let src1 := ris_sreg_set hst_HI hst_H.(ris_sreg) in
  let src2 := ris_input_false hst_HI in
  (* The initial state for the source block is the empty state with the failure list
     of the target block initial state. *)
  DO hst1_E <~ hrexec ibf1.(entry) src1;;
  (*DEBUG("hrexec ibf1=>OK");;*)
  DO hst2_IE <~ hrexec ibf2.(entry) src2;;
  (*DEBUG("hrexec ibf2=>OK");;*)
  DO hst1_EH <~ tr_hstate (fun pc => history (gm pc)) hst1_E;;
  (*DEBUG("tr_hstate history hst1E=>OK");;*)
  DO hst1_EI <~ tr_hstate (fun pc => glue (gm pc)) hst1_E;;
  (*DEBUG("tr_hstate gluing hst1E=>OK");;*)
  (** comparing the executions *)
  DO _ <~ rst_simu_check true hst1_EH hst1_E;;
  rst_simu_check false hst1_EI hst2_IE.

Lemma simu_check_single_correct (RRULES: rrules_set) (f1 f2: function) (ibf1 ibf2: iblock_info)
  (gm: gluemap) pc:
  WHEN simu_check_single RRULES f1 f2 ibf1 ibf2 gm pc ~> _ THEN
  instantiate_context match_sexec_si_ref gm ibf1.(entry) ibf2.(entry) pc. 
Proof.
  unfold instantiate_context, match_sexec_si_ref, match_sexec_live_ref, 
  symb_exec_ok; wlp_simplify.
  set (HCF := {| hC_sval := hC (fst (fst exta));
               hC_list_sval := hC (snd (fst exta));
               hC_smem := hC (snd exta) |}).
  rename H1 into SOUT1E; rename H2 into SOK1E.
  set (ssE := (sexec (entry ibf1) (sis_source (gm pc)))).
  exploit (@sis_history_refines _ _ RRULES); eauto; wlp_simplify.
  intros (REF_H & RINIT_H & MEM_EQ_H & SIM_H).
  exploit (@sis_invariants_refines _ _ RRULES); eauto. clear Hexta0.
  intros (REF_INVS & MEM_EQ_INVS & RINIT_INVS & INCL_INVS & GLUE_ALIVE).
  assert (MEM_EQ_H_INV: ris_smem exta1 = ris_smem exta0) by (rewrite MEM_EQ_H, MEM_EQ_INVS; auto).
  exploit sis_source_refines; eauto; wlp_simplify.
  intros REF_SRC. assert (RST1E: rst_refines true (bcctx1 ctx) exta2 ssE) by
  (eapply hrexec_correct1; eauto; wlp_simplify).
  eapply tr_hstate_correct with (ctx2:=bcctx1 ctx) in Hexta4; eauto; wlp_simplify.
  2: unfold ctx_switch_prop; left; reflexivity.
  eapply tr_hstate_correct with (ctx2:=bcctx2 ctx) in Hexta5; eauto; wlp_simplify.
  2: unfold ctx_switch_prop; right; eauto.
  destruct Hexta4 as (ss1EH & THC_EH); inversion THC_EH;
  destruct SYMB as (sis1EH & SOUT1EH & SOK1EH).
  destruct Hexta5 as (ss1EG & THC_EG); inversion THC_EG;
  destruct SYMB as (sis1EG & SOUT1EG & SOK1EG).
  exists ss1EH; do 2 (split; auto).
  exists ss1EG; do 2 (split; auto).
  exists sis1EH; exists sfv1E. split; auto.
  split; [ eapply rst_simu_check_correct in Hexta6; eauto;
           eapply rst_simu_light_correct; unfold symb_exec_ok; intuition eauto |].
  exists sis1EG; exists sfv1E; split; auto.
  eapply rst_simu_check_correct in Hexta7; eauto.
  eapply rst_simu_hard_correct; unfold symb_exec_ok; simplify_ctx; intuition eauto.
  eapply hrexec_correct2; eauto; wlp_simplify.
  eapply (sis_target_refines (bcctx2 ctx) (gm pc) exta0 exta1); eauto.
  1,2: apply ris_refines_preserved; auto.
Qed.
Global Opaque simu_check_single.
Local Hint Resolve simu_check_single_correct: wlp.

(** * Coerce simu_check_single into a pure function. *)

(* FOR DISABLING simu_check *)


Definition aux_simu_check (RRULES: rrules_set) (f1 f2: function) (ibf1 ibf2: iblock_info)
  (gm: gluemap) (pc: node): ?? unit :=
   XDEBUG pc (fun pc => DO name_pc <~ string_of_Z (Zpos pc);; RET ("simu_check_single?:BTL block n°" +; name_pc)%string);;
   simu_check_single RRULES f1 f2 ibf1 ibf2 gm pc;; 
   DEBUG("simu_check_single=>OK").

Lemma aux_simu_check_correct (RRULES: rrules_set) (f1 f2: function) (ibf1 ibf2: iblock_info)
  (gm: gluemap) (pc: node):
  WHEN aux_simu_check RRULES f1 f2 ibf1 ibf2 gm pc ~> _ THEN
  instantiate_context match_sexec_si gm ibf1.(entry) ibf2.(entry) pc.
Proof.
  unfold aux_simu_check; wlp_simplify.
  unfold instantiate_context in *; intros ctx.
  apply match_sexec_si_ref_imp; auto.
Qed.

Definition simu_check (RRULES: rrules_set) (f1 f2: function) (ibf1 ibf2: iblock_info)
  (gm: gluemap) (pc: node): res unit :=
  if has_returned (aux_simu_check RRULES f1 f2 ibf1 ibf2 gm pc)
  then OK tt
  else Error (msg "unexpected value of has_returned")
  .

Lemma simu_check_correct RRULES f1 f2 ibf1 ibf2 gm pc:
  simu_check RRULES f1 f2 ibf1 ibf2 gm pc = OK tt ->
  instantiate_context match_sexec_si gm ibf1.(entry) ibf2.(entry) pc.
Proof.
  unfold simu_check.
  destruct (has_returned _) eqn:Hres; try discriminate.
  intros _.
  exploit has_returned_correct; eauto.
  intros (r & X); eapply aux_simu_check_correct; eauto.
Qed.
