(* *************************************************************)
(*                                                             *)
(*             The Compcert verified compiler                  *)
(*                                                             *)
(*           Léo Gourdin        VERIMAG, UGA                   *)
(*                                                             *)
(*  Copyright VERIMAG. All rights reserved.                    *)
(*  This file is distributed under the terms of the INRIA      *)
(*  Non-Commercial License Agreement.                          *)
(*                                                             *)
(* *************************************************************)

(** Core definitions for the LCT oracle *)

open Op
open AST
open Maps
open BTLtypes
open RTLcommonaux
open BTLcommonaux
open Printf
open HashedSet
open Camlcoq
open PrintOp
open PrepassSchedulingOracleDeps

(** Types *)

module IM = Map.Make (Int)

type ci64 = Integers.Int64.int
(** Coq Int64 type *)

type lct_predicates = {
  mutable transp : Bitv.t option;
  mutable ncomp : Bitv.t option;
  mutable xcomp : Bitv.t option;
  mutable injured : Bitv.t option;
  mutable update : Bitv.t option;
  mutable ndsafe : Bitv.t option;
  mutable xdsafe : Bitv.t option;
  mutable nusafe : Bitv.t option;
  mutable xusafe : Bitv.t option;
  mutable nearliest : Bitv.t option;
  mutable xearliest : Bitv.t option;
  mutable ndelayed : Bitv.t option;
  mutable xdelayed : Bitv.t option;
  mutable nlatest : Bitv.t option;
  mutable xlatest : Bitv.t option;
  mutable nisolated : Bitv.t option;
  mutable xisolated : Bitv.t option;
  mutable ninsert : Bitv.t option;
  mutable xinsert : Bitv.t option;
  mutable nreplace : Bitv.t option;
  mutable xreplace : Bitv.t option;
  mutable ncritical : Bitv.t option;
  mutable xcritical : Bitv.t option;
  mutable ncritinsert : Bitv.t option;
  mutable xcritinsert : Bitv.t option;
  mutable nsubstcrit : Bitv.t option;
  mutable xsubstcrit : Bitv.t option;
}
(** Each candidate for CM/SR have a list of bitvector predicates.
    The six last ones are only used for SR. *)

type lct_predicate_t =
  | P_transp
  | P_ncomp
  | P_xcomp
  | P_injured
  | P_update
  | P_ndsafe
  | P_xdsafe
  | P_nusafe
  | P_xusafe
  | P_nearliest
  | P_xearliest
  | P_ndelayed
  | P_xdelayed
  | P_nlatest
  | P_xlatest
  | P_nisolated
  | P_xisolated
  | P_ninsert
  | P_xinsert
  | P_nreplace
  | P_xreplace
  | P_ncritical
  | P_xcritical
  | P_ncritinsert
  | P_xcritinsert
  | P_nsubstcrit
  | P_xsubstcrit

type cm_ckey_t =
  | CMop of trapping_mode * operation * P.t list
  | CMload of trapping_mode * memory_chunk * addressing * P.t list

type sr_t = SRmul | SRadd
type ckey_t = CCM of cm_ckey_t | CSR of sr_t * operation * P.t list

(** Kind of candidates for CM/SR (splitted with the CCM|CSR type):
  - CMop: Code Motion candidate representing a BTL Op;
  - CMload: Code Motion candidate representing a BTL Load;
  - CSR: Strength-Reduction candidate, as for CMop but with one one less information as
    trapping_mode is not considered for SR (always non-trap).
    SR candidates also have two possible types :
    {ul
    {- SRmul where we expect to insert an update operation;}
    {- SRadd to represent potentially addition SR (SRadd candidates might be downgraded to CM
       ones if they do not contain an auxiliary variable in their argument when starting the
       LCT process. See the downgrade function in LazyCodeOracle module).
       SRadd candidates have an update operation computed using affine values
       Their injuring operation corresponds to the update operation of SRmul.}} *)

type cand_t = {
  mutable lhs : OrdIS.t IM.t;
  state : lct_predicates;
  mutable vaux : P.t option;
  memdep : bool;
  mutable was_reduced : bool;
  mutable updated_args : P.t list option;
  orig_args : P.t list;
}
(** A candidate is a struct with:
  - A map (block |-> offset set) of where the operation occurs in [lhs];
  - The bitvector predicates in [state];
  - A dedicated auxialiary variable to insert and replace the instruction;
  - A boolean indicating if the candidate depends on memory;
  - A boolean indicating if the candidate was strength-reduced;
  - A mutable list of arguments that is modified to permit substitution during the algorithm
    (a previous candidate can replace arguments of a future one). This list can also be emptied
    when the candidate was merged with another, so that it will be skipped by the algorithm;
  - The list of initial arguments before any substitution. *)

type blk_tools_t = {
  blk2id : int PTree.t;
  id2blk : P.t IM.t;
  wl : OrdIS.t;
  card : int;
  last_reg : P.t;
  mutable sr_vauxs : PSet.t;
}
(** We define a struct containing useful information for the algorithm:
  - [blk2id] and [id2blk] are two maps from block ids to bitvector ids and the
    reverse, respectively;
  - A worklist in [wl] to be used with the round-robin fixpoint;
  - The corresponding cardinality in [card];
  - In [last_reg], the Coq positive corresponding to the last register id before executing the
    oracle (thus, for all [n] > [last_reg], we know that n is a fresh variable);
  - A set [sr_vauxs] of fresh SR-variables from the oracle. *)

type const_t = operation * P.t
(** Type for constants, were the Coq positive corresponds to the block in which the constant is. *)

type 'a affine_form = Aff_term of 'a * P.t * 'a affine_form | Aff_const of 'a

(** The above is a simple inductive polymorphic affine form, with ['a] the type of constants,
    and Coq positive for registers. *)

(** Tools for managing variables and predicates *)

(** Maps registers to immediate constant operation (if they are unchanged over
    the whole function). *)
let constants : (P.t, const_t) Hashtbl.t = Hashtbl.create 100

(** Maps candidates keys to candidate type [cand_t]. *)
let candidates : (ckey_t, cand_t) Hashtbl.t = Hashtbl.create 100

(** Maps a tuple (block pc, register) to its [int64] affine value, if applicable. *)
let affine_int64 : (P.t * P.t, ci64 affine_form) Hashtbl.t = Hashtbl.create 30

(** Affine operations: multiplication, addition, and accumulation of injuring amounts for a
    given set of variables. *)

(** Affine value times a constant of type ['a]. *)
let rec affine_mul (mul : 'a -> 'a -> 'a) (a : 'a) : 'a affine_form -> 'a affine_form = function
  | Aff_term (a', r, aff) -> Aff_term (mul a a', r, affine_mul mul a aff)
  | Aff_const a' -> Aff_const (mul a a')

(** Addition over affine values with type ['a].
    We sort them by increasing order of registers in terms. *)
let rec affine_add (add : 'a -> 'a -> 'a) (aff1 : 'a affine_form) (aff2 : 'a affine_form) :
    'a affine_form =
  match (aff1, aff2) with
  | Aff_term (a1, r1, aff1'), Aff_term (a2, r2, aff2') ->
      if r1 = r2 then Aff_term (add a1 a2, r1, affine_add add aff1' aff2')
      else if P.( < ) r1 r2 then Aff_term (a1, r1, affine_add add aff1' aff2)
      else Aff_term (a2, r2, affine_add add aff2' aff1)
  | Aff_term (a, r, aff), Aff_const a' | Aff_const a', Aff_term (a, r, aff) ->
      Aff_term (a, r, affine_add add (Aff_const a') aff)
  | Aff_const a, Aff_const a' -> Aff_const (add a a')

let rec affine_inj_const : 'a affine_form -> 'a = function
  | Aff_term (_, _, aff) -> affine_inj_const aff
  | Aff_const c -> c

(** Wrappers to mul add, and inj_factor over affine_int64. *)

let aff_mul_int64 : ci64 -> 'a affine_form -> 'a affine_form = affine_mul Integers.Int64.mul

let aff_add_int64 : 'a affine_form -> 'a affine_form -> 'a affine_form =
  affine_add Integers.Int64.add

(** Grab the constant of affine form with key [tup];
    Used for injuring operations. *)
let aff_inj_const_int64 (tup : P.t * P.t) : ci64 =
  (*Printf.eprintf "aff_inj_const_int64 at (%d, %d)\n" (p2i (fst tup)) (p2i (snd tup));*)
  match Hashtbl.find_opt affine_int64 tup with
  | Some aff -> affine_inj_const aff
  | None -> Integers.Int64.zero

(** Compute the sum of all [Aff_term(ci, ri, ...)] with
    [ci * (aff_inj_const_int64 (pc, ri))].
    The last constant [Aff_const c] is included only if [arg] is equal to at
    least one [ri]. This trick is needed to avoid adding the last constant of
    single term affine forms [c * r + const] when [r = arg]. Indeed, those
    constants are invariant so we don't need to compensate them. *)
let rec aff_inj_update_int64 (pc : P.t) (arg : P.t) (aff : ci64 affine_form) (zero_const : bool) :
    ci64 =
  match aff with
  | Aff_term (c, r, aff') ->
      if arg = r then aff_inj_update_int64 pc arg aff' false
      else
        Integers.Int64.add
          (Integers.Int64.mul (aff_inj_const_int64 (pc, r)) c)
          (aff_inj_update_int64 pc arg aff' zero_const)
  | Aff_const c -> if zero_const then Integers.Int64.zero else c

(** Finds the first defined affine form with key [(pt, arg)]
    for each [pt in pts]. *)
let rec aff_find_update_int64 (pts : P.t list) (arg : P.t) : ci64 affine_form option =
  match pts with
  | pt :: pts -> (
      match Hashtbl.find_opt affine_int64 (pt, arg) with
      | Some aff -> Some aff
      | None -> aff_find_update_int64 pts arg)
  | [] -> None

(** Fold computing the overall compensation value to add for a given
    list of arguments [cargs], seeking for defined affine forms in block [pc]
    first, and in [replace_pts] otherwise (in case some values are out of
    the block's scope; it happens in practice). *)
let aff_cargs_update_int64 (replace_pts : P.t list) (pc : P.t) (cargs : P.t list) : ci64 =
  (*Printf.eprintf "entering aff_cargs_update_int64\n";*)
  List.fold_left
    (fun acc arg ->
      match aff_find_update_int64 (pc :: replace_pts) arg with
      | Some aff ->
          (*Printf.eprintf "found aff at (%d, %d)\n" (p2i pc) (p2i arg);*)
          let upd = aff_inj_update_int64 pc arg aff true in
          (*Printf.eprintf "acc is %Ld and upd is %Ld\n" (Camlcoq.camlint64_of_coqint acc)
            (Camlcoq.camlint64_of_coqint upd);*)
          Integers.Int64.add acc upd
      | None ->
          (*Printf.eprintf "Nothing for (%d, %d)\n" (p2i pc) (p2i arg);*)
          acc)
    Integers.Int64.zero cargs

(** If [force], replace the value at [tup] by [aff],
    else, add it only if [tup] is not already defined. *)
let aff_may_replace_int64 (tup : P.t * P.t) (aff : 'a affine_form) (force : bool) : unit =
  if force then Hashtbl.replace affine_int64 tup aff
  else if Hashtbl.find_opt affine_int64 tup |> Option.is_none then Hashtbl.add affine_int64 tup aff

(** Set [a1 * l] in [(pc, dst)] after eventually updating with the
    old value at [(pc, a1)]. *)
let aff_mul_int64_uset (pc : P.t) (dst : P.t) (a1 : P.t) (l : ci64) (force : bool) : unit =
  let aff =
    match Hashtbl.find_opt affine_int64 (pc, a1) with
    | Some aff -> aff_mul_int64 l aff
    | None -> Aff_term (l, a1, Aff_const Integers.Int64.zero)
  in
  aff_may_replace_int64 (pc, dst) aff force

(** Set [a1 + l] in [(pc, dst)] after eventually updating with the
    old value at [(pc, a1)]. *)
let aff_add1_int64_uset (pc : P.t) (dst : P.t) (a1 : P.t) (l : ci64) (force : bool) : unit =
  let aff =
    match Hashtbl.find_opt affine_int64 (pc, a1) with
    | Some aff -> aff_add_int64 aff (Aff_const l)
    | None -> Aff_term (Integers.Int64.one, a1, Aff_const l)
  in
  aff_may_replace_int64 (pc, dst) aff force

(** Set [a1 + a2] in [(pc, dst)] after eventually updating with the
    old values of [{(pc, a1), (pc, a2)}]. *)
let aff_add2_int64_uset (pc : P.t) (dst : P.t) (a1 : P.t) (a2 : P.t) (force : bool) : unit =
  let aff =
    match (Hashtbl.find_opt affine_int64 (pc, a1), Hashtbl.find_opt affine_int64 (pc, a2)) with
    | Some aff1, Some aff2 -> aff_add_int64 aff1 aff2
    | Some aff, None ->
        aff_add_int64 aff (Aff_term (Integers.Int64.one, a2, Aff_const Integers.Int64.zero))
    | None, Some aff ->
        aff_add_int64 aff (Aff_term (Integers.Int64.one, a1, Aff_const Integers.Int64.zero))
    | None, None -> Aff_const Integers.Int64.zero
  in
  aff_may_replace_int64 (pc, dst) aff force

(** Utility functions for candidates *)

let candidate_op_filter : operation -> bool = function Omove -> false | _ -> true
let is_sr_ckey : ckey_t -> bool = function CSR _ -> true | _ -> false
let is_ld_ckey : ckey_t -> bool = function CCM (CMload (_, _, _, _)) -> true | _ -> false

let is_sr_mul_ckey : ckey_t -> bool = function
  | CSR (SRmul, _, _) -> true
  | CSR (SRadd, _, _) -> false
  | _ -> false

let is_constant_reg (r : P.t) : bool = Option.is_some (Hashtbl.find_opt constants r)

let is_trapping_ckey : ckey_t -> bool = function
  | CCM (CMload (_, _, _, _)) | CCM (CMop (TRAP, _, _)) -> true
  | _ -> false

(** Default predicate value, this setting is very important and quite touchy... *)
let default_predicate_value : lct_predicate_t -> bool = function
  | P_ncomp | P_xcomp | P_injured | P_nearliest | P_xearliest | P_nlatest | P_xlatest | P_ninsert
  | P_xinsert | P_nreplace | P_xreplace | P_update ->
      false
  | _ -> true

(** Is the predicate requiring a forward analysis? *)
let is_fwd_predicate : lct_predicate_t -> bool = function
  | P_ndsafe | P_xdsafe | P_nisolated | P_xisolated | P_ncritical | P_xcritical | P_update -> false
  | _ -> true

(** At the beginning, every predicate option is set to None. *)
let mk_def_df_predicates () : lct_predicates =
  {
    transp = None;
    ncomp = None;
    xcomp = None;
    injured = None;
    update = None;
    ndsafe = None;
    xdsafe = None;
    nusafe = None;
    xusafe = None;
    nearliest = None;
    xearliest = None;
    ndelayed = None;
    xdelayed = None;
    nlatest = None;
    xlatest = None;
    nisolated = None;
    xisolated = None;
    ninsert = None;
    xinsert = None;
    nreplace = None;
    xreplace = None;
    ncritical = None;
    xcritical = None;
    ncritinsert = None;
    xcritinsert = None;
    nsubstcrit = None;
    xsubstcrit = None;
  }

(** Initialize a new candidate appearing at [pc, offset] with [_memdep, args] 
    and default values. *)
let mk_cand (pc : int) (offset : OrdIS.elt) (_memdep : bool) (args : P.t list) : cand_t =
  let im = IM.empty in
  {
    lhs = IM.add pc ~$offset im;
    state = mk_def_df_predicates ();
    vaux = None;
    memdep = _memdep;
    was_reduced = false;
    updated_args = None;
    orig_args = args;
  }

(** Read and write a predicate, or its values in [cand]. *)

let get_predicate_cand (cand : cand_t) (pred : lct_predicate_t) : Bitv.t =
  get_some
  @@
  match pred with
  | P_transp -> cand.state.transp
  | P_ncomp -> cand.state.ncomp
  | P_xcomp -> cand.state.xcomp
  | P_injured -> cand.state.injured
  | P_update -> cand.state.update
  | P_ndsafe -> cand.state.ndsafe
  | P_xdsafe -> cand.state.xdsafe
  | P_nusafe -> cand.state.nusafe
  | P_xusafe -> cand.state.xusafe
  | P_nearliest -> cand.state.nearliest
  | P_xearliest -> cand.state.xearliest
  | P_ndelayed -> cand.state.ndelayed
  | P_xdelayed -> cand.state.xdelayed
  | P_nlatest -> cand.state.nlatest
  | P_xlatest -> cand.state.xlatest
  | P_nisolated -> cand.state.nisolated
  | P_xisolated -> cand.state.xisolated
  | P_ninsert -> cand.state.ninsert
  | P_xinsert -> cand.state.xinsert
  | P_nreplace -> cand.state.nreplace
  | P_xreplace -> cand.state.xreplace
  | P_ncritical -> cand.state.ncritical
  | P_xcritical -> cand.state.xcritical
  | P_ncritinsert -> cand.state.ncritinsert
  | P_xcritinsert -> cand.state.xcritinsert
  | P_nsubstcrit -> cand.state.nsubstcrit
  | P_xsubstcrit -> cand.state.xsubstcrit

let get_predicate_value (cand : cand_t) (bitvi : int) (pred : lct_predicate_t) : bool =
  let bv_pred = get_predicate_cand cand pred in
  Bitv.get bv_pred bitvi

let set_predicate_cand (cand : cand_t) (bv : Bitv.t) : lct_predicate_t -> unit = function
  | P_transp -> cand.state.transp <- Some bv
  | P_ncomp -> cand.state.ncomp <- Some bv
  | P_xcomp -> cand.state.xcomp <- Some bv
  | P_injured -> cand.state.injured <- Some bv
  | P_update -> cand.state.update <- Some bv
  | P_ndsafe -> cand.state.ndsafe <- Some bv
  | P_xdsafe -> cand.state.xdsafe <- Some bv
  | P_nusafe -> cand.state.nusafe <- Some bv
  | P_xusafe -> cand.state.xusafe <- Some bv
  | P_nearliest -> cand.state.nearliest <- Some bv
  | P_xearliest -> cand.state.xearliest <- Some bv
  | P_ndelayed -> cand.state.ndelayed <- Some bv
  | P_xdelayed -> cand.state.xdelayed <- Some bv
  | P_nlatest -> cand.state.nlatest <- Some bv
  | P_xlatest -> cand.state.xlatest <- Some bv
  | P_nisolated -> cand.state.nisolated <- Some bv
  | P_xisolated -> cand.state.xisolated <- Some bv
  | P_ninsert -> cand.state.ninsert <- Some bv
  | P_xinsert -> cand.state.xinsert <- Some bv
  | P_nreplace -> cand.state.nreplace <- Some bv
  | P_xreplace -> cand.state.xreplace <- Some bv
  | P_ncritical -> cand.state.ncritical <- Some bv
  | P_xcritical -> cand.state.xcritical <- Some bv
  | P_ncritinsert -> cand.state.ncritinsert <- Some bv
  | P_xcritinsert -> cand.state.xcritinsert <- Some bv
  | P_nsubstcrit -> cand.state.nsubstcrit <- Some bv
  | P_xsubstcrit -> cand.state.xsubstcrit <- Some bv

let set_predicate_value (cand : cand_t) (bitvi : int) (pred : lct_predicate_t) (b : bool) : unit =
  Bitv.set (get_predicate_cand cand pred) bitvi b

(** Write an array of predicates [preds] with an array of solutions [sols]. *)
let set_predicates_array (cand : cand_t) (preds : lct_predicate_t array) (sols : Bitv.t array) :
    unit =
  Array.iteri (fun i p -> set_predicate_cand cand sols.(i) p) preds

(** If a candidate for strength-reduction is never injured, then we can
    perform the code-motion dataflow analysis only. *)
let is_really_sr_cand (cand : cand_t) : bool =
  not (Bitv.all_zeros (get_predicate_cand cand P_injured))

let is_real_sr_mul_cand (ckey : ckey_t) (cand : cand_t) : bool =
  is_sr_mul_ckey ckey && is_really_sr_cand cand

(** Flags default values *)

type t_flags = {
  mutable ok_sr : bool;
  mutable ok_trap : bool;
  mutable tlimit : int;
  mutable nlimit : int;
}

let flags = { ok_sr = false; ok_trap = false; tlimit = 0; nlimit = 0 }

(** Logs and Statistics *)

let log_lvl = 0

let llog (lvl : int) (fmt : ('a, out_channel, unit) format) : 'a =
  if lvl <= log_lvl then (
    fprintf stderr "[LCT lvl=%d] " lvl;
    fprintf stderr fmt)
  else ifprintf stderr fmt

type t_lct_stats = {
  mutable tstart : float;
  mutable tcounter : int;
  mutable nb_notrap : int;
  mutable nb_trap : int;
  mutable nb_nins_notrap : int;
  mutable nb_xins_notrap : int;
  mutable nb_nins_trap : int;
  mutable nb_xins_trap : int;
  mutable nb_nrep_notrap : int;
  mutable nb_xrep_notrap : int;
  mutable nb_nrep_trap : int;
  mutable nb_xrep_trap : int;
  mutable nb_sr : int;
  mutable nb_nins_sr : int;
  mutable nb_xins_sr : int;
  mutable nb_nrep_sr : int;
  mutable nb_xrep_sr : int;
  mutable nb_upd_sr : int;
  mutable nb_aseq_ginv : int;
  mutable nb_aseq_hinv : int;
  mutable lct_exec_time : float;
}

let save_stats = false

let stats =
  {
    tstart = 0.;
    tcounter = 0;
    nb_notrap = 0;
    nb_trap = 0;
    nb_nins_notrap = 0;
    nb_xins_notrap = 0;
    nb_nins_trap = 0;
    nb_xins_trap = 0;
    nb_nrep_notrap = 0;
    nb_xrep_notrap = 0;
    nb_nrep_trap = 0;
    nb_xrep_trap = 0;
    nb_sr = 0;
    nb_nins_sr = 0;
    nb_xins_sr = 0;
    nb_nrep_sr = 0;
    nb_xrep_sr = 0;
    nb_upd_sr = 0;
    nb_aseq_ginv = 0;
    nb_aseq_hinv = 0;
    lct_exec_time = 0.;
  }

let reset_stats () : unit =
  stats.tstart <- 0.;
  stats.tcounter <- 0;
  stats.nb_notrap <- 0;
  stats.nb_trap <- 0;
  stats.nb_nins_notrap <- 0;
  stats.nb_xins_notrap <- 0;
  stats.nb_nins_trap <- 0;
  stats.nb_xins_trap <- 0;
  stats.nb_nrep_notrap <- 0;
  stats.nb_xrep_notrap <- 0;
  stats.nb_nrep_trap <- 0;
  stats.nb_xrep_trap <- 0;
  stats.nb_sr <- 0;
  stats.nb_nins_sr <- 0;
  stats.nb_xins_sr <- 0;
  stats.nb_nrep_sr <- 0;
  stats.nb_xrep_sr <- 0;
  stats.nb_upd_sr <- 0;
  stats.nb_aseq_ginv <- 0;
  stats.nb_aseq_hinv <- 0;
  stats.lct_exec_time <- 0.

(** Below tools to limit the oracle in runtime. *)

exception TimeLimitReached

let has_timeout () : bool =
  stats.tcounter <- stats.tcounter + 1;
  if stats.tcounter mod 1000 = 0 then
    let tcur = int_of_float (Unix.gettimeofday () -. stats.tstart) in
    tcur > flags.tlimit
  else false

let check_time () : unit = if flags.tlimit <> 0 && has_timeout () then raise TimeLimitReached

(** Limit the number of candidates according to the user flag. *)
let filter_candidates () : unit =
  let clen = Hashtbl.length candidates in
  if flags.nlimit <> 0 && clen > flags.nlimit then (
    let counter = ref 0 and opweights = OpWeights.get_opweights () in
    let larray =
      Hashtbl.fold
        (fun k v a ->
          a.(!counter) <-
            (match k with
            | CCM (CMop (_, op, args)) -> (opweights.latency_of_op op (List.length args), Some k)
            | CCM (CMload (trap, chk, addr, args)) ->
                (opweights.latency_of_load trap chk addr (List.length args), Some k)
            | _ -> (Int.max_int, Some k));
          counter := !counter + 1;
          a)
        candidates
        (Array.make clen (0, None))
    in
    Array.sort (fun a b -> compare (fst a) (fst b)) larray;
    for i = 0 to clen - flags.nlimit - 1 do
      Hashtbl.remove candidates (get_some @@ snd @@ larray.(i))
    done;
    llog 1 "Too many candidates! Filtered map: (clen=%d, nlimit=%d, newlen=%d)\n" clen flags.nlimit
      (Hashtbl.length candidates))

(** Debugging functions, and statistics. *)

let stat_code_motion (is_entry : bool) (ckey : ckey_t) (is_insert : bool) : unit =
  let is_trap = is_trapping_ckey ckey and is_sr = is_sr_ckey ckey in
  if is_entry then
    if is_trap then
      if is_insert then stats.nb_nins_trap <- stats.nb_nins_trap + 1
      else stats.nb_nrep_trap <- stats.nb_nrep_trap + 1
    else if is_sr then
      if is_insert then stats.nb_nins_sr <- stats.nb_nins_sr + 1
      else stats.nb_nrep_sr <- stats.nb_nrep_sr + 1
    else if is_insert then stats.nb_nins_notrap <- stats.nb_nins_notrap + 1
    else stats.nb_nrep_notrap <- stats.nb_nrep_notrap + 1
  else if is_trap then
    if is_insert then stats.nb_xins_trap <- stats.nb_xins_trap + 1
    else stats.nb_xrep_trap <- stats.nb_xrep_trap + 1
  else if is_sr then
    if is_insert then stats.nb_xins_sr <- stats.nb_xins_sr + 1
    else stats.nb_xrep_sr <- stats.nb_xrep_sr + 1
  else if is_insert then stats.nb_xins_notrap <- stats.nb_xins_notrap + 1
  else stats.nb_xrep_notrap <- stats.nb_xrep_notrap + 1

let fprint_stats_header (oc : out_channel) () : 'a =
  fprintf oc
    "notrap, ni_notrap, xi_notrap, nr_notrap, xr_notrap, trap, ni_trap, xi_trap, nr_trap, xr_trap, \
     sr, ni_sr, xi_sr, nr_sr, xr_sr, u_sr, nb_aseq_ginv, nb_aseq_hinv, etime\n"

let fprint_stats (oc : out_channel) () : 'a =
  fprintf oc "%d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d, %f\n"
    stats.nb_notrap stats.nb_nins_notrap stats.nb_xins_notrap stats.nb_nrep_notrap
    stats.nb_xrep_notrap stats.nb_trap stats.nb_nins_trap stats.nb_xins_trap stats.nb_nrep_trap
    stats.nb_xrep_trap stats.nb_sr stats.nb_nins_sr stats.nb_xins_sr stats.nb_nrep_sr
    stats.nb_xrep_sr stats.nb_upd_sr stats.nb_aseq_ginv stats.nb_aseq_hinv stats.lct_exec_time

let write_stats () : unit =
  let oc = open_out_gen [ Open_append; Open_creat; Open_text ] 0o666 "/tmp/lct_stats.csv" in
  fprint_stats oc ();
  close_out oc

let print_ordis (pp : out_channel) (is : OrdIS.t) : unit =
  OrdIS.iter (fun i -> fprintf pp "%d " i) is

let print_imap (pp : out_channel) (im : OrdIS.t IM.t) : unit =
  IM.iter (fun k v -> fprintf pp "(%d |-> [%a]) " k print_ordis v) im

let print_sr_type (pp : out_channel) : sr_t -> 'a = function
  | SRmul -> fprintf pp "SRmul"
  | SRadd -> fprintf pp "SRadd"

let print_cand pp (ckey, cand) : 'a =
  if log_lvl >= 2 then
    match ckey with
    | CCM (CMop (trap, op, args)) ->
        fprintf pp "[CMop of (%a, %a)] (n=[%a], memdep=%b) / " PrintAST.print_trapping_mode trap
          (print_operation PrintRTL.reg) (op, args) print_imap cand.lhs cand.memdep
    | CCM (CMload (trap, chk, addr, args)) ->
        fprintf pp "[CMload of (%a, %s, %a)] (n=[%a], memdep=%b) / " PrintAST.print_trapping_mode
          trap (PrintAST.name_of_chunk chk)
          (PrintOp.print_addressing PrintRTL.reg)
          (addr, args) print_imap cand.lhs cand.memdep
    | CSR (sr_type, op, args) ->
        fprintf pp "[CSR of (%a, %a)], (n=[%a], memdep=false) / " print_sr_type sr_type
          (print_operation PrintRTL.reg) (op, args) print_imap cand.lhs

(** Print every predicates of every candidates in a CSV-like format
    separated with semicolumns. *)
let print_predicates (wl : OrdIS.t) (blk2id : int PTree.t) : unit =
  if log_lvl >= 2 then
    Hashtbl.iter
      (fun ckey cand ->
        let sr_ckey = is_sr_ckey ckey and real_sr_mul_cand = is_real_sr_mul_cand ckey cand in
        eprintf "%a\n" print_cand (ckey, cand);
        eprintf
          "\n\
           index;P_transp;P_injured;P_update;P_ncomp;P_xcomp;P_ndsafe;P_xdsafe;P_nusafe;P_xusafe;P_nearliest;P_xearliest;P_ndelayed;P_xdelayed;P_nlatest;P_xlatest;P_nisolated;P_xisolated;P_ninsert;P_xinsert;P_nreplace;P_xreplace;P_ncritical;P_xcritical;P_ncritinsert;P_xcritinsert;P_nsubstcrit;P_xsubstcrit\n";
        OrdIS.iter
          (fun n ->
            let index = pget (i2p n) blk2id in
            let transp = get_predicate_value cand index P_transp
            and injured = if sr_ckey then get_predicate_value cand index P_injured else false
            and update = if sr_ckey then get_predicate_value cand index P_update else false
            and ncomp = get_predicate_value cand index P_ncomp
            and xcomp = get_predicate_value cand index P_xcomp
            and ndsafe = get_predicate_value cand index P_ndsafe
            and xdsafe = get_predicate_value cand index P_xdsafe
            and nusafe = get_predicate_value cand index P_nusafe
            and xusafe = get_predicate_value cand index P_xusafe
            and nearliest = get_predicate_value cand index P_nearliest
            and xearliest = get_predicate_value cand index P_xearliest
            and ndelayed = get_predicate_value cand index P_ndelayed
            and xdelayed = get_predicate_value cand index P_xdelayed
            and nlatest = get_predicate_value cand index P_nlatest
            and xlatest = get_predicate_value cand index P_xlatest
            and nisolated = get_predicate_value cand index P_nisolated
            and xisolated = get_predicate_value cand index P_xisolated
            and ninsert = get_predicate_value cand index P_ninsert
            and xinsert = get_predicate_value cand index P_xinsert
            and nreplace = get_predicate_value cand index P_nreplace
            and xreplace = get_predicate_value cand index P_xreplace
            and ncritical =
              if real_sr_mul_cand then get_predicate_value cand index P_ncritical else false
            and xcritical =
              if real_sr_mul_cand then get_predicate_value cand index P_xcritical else false
            and ncritinsert =
              if real_sr_mul_cand then get_predicate_value cand index P_ncritinsert else false
            and xcritinsert =
              if real_sr_mul_cand then get_predicate_value cand index P_xcritinsert else false
            and nsubstcrit =
              if real_sr_mul_cand then get_predicate_value cand index P_nsubstcrit else false
            and xsubstcrit =
              if real_sr_mul_cand then get_predicate_value cand index P_xsubstcrit else false
            in
            eprintf
              "%d;\t%b;\t%b;\t%b;\t%b;\t%b;\t%b;\t%b;\t%b;\t%b;\t%b;\t%b;\t%b;\t%b;\t%b;\t%b;\t%b;\t%b;\t%b;\t%b;\t%b;\t%b;\t%b;\t%b;\t%b;\t%b;\t%b;\t%b\n"
              n transp injured update ncomp xcomp ndsafe xdsafe nusafe xusafe nearliest xearliest
              ndelayed xdelayed nlatest xlatest nisolated xisolated ninsert xinsert nreplace
              xreplace ncritical xcritical ncritinsert xcritinsert nsubstcrit xsubstcrit)
          wl)
      candidates

let print_constants () : unit =
  if log_lvl >= 2 then Hashtbl.iter (fun dst _ -> eprintf "Constant dst=%d\n" (p2i dst)) constants

let print_candidates () : unit =
  if log_lvl >= 2 then
    Hashtbl.iter (fun ckey cand -> eprintf "%a\n" print_cand (ckey, cand)) candidates

let rec print_affine_form (pp : out_channel) (tol, aff) : 'a =
  if log_lvl >= 2 then
    match aff with
    | Aff_term (a, r, aff) ->
        fprintf pp "AT[%Ld * x%d] + %a" (tol a) (p2i r) print_affine_form (tol, aff)
    | Aff_const a -> fprintf pp "AC [%Ld]\n" (tol a)

let print_affine_map (m : (P.t * P.t, 'a affine_form) Hashtbl.t) (tol : 'a -> int64) : unit =
  if log_lvl >= 2 then
    Hashtbl.iter
      (fun (blk, r) a -> eprintf "(%d, %d) |-> %a\n" (p2i blk) (p2i r) print_affine_form (tol, a))
      m
