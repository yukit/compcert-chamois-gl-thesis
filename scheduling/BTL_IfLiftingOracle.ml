(* *************************************************************)
(*                                                             *)
(*             The Compcert verified compiler                  *)
(*                                                             *)
(*           Alexandre Berard   VERIMAG, UGA                   *)
(*                                                             *)
(*  Copyright VERIMAG. All rights reserved.                    *)
(*  This file is distributed under the terms of the INRIA      *)
(*  Non-Commercial License Agreement.                          *)
(*                                                             *)
(* *************************************************************)

(** Prepass oracle extension to lift conditional branches *)

open PrepassSchedulingOracleDeps
open PrepassSchedulingOracle
open PrintBTL
open BTL
open BTLtypes
open BTL_Liveness
open RTLcommonaux  
open BTLcommonaux  
open DebugPrint
open Maps
open Registers
open BTL_BlockOptimizeraux
open Printf

module IM = Map.Make(Int)

let stat_nb_if_lift = ref 0
let stat_nb_compense = ref 0
let stat_nb_makespan_gain = ref 0

let save_stats = false

let fprint_stats_header oc () =
  fprintf oc "nb_compense, nb_if_lift, nb_makespan_gain\n"

let fprint_stats oc () =
  fprintf oc "%d, %d, %d\n" !stat_nb_compense !stat_nb_if_lift !stat_nb_makespan_gain

let write_stats () =
  let oc =
    open_out_gen
      [ Open_append; Open_creat; Open_text ]
      0o666 "/tmp/iflift_stats.csv"
  in
  fprint_stats oc ();
  close_out oc

let schedule_get_positions seqa btl ibf live_regs_entry typing va ifliftopt =
  let refcount = reference_counting seqa ibf.binfo.s_output_regs typing in
  schedule_sequence seqa btl live_regs_entry typing va refcount ifliftopt

let compute_compense_makespan bseq =
  let opweights = OpWeights.get_opweights () in
  List.fold_left (fun n inst ->
      n + (match inst with
      | Bop (op, args, _, _) ->
          opweights.latency_of_op op (List.length args)
      | Bload (trap, chunk, addr, args, _, _) ->
          opweights.latency_of_load trap chunk addr (List.length args)
      | Bstore (_, _, _, _, _) -> 0
      | _ -> 0)) 0 bseq

let if_lift n ibf btl typing va ren_map =
  if not !Clflags.option_if_lift || not !Clflags.option_fprepass then None else
  (* scheduling for two policies:
   * - one reference schedule (Compare), the schedule is constrained with register
   *   liveness for outer blocks
   * - impossible schedules (Ignore_Liveness), the scheduler is not constrained with register
   *   liveness for outer blocks. Input mem for Bcond instructions are constrained
   *   with a latency of 0 *)
  let move_pol = match !Clflags.option_if_lift_pol with
  | "movstores" -> Move_Stores
  | "passlive" -> Ignore_Liveness
  | s -> failwith ("unknown if-lifting policy: " ^ s) in

  reset_visited_ib_rec n ibf.entry;

  let retreive_compense ibf =
    let rec retrieve_goto ib =
      match ib with
      | Bseq (ib1, ib2) -> retrieve_goto ib2
      | BF (Bgoto _, _) -> ib
      | _ -> failwith "if_lift: compense code not valid" in
    let rec traverse ib =
      match ib with
      | Bseq (ib1, ib2) ->
        (match ib1 with Bcond (_, _, _, _, iinfo) ->
          (match ib2 with BF _ -> iinfo.visited <- true
                          | _ -> ())
        | _ -> ());
        let ib1' = traverse ib1 in
        let ib2' = traverse ib2 in
        Bseq (ib1', ib2')
      | Bcond (cond, args, ifso, ifnot, iinfo) ->
        Bcond (cond, args, retrieve_goto ifso, ifnot, iinfo)
      | _ -> ib in
    let ib = traverse ibf.entry in
    { entry = ib; binfo = ibf.binfo } in

  let ibf_nc = retreive_compense ibf in
  (*debug_flag := true;*)
  debug "ibf:\n";
  print_btl_block stderr ibf n;
  debug "transformed ibf:\n";
  print_btl_block stderr ibf_nc n;
  (*debug_flag := false;*)

  let bseq, olast = flatten_blk_basics ibf in
  let seqa = Array.map (fun inst -> (inst, get_liveins inst)) bseq in
  let live_regs_entry = get_live_regs_entry seqa ibf btl in

  let bseq_nc, olast_nc = flatten_blk_basics ibf_nc in
  let seqa_nc = Array.map (fun inst -> (inst, get_liveins inst)) bseq_nc in
  let live_regs_entry_nc = get_live_regs_entry seqa_nc ibf_nc btl in

  let sched_iflifted = schedule_get_positions seqa_nc btl ibf_nc live_regs_entry_nc typing va move_pol in
  let sched_ref = schedule_get_positions seqa btl ibf live_regs_entry typing va Default in

  match sched_ref, sched_iflifted with
  | Some (pr, sr), Some (pi, si) ->
      (*debug_flag := true;*)
      debug "pr:\n";
      Array.iteri (fun i i' -> debug "%d -> %d\n" i i') pr;
      debug "pi:\n";
      Array.iteri (fun i i' -> debug "%d -> %d\n" i i') pi;
      (*debug_flag := false;*)

      let ibf_ref = { entry = apply_schedule bseq olast pr; binfo = ibf.binfo } in
      (* If schedule gives same score, give the non-if-lifted one *)
      if (sr - si = 0) then
        Some (PTree.set n ibf_ref btl)
      else
      let ibf_iflifted = { entry = apply_schedule bseq olast pi; binfo = ibf.binfo } in
     
      (* construct (i -> (i_ref, i_iflift)) array *)
      let index_of e a =
        let rec index_of e a i =
          if i >= Array.length a then None
          else if e = a.(i) then Some i
          else index_of e a (i + 1) in
        index_of e a 0 in
      let get_i = Array.mapi
          (fun i _ -> (get_some @@ (index_of i pr), get_some @@ (index_of i pi))) bseq in

      (* searching for illegal moves, and construct conditional renaming dependencies *)
      let illegal_moves = ref IM.empty in
      let cond_renames = ref IM.empty in
      let m = ref IM.empty in
      Array.iteri (fun i_cond inst ->
        let inumb = (get_inumb false inst) in
        (match IM.find_opt inumb ren_map with
        | Some (r, r') -> 
            m := IM.add r r' !m
        | None -> ());
        match inst with
        | Bcond (_, _, _, _, _) ->
            cond_renames := IM.add inumb !m !cond_renames;
            let i_se, i_se' = get_i.(i_cond) in
            for i = 0 to (Array.length bseq) - 1 do
              let i_sched = pr.(i) in
              let sched_inst = bseq.(i_sched) in
              let i_dep, i_dep' = get_i.(i_sched) in
              if i_dep < i_se && i_dep' > i_se' then
                illegal_moves := IM.add i_cond 
                  (match (IM.find_opt i_cond !illegal_moves) with
                  | Some l -> sched_inst::l
                  | None -> [sched_inst]) !illegal_moves;
            done;
        | _ -> ()
      ) bseq;

      (* ratio compute *)
      let ratio = !Clflags.option_if_lift_ratio in
      if ratio > 0 && (
        let gain = sr - si in
        let total_compense_code = IM.fold (fun _ l tl -> tl @ l) !illegal_moves [] in
        let duplicated_code_length = compute_compense_makespan total_compense_code in
        if gain * ratio < duplicated_code_length then (
          (*debug_flag := true;*)
          debug "duplicated code ratio is greater, not applying if-lifting (gain:%d, length=%d)\n"
            gain duplicated_code_length;
          (*debug_flag := false;*)
          true)
        else false)
      then None
      else

      let get_dest inst =
        match inst with
        | Bop (_, _, dest, _)
        | Bload (_, _, _, _, dest, _) -> Some dest
        | Bnop _ | Bstore (_, _, _, _, _) -> None
        | _ -> failwith "if_lift: cannot get dest reg" in
      let get_args inst =
        match inst with 
        | Bop (_, args, _, _)
        | Bload (_, _, _, args, _, _) ->
            args
        | Bstore (_, _, args, src, _) ->
            (src :: args)
        | _ -> [] in
      let new_inst inst =
        match inst with
        | Bnop None ->
            Bnop None
        | Bnop (Some _) ->
            Bnop (Some (mk_iinfo undef_node None))
        | Bop (op, args, dest, _) ->
            Bop (op, args, dest, mk_iinfo undef_node None)
        | Bload (trap, chunk, addr, args, dest, iinfo) ->
            Bload (trap, chunk, addr, args, dest, mk_iinfo undef_node iinfo.opt_info)
        | Bstore (chunk, addr, args, src, _) ->
            Bstore (chunk, addr, args, src, mk_iinfo undef_node None)
        | _ -> failwith "if_lift: illegal move instruction type" in

      (* build compense blocks *)
      let compense_blocks = IM.fold (fun n l compense_blocks ->
        let next_block block inst =
          match block with
          | Some block ->
              Some (Bseq (new_inst inst, block))
          | None ->
              Some (new_inst inst) in
        let cond_inst, liveins = seqa.(n) in
        let inumb = get_inumb false cond_inst in
        let ren = match IM.find_opt inumb !cond_renames with
          | Some e -> e
          | None -> IM.empty in
        let used, block = List.fold_left (fun (used, block) inst ->
          let dest = get_dest inst in
          let args = get_args inst in
          match dest with
          | Some dest ->
              if Regset.mem dest used then
                List.fold_left (fun m r -> Regset.add r m) (Regset.remove dest used) args,
                next_block block inst
              else
                used, block
          | None ->
              List.fold_left (fun m r -> Regset.add r m) used args, next_block block inst)
          (Regset.union liveins
             (Regset.fold (fun r m -> Regset.add
                              (match IM.find_opt (p2i r) ren with | Some r' -> (i2p r') | None -> r) m)
                 liveins Regset.empty), None) l in
        match block with
        | Some b ->
          (* reschedule block *)
          let b = if !Clflags.option_schedule_compense then
            let ibf = { entry = b; binfo = ibf.binfo } in
            let bseq, olast = flatten_blk_basics ibf in
            let seqa = Array.map (fun inst -> (inst, get_liveins inst)) bseq in
            let live_regs_entry = get_live_regs_entry seqa ibf btl in
            let refcount =
              reference_counting seqa ibf.binfo.s_output_regs typing
            in
            match
              schedule_sequence seqa btl live_regs_entry typing va refcount Default
            with
            | Some (positions, _) ->
                apply_schedule bseq olast positions
            | None -> b
          else b in
          IM.add (get_inumb false cond_inst) b compense_blocks
        | None -> compense_blocks 
      ) !illegal_moves IM.empty in

      (*debug_flag := true;*)
      let nb_compense = IM.cardinal compense_blocks in
      if (nb_compense > 0) then
      (debug "default schedule makespan: %d\n" sr;
       debug " iflift schedule makespan: %d\n" si;
       stat_nb_compense := !stat_nb_compense + nb_compense;
       stat_nb_if_lift := !stat_nb_if_lift + 1;
       stat_nb_makespan_gain := !stat_nb_makespan_gain + (sr - si));
      (*debug_flag := false;*)

      let rec insert_last add code =
        match code with
        | Bseq (ib1, ib2) -> Bseq (ib1, insert_last add ib2)
        | _ -> Bseq (code, add) in

      (* rebuild if_lifted block *)
      let rec rebuild_iflifted ib =
        match ib with
        | Bcond (cond, args, ifso, ifnot, iinfo) ->
            (match IM.find_opt iinfo.inumb compense_blocks with
            | Some block -> 
                Bcond (cond, args, insert_last ifso block, ifnot, iinfo)
            | None -> ib)
        | Bseq (ib1, ib2) ->
            Bseq (rebuild_iflifted ib1, rebuild_iflifted ib2)
        | _ -> ib in

      let ibf_iflifted = { entry = rebuild_iflifted ibf_iflifted.entry; binfo = ibf_iflifted.binfo } in

      (*debug_flag := true;*)
      debug "No schedule\n";
      print_btl_block stderr ibf n;
      debug "Default schedule\n";
      print_btl_block stderr ibf_ref n;
      debug "Ignore_Liveness schedule\n";
      print_btl_block stderr ibf_iflifted n;
      (*debug_flag := false;*)

      Some (PTree.set n ibf_iflifted btl)
  | Some (pr, sr), None ->
      debug "if_lift: schedule for if lifting is not possible, keeping default schedule\n";
      let ibf_ref = { entry = apply_schedule bseq olast pr; binfo = ibf.binfo } in
      Some (PTree.set n ibf_ref btl)
  | _ ->
      debug "if_lift: no schedule applied\n";
      None
