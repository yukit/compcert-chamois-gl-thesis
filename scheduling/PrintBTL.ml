(* *************************************************************)
(*                                                             *)
(*             The Compcert verified compiler                  *)
(*                                                             *)
(*           Léo Gourdin        VERIMAG, UGA                   *)
(*                                                             *)
(*  Copyright VERIMAG. All rights reserved.                    *)
(*  This file is distributed under the terms of the INRIA      *)
(*  Non-Commercial License Agreement.                          *)
(*                                                             *)
(* *************************************************************)

(** BTL instruction printer *)

open Printf
open Camlcoq
open Datatypes
open Maps
open BTL
open PrintAST
open DebugPrint
open BTLtypes
open BTL_Invariants
open BTL_SEsimuref
open PrintOp
open RTLcommonaux

(* Printing of BTL code *)

(* options *)
let print_sval_hashcode_opt = ref true

let reg pp r = fprintf pp "x%d" (P.to_int r)

let rec regs pp = function
  | [] -> ()
  | [ r ] -> reg pp r
  | r1 :: rl -> fprintf pp "%a, %a" reg r1 regs rl

let ros pp = function
  | Coq_inl r -> reg pp r
  | Coq_inr s -> fprintf pp "\"%s\"" (extern_atom s)

let print_succ pp s = fprintf pp "\tsucc %d\n" (P.to_int s)
let print_pref pp pref = fprintf pp "%s" pref

let rec print_iblock pp is_rec pref ib =
  match ib with
  | Bnop None ->
      print_pref pp pref;
      fprintf pp "??: Bnop None\n"
  | Bnop (Some iinfo) ->
      print_pref pp pref;
      fprintf pp "%d: Bnop\n" iinfo.inumb
  | Bop (op, args, res, iinfo) ->
      print_pref pp pref;
      fprintf pp "%d: Bop: %a = %a\n" iinfo.inumb reg res (print_operation reg)
        (op, args)
  | Bload (trap, chunk, addr, args, dst, iinfo) ->
      print_pref pp pref;
      fprintf pp "%d: Bload: %a = %s[%a]%a\n" iinfo.inumb reg dst
        (name_of_chunk chunk) (print_addressing reg) (addr, args)
        print_trapping_mode trap
  | Bstore (chunk, addr, args, src, iinfo) ->
      print_pref pp pref;
      fprintf pp "%d: Bstore: %s[%a] = %a\n" iinfo.inumb (name_of_chunk chunk)
        (print_addressing reg) (addr, args) reg src
  | BF (Bcall (sg, fn, args, res, s), iinfo) ->
      print_pref pp pref;
      fprintf pp "%d: Bcall: %a = %a(%a)\n" iinfo.inumb reg res ros fn regs args;
      print_succ pp s
  | BF (Btailcall (sg, fn, args), iinfo) ->
      print_pref pp pref;
      fprintf pp "%d: Btailcall: %a(%a)\n" iinfo.inumb ros fn regs args
  | BF (Bbuiltin (ef, args, res, s), iinfo) ->
      print_pref pp pref;
      fprintf pp "%d: Bbuiltin: %a = %s(%a)\n" iinfo.inumb
        (print_builtin_res reg) res (name_of_external ef)
        (print_builtin_args reg) args;
      print_succ pp s
  | Bcond (cond, args, ib1, ib2, iinfo) ->
      print_pref pp pref;
      fprintf pp "%d: Bcond: (%a) (prediction: %s)\n" iinfo.inumb
        (print_condition reg) (cond, args)
        (match iinfo.opt_info with
        | None -> "none"
        | Some true -> "branch"
        | Some false -> "fallthrough");
      let pref' = pref ^ "  " in
      fprintf pp "%sifso = [\n" pref;
      if is_rec then print_iblock pp is_rec pref' ib1 else fprintf pp "...\n";
      fprintf pp "%s]\n" pref;
      fprintf pp "%sifnot = [\n" pref;
      if is_rec then print_iblock pp is_rec pref' ib2 else fprintf pp "...\n";
      fprintf pp "%s]\n" pref
  | BF (Bjumptable (arg, tbl), iinfo) ->
      let tbl = Array.of_list tbl in
      print_pref pp pref;
      fprintf pp "%d: Bjumptable: (%a)\n" iinfo.inumb reg arg;
      for i = 0 to Array.length tbl - 1 do
        fprintf pp "\t\tcase %d: goto %d\n" i (P.to_int tbl.(i))
      done
  | BF (Breturn None, iinfo) ->
      print_pref pp pref;
      fprintf pp "%d: Breturn\n" iinfo.inumb
  | BF (Breturn (Some arg), iinfo) ->
      print_pref pp pref;
      fprintf pp "%d: Breturn: %a\n" iinfo.inumb reg arg
  | BF (Bgoto s, iinfo) ->
      print_pref pp pref;
      fprintf pp "%d: Bgoto: %d\n" iinfo.inumb (P.to_int s)
  | Bseq (ib1, ib2) ->
      if is_rec then (
        print_iblock pp is_rec pref ib1;
        print_iblock pp is_rec pref ib2)
      else fprintf pp "Bseq...\n"

let print_btl_inst pp ib =
  if !debug_flag then print_iblock pp false "  " ib else ()

let print_btl_block pp ibf n =
  if !debug_flag then (
    fprintf pp "\n";
    fprintf pp "[BTL Liveness] ";
    print_regset ibf.binfo.input_regs;
    fprintf pp "\n";
    fprintf pp "[BTL block %d with bnumb %d]\n" (P.to_int n) ibf.binfo.bnumb;
    print_iblock pp true "  " ibf.entry;
    fprintf pp "\n")
  else ()

let sort_elts ptree = List.sort (fun (k1, _) (k2, _) -> P.compare k2 k1) (PTree.elements ptree)

let print_btl_code pp btl =
  if !debug_flag then (
    fprintf pp "\n";
    let elts = sort_elts btl in
    List.iter
      (fun (n, ibf) ->
        fprintf pp "[BTL Liveness] ";
        print_regset ibf.binfo.input_regs;
        fprintf pp "\n";
        fprintf pp "[BTL block %d with bnumb %d]\n" (P.to_int n) ibf.binfo.bnumb;
        fprintf pp "\n";
        print_iblock pp true "  " ibf.entry;
        fprintf pp "\n")
      elts;
    fprintf pp "\n")
  else ()

let destination : string option ref = ref None

let print_if passno prog =
  match !destination with
  | None -> ()
  | Some f ->
      let oc = open_out (f ^ "." ^ Z.to_string passno) in
      print_btl_code oc prog;
      close_out oc

let print_ireg pp ir =
  if ir.force_input then fprintf pp "input/%d " (P.to_int ir.regof)
  else fprintf pp "last/%d " (P.to_int ir.regof)

let rec print_ireg_list pp = function
  | [] -> ()
  | ir :: lir ->
      print_ireg pp ir;
      print_ireg_list pp lir

let print_rop pp (rop, args) =
  match rop with
  | Rop op -> fprintf pp "Rop [%a]" (print_operation reg) (op, args)
  | Rload (trap, chk, addr) ->
      fprintf pp "Rload [%a, %s, %a]" print_trapping_mode trap
        (name_of_chunk chk) (print_addressing reg) (addr, args)

let rec lir_to_lr = function [] -> [] | ir :: lir -> ir.regof :: lir_to_lr lir

let print_ival pp = function
  | Ireg ir -> fprintf pp "Ireg %a" print_ireg ir
  | Iop (rop, args) ->
      fprintf pp "Iop [%a, %a]" print_ireg_list args print_rop
        (rop, lir_to_lr args)

let rec lsval_length = function
  | Snil _ -> 0
  | Scons (_, lsv, _) -> 1 + lsval_length lsv

let fake_args lsv = List.init (lsval_length lsv) (fun _ -> i2p 1)

let print_sval_hc pp hc =
    if !print_sval_hashcode_opt
    then fprintf pp "#%d" hc
    else ()

let rec print_sval pp = function
  | Sinput (r, hc) ->
      fprintf pp "Sinput%a (%d)"
        print_sval_hc hc (P.to_int r)
  | Sop (op, lsv, hc) ->
      fprintf pp "Sop%a (%a, %a)"
        print_sval_hc hc
        (print_operation reg) (op, fake_args lsv)
        print_lsval lsv
  | Sfoldr (op, lsv, sv0, _) ->
      let fargs = [i2p 1; i2p 1] in
      fprintf pp "Sfoldr (%a, %a, %a)" (print_operation reg)
        (op, fargs) print_lsval lsv print_sval sv0
  | Sload (sm, trap, chk, addr, lsv, hc) ->
      fprintf pp "Sload%a (%a, %a, %s, %a, %a)"
        print_sval_hc hc
        print_smem sm
        print_trapping_mode trap
        (name_of_chunk chk)
        (print_addressing reg) (addr, fake_args lsv)
        print_lsval lsv

and print_lsval pp = function
  | Snil hc -> fprintf pp "%a" print_sval_hc hc
  | Scons (sv, lsv, hc) ->
      fprintf pp "Scons%a (%a, %a)" print_sval_hc hc print_sval sv print_lsval lsv

and print_smem pp = function
  | Sinit _ -> fprintf pp "Sinit"
  | Sstore (sm, chk, addr, lsv, src, _) ->
      fprintf pp "Sstore (%a, %s, %a, %a, %a)" print_smem sm (name_of_chunk chk)
        (print_addressing reg)
        (addr, fake_args lsv)
        print_lsval lsv print_sval src

let rec print_list_sval pp = function
  | [] -> ()
  | sv :: l -> fprintf pp "%a, " print_sval sv; print_list_sval pp l

let print_sreg sreg =
  List.iter
    (fun (n, sv) -> debug "%d |-> %a\n" (P.to_int n) print_sval sv)
    (PTree.elements sreg)

let print_sstate (sst: BTL_SEsimuref.ristate) =
  debug "ris_smem=%a;\n" print_smem sst.ris_smem;
  debug "ris_input_init=%b;\n" sst.ris_input_init;
  debug "si_mode=%b;\n" sst.si_mode;
  debug "ok_rsval=%a;\n" print_list_sval sst.ok_rsval;
  debug "{\nris_sreg =\n";
  print_sreg sst.ris_sreg;
  debug "\n}\n"

let print_sstates sst1 sst2 =
  debug "Symbolic state 1:\n";
  print_sstate sst1;
  debug "Symbolic state 2:\n";
  print_sstate sst2

let print_gluemap gm =
  if !debug_flag then
    let print_invmap invmap s =
      if List.length invmap.aseq > 0 then (
        debug "%s.aseq := [" s;
        List.iter
          (fun (r, iv) -> debug "(%d, %a)" (P.to_int r) print_ival iv)
          invmap.aseq;
        debug " ]\n");
      if Registers.Regset.cardinal invmap.outputs > Camlcoq.Nat.of_int 0 then (
        debug "%s.outputs := " s;
        print_regset invmap.outputs;
        debug "\n")
    in
    List.iter
      (fun (n, inv) ->
        debug "Invariants for block %d\n" (P.to_int n);
        print_invmap inv.glue "glue";
        print_invmap inv.history "hist")
      (sort_elts gm)
