(* *************************************************************)
(*                                                             *)
(*             The Compcert verified compiler                  *)
(*                                                             *)
(*           Léo Gourdin        VERIMAG, UGA                   *)
(*           Sylvain Boulmé     Grenoble-INP, VERIMAG          *)
(*                                                             *)
(*  Copyright VERIMAG. All rights reserved.                    *)
(*  This file is distributed under the terms of the INRIA      *)
(*  Non-Commercial License Agreement.                          *)
(*                                                             *)
(* *************************************************************)

Require Import Coqlib Maps.
Require Import AST.
Require Import RTL Registers OptionMonad BTL.

Require Import Errors Linking BTL_SEtheory BTL_SEsimuref BTL_SEimpl_check.

Require ValueDomain ValueAnalysis.

Module Type BTL_BlockSimulationConfig.

  (** External oracle that can be configured according to the optimization mode *)
  Parameter btl_optim_oracle: BTL.function -> BTL.code * function_info * gluemap.
  (** Set of rewriting rules to use in SE *)
  Parameter btl_rrules: unit -> rrules_set.

End BTL_BlockSimulationConfig.

Module BTL_BlockSimulation (B: BTL_BlockSimulationConfig).

Export B.

(* a specification of the verification to do on each function *)
Record match_function (f1 f2: BTL.function) : Prop := {
  preserv_fnsig: fn_sig f1 = fn_sig f2;
  preserv_fnparams: fn_params f1 = fn_params f2;
  preserv_fnstacksize: fn_stacksize f1 = fn_stacksize f2;
  preserv_entrypoint: fn_entrypoint f1 = fn_entrypoint f2;
  trivial_histinv_entrypoint: only_liveness (history (f2.(fn_gm) (fn_entrypoint f1)));
  trivial_glueinv_entrypoint: only_liveness (glue (f2.(fn_gm) (fn_entrypoint f1)));
  match_sexec_ok: forall pc ib1, (fn_code f1)!pc = Some ib1 ->
                    exists ib2, (fn_code f2)!pc = Some ib2 /\ instantiate_context match_sexec_si f2.(fn_gm) (entry ib1) (entry ib2) pc;
}.

Inductive match_fundef: fundef -> fundef -> Prop :=
  | match_Internal f f': match_function f f' -> match_fundef (Internal f) (Internal f')
  | match_External ef: match_fundef (External ef) (External ef).

Local Open Scope option_monad_scope.

Inductive match_stackframes (ge: genv): stackframe -> stackframe -> Prop :=
  | match_stackframe_intro 
      sp res f pc rs1 rs2 f'
      (TRANSF: match_function f f')
      (MATCHI: forall v m, match_invs (Bcctx ge sp (rs1#res <- v) m) (f'.(fn_gm) pc) (rs2#res <- v))
      : match_stackframes ge (BTL.Stackframe res f sp pc rs1) (BTL.Stackframe res f' sp pc rs2).

Inductive match_states (ge: genv): state -> state -> Prop :=
  | match_states_intro 
      st f pc sp rs1 rs2 m st' f'
      (TRANSF: match_function f f')
      (MATCHI: match_invs (Bcctx ge sp rs1 m) (f'.(fn_gm) pc) rs2)
      (STACKS: list_forall2 (match_stackframes ge) st st')
      : match_states ge (State st f sp pc rs1 m) (State st' f' sp pc rs2 m)
  | match_states_call
      st st' f f' args m
      (STACKS: list_forall2 (match_stackframes ge) st st')
      (TRANSF: match_fundef f f')
      : match_states ge (Callstate st f args m) (Callstate st' f' args m)
  | match_states_return
      st st' v m
      (STACKS: list_forall2 (match_stackframes ge) st st')
      : match_states ge (Returnstate st v m) (Returnstate st' v m)
   .
Local Hint Resolve match_invs_eqlive: core.

Lemma match_stackframes_eqlive ge stf1 stf2:
  match_stackframes ge stf1 stf2 ->
  forall stf3, eqlive_stackframes stf2 stf3 ->
  match_stackframes ge stf1 stf3.
Proof.
  destruct 1; intros stf3 EQLIVE; inv EQLIVE; intuition subst.
  simpl in *; econstructor; eauto.
Qed.

Local Hint Resolve match_stackframes_eqlive: core.

Lemma match_stacks_eqlive ge stk1 stk2:
  list_forall2 (match_stackframes ge) stk1 stk2 ->
  forall stk3, list_forall2 eqlive_stackframes stk2 stk3 ->
  list_forall2 (match_stackframes ge) stk1 stk3.
Proof.
  induction 1; intros stk3 EQLIVE; inv EQLIVE; econstructor; eauto.
Qed.

Local Hint Resolve match_stacks_eqlive: core.


Lemma match_states_eqlive ge s1 s2:
  match_states ge s1 s2 ->
  forall s3, eqlive_states s2 s3 ->
  match_states ge s1 s3.
Proof.
  destruct 1; intros s3 EQLIVE; inv EQLIVE; intuition subst;
  econstructor; eauto.
Qed.

Local Open Scope error_monad_scope.
Local Open Scope lazy_bool_scope.

Definition check_only_liveness f: res unit :=
  if test_only_liveness (history (fn_gm f (fn_entrypoint f))) &&& test_only_liveness (glue (fn_gm f (fn_entrypoint f)))
  then OK tt
  else Error (msg "check_only_liveness: some non-liveness-only invariant at entrypoint").

Fixpoint check_symbolic_simu_rec (f1 f2: BTL.function) (lpc: list node): res unit :=
  match lpc with
  | nil => OK tt
  | pc :: lpc' =>
      match (fn_code f1)!pc, (fn_code f2)!pc with
      | Some ibf1, Some ibf2 =>
          do dummy <- simu_check (btl_rrules tt) f1 f2 ibf1 ibf2 (fn_gm f2) pc;
          check_symbolic_simu_rec f1 f2 lpc'
      | _, _ => Error (msg "check_symbolic_simu_rec: code tree mismatch")
      end
  end.

Definition check_symbolic_simu (f1 f2: BTL.function): res unit :=
  check_symbolic_simu_rec f1 f2 (List.map (fun elt => fst elt) (PTree.elements (fn_code f1))).

Definition transf_function (f: BTL.function) :=
  let (tcfi, gm) := btl_optim_oracle f in
  let (tc, fi) := tcfi in
  let tf := BTL.mkfunction (fn_sig f) (fn_params f) (fn_stacksize f) tc (fn_entrypoint f) gm fi in
  do dummy1 <- check_only_liveness tf;
  do dummy2 <- check_symbolic_simu f tf;
  OK tf.

Local Hint Resolve test_only_liveness_correct: core.

Lemma transf_function_only_liveness f tf:
  transf_function f = OK tf ->
  only_liveness (history (tf.(fn_gm) (fn_entrypoint f))) 
  /\ only_liveness (glue (tf.(fn_gm) (fn_entrypoint f))).
Proof.
  unfold transf_function.
  destruct (btl_optim_oracle f).
  unfold Errors.bind in *; unfold check_only_liveness; repeat autodestruct; simpl.
  intros _ EQ1 EQ2 _ TF; monadInv TF; simpl.
  intuition eauto.
Qed.

Lemma check_symbolic_simu_rec_correct lpc: forall f1 f2 x,
  check_symbolic_simu_rec f1 f2 lpc = OK x ->
  forall pc ib1, (fn_code f1)!pc = Some ib1 /\ In pc lpc ->
    exists ib2, (fn_code f2)!pc = Some ib2
    /\ instantiate_context match_sexec_si (fn_gm f2) (entry ib1) (entry ib2) pc.
Proof.
  induction lpc; simpl; intros f1 f2 x X pc ib1 (PC & HIN); try contradiction.
  destruct HIN; subst.
  - rewrite PC in X; destruct ((fn_code f2)!pc); monadInv X.
    exists i; split; auto. destruct x0. eapply simu_check_correct; eauto.
  - destruct ((fn_code f1)!a), ((fn_code f2)!a); monadInv X.
    eapply IHlpc; eauto.
Qed.

Lemma check_symbolic_simu_correct x f1 f2:
  check_symbolic_simu f1 f2 = OK x ->
  forall pc ib1, (fn_code f1)!pc = Some ib1 ->
    exists ib2, (fn_code f2)!pc = Some ib2
    /\ instantiate_context match_sexec_si (fn_gm f2) (entry ib1) (entry ib2) pc.
Proof.
  unfold check_symbolic_simu; intros X pc ib1 PC.
  eapply check_symbolic_simu_rec_correct; intuition eauto.
  apply PTree.elements_correct in PC. eapply (in_map fst) in PC; auto.
Qed.

Definition transf_fundef (f: fundef) : res fundef :=
  transf_partial_fundef (fun f => transf_function f) f.

Definition transf_program (p: program) : res program :=
  transform_partial_program transf_fundef p.

End BTL_BlockSimulation.
