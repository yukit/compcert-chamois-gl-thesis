(* *************************************************************)
(*                                                             *)
(*             The Compcert verified compiler                  *)
(*                                                             *)
(*           Léo Gourdin        VERIMAG, UGA                   *)
(*                                                             *)
(*  Copyright VERIMAG. All rights reserved.                    *)
(*  This file is distributed under the terms of the INRIA      *)
(*  Non-Commercial License Agreement.                          *)
(*                                                             *)
(* *************************************************************)

(** Translation from RTL to BTL and block selection *)

open Maps
open RTL
open BTL
open PrintBTL
open RTLcommonaux
open DebugPrint
open BTLtypes
open BTLcommonaux
open Registers

(* adapted from Linearizeaux.get_join_points *)
let get_join_points code entry =
  let len = List.length (PTree.elements code) in
  let reached = Bitv.create len false in
  let reached_twice = Bitv.create len false in
  let rec traverse pc =
    let ipc = p2id pc in
    if Bitv.get reached ipc then (
      if not (Bitv.get reached_twice ipc) then Bitv.set reached_twice ipc true)
    else (
      Bitv.set reached ipc true;
      let inst = pget pc code in
      let succs = plist2IS (successors_instr inst) in
      traverse_succs succs)
  and traverse_succs succs = OrdIS.iter (fun pc -> traverse (i2p pc)) succs in
  traverse entry;
  reached_twice

let make_synth_node code split_critical_edges join_points succ =
  if split_critical_edges && Bitv.get join_points (p2id succ) then (
    let nsucc = n2pi () and ib_synth = BF (Bgoto succ, def_iinfo ()) in
    let ibf_synth = mk_ibinfo ib_synth (def_binfo ()) in
    code := PTree.set nsucc ibf_synth !code;
    nsucc)
  else succ

let encaps_final code inst osucc split_critical_edges join_points =
  let encaps_final_aux inst =
    let succ = get_some @@ osucc in
    let nsucc = make_synth_node code split_critical_edges join_points succ in
    Bseq (inst, BF (Bgoto nsucc, def_iinfo ()))
  in
  match inst with
  | BF _ ->
      let inst = ref inst in
      (if split_critical_edges then
       let succs = plist2IS (successors_instr_btl !inst) in
       OrdIS.iter
         (fun succ ->
           let nsucc =
             make_synth_node code split_critical_edges join_points (i2p succ)
           in
           inst := change_final_successor_btl (i2p succ) nsucc !inst)
         succs);
      !inst
  | Bcond (cond, lr, BF (Bgoto ifso, iinfo1), Bnop None, iinfo2) ->
      let nsucc = make_synth_node code split_critical_edges join_points ifso in
      let inst =
        Bcond (cond, lr, BF (Bgoto nsucc, iinfo1), Bnop None, iinfo2)
      in
      encaps_final_aux inst
  | _ -> encaps_final_aux inst

let translate_inst code (iinfo : BTL.inst_info) inst is_final
    split_critical_edges join_points =
  let osucc = ref None in
  let btli =
    match inst with
    | Inop s ->
        osucc := Some s;
        Bnop (Some iinfo)
    | Iop (op, lr, rd, s) ->
        osucc := Some s;
        Bop (op, lr, rd, iinfo)
    | Iload (trap, chk, addr, lr, rd, s) ->
        osucc := Some s;
        Bload (trap, chk, addr, lr, rd, iinfo)
    | Istore (chk, addr, lr, src, s) ->
        osucc := Some s;
        Bstore (chk, addr, lr, src, iinfo)
    | Icall (sign, fn, lr, rd, s) -> BF (Bcall (sign, fn, lr, rd, s), iinfo)
    | Itailcall (sign, fn, lr) -> BF (Btailcall (sign, fn, lr), iinfo)
    | Ibuiltin (ef, lr, rd, s) -> BF (Bbuiltin (ef, lr, rd, s), iinfo)
    | Icond (cond, lr, ifso, ifnot, info) ->
        osucc := Some ifnot;
        iinfo.opt_info <- info;
        Bcond (cond, lr, BF (Bgoto ifso, def_iinfo ()), Bnop None, iinfo)
    | Ijumptable (arg, tbl) -> BF (Bjumptable (arg, tbl), iinfo)
    | Ireturn oreg -> BF (Breturn oreg, iinfo)
  in
  if is_final then
    encaps_final code btli !osucc split_critical_edges join_points
  else btli

let no_join_point_successor join_points = function
  | Some ps -> if Bitv.get join_points (p2id ps) then None else Some ps
  | None -> None

let translate_function code entry join_points mk_basicblocks
    split_critical_edges =
  let clen = List.length (PTree.elements code) in
  let reached = Bitv.create clen false
  and btl_code = ref PTree.empty
  and dm = ref PTree.empty in
  let rec build_btl_tree e =
    if Bitv.get reached (p2id e) then ()
    else (
      Bitv.set reached (p2id e) true;
      let next_nodes = ref [] in
      let rec build_btl_block n =
        let end_block inst iinfo succs =
          next_nodes := succs @ !next_nodes;
          translate_inst btl_code iinfo inst true split_critical_edges
            join_points
        in
        let inst = get_some @@ PTree.get n code in
        let psucc = predicted_successor inst in
        let iinfo = mk_iinfo (p2i n) None in
        let succ = no_join_point_successor join_points psucc in
        match succ with
        | Some s -> (
            match inst with
            | Icond (cond, lr, ifso, ifnot, info) ->
                if mk_basicblocks then end_block inst iinfo [ ifso; ifnot ]
                else (
                  (* Some BTL superblocks passes expect all predicted
                     branches (-fpredict) to be oriented in the same direction;
                     such that the predicted branch is the else branch (fallthrough).
                     The below assertion ensures this constraint. *)
                  assert (s = ifnot);
                  next_nodes :=
                    !next_nodes @ non_predicted_successors inst psucc;
                  Bseq
                    ( translate_inst btl_code iinfo inst false
                        split_critical_edges join_points,
                      build_btl_block s ))
            | _ ->
                Bseq
                  ( translate_inst btl_code iinfo inst false
                      split_critical_edges join_points,
                    build_btl_block s ))
        | None -> end_block inst iinfo (successors_instr inst)
      in
      let ib = build_btl_block e and rempty = Regset.empty in
      let ibf = mk_binfo (p2i e) rempty rempty [] [] |> mk_ibinfo ib in
      btl_code := PTree.set e ibf !btl_code;
      dm := PTree.set e e !dm;
      List.iter build_btl_tree !next_nodes)
  in
  build_btl_tree entry;
  let new_entry =
    make_synth_node btl_code split_critical_edges join_points entry
  in
  (!btl_code, new_entry, !dm)

(** Main translation function, that can either build basic-blocks or
 superblocks, and that can optionnaly insert synthetic nodes *)
let rtl2btl (f : RTL.coq_function) btl_bb btl_sn =
  let rtl = f.RTL.fn_code and entry_rtl = f.fn_entrypoint in
  let join_points = get_join_points rtl entry_rtl
  and typing = get_ok "RTLtoBTLaux.rtl2btl" (RTLtyping.type_function f) in
  find_last_node (PTree.elements rtl);
  let btl_trans, entry_trans, dm_trans =
    translate_function rtl entry_rtl join_points btl_bb btl_sn
  in
  let (btl_ren, entry_ren), dm_ren =
    BTL_Renumber.renumber btl_trans entry_trans
  in
  (*debug_flag := true;*)
  debug "Entry trans %d\n" (p2i entry_trans);
  debug "Entry ren %d\n" (p2i entry_ren);
  debug "RTL Code: ";
  print_code rtl;
  debug "BTL Code:\n";
  print_btl_code stderr btl_ren;
  debug "Dupmap trans:\n";
  print_ptree print_pint dm_trans;
  debug "Dupmap ren:\n";
  print_ptree print_pint dm_ren;
  let dm_merge =
    PTree.fold
      (fun nm old_pos new_pos ->
        match PTree.get old_pos dm_trans with
        | Some n -> PTree.set new_pos n nm
        | None -> nm)
      dm_ren PTree.empty
  in
  debug "Dupmap merge:\n";
  print_ptree print_pint dm_ren;
  debug "\n";
  (*debug_flag := false;*)
  (((btl_ren, entry_ren), mk_finfo typing btl_bb), dm_merge)

(** Translate from RTL to BTL with basic-blocks *)
let rtl2btl_BB (f : RTL.coq_function) = rtl2btl f true false

(** Translate from RTL to BTL with basic-blocks and synthetic nodes *)
let rtl2btl_BBSN (f : RTL.coq_function) = rtl2btl f true true

(** Translate from RTL to BTL with superblocks *)
let rtl2btl_SB (f : RTL.coq_function) = rtl2btl f false false
