(** Common type definitions for prepass scheduling *)

type called_function = (Registers.reg, AST.ident) Datatypes.sum
  
type opweights =
  {
    pipelined_resource_bounds : int array;
    nr_non_pipelined_units : int;
    latency_of_op : Op.operation -> int -> int;
    resources_of_op : Op.operation -> int -> int array;
    non_pipelined_resources_of_op : Op.operation -> int -> int array;
    latency_of_load : AST.trapping_mode -> AST.memory_chunk -> Op.addressing -> int -> int;
    resources_of_load : AST.trapping_mode -> AST.memory_chunk -> Op.addressing -> int -> int array;
    resources_of_store : AST.memory_chunk -> Op.addressing -> int -> int array;
    resources_of_cond : Op.condition -> int -> int array;
    latency_of_call : AST.signature -> called_function -> int;
    resources_of_call : AST.signature -> called_function -> int array;
    resources_of_builtin : AST.external_function -> int array
  };;
