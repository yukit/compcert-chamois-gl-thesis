(* *************************************************************)
(*                                                             *)
(*             The Compcert verified compiler                  *)
(*                                                             *)
(*           Léo Gourdin        VERIMAG, UGA                   *)
(*           Sylvain Boulmé     Grenoble-INP, VERIMAG          *)
(*                                                             *)
(*  Copyright VERIMAG. All rights reserved.                    *)
(*  This file is distributed under the terms of the INRIA      *)
(*  Non-Commercial License Agreement.                          *)
(*                                                             *)
(* *************************************************************)

(** Refinement of BTL_SEtheory data-structures
    in order to introduce (and prove correct) a lower-level specification of the simulation test.
*)

Require Import Coqlib Maps Floats.
Require Import AST Integers Values Events Memory Globalenvs Smallstep.
Require Import Op Registers.
Require Import OptionMonad BTL_SEtheory.
Import SvalNotations.
Import HConsing.

Local Open Scope option_monad_scope.
Local Open Scope lazy_bool_scope.

Local Hint Resolve OK_PRE OK_SMEM OK_SREG: core.
Local Hint Constructors sis_ok: core.

(* enumeration to control the set of rewriting rules used *)
Inductive rrules_set: Type := RRexpansions | RRlct | RRnone.
(** Getters and setters for hid *)

Definition sval_get_hid (sv: sval): hashcode :=
  match sv with
  | Sinput _ hid => hid
  | Sop _ _ hid => hid
  | Sfoldr _ _ _ hid => hid
  | Sload _ _ _ _ _ hid => hid
  end.

Definition list_sval_get_hid (lsv: list_sval): hashcode :=
  match lsv with
  | Snil hid => hid
  | Scons _ _ hid => hid
  end.

Definition smem_get_hid (sm: smem): hashcode :=
  match sm with
  | Sinit hid => hid
  | Sstore _ _ _ _ _ hid => hid
  end.

Definition sval_set_hid (sv: sval) (hid: hashcode): sval :=
  match sv with
  | Sinput r _ => Sinput r hid
  | Sop o lsv _ => Sop o lsv hid
  | Sfoldr op lsv sv0 _ => Sfoldr op lsv sv0 hid
  | Sload sm trap chunk addr lsv _ => Sload sm trap chunk addr lsv hid
  end.

Definition list_sval_set_hid (lsv: list_sval) (hid: hashcode): list_sval :=
  match lsv with
  | Snil _ => Snil hid
  | Scons sv lsv _ => Scons sv lsv hid
  end.

Definition smem_set_hid (sm: smem) (hid: hashcode): smem :=
  match sm with
  | Sinit _ => Sinit hid
  | Sstore sm chunk addr lsv srce _ => Sstore sm chunk addr lsv srce hid
  end.

Lemma sval_set_hid_correct ctx x y:
  sval_set_hid x unknown_hid = sval_set_hid y unknown_hid ->
  sval_equiv ctx x y.
Proof.
  destruct x, y; intro H; inversion H; subst; simpl; auto.
Qed.
Global Hint Resolve sval_set_hid_correct: core.

Lemma list_sval_set_hid_correct ctx x y:
  list_sval_set_hid x unknown_hid = list_sval_set_hid y unknown_hid ->
  list_sval_equiv ctx x y.
Proof.
  destruct x, y; intro H; inversion H; subst; simpl; auto.
Qed.
Global Hint Resolve list_sval_set_hid_correct: core.

Lemma smem_set_hid_correct ctx x y:
  smem_set_hid x unknown_hid = smem_set_hid y unknown_hid ->
  smem_equiv ctx x y.
Proof.
  destruct x, y; intro H; inversion H; subst; simpl; auto.
Qed.
Global Hint Resolve smem_set_hid_correct: core.

(** * refined (symbolic) internal state and abstract operators over states *)

Record ristate := 
  { 
    (** [ris_smem] represents the current smem symbolic evaluations.
        (we also recover the history of smem in ris_smem)  *)
    ris_smem:> smem;
    (** For the values in registers:
        1) we store a list [ok_rsval] of sval evaluations
        2) we encode the symbolic regset by a PTree [ris_sreg_get]
           + a boolean [ris_input_init] indicating the default option sval
        See [ris_sreg_get] below.
     *)
    ris_input_init: bool;
    si_mode: bool;
    ok_rsval: list sval;
    ris_sreg: PTree.t sval
  }.

Definition ris_sreg_get (ris: ristate) r: option sval :=
   match ris.(ris_sreg)!r with
   | None => ASSERT ris_input_init ris IN Some (fSinput r)
   | Some sv => Some sv
   end.

Coercion ris_sreg_get: ristate >-> Funclass.

Definition ris_sreg_set (ris: ristate) (sreg: PTree.t sval): ristate :=
  {| ris_smem := ris_smem ris;
     ris_input_init := ris_input_init ris;
     si_mode := si_mode ris;
     ok_rsval := ok_rsval ris;
     ris_sreg := sreg |}.

Definition ris_input_false (ris: ristate): ristate :=
  {| ris_smem := ris.(ris_smem);
     ris_input_init := false;
     si_mode := si_mode ris;
     ok_rsval := ris.(ok_rsval);
     ris_sreg := ris_sreg ris |}.

Definition ris_si_mode_true (ris: ristate): ristate :=
  {| ris_smem := ris.(ris_smem);
     ris_input_init := ris_input_init ris;
     si_mode := true;
     ok_rsval := ris.(ok_rsval);
     ris_sreg := ris_sreg ris |}.

Definition ris_sreg_ok_set (ris: ristate) sreg rsvals: ristate :=
  {| ris_smem := ris.(ris_smem);
     ris_input_init := ris_input_init ris;
     si_mode := si_mode ris;
     ok_rsval := rsvals;
     ris_sreg := sreg |}.

Definition rset_smem rm (ris:ristate): ristate :=
  {| ris_smem := rm;
     ris_input_init := ris_input_init ris;
     si_mode := si_mode ris;
     ok_rsval := ok_rsval ris;
     ris_sreg := ris_sreg ris |}.

Record ris_ok ctx (ris: ristate) : Prop := {
  OK_RMEM: (eval_smem ctx (ris_smem ris)) <> None;
  OK_RREG: forall sv, List.In sv (ok_rsval ris) -> eval_sval ctx sv <> None
}.
Local Hint Resolve OK_RMEM OK_RREG: core.
Local Hint Constructors ris_ok: core.

Lemma ris_ok_preserved ctx ris:
  ris_ok (bcctx1 ctx) ris <-> ris_ok (bcctx2 ctx) ris.
Proof.
  split; intros H; inv H; econstructor; eauto;
  try (rewrite smem_eval_preserved in *; auto);
  try (intros; (rewrite eval_sval_preserved || rewrite <- eval_sval_preserved); auto).
Qed.

Lemma ris_ok_preserv_sim ctx ris:
  ris_ok ctx (ris_si_mode_true ris) <-> ris_ok ctx ris.
Proof.
  split; intros H; inv H; econstructor; eauto.
Qed.

Lemma ris_ok_set_sreg ctx ris sreg:
  ris_ok ctx ris <-> ris_ok ctx (ris_sreg_set ris sreg).
Proof.
  split; intro OK; inv OK; econstructor; auto.
Qed.

Lemma ris_sreg_set_incl_ok ctx ris ris' sreg:
  ris_smem ris = ris_smem ris' ->
  incl (ok_rsval ris') (ok_rsval ris) ->
  ris_ok ctx (ris_sreg_set ris sreg) ->
  ris_ok ctx ris'.
Proof.
  intros MEM_EQ INCL ROK; inv ROK; constructor; [ rewrite <- MEM_EQ |]; auto.
Qed.

Lemma ris_sreg_set_access (ris: ristate) (sreg: PTree.t sval) r rsval:
  ris_sreg_ok_set ris sreg rsval r = ris_sreg_set ris sreg r.
Proof.
  unfold ris_sreg_set, ris_sreg_get; simpl.
  reflexivity.
Qed.

Lemma ris_input_false_ok ctx ris:
  ris_ok ctx ris <-> ris_ok ctx (ris_input_false ris).
Proof.
  split; intros ROK; inv ROK; constructor; auto.
Qed.

Ltac simplify_ctx :=
  try erewrite <- !sis_ok_preserved in *;
  try erewrite <- !ris_ok_preserved in *;
  try erewrite <- !smem_eval_preserved in *;
  try erewrite <- !eval_sval_preserved in *;
  try erewrite <- !eval_scondition_preserved in *;
  try erewrite <- !sis_pre_preserved in *.

(** * refinement of (symbolic) internal states *)

(** NOTE that this refinement relation *cannot* be decomposed into a abstraction function of type 
    ristate -> sistate & an equivalence relation on istate.

    Indeed, any [sis] satisfies [forall ctx r, sis_ok ctx sis -> eval_sval ctx (sis r) <> None].
    whereas this is generally not true for [ris] that
    [forall ctx r, ris_ok ctx ris -> eval_sval ctx (ris r) <> None], 
    except when, precisely, [ris_refines ris sis].

    An alternative design enabling to define ris_refines as the composition of an equivalence on
    sistate and a abstraction function would consist in constraining sistate
    with an additional [wf] field:

    Record sistate := 
     { sis_pre: iblock_exec_context -> Prop; 
       sis_sreg:> reg -> sval;
       sis_smem: smem;
       si_wf: forall ctx, sis_pre ctx -> sis_smem <> None /\ forall r, sis_sreg r <> None 
    }.

    Such a constraint should also appear in [ristate].
    This is not clear whether this alternative design would be really simpler.

*)

Definition ris_empty := {| ris_smem := fSinit;
                           ris_input_init := true;
                           si_mode := false;
                           ok_rsval := nil;
                           ris_sreg := PTree.empty _ |}.

Record ris_refines ctx (ris: ristate) (sis: sistate): Prop := {
  OK_EQUIV: sis_ok ctx sis <-> ris_ok ctx ris;
  MEM_EQ: ris_ok ctx ris ->  eval_smem ctx ris.(ris_smem) = eval_smem ctx sis.(sis_smem);
  ALIVE_EQ: ris_ok ctx ris -> forall r, ris r = None <-> sis r = None;
  REG_EQ: ris_ok ctx ris -> forall r, osval_equiv ctx (ris r) (sis r)
}.

Lemma ris_ok_empty ctx:
  ris_ok ctx ris_empty.
Proof.
  unfold ris_empty; constructor; simpl; congruence.
Qed.

Lemma ris_refines_empty ctx:
  ris_refines ctx ris_empty sis_empty.
Proof.
  unfold ris_empty, sis_empty; constructor; auto; simpl.
  - split; intros OK; inv OK; constructor; simpl in *; auto.
    intros r sv H; inv H; simpl; congruence.
  - intros; unfold ris_sreg_get; simpl; split; auto.
Qed.
Global Hint Resolve ris_ok_empty ris_refines_empty: sempty.

Local Hint Resolve OK_EQUIV MEM_EQ REG_EQ: core.
Local Hint Constructors ris_refines: core.
Local Hint Unfold build_frame: core.
Local Hint Resolve build_frame_intro: core.

Lemma ris_refines_preserved ctx ris sis:
  ris_refines (bcctx1 ctx) ris sis <-> ris_refines (bcctx2 ctx) ris sis.
Proof.
  intuition; econstructor; inv H; simplify_ctx; auto.
  all:
    intros H r; specialize REG_EQ0 with r; apply REG_EQ0 in H;
    unfold eval_osv in *; intros; repeat autodestruct; simplify_ctx; auto.
Qed.

Lemma ris_refines_preserv_sim ctx ris sis:
  ris_refines ctx (ris_si_mode_true ris) sis <-> ris_refines ctx ris sis.
Proof.
  intuition; econstructor; inv H; unfold ris_si_mode_true in *; simpl in *; auto.
  all: try rewrite <- ris_ok_preserv_sim in *; auto.
Qed.

Lemma eval_osv_match_sreg ctx si1 si2:
  (forall r, si1 r = None -> si2 r = None) ->
  (forall r, osval_equiv ctx (si1 r) (si2 r)) ->
  forall rs, match_sreg ctx si1 rs -> match_sreg ctx si2 rs.
Proof.
  unfold match_sreg; intros ALIVE EVAL rs MATCH r sv H.
  generalize (EVAL r); intros EVALr; try_simplify_someHyps.
  intros; rewrite <- EVALr.
  unfold eval_osv; autodestruct; simpl in *; try_simplify_someHyps.
  intros; exploit ALIVE; eauto. congruence.
Qed.

Lemma ris_refines_build_frame ctx ris sis:
  ris_refines ctx ris sis ->
  ris_ok ctx ris -> 
  forall r, (build_frame ris r) <-> (build_frame sis r).
Proof.
  unfold build_frame; intros REF OK r. erewrite ALIVE_EQ; eauto; intuition.
Qed.

Lemma ris_refines_match_sreg ctx ris sis:
  ris_refines ctx ris sis ->
  ris_ok ctx ris ->
  forall rs, match_sreg ctx ris rs <-> match_sreg ctx sis rs.
Proof.
  intros REF OK0 rs. split; eapply eval_osv_match_sreg.
  - intros; erewrite <- ALIVE_EQ; eauto.
  - intros; erewrite REG_EQ; eauto.
  - intros; erewrite ALIVE_EQ; eauto.
  - intros; erewrite REG_EQ; eauto.
Qed.

Lemma ris_refines_ok ctx ris sis:
  ris_refines ctx ris sis ->
  ris_ok ctx ris ->
  sreg_ok ctx ris.
Proof.
  intros REF OK0 r sv H1 H2.
  exploit REG_EQ; eauto. 
  erewrite H1; simpl.
  generalize OK0.
  erewrite <- OK_EQUIV; eauto.
  erewrite H2. intros;  destruct (sis r) eqn: SISr; simpl in *; try congruence.
  + exploit OK_SREG; eauto.
  + erewrite <- ALIVE_EQ in SISr; eauto. congruence.
Qed.
Local Hint Resolve ris_refines_ok: core.

(** * Properties of the (abstract) operators involved in symbolic execution *)

Lemma ok_set_sreg ctx r sv sis:
  sis_ok ctx (set_sreg r sv sis)
  <-> (sis_ok ctx sis /\ eval_sval ctx sv <> None).
Proof.
  unfold set_sreg; split.
  + intros [(SVAL & PRE) SMEM SREG]; simpl in *; split.
    - econstructor; eauto.
      intros r0; generalize (SREG r0); destruct (Pos.eq_dec r r0);
      intuition subst. specialize (SVAL sv0); auto.
    - generalize (SREG r sv); destruct (Pos.eq_dec r r); auto.
  + intros ([PRE SMEM SREG] & SVAL).
    econstructor; simpl; eauto.
    intros r0; destruct (Pos.eq_dec r r0); try congruence.
    intros; specialize (SREG r0 sv0); auto.
Qed.

Lemma ok_set_mem ctx sm sis:
  sis_ok ctx (set_smem sm sis)
  <-> (sis_ok ctx sis /\ eval_smem ctx sm <> None).
Proof.
  split; destruct 1; econstructor; simpl in *; eauto.
  intuition eauto.
Qed.

Lemma ok_rset_mem ctx rm (ris: ristate):
  (eval_smem ctx ris.(ris_smem) = None -> eval_smem ctx rm = None) ->
  ris_ok ctx (rset_smem rm ris)
  <-> (ris_ok ctx ris /\ eval_smem ctx rm <> None).
Proof.
   split; destruct 1; econstructor; simpl in *; eauto.
Qed.

Lemma rset_mem_correct ctx rm sm ris sis:
  (eval_smem ctx ris.(ris_smem) = None -> eval_smem ctx rm = None) ->
  ris_refines ctx ris sis ->
  (ris_ok ctx ris -> smem_equiv ctx rm sm) ->
  ris_refines ctx (rset_smem rm ris) (set_smem sm sis).
Proof.
  destruct 2; intros.
  econstructor; eauto;
  try (rewrite ok_rset_mem; intuition eauto; fail).
  rewrite ok_set_mem, ok_rset_mem; intuition congruence.
Qed.

(** * refinement of (symbolic) final values *)

Inductive rsvident_refines ctx: (sval + ident) -> (sval + ident) -> Prop :=
  | RefLeft rsv sv
     (REF: sval_equiv ctx rsv sv)
     :rsvident_refines ctx (inl rsv) (inl sv)
  | RefRight id1 id2
     (IDSIMU: id1 = id2)
     :rsvident_refines ctx (inr id1) (inr id2)
  .

Lemma rsvident_refines_preserved ctx rvos ros:
  rsvident_refines (bcctx1 ctx) rvos ros <-> rsvident_refines (bcctx2 ctx) rvos ros.
Proof.
  split; intros H; inv H; econstructor; simplify_ctx; eauto.
Qed.

Inductive optrsv_refines ctx: (option sval) -> (option sval) -> Prop :=
  | RefSome rsv sv
     (REF: sval_equiv ctx rsv sv)
     :optrsv_refines ctx (Some rsv) (Some sv)
  | RefNone: optrsv_refines ctx None None
  .

Definition bargs_refines ctx
  (rargs: list (builtin_arg sval)) (args: list (builtin_arg sval)): Prop :=
  eval_list_builtin_sval ctx rargs = eval_list_builtin_sval ctx args.

Inductive rfv_refines ctx: sfval -> sfval -> Prop :=
  | RefGoto pc: rfv_refines ctx (Sgoto pc) (Sgoto pc)
  | RefCall sig rvos ros rargs args r pc
      (SV: rsvident_refines ctx rvos ros)
      (LIST: list_sval_equiv ctx rargs args)
      :rfv_refines ctx (Scall sig rvos rargs r pc) (Scall sig ros args r pc)
  | RefTailcall sig rvos ros rargs args
      (SV: rsvident_refines ctx rvos ros)
      (LIST: list_sval_equiv ctx rargs args)
      :rfv_refines ctx (Stailcall sig rvos rargs) (Stailcall sig ros args)
  | RefBuiltin ef lbra lba br pc
      (BARGS: bargs_refines ctx lbra lba)
      :rfv_refines ctx (Sbuiltin ef lbra br pc) (Sbuiltin ef lba br pc)
  | RefJumptable rsv sv lpc
      (VAL: sval_equiv ctx rsv sv)
      :rfv_refines ctx (Sjumptable rsv lpc) (Sjumptable sv lpc)
  | RefReturn orsv osv
      (OPT: optrsv_refines ctx orsv osv)
      :rfv_refines ctx (Sreturn orsv) (Sreturn osv)
.

Lemma rfv_refines_preserved ctx sfv1 sfv2:
  rfv_refines (bcctx1 ctx) sfv1 sfv2 <-> rfv_refines (bcctx2 ctx) sfv1 sfv2.
Proof.
  split; intros H; inv H; econstructor; eauto;
  unfold bargs_refines in *;
  try (apply rsvident_refines_preserved; auto);
  try (rewrite !list_sval_eval_preserved in *; auto);
  try (rewrite !eval_list_builtin_sval_preserved in *; auto);
  try (inv OPT; econstructor);
  simplify_ctx; auto.
Qed.

(** * Some preservation properties wrt theory *)
 
Lemma refines_opt_sv ctx ris sis r sS sR:
  ris_refines ctx ris sis ->
  ris_ok ctx ris ->
  sis r = Some sS -> ris r = Some sR ->
  sval_equiv ctx sR sS.
Proof.
  intros. destruct H.
  specialize REG_EQ0 with r. rewrite H1, H2 in REG_EQ0.
  simpl in REG_EQ0. auto.
Qed.

Lemma eval_list_sval_refpreserv ctx args ris sis:
  ris_refines ctx ris sis ->
  ris_ok ctx ris ->
  forall lsv lsv',
  lmap_sv ris args = Some lsv ->
  lmap_sv (sis_sreg sis) args = Some lsv' ->
  list_sval_equiv ctx lsv lsv'.
Proof.
  intros REF OK.
  induction args; simpl; try_simplify_someHyps.
  repeat autodestruct; intros.
  try_simplify_someHyps; intros.
  erewrite refines_opt_sv; eauto.
  erewrite IHargs; eauto.
Qed.
Local Hint Resolve eval_list_sval_refpreserv: core.

Lemma eval_builtin_sval_refpreserv ctx arg ris sis:
  ris_refines ctx ris sis ->
  ris_ok ctx ris ->
  forall ba ba',
  map_builtin_arg_opt ris arg = Some ba ->
  map_builtin_arg_opt sis arg = Some ba' ->
  eval_builtin_sval ctx ba = eval_builtin_sval ctx ba'.
Proof.
  induction arg; simpl; repeat autodestruct; try_simplify_someHyps.
  - intros; erewrite refines_opt_sv; eauto.
  - intros; erewrite (IHarg1 _ _ b b1), (IHarg2 _ _ b0 b2); eauto.
  - intros; erewrite (IHarg1 _ _ b b1), (IHarg2 _ _ b0 b2); eauto.
  Unshelve. all: auto.
Qed.

Lemma bargs_refpreserv ctx args ris sis:
  ris_refines ctx ris sis ->
  ris_ok ctx ris ->
  forall lba lba',
  bamap_opt (map_builtin_arg_opt ris) args = Some lba ->
  bamap_opt (map_builtin_arg_opt sis) args = Some lba' ->
  bargs_refines ctx lba lba'.
Proof.
  unfold bargs_refines. intros REF OK.
  induction args; simpl; try_simplify_someHyps.
  repeat autodestruct; try_simplify_someHyps.
  intros; erewrite eval_builtin_sval_refpreserv, IHargs; eauto.
Qed.

Local Hint Resolve bargs_refpreserv refines_opt_sv: core.
Local Hint Constructors rfv_refines optrsv_refines rsvident_refines rsvident_refines: core.

Lemma exec_final_refpreserv ctx i ris sis sfv sfv':
  ris_refines ctx ris sis ->
  ris_ok ctx ris ->
  sexec_final_sfv i ris = Some sfv ->
  sexec_final_sfv i sis = Some sfv' ->
  rfv_refines ctx sfv sfv'.
Proof.
  destruct i; simpl; unfold sum_left_optmap;
  repeat autodestruct; try_simplify_someHyps; eauto.
Qed.

(** * refinement of (symbolic) states *)

Inductive rstate :=
  | Rfinal (ris: ristate) (rfv: sfval)
  | Rcond (cond: condition) (rargs: list_sval) (rifso rifnot: rstate)
  | Rabort
  .

Inductive rst_ok_in ctx: rstate -> rstate -> Prop :=
  | Okinfinal ris1 ris2 sfv1 sfv2
      (INCL: incl (ok_rsval ris1) (ok_rsval ris2))
      :rst_ok_in ctx (Rfinal ris1 sfv1) (Rfinal ris2 sfv2)
  | Okincond rcond rargs rifso1 rifso2 rifnot1 rifnot2
      (OKso: eval_scondition ctx rcond rargs = Some true -> rst_ok_in ctx rifso1 rifso2)
      (OKnot: eval_scondition ctx rcond rargs = Some false -> rst_ok_in ctx rifnot1 rifnot2)
      :rst_ok_in ctx (Rcond rcond rargs rifso1 rifnot1) (Rcond rcond rargs rifso2 rifnot2)
  | Okinabort: rst_ok_in ctx Rabort Rabort.
Global Hint Constructors rst_ok_in: core.

Inductive rst_refines (input_init: bool) ctx: rstate -> sstate -> Prop :=
  | Reffinal ris sis rfv sfv
      (RIS: ris_refines ctx ris sis)
      (RIS_init: ris_input_init ris = input_init)
      (RFV: ris_ok ctx ris -> rfv_refines ctx rfv sfv)
      :rst_refines input_init ctx (Rfinal ris rfv) (Sfinal sis sfv)
  | Refcond rcond cond rargs args rifso rifnot ifso ifnot
      (RCOND: eval_scondition ctx rcond rargs = eval_scondition ctx cond args)
      (REFso: eval_scondition ctx rcond rargs = Some true ->
        rst_refines input_init ctx rifso ifso)
      (REFnot: eval_scondition ctx rcond rargs = Some false ->
        rst_refines input_init ctx rifnot ifnot)
      :rst_refines input_init ctx (Rcond rcond rargs rifso rifnot) (Scond cond args ifso ifnot)
  | Refabort
      :rst_refines input_init ctx Rabort Sabort
  .

Lemma rst_refines_trsis_final_preserved ctx csi sis1 sis2 sfv1 sfv2:
  rfv_refines (bcctx1 ctx) sfv1 sfv2 ->
  ris_refines (bcctx2 ctx) sis1 (tr_sis sis2 csi false) ->
  ris_input_init sis1 = false ->
  rst_refines false (bcctx2 ctx) (Rfinal sis1 sfv1) (Sfinal (tr_sis sis2 csi false) sfv2).
Proof.
  econstructor; eauto.
  intros; apply rfv_refines_preserved; auto.
Qed.
Local Hint Resolve rst_refines_trsis_final_preserved: core.

(** * Simulation properties on internal states *)

Record ris_simu ris1 ris2: Prop := {
  SIMU_FAILS: forall sv, List.In sv ris2.(ok_rsval) -> List.In sv ris1.(ok_rsval);
  SIMU_MEM: ris1.(ris_smem) = ris2.(ris_smem);
  (* rem: simulation only on live variables *)
  SIMU_REG: forall r, alive (ris1 r) -> ris1 r = ris2 r
}.
Local Hint Resolve SIMU_FAILS SIMU_MEM SIMU_REG: core.
Local Hint Constructors ris_simu: core.
Local Hint Resolve sge_match: core.

(* NB: this lemma should only need to work on iblock_common_context, 
       because now, the simulation is tested only in the target context ! *)
Lemma ris_simu_ok_preserv ris1 ris2 ctx:
  ris_simu ris1 ris2 -> ris_ok ctx ris1 -> ris_ok ctx ris2.
Proof.
  intros SIMU OK; econstructor; eauto.
  erewrite <- SIMU_MEM; eauto.
Qed.

Lemma ris_simu_match_sreg_preserv ris1 ris2 ctx rs:
  ris_simu ris1 ris2 ->
  sreg_ok ctx ris2 ->
  match_sreg ctx ris1 rs -> 
  exists rs', match_sreg ctx ris2 rs' /\ eqlive_reg (build_frame ris1) rs rs'.
Proof.
  intros SIMU OK MATCH; inv SIMU.
  exists (eval_map_sreg ctx (fun x => x) ris2.(ris_sreg)); split.
  - intros r sv H. generalize H. unfold ris_sreg_get.
    repeat autodestruct; try_simplify_someHyps.
    + intros; erewrite eval_map_sreg_correct_some; eauto.
      autodestruct. intros; exploit OK; eauto; contradiction.
    + intros; rewrite eval_map_sreg_correct_none; auto.
  - unfold build_frame; intros r H.
    exploit SIMU_REG0; eauto.
    destruct (ris1 r) eqn: X; try congruence.
    unfold ris_sreg_get.
    repeat autodestruct; try_simplify_someHyps.
    + intros; erewrite eval_map_sreg_correct_some; eauto.
      erewrite MATCH; eauto.
    + intros; rewrite eval_map_sreg_correct_none; eauto.
      exploit MATCH; eauto. simpl; intros HINV; inv HINV; reflexivity.
Qed.

Lemma ris_simu_correct ris1 ris2 ctx sis1 sis2: 
  ris_simu ris1 ris2 ->
  ris_refines ctx ris1 sis1 ->
  ris_refines ctx ris2 sis2 ->
  sistate_simu ctx sis1 sis2.
Proof.
  intros SIMU REF1 REF2 rs m SEM.
  exploit sem_sis_ok; eauto.
  erewrite OK_EQUIV; eauto.
  intros ROK1.
  exploit ris_simu_ok_preserv; eauto.
  intros SOK2.
  destruct SEM as (PRE & SMEM & SREG).
  exploit ris_simu_match_sreg_preserv; eauto.
  { erewrite ris_refines_match_sreg; eauto. }
  intros (rs2 & MATCH & EQLIVE).
  exists rs2; unfold sem_sistate; repeat split.
  + eapply OK_EQUIV; eauto.
  + erewrite <- MEM_EQ, <- SIMU_MEM; eauto.
    erewrite MEM_EQ; eauto.
  + erewrite <- ris_refines_match_sreg; eauto.
  + intros; eapply eqlive_reg_monotonic; eauto.
    intros; erewrite ris_refines_build_frame; eauto.
Qed.

Lemma ris_simu_correct_build_frame ris1 ris2 ctx sis1 sis2: 
  ris_simu ris1 ris2 ->
  ris_refines ctx ris1 sis1 ->
  ris_refines ctx ris2 sis2 ->
  ris_ok ctx ris1 ->
  forall r, build_frame sis1 r -> build_frame sis2 r.
Proof.
  intros RIS REF1 REF2 ROK1.
  exploit ris_simu_ok_preserv; eauto.
  intros ROK2 r.
  erewrite <- ris_refines_build_frame; eauto.
  erewrite <- (ris_refines_build_frame ctx ris2 sis2); eauto.
  destruct (ris1 r) eqn: X; auto.
  try_simplify_someHyps; intros.
  inv RIS; rewrite SIMU_REG0 in X; eauto.
Qed.

(** * Simulation relation *)

Definition rfv_simu (rfv1 rfv2: sfval): Prop := rfv1 = rfv2.

(** The simlulation can either be "light" (i.e. only on the final internal state),
    or "hard", with an additional check on final values.
    The "light" simulation is used to verify application of history invariants.
*)
Inductive rst_simu ctx: bool -> rstate -> rstate -> Prop :=
  | Rfinal_simu_light ris1 ris2 rfv1 rfv2
      (RIS: ris_simu ris1 ris2)
      : rst_simu ctx true (Rfinal ris1 rfv1) (Rfinal ris2 rfv2)
  | Rfinal_simu_hard ris1 ris2 rfv1 rfv2
      (RIS: ris_simu ris1 ris2)
      (RFV: rfv_simu rfv1 rfv2)
      : rst_simu ctx false (Rfinal ris1 rfv1) (Rfinal ris2 rfv2)
  | Rcond_simu cond rargs rifso1 rifnot1 rifso2 rifnot2 light_check
      (IFSO: eval_scondition ctx cond rargs = Some true ->
        rst_simu ctx light_check rifso1 rifso2)
      (IFNOT: eval_scondition ctx cond rargs = Some false ->
        rst_simu ctx light_check rifnot1 rifnot2)
      : rst_simu ctx light_check
        (Rcond cond rargs rifso1 rifnot1) (Rcond cond rargs rifso2 rifnot2)
  | Rabort_simu light_check: rst_simu ctx light_check Rabort Rabort
(* TODO: extension à voir dans un second temps !
  | Rcond_skip cond rargs rifso1 rifnot1 rst:
      rst_simu rifso1 rst ->
      rst_simu rifnot1 rst ->
      rst_simu (Rcond cond rargs rifso1 rifnot1) rst
*)
  .

Record routcome := rout {
  _ris: ristate;
  _rfv: sfval;
}.

Fixpoint get_routcome ctx (rst:rstate): option routcome :=
  match rst with
  | Rfinal ris rfv => Some (rout ris rfv)
  | Rcond cond args ifso ifnot =>
     SOME b <- eval_scondition ctx cond args IN
     get_routcome ctx (if b then ifso else ifnot)
  | Rabort => None
  end.


Local Hint Resolve eval_sval_preserved list_sval_eval_preserved
  smem_eval_preserved eval_list_builtin_sval_preserved: core.

Lemma rvf_simu_correct rfv1 rfv2 ctx sfv1 sfv2: 
  rfv_simu rfv1 rfv2 ->
  rfv_refines ctx rfv1 sfv1 ->
  rfv_refines ctx rfv2 sfv2 ->
  sfv_simu ctx sfv1 sfv2.
Proof.
  unfold rfv_simu; intros X REF1 REF2. subst.
  destruct REF1; inv REF2; simpl; econstructor; eauto.
  - (* call svid *)
    inv SV; inv SV0; econstructor; eauto.
    rewrite <- REF, <- REF0; eauto.
  - (* call args *)
    rewrite <- LIST, <- LIST0; eauto.
  - (* taillcall svid *)
    inv SV; inv SV0; econstructor; eauto.
    rewrite <- REF, <- REF0; eauto.
  - (* tailcall args *)
    rewrite <- LIST, <- LIST0; eauto.
  - (* builtin args *)
    unfold bargs_refines, bargs_simu in *.
    rewrite <- BARGS, <- BARGS0; eauto.
  - (* jumptable arg *)
    rewrite <- VAL, <- VAL0; eauto.
  - (* return *)
    inv OPT; inv OPT0; econstructor; eauto.
    rewrite <- REF, <- REF0; eauto.
Qed.

Lemma rst_simu_lroutcome_light ctx light_check rst1 rst2:
   rst_simu ctx light_check rst1 rst2 ->
   light_check = true ->
   forall ris1 ris2 rfv1 rfv2,
   get_routcome ctx rst1 = Some (rout ris1 rfv1) ->
   get_routcome ctx rst2 = Some (rout ris2 rfv2) ->
   ris_simu ris1 ris2.
Proof.
  induction 1; simpl; intros lc lris1 lris2 lrfv1 lrfv2 ROUT1 ROUT2; try_simplify_someHyps.
  repeat autodestruct; eauto.
Qed.

Lemma rst_simu_lroutcome_hard ctx light_check rst1 rst2:
   rst_simu ctx light_check rst1 rst2 ->
   light_check = false ->
   forall ris1 rfv1,
   get_routcome ctx rst1 = Some (rout ris1 rfv1) ->
   exists ris2 rfv2,
    get_routcome ctx rst2 = Some (rout ris2 rfv2) /\ ris_simu ris1 ris2 /\ rfv_simu rfv1 rfv2.
Proof.
  induction 1; simpl; intros lc lris1 lrfv1 ROUT; try_simplify_someHyps.
  autodestruct. destruct b; simpl; auto.
Qed.

Lemma rst_refines_outcome_up light_check ctx rst st:
   rst_refines light_check ctx rst st ->
   forall ris rfv,
   get_routcome ctx rst = Some (rout ris rfv) ->
   exists sis sfv,
    get_soutcome ctx st = Some (sout sis sfv) /\ ris_refines ctx ris sis
    /\ (ris_ok ctx ris -> rfv_refines ctx rfv sfv).
Proof.
  induction 1; simpl; intros lris lrfv ROUT; try_simplify_someHyps.
  rewrite RCOND.
  autodestruct.
  destruct b; simpl; auto.
Qed.

Lemma rst_refines_outcome_okpreserv light_check ctx rst st:
   rst_refines light_check ctx rst st ->
   forall sis sfv,
   get_soutcome ctx st = Some (sout sis sfv) ->
   exists ris rfv,
    get_routcome ctx rst = Some (rout ris rfv) /\ ris_refines ctx ris sis
    /\ (ris_ok ctx ris -> rfv_refines ctx rfv sfv).
Proof.
  induction 1; simpl; intros lris lrfv ROUT; try_simplify_someHyps.
  rewrite RCOND.
  autodestruct.
  destruct b; simpl; auto.
Qed.

Local Hint Resolve ris_simu_correct rvf_simu_correct: core.

Lemma rst_simu_light_correct b1 b2 ctx rst1 rst2 st1 st2 sis1 sis2 sfv1 sfv2
  (SIMU: rst_simu ctx true rst1 rst2)
  (REF1: get_soutcome ctx st1 = Some (sout sis1 sfv1) -> sis_ok ctx sis1 ->
    rst_refines b1 ctx rst1 st1)
  (REF2: get_soutcome ctx st2 = Some (sout sis2 sfv2) -> sis_ok ctx sis2 ->
    rst_refines b2 ctx rst2 st2)
  (REF1_OK: symb_exec_ok ctx st1 sis1 sfv1)
  (REF2_OK: symb_exec_ok ctx st2 sis2 sfv2)
  :sistate_simu ctx sis1 sis2.
Proof.
  destruct REF1_OK as (SOUT1 & OK1); destruct REF2_OK as (SOUT2 & OK2).
  exploit REF1; eauto; exploit REF2; eauto.
  clear REF2; clear REF1; intros REF2 REF1.
  exploit rst_refines_outcome_okpreserv; eauto; clear REF1 SOUT1.
  intros (ris1 & rfv1 & ROUT1 & REFI1 & REFF1).
  rewrite OK_EQUIV in OK1; eauto. 
  exploit rst_refines_outcome_okpreserv; eauto; clear REF2 SOUT2.
  intros (ris2 & rfv2 & ROUT2 & REFI2 & REFF2).
  rewrite OK_EQUIV in OK2; eauto. 
  exploit rst_simu_lroutcome_light; eauto.
Qed.

(* NB: b1 and b2 are no relevant below, because there is already a similar condition in rst_simu *)
Lemma rst_simu_hard_correct b1 b2 ctx rst1 rst2 st1 st2 
  (SIMU: rst_simu ctx false rst1 rst2)
  (REF1: forall sis sfv, get_soutcome ctx st1 = Some (sout sis sfv) -> sis_ok ctx sis ->
    rst_refines b1 ctx rst1 st1)
  (REF2: forall ris rfv, get_routcome ctx rst2 = Some (rout ris rfv) -> ris_ok ctx ris ->
    rst_refines b2 ctx rst2 st2)
  :match_sexec_live ctx st1 st2.
Proof.
  unfold match_sexec_live.
  intros sis1 sfv1 (SOUT & OK1).
  exploit REF1; eauto.
  clear REF1; intros REF1.
  exploit rst_refines_outcome_okpreserv; eauto. clear REF1 SOUT.
  intros (ris1 & rfv1 & ROUT1 & REFI1 & REFF1).
  rewrite OK_EQUIV in OK1; eauto. 
  exploit REFF1; eauto. clear REFF1; intros REFF1.
  exploit rst_simu_lroutcome_hard; eauto.
  intros (ris2 & rfv2 & ROUT2 & SIMUI & SIMUF). clear ROUT1.
  exploit ris_simu_ok_preserv; eauto.
  intros OK2.
  exploit REF2; eauto. clear REF2; intros REF2.
  exploit rst_refines_outcome_up; eauto.
  intros (sis2 & sfv2 & SOUT & REFI2 & REFF2).
  exists sis2; exists sfv2. repeat (split; eauto).
  intros; eapply ris_simu_correct_build_frame; eauto.
Qed.

(** si_ok preservation under [sexec] *)

Lemma sis_ok_op_okpreserv ctx op args dest sis sis':
  sexec_op op args dest sis = Some sis' ->
  sis_ok ctx sis' -> sis_ok ctx sis.
Proof.
  unfold sexec_op. autodestruct.
  intros H1 H2; inv H2.
  rewrite ok_set_sreg. intuition.
Qed.

Lemma sis_ok_load_okpreserv ctx chunk addr args dest sis sis' trap:
  sexec_load trap chunk addr args dest sis = Some sis' ->
  sis_ok ctx sis' ->
  sis_ok ctx sis.
Proof.
  unfold sexec_load. autodestruct. 
  intros H1 H2; inv H2.
  rewrite ok_set_sreg. intuition.
Qed.

Lemma sis_ok_store_okpreserv ctx chunk addr args src sis sis':
  sexec_store chunk addr args src sis = Some sis' ->
  sis_ok ctx sis' ->
  sis_ok ctx sis.
Proof.
  unfold sexec_store. repeat autodestruct. 
  intros H1 H2 H3; inv H1; inv H3.
  rewrite ok_set_mem. intuition.
Qed.

(* These lemmas are very bad for eauto: we put them into a dedicated basis. *)
Local Hint Resolve sis_ok_op_okpreserv sis_ok_load_okpreserv sis_ok_store_okpreserv: sis_ok.

(** This lemma is used in BTL_SEimpl.v *)
Lemma sexec_rec_okpreserv ctx ib: 
  forall k
  (CONT: forall sis lsis sfv,
    get_soutcome ctx (k sis) =  Some (sout lsis sfv) ->
    sis_ok ctx lsis ->
    sis_ok ctx sis)
  sis lsis sfv
  (SOUT: get_soutcome ctx (sexec_rec ib sis k) = Some (sout lsis sfv))
  (SOK: sis_ok ctx lsis),
  sis_ok ctx sis.
Proof.
  induction ib; simpl.
  1-5: try_simplify_someHyps; try autodestruct; eauto with sis_ok; try discriminate.
  - simpl. intros; inv SOUT. eauto.
  - intros. eapply IHib1. 2-3: eauto.
    eapply IHib2; eauto.
  - intros k CONT sis lsis sfv.
    repeat (intros; try_simplify_someHyps; autodestruct).
    simpl; congruence.
Qed.

(** * Refinement definitions used in the implementation and in the intermediate ref proof. *)

(** Substitution property *)

Definition reg_subst (ris: ristate) (sis: sistate) (si: fpasv) ctx :=
  forall r, eval_osv ctx (ris r) = eval_sval ctx (sv_subst (sis_smem sis) sis (ext si r)).

Lemma substitution_rule_si_empty ctx (ris: ristate) sis
  (RR: ris_refines ctx ris sis)
  (ROK: ris_ok ctx ris)
  (RINIT: ris_input_init ris = true)
  :reg_subst ris sis si_empty ctx.
Proof.
  inversion RR; simpl;
  unfold reg_subst, eval_osv; intros; autodestruct; simpl.
  - autodestruct; eauto.
    rewrite <- ALIVE_EQ0; congruence.
  - unfold ris_sreg_get. rewrite RINIT.
    autodestruct. 
Qed.
Global Hint Resolve substitution_rule_si_empty: sempty.

(** [root_op] application *)

Definition root_sapply (rsv: root_op) (lir: list ireg) (sis: sistate) si: option sval :=
  SOME lsv <- lmap_sv (ir_subst_si sis si) lir IN
  let sm := sis_smem sis in
  match rsv with
  | Rop op => Some (fSop op lsv)
  | Rload trap chunk addr => Some (fSload sm trap chunk addr lsv)
  end.
Coercion root_sapply: root_op >-> Funclass.

Lemma root_sapply_rop_subst ctx (rop: root_op) args sis si sv
  (RSV: rop args sis si = Some sv)
  :sval_equiv ctx sv (sv_subst (sis_smem sis) sis (rop_subst (ext si) rop args)).
Proof.
  revert RSV; unfold root_sapply, rop_subst.
  repeat autodestruct; intros ROP LMAP HINV; inv HINV; simpl;
  erewrite lmap_sv_lsv_subst; auto.
Qed.

Lemma ok_rsv_set_sreg (rsv:root_op) ctx (sis: sistate) si r lr sv
  (RSV: rsv lr sis si = Some sv)
  :sis_ok ctx (set_sreg r sv sis)
  <-> (sis_ok ctx sis /\ eval_sval ctx sv <> None).
Proof.
  unfold set_sreg; simpl; split.
  - intros. destruct H as [[OK_SV OK_PRE] OK_SMEM OK_SREG]; simpl in *.
    repeat (split; try tauto).
    + intros r0 sv0; generalize (OK_SREG r0 sv0); clear OK_SREG;
      destruct (Pos.eq_dec r r0); subst; intros; try_simplify_someHyps.
    + generalize (OK_SREG r sv); clear OK_SREG; destruct (Pos.eq_dec r r); try_simplify_someHyps.
  - intros (OK & SEVAL). inv OK.
    repeat (split; try tauto; eauto).
    intros r0 sv0; destruct (Pos.eq_dec r r0) eqn:Heq; subst; simpl;
    rewrite Heq; eauto. intros HSV; inv HSV; assumption.
Qed.

(* NB: return [false] if the rsv cannot fail *)
Definition may_trap {A} (rsv: root_op) (args: list A): bool :=
  match rsv with 
  (* cf. lemma is_trapping_op_sound *)
  | Rop op => is_trapping_op op ||| negb (Nat.eqb (length args) (args_of_operation op))
  | Rload TRAP _ _  => true
  | _ => false
  end.

Lemma lazy_orb_negb_false (b1 b2:bool):
  (b1 ||| negb b2) = false <-> (b1 = false /\ b2 = true).
Proof.
  unfold negb. repeat autodestruct; simpl; intuition (try congruence).
Qed.

Lemma eval_list_sval_length ctx sis si (lir:list ireg):
  forall l,
  lmap_sv (ir_subst_si sis si) lir = Some l ->
  forall l', eval_list_sval ctx l = Some l' ->
  Datatypes.length lir = Datatypes.length l'.
Proof.
  induction lir; simplify_option.
  simpl. intros. inv H0. reflexivity.
Qed.

Lemma may_trap_correct ctx (rsv: root_op) (lir: list ireg) (sis: sistate) si:
  forall lsv sv,
  may_trap rsv lir = false -> 
  lmap_sv (ir_subst_si sis si) lir = Some lsv ->
  eval_list_sval ctx lsv <> None ->
  eval_smem ctx (sis_smem sis) <> None ->
  rsv lir sis si = Some sv ->
  eval_sval ctx sv <> None.
Proof.
  destruct rsv; simpl; try congruence; intros lsv sv.
  - rewrite lazy_orb_negb_false. intros (TRAP1 & LEN) LMAP OK1 OK2.
    unfold root_sapply; rewrite LMAP.
    intros H; inv H.
    simpl; autodestruct; intros.
    eapply is_trapping_op_sound; eauto.
    erewrite <- eval_list_sval_length; eauto.
    apply Nat.eqb_eq in LEN.
    eassumption.
  - intros X LMAP OK1 OK2.
    unfold root_sapply.
    rewrite LMAP. intros H; inv H; simpl.
    repeat autodestruct; try congruence.
Qed.
