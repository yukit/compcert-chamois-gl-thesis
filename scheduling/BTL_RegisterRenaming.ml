(* *************************************************************)
(*                                                             *)
(*             The Compcert verified compiler                  *)
(*                                                             *)
(*           Alexandre Berard   VERIMAG, UGA                   *)
(*                                                             *)
(*  Copyright VERIMAG. All rights reserved.                    *)
(*  This file is distributed under the terms of the INRIA      *)
(*  Non-Commercial License Agreement.                          *)
(*                                                             *)
(* *************************************************************)

(** BTL register renaming oracle, to be used with if-lifting *)

open Maps
open RTLcommonaux
open BTL
open Registers
open Op
open Datatypes
open AST
open BTLtypes
open BTL_Renumber

module IM = Map.Make (Int)

(* put fresh variables, replace them in code and restore before exits *)
let get_freg_back n ibf btl =
  if not !Clflags.option_reg_renaming then btl, IM.empty else
  let new_dest dest m p inumb =
    let dest = p2i dest in
    match IM.find_opt dest m with
    | Some r ->
        reg := !reg + 1;
        (i2p !reg), IM.add dest !reg m, IM.add inumb (dest, !reg) p
    | None ->
        (i2p dest), IM.add dest dest m, IM.add inumb (dest, dest) p in
  let rec ren_dest ib m p =
    match ib with
    | Bseq (ib1, ib2) ->
        let ib2', m, p = ren_dest ib2 m p in
        let ib1', m, p = ren_dest ib1 m p in
        Bseq (ib1', ib2'), m, p
    | Bop (op, args, dest, iinfo) ->
        let dest', m, p = new_dest dest m p iinfo.inumb in
        Bop (op, args, dest', iinfo), m, p
    | Bload (trap, chunk, addr, args, dest, iinfo) ->
        let dest', m, p = new_dest dest m p iinfo.inumb in
        Bload (trap, chunk, addr, args, dest', iinfo), m, p
    | _ -> ib, m, p in
  let get_reg r m =
    let rp = p2i r in
    match IM.find_opt rp m with
    | Some r' -> (i2p r')
    | None -> r in
  let restore_block live_list ib m =
    let rec traverse_live live_list =
      match live_list with
      | l::next ->
          let l' = get_reg l m in
          if l' = l then
            traverse_live next
          else 
            Bseq (Bop (Omove, l'::[], l, BTLcommonaux.def_iinfo ()), traverse_live next)
      | [] -> ib in
    traverse_live live_list in
  let change_args l m = List.fold_right (fun r l -> (get_reg r m)::l) l [] in
  let change_ren inumb m p =
    match IM.find_opt inumb p with
    | Some (f, t) -> IM.add f t m
    | None -> m in
  let rec ren_args ib m p =
    match ib with
    | Bseq (ib1, ib2) ->
        let ib1', m, p = ren_args ib1 m p in
        let ib2', m, p = ren_args ib2 m p in
        Bseq (ib1', ib2'), m, p
    | Bop (op, args, dest, iinfo) ->
        let args' = change_args args m in
        let m = change_ren iinfo.inumb m p in
        Bop (op, args', dest, iinfo), m, p
    | Bload (trap, chunk, addr, args, dest, iinfo) ->
        let args' = change_args args m in
        let m = change_ren iinfo.inumb m p in
        Bload (trap, chunk, addr, args', dest, iinfo), m, p
    | Bstore (chunk, addr, args, src, iinfo) ->
        let args' = change_args args m in
        Bstore (chunk, addr, args', (get_reg src m), iinfo), m, p
    | Bcond (cond, args, ifso, ifnot, iinfo) ->
        let args' = change_args args m in
        let ifso', m, p = ren_args ifso m p in
        let ifnot', m, p = ren_args ifnot m p in
        Bcond (cond, args', ifso', ifnot', iinfo), m, p
    | BF (Bgoto succ, _) ->
        let ibf_go = get_some @@ PTree.get succ btl in
        restore_block (Regset.elements ibf_go.binfo.input_regs) ib m, m, p
    | BF (Breturn res, _) ->
        (match res with
        | Some reg -> restore_block (reg::[]) ib m, m, p
        | None -> ib, m, p)
    | BF (Bcall (si, fn, args, dest, succ), iinfo) ->
        let ibf_go = get_some @@ PTree.get succ btl in
        let args' = change_args args m in
        let lives = Regset.elements (Regset.remove dest ibf_go.binfo.input_regs) in
        let fn' =
          match fn with
          | Coq_inl r -> Coq_inl (get_reg r m)
          | _ -> fn in
        let ib' = BF (Bcall (si, fn', args', dest, succ), iinfo) in
        restore_block lives ib' m, m, p
    | BF (Btailcall (si, fn, args), iinfo) ->
        let args' = change_args args m in
        let fn' =
          match fn with
          | Coq_inl r -> Coq_inl (get_reg r m)
          | _ -> fn in
        BF (Btailcall (si, fn', args'), iinfo), m, p
    | BF (Bjumptable (arg, tbl), iinfo) ->
        let arg' = get_reg arg m in
        let rec tbl_union tbl =
          match tbl with
          | succ::next ->
              let ibf_go = get_some @@ PTree.get succ btl in
              Regset.union ibf_go.binfo.input_regs (tbl_union next)
          | [] -> Regset.empty in
        let lives = Regset.elements (tbl_union tbl) in
        let ib' = BF (Bjumptable (arg', tbl), iinfo) in
        restore_block lives ib' m, m, p
    | BF (Bbuiltin (ef, args, dest, succ), iinfo) ->
        let ibf_go = get_some @@ PTree.get succ btl in
        let builtin_regs = params_of_builtin_args args in
        let lives =
          Regset.elements
            (Regset.union ibf_go.binfo.input_regs
              (List.fold_left (fun m e -> Regset.add e m) Regset.empty builtin_regs)) in
        let ib' = BF (Bbuiltin (ef, args, dest, succ), iinfo) in
        restore_block lives ib' m, m, p
    | _ -> ib, m, p in
  recompute_inumbs btl n;
  let ib', _, p = ren_dest ibf.entry IM.empty IM.empty in
  let ib', _, _ = ren_args ib' IM.empty p in
  let ibf' = { entry = ib'; binfo = ibf.binfo } in
  PTree.set n ibf' btl, p
(* put fresh variables, replace them in code and restore before exits *)
let get_freg_forw n ibf btl =
  if not !Clflags.option_reg_renaming then btl, IM.empty else
  let m = ref IM.empty in
  let ren_map = ref IM.empty in
  let get_reg r =
    match IM.find_opt r !m with
    | Some r' -> r'
    | None -> r in
  let add_reg dest inumb =
    if IM.mem dest !m then
      (reg := !reg + 1;
      m := IM.add dest !reg !m;
      ren_map := IM.add inumb (dest, !reg) !ren_map;
      !reg)
    else
      (m := IM.add dest dest !m;
      ren_map := IM.add inumb (dest, dest) !ren_map;
      dest) in
  let rec change_args = function
    | r::next -> i2p (get_reg (p2i r))::(change_args next)
    | [] -> [] in
  let rec list_to_rset = function
    | e::next -> Regset.add e (list_to_rset next)
    | [] -> Regset.empty in
  let rec traverse_rec ib =
    (* restoring registers before exit *)
    let restore_block live_list ib =
      let rec traverse_live live_list =
        match live_list with
        | l::next ->
            let l' = i2p (get_reg (p2i l)) in
            if l' = l then
              traverse_live next
            else 
              Bseq (Bop (Omove, l'::[], l, (BTLcommonaux.def_iinfo ())), (traverse_live next))
        | [] -> ib in
      traverse_live live_list in
    match ib with
    | Bseq (ib1, ib2) ->
        let ib1' = traverse_rec ib1 in
        let ib2' = traverse_rec ib2 in
        Bseq(ib1', ib2')
    | Bop (op, args, dest, iinfo) ->
        let args' = change_args args in
        let dest' = i2p (add_reg (p2i dest) iinfo.inumb) in
        Bop (op, args', dest', iinfo)
    | Bload (trap, chunk, addr, args, dest, iinfo) ->
        let args' = change_args args in
        let dest' = i2p (add_reg (p2i dest) iinfo.inumb) in
        Bload (trap, chunk, addr, args', dest', iinfo)
    | Bstore (chunk, addr, args, src, iinfo) ->
        let args' = change_args args in
        Bstore (chunk, addr, args', i2p (get_reg (p2i src)), iinfo)
    | Bcond (cond, args, ifso, ifnot, iinfo) ->
        let args' = change_args args in
        let ifso' = traverse_rec ifso in
        let ifnot' = traverse_rec ifnot in
        Bcond (cond, args', ifso', ifnot', iinfo)
    | BF (Bgoto succ, _) ->
        let ibf_go = get_some @@ PTree.get succ btl in
        restore_block (Regset.elements ibf_go.binfo.input_regs) ib
    | BF (Breturn res, _) ->
        (match res with
        | Some(reg) -> restore_block (reg::[]) ib
        | None -> ib)
    | BF (Bcall (si, fn, args, dest, succ), iinfo) ->
        let ibf_go = get_some @@ PTree.get succ btl in
        let args' = change_args args in
        let lives = Regset.elements (Regset.remove dest ibf_go.binfo.input_regs) in
        let fn' =
          match fn with
          | Coq_inl r -> Coq_inl (i2p (get_reg (p2i r)))
          | _ -> fn in
        let ib' = BF (Bcall (si, fn', args', dest, succ), iinfo) in
        restore_block lives ib'
    | BF (Btailcall (si, fn, args), iinfo) ->
        let args' = change_args args in
        let fn' =
          match fn with
          | Coq_inl r -> Coq_inl (i2p (get_reg (p2i r)))
          | _ -> fn in
        BF (Btailcall (si, fn', args'), iinfo)
    | BF (Bjumptable (arg, tbl), iinfo) ->
        let arg' = i2p (get_reg (p2i arg)) in
        let rec tbl_union tbl =
          match tbl with
          | succ::next ->
              let ibf_go = get_some @@ PTree.get succ btl in
              Regset.union ibf_go.binfo.input_regs (tbl_union next)
          | [] -> Regset.empty in
        let lives = Regset.elements (tbl_union tbl) in
        let ib' = BF (Bjumptable (arg', tbl), iinfo) in
        restore_block lives ib'
    | BF (Bbuiltin (ef, args, dest, succ), iinfo) ->
        let ibf_go = get_some @@ PTree.get succ btl in
        let builtin_regs = params_of_builtin_args args in
        let lives = Regset.elements (Regset.union ibf_go.binfo.input_regs (list_to_rset builtin_regs)) in
        let ib' = BF (Bbuiltin (ef, args, dest, succ), iinfo) in
        restore_block lives ib'
    | _ -> ib in
  let ib' = traverse_rec ibf.entry in
  let ibf' = { entry = ib'; binfo = ibf.binfo } in
  PTree.set n ibf' btl, !ren_map

let get_freg n ibf btl =
  if not !Clflags.option_reg_renaming then btl, IM.empty else
  let s = !Clflags.option_reg_renaming_opt in
  let btl', ren_map = if s = "forward" then get_freg_forw n ibf btl
  else if (s = "backward") then get_freg_back n ibf btl
  else failwith ("unknown reg-renaming option: " ^ s)
  in
  btl', ren_map
