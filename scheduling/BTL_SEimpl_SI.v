(* *************************************************************)
(*                                                             *)
(*             The Compcert verified compiler                  *)
(*                                                             *)
(*           Léo Gourdin        VERIMAG, UGA                   *)
(*           Sylvain Boulmé     Grenoble-INP, VERIMAG          *)
(*                                                             *)
(*  Copyright VERIMAG. All rights reserved.                    *)
(*  This file is distributed under the terms of the INRIA      *)
(*  Non-Commercial License Agreement.                          *)
(*                                                             *)
(* *************************************************************)

(* This module implements the symbolic execution of invariants ([tr_sis] and memory invariants) and
the generation of the invariants at the end of the blocks ([si_sfv], [meminv_sfv]). *)
Require Import Coqlib Maps.
Require Import Registers.
Require Import RTL BTL.
Require Import BTL_SEsimuref BTL_SEtheory OptionMonad.
Require Import BTL_SEimpl_prelude.

Import Notations.
Import HConsing.
Import SvalNotations.

Import ListNotations.
Local Open Scope list_scope.
Local Open Scope lazy_bool_scope.
Local Open Scope option_monad_scope.
Local Open Scope impure.

Section SymbolicInvariants.

  Context `{HCF : HashConsingFcts}.
  Context `{HC : HashConsingHyps HCF}.
  Context `{RRULES: rrules_set}.

Local Transparent hrs_rsv_set.
Definition hrs_sv_set r sv (hrs: ristate): ?? ristate :=
  RET {| ris_smem := hrs.(ris_smem);
         ris_input_init := hrs.(ris_input_init);
         si_mode := hrs.(si_mode);
         ok_rsval := hrs.(ok_rsval);
         ris_sreg:= red_PTree_set r sv hrs |}.

Definition hrs_ival_set (hrs hrs_old: ristate) (r: reg) (iv: ival): ?? ristate :=
  match iv with
  | Ireg ir =>
      DO sv <~ @hrs_ir_get HCF hrs hrs_old ir;;
      hrs_sv_set r sv hrs
  | Iop rop args => @hrs_rsv_set HCF RRULES r args rop hrs hrs_old
  end.

Lemma hrs_ival_set_preserv_rinit hrs hrs_old r iv:
  WHEN hrs_ival_set hrs hrs_old r iv ~> hrs' THEN
  hrs.(ris_input_init) = hrs'.(ris_input_init).
Proof.
  unfold hrs_ival_set; destruct iv; wlp_simplify.
Qed.

Lemma hrs_ival_set_preserv_smem hrs hrs_old r iv:
  WHEN hrs_ival_set hrs hrs_old r iv ~> hrs' THEN
  hrs.(ris_smem) = hrs'.(ris_smem).
Proof.
  unfold hrs_ival_set; destruct iv; wlp_simplify.
Qed.

Lemma hrs_ival_set_preserv_sim hrs hrs_old r iv:
  WHEN hrs_ival_set hrs hrs_old r iv ~> hrs' THEN
  hrs.(si_mode) = hrs'.(si_mode).
Proof.
  unfold hrs_ival_set; destruct iv; wlp_simplify.
Qed.

Lemma hrs_ival_set_incl hrs hrs_old r iv:
  WHEN hrs_ival_set hrs hrs_old r iv ~> hrs' THEN
  incl (ok_rsval hrs) (ok_rsval hrs').
Proof.
  destruct iv; unfold hrs_ival_set; wlp_simplify; try apply incl_refl.
  apply incl_tl; apply incl_refl; auto.
Qed.

Lemma hrs_ival_set_incl_rev hrs hrs_old r iv:
  WHEN hrs_ival_set hrs hrs_old r iv ~> hrs' THEN forall
  (SIM: si_mode hrs = true),
  incl (ok_rsval hrs') (ok_rsval hrs).
Proof.
  destruct iv; wlp_simplify; try apply incl_refl.
  rewrite SIM in H0; discriminate.
Qed.

Lemma hrs_ival_set_preserv_rok ctx hrs hrs_old r iv:
  WHEN hrs_ival_set hrs hrs_old r iv ~> hrs' THEN forall
  (ROK: ris_ok ctx hrs)
  (SIM: si_mode hrs = true),
  ris_ok ctx hrs'.
Proof.
  unfold hrs_ival_set; destruct iv; wlp_simplify;
  inv ROK; constructor; auto.
  rewrite SIM in H0; discriminate.
Qed.

Hint Resolve hrs_sreg_get_correct: wlp.
Lemma hrs_ival_set_in hrs hrs_old r iv
  :WHEN hrs_ival_set hrs hrs_old r iv ~> hrs' THEN forall ctx sv (si: fpasv) sis
  (ROK_OLD: ris_ok ctx hrs_old)
  (RR_OLD: ris_refines ctx hrs_old sis)
  (HIND: In sv (ok_rsval hrs) ->
         exists sv',
           sval_equiv ctx sv (sv_subst (sis_smem sis) sis sv')
           /\ In sv' (fpa_ok si))
  (SIM: si_mode hrs = false)
  (MEM_EQ: ris_smem hrs = ris_smem hrs_old)
  (REG_SUBST: reg_subst hrs sis si ctx)
  (HIN: In sv (ok_rsval hrs')),
  exists sv',
    sval_equiv ctx sv (sv_subst (sis_smem sis) sis sv')
    /\ In sv' (fpa_ok (si_set r (iv_subst (ext si) iv) si)).
Proof.
  destruct iv; wlp_simplify.
  1: rename H into HIND. 3: rename H2 into HIND.
  4: rename H1 into HIND. 5: rename H0 into HIND.
  1,3,4,5: destruct HIND as (sv' & SEVAL & HIN_SI); intuition eauto.
  exploit (hrs_lir_get_exists_rop ctx args hrs hrs_old rop sis si); eauto;
  intros (sv' & RSV); subst.
  exploit root_sapply_rop_subst; eauto; intros ESVEQ1.
  exploit root_happly_correct; eauto; intros ESVEQ2.
  exists (rop_subst (ext si) rop args); split; eauto.
  rewrite ESVEQ2; eassumption.
Qed.

Lemma hrs_ival_set_reg_subst_nosim hrs hrs_old r0 iv
  :WHEN hrs_ival_set hrs hrs_old r0 iv ~> hrs' THEN forall ctx (si: fpasv) sis
  (ROK_OLD: ris_ok ctx hrs_old)
  (RR_OLD: ris_refines ctx hrs_old sis)
  (MEM_EQ: ris_smem hrs = ris_smem hrs_old)
  (SMEM: sis_smem sis = fSinit)
  (OK_SUBST: si_ok_subst sis (si_set r0 (iv_subst (ext si) iv) si) ctx)
  (REG_SUBST: reg_subst hrs sis si ctx),
  reg_subst hrs' sis (si_set r0 (iv_subst (ext si) iv) si) ctx.
Proof.
  destruct iv; wlp_simplify.
  2,3,4:
    unfold reg_subst, ris_sreg_get; simpl; intros;
    eapply hrs_lir_get_reg_subst_rop; eauto.
  intros; exploit hrs_ir_get_subst; eauto; intros.
  eapply red_PTree_set_ir_sv_subst; eauto.
Qed.

Hint Resolve lsv_in_imp_correct: wlp.

Lemma hrs_ival_set_reg_subst_sim ctx hrs hrs_old sis (si: fpasv) r0 iv:
  WHEN hrs_ival_set hrs hrs_old r0 iv ~> hrs' THEN forall
  (SOK: sis_ok ctx sis)
  (ROK: ris_ok ctx hrs)
  (ROK_OLD: ris_ok ctx hrs_old)
  (RR_OLD: ris_refines ctx hrs_old sis)
  (SIM: si_mode hrs = true)
  (MEM_EQ: (ris_smem hrs) = (ris_smem hrs_old))
  (OK_SUBST: si_ok_subst sis si ctx)
  (REG_SUBST: reg_subst hrs sis si ctx),
  reg_subst hrs' sis (si_set r0 (iv_subst (ext si) iv) si) ctx.
Proof.
  destruct iv; unfold hrs_ival_set, reg_subst; wlp_ssimplify ltac:(intuition eauto with wlp);
  inversion RR_OLD.
  2: rewrite SIM in H0; inv H0.
  - exploit hrs_ir_get_subst; eauto; intros ESVEQ.
    erewrite <- red_PTree_set_ir_sv_subst; eauto.
    unfold ris_sreg_set, ris_sreg_get; simpl. reflexivity.
  - exploit (hrs_lir_get_exists_rop ctx args hrs hrs_old rop sis si); eauto; intros (sv & ROP).
    exploit hrs_lir_get_subst; eauto; intros ELSVEQ.
    remember ROP as ROP'; clear HeqROP'; revert ROP'; unfold root_sapply; autodestruct.
    intros LMAP HROP; exploit hrs_lir_get_correct; eauto; intros LREQ.
    exploit root_happly_correct; eauto; intros SEVAL_RHA.
    exploit (@simplify_correct _ _ RRULES); eauto; [ rewrite <- SEVAL_RHA; inv ROK; apply OK_RREG; auto |].
    intros ESVEQ; erewrite <- red_PTree_set_rop_sv_subst; eauto.
    unfold ris_sreg_set, ris_sreg_get; reflexivity.
  - exploit (hrs_lir_get_exists_rop ctx args hrs hrs_old rop sis si); eauto; intros (sv & ROP).
    exploit hrs_lir_get_subst; eauto; intros LRSUBST.
    remember ROP as ROP'; clear HeqROP'; revert ROP'; unfold root_sapply; autodestruct.
    intros LMAP HROP; exploit hrs_lir_get_correct; eauto; intros LREQ.
    exploit (@simplify_correct _ _ RRULES); eauto.
    + intros RSVE; inversion SOK; eapply may_trap_correct; eauto.
      rewrite <- LREQ; eapply hrs_lir_get_eval_nofail; eauto.
    + intros ESVEQ; erewrite <- red_PTree_set_rop_sv_subst; eauto.
      unfold ris_sreg_set, ris_sreg_get; reflexivity.
Qed.

Lemma hrs_ival_set_si_ok_subst_nosim hrs hrs_old r iv
  :WHEN hrs_ival_set hrs hrs_old r iv ~> hrs' THEN forall ctx (si: fpasv) sis
  (ROK_OLD: ris_ok ctx hrs_old)
  (RR_OLD: ris_refines ctx hrs_old sis)
  (SIM: si_mode hrs = false)
  (MEM_EQ: ris_smem hrs = ris_smem hrs_old)
  (OK_SUBST: si_ok_subst sis si ctx)
  (REG_SUBST: reg_subst hrs sis si ctx)
  (OK_RREG: forall sv, In sv (ok_rsval hrs') -> eval_sval ctx sv <> None),
  si_ok_subst sis (si_set r (iv_subst (ext si) iv) si) ctx.
Proof.
  destruct iv; wlp_simplify;
  intros sv; simpl; intros [SVEQ|HIN]; subst; eauto.
  3: rewrite SIM in H0; inv H0.
  all: inversion ROK_OLD; inversion RR_OLD; apply OK_EQUIV in ROK_OLD as SOK; inversion SOK.
  - unfold ir_subst, ext; repeat (autodestruct; simpl); try congruence; eauto.
    intros; destruct (is_input_dec s).
    + inv i; simpl; autodestruct; simpl; try congruence; eauto.
    + apply OK_SUBST; eapply si_wf; eauto.
  - exploit (hrs_lir_get_exists_rop ctx args hrs hrs_old rop sis si); eauto.
    intros (sv' & RSV); subst.
    exploit root_sapply_rop_subst; eauto; intros ESVEQ1.
    exploit root_happly_correct; eauto; intros ESVEQ2.
    rewrite <- ESVEQ1, <- ESVEQ2; eauto.
  - exploit (hrs_lir_get_exists_rop ctx args hrs hrs_old rop sis si); eauto.
    intros (sv' & RSV); subst.
    exploit root_sapply_rop_subst; eauto; intros ESVEQ; rewrite <- ESVEQ.
    destruct (lmap_sv (ir_subst_si sis si) args) eqn:LMAP;
    [| wlp_exploit hrs_lir_get_lmap_nofail ].
    eapply may_trap_correct; eauto.
    exploit hrs_lir_get_correct; eauto; intros ELSVEQ.
    rewrite <- ELSVEQ. eapply hrs_lir_get_eval_nofail; eauto.
Qed.

Lemma hrs_ival_set_si_ok_subst_sim ctx hrs hrs_old sis (si: fpasv) r iv:
  WHEN hrs_ival_set hrs hrs_old r iv ~> hrs' THEN forall
  (SOK: sis_ok ctx sis)
  (ROK: ris_ok ctx hrs)
  (ROK_OLD: ris_ok ctx hrs_old)
  (RR_OLD: ris_refines ctx hrs_old sis)
  (SIM: si_mode hrs = true)
  (MEM_EQ: (ris_smem hrs) = (ris_smem hrs_old))
  (OK_SUBST: si_ok_subst sis si ctx)
  (REG_SUBST: reg_subst hrs sis si ctx),
  si_ok_subst sis (si_set r (iv_subst (ext si) iv) si) ctx.
Proof.
  destruct iv; unfold hrs_ival_set, si_ok_subst; wlp_ssimplify ltac:(intuition eauto with wlp);
  intros; inversion RR_OLD; inversion SOK; inversion ROK.
  2: rewrite SIM in H0; inv H0.
  - revert H0; revert H1; unfold ir_subst.
    autodestruct; intros FINPUT H; inv H; simpl.
    + autodestruct; simpl; try congruence; apply OK_SREG.
    + destruct (si (regof ir)) eqn:HSI;
      unfold ext; rewrite HSI.
      1: destruct (s); try (apply si_wf in HSI; [| intros CONTRA; inv CONTRA ]; eauto; fail).
      all: simpl; autodestruct; simpl; try congruence; apply OK_SREG.
  - revert H2; destruct (rop args sis si) eqn:RSV.
    + remember RSV as RSV'; clear HeqRSV'; revert RSV'; unfold root_sapply; autodestruct.
      intros LMAP HROP; exploit hrs_lir_get_correct; eauto; intros LREQ.
      revert H3; unfold rop_subst.
      exploit root_happly_correct; eauto; intros SEVAL_RHA.
      exploit hrs_lir_get_subst; eauto; intros LRSUBST.
      eapply lsv_in_imp_correct in Hexta1; eauto.
      assert (OK_RHA: eval_sval ctx exta0 <> None) by eauto.
      rewrite SEVAL_RHA in OK_RHA. revert HROP.
      autodestruct; intros ROP HINV ?; inv HINV; simpl in *.
      all: rewrite <- LRSUBST, LREQ; congruence.
    + revert RSV; unfold root_sapply.
      autodestruct; [ autodestruct |];
      wlp_exploit hrs_lir_get_lmap_nofail.
  - revert H1; destruct (rop args sis si) eqn:RSV.
    + remember RSV as RSV'; clear HeqRSV'; revert RSV'; unfold root_sapply; autodestruct.
      intros LMAP HROP; exploit hrs_lir_get_correct; eauto; intros LREQ.
      rewrite <- H2. unfold rop_subst.
      exploit hrs_lir_get_subst; eauto; intros LRSUBST.
      revert HROP. autodestruct; intros HR HROP; inv HROP; simpl;
      eapply may_trap_correct in H; eauto.
      1,3: simpl in H; rewrite <- LRSUBST, LREQ; auto.
      all: rewrite <- LREQ; eapply hrs_lir_get_eval_nofail; eauto.
    + revert RSV; unfold root_sapply.
      autodestruct; [ autodestruct |];
      wlp_exploit hrs_lir_get_lmap_nofail.
Qed.
Global Opaque hrs_ival_set hrs_sv_set.

(** * Applying symbolic invariants *)

Fixpoint exec_seq_imp (hrs hrs_old: ristate) (l: list (reg*ival)): ?? ristate :=
  match l with
  | nil => RET hrs
  | (r,iv)::l =>
      DO hrs' <~ hrs_ival_set hrs hrs_old r iv;;
      exec_seq_imp hrs' hrs_old l
  end.

Lemma exec_seq_imp_preserv_rinit l: forall hrs hrs_old,
  WHEN exec_seq_imp hrs hrs_old l ~> hrs' THEN
  hrs.(ris_input_init) = hrs'.(ris_input_init).
Proof.
  induction l; wlp_simplify.
  inv H. exploit hrs_ival_set_preserv_rinit; eauto.
Qed.

Lemma exec_seq_imp_preserv_smem l: forall hrs hrs_old,
  WHEN exec_seq_imp hrs hrs_old l ~> hrs' THEN
  hrs.(ris_smem) = hrs'.(ris_smem).
Proof.
  induction l; wlp_simplify.
  rewrite <- H. exploit hrs_ival_set_preserv_smem; eauto.
Qed.

Lemma exec_seq_imp_preserv_sim_smem lseq: forall (hrs hrs_old: ristate),
  WHEN exec_seq_imp hrs hrs_old lseq ~> hrs' THEN
  (si_mode hrs) = (si_mode hrs') /\ (ris_smem hrs) = (ris_smem hrs').
Proof.
  induction lseq; wlp_simplify.
  - rewrite <- H0; eapply hrs_ival_set_preserv_sim; eauto.
  - rewrite <- H1; eapply hrs_ival_set_preserv_smem; eauto.
Qed.

Lemma exec_seq_imp_incl lseq: forall (hrs hrs_old: ristate),
  WHEN exec_seq_imp hrs hrs_old lseq ~> hrs' THEN
  incl (ok_rsval hrs) (ok_rsval hrs').
Proof.
  induction lseq; wlp_simplify.
  - apply incl_refl.
  - apply hrs_ival_set_incl in Hexta as INCL; auto.
    eapply incl_tran with (l:=(ok_rsval hrs)); eauto.
Qed.

Lemma exec_seq_imp_incl_rev lseq: forall (hrs hrs_old: ristate),
  WHEN exec_seq_imp hrs hrs_old lseq ~> hrs' THEN forall
  (SIM: si_mode hrs = true),
  incl (ok_rsval hrs') (ok_rsval hrs).
Proof.
  induction lseq; wlp_simplify.
  - apply incl_refl.
  - apply hrs_ival_set_incl_rev in Hexta as INCL; auto.
    eapply incl_tran with (l:=(ok_rsval exta0)); eauto.
    exploit hrs_ival_set_preserv_sim; eauto.
Qed.

Lemma exec_seq_imp_in lseq: forall (hrs hrs_old: ristate),
  WHEN exec_seq_imp hrs hrs_old lseq ~> hrs' THEN forall ctx sv (si: fpasv) sis
  (ROK_OLD: ris_ok ctx hrs_old)
  (RR_OLD: ris_refines ctx hrs_old sis)
  (HIND: In sv (ok_rsval hrs) ->
         exists sv',
         sval_equiv ctx sv (sv_subst (sis_smem sis) sis sv')
         /\ In sv' (fpa_ok si))
  (SIM: si_mode hrs = false)
  (MEM_EQ: ris_smem hrs = ris_smem hrs_old)
  (SMEM: sis_smem sis = fSinit)
  (OK_SUBST: si_ok_subst sis (exec_seq lseq si) ctx)
  (REG_SUBST: reg_subst hrs sis si ctx)
  (HIN: In sv (ok_rsval hrs')),
  exists sv',
    sval_equiv ctx sv (sv_subst (sis_smem sis) sis sv')
    /\ In sv' (fpa_ok (exec_seq lseq si)).
Proof.
  induction lseq; wlp_simplify.
  eapply hrs_ival_set_preserv_sim in Hexta as SIMEQ; eauto.
  eapply hrs_ival_set_preserv_smem in Hexta as MEM_EQ'; eauto.
  eapply si_ok_subst_hd in OK_SUBST as OK_SUBST_SET.
  eapply H; eauto.
  - eapply hrs_ival_set_in; eauto.
  - rewrite <- MEM_EQ'; assumption.
  - eapply hrs_ival_set_reg_subst_nosim; eauto.
Qed.

Lemma exec_seq_imp_si_ok_subst_nosim lseq: forall (hrs hrs_old: ristate),
  WHEN exec_seq_imp hrs hrs_old lseq ~> hrs' THEN forall ctx (si: fpasv) sis
  (ROK_OLD: ris_ok ctx hrs_old)
  (RR_OLD: ris_refines ctx hrs_old sis)
  (SIM: si_mode hrs = false)
  (MEM_EQ: ris_smem hrs = ris_smem hrs_old)
  (SMEM: sis_smem sis = fSinit)
  (OK_SUBST: si_ok_subst sis si ctx)
  (REG_SUBST: reg_subst hrs sis si ctx)
  (OK_RREG: forall sv, In sv (ok_rsval hrs') -> eval_sval ctx sv <> None),
  si_ok_subst sis (exec_seq lseq si) ctx.
Proof.
  induction lseq; wlp_simplify.
  eapply hrs_ival_set_preserv_sim in Hexta as SIMEQ; eauto.
  eapply hrs_ival_set_preserv_smem in Hexta as MEM_EQ'; eauto.
  eapply exec_seq_imp_incl in Hexta0 as INCL; eauto.
  eapply H; eauto;
  [ rewrite <- MEM_EQ'; assumption | idtac | eapply hrs_ival_set_reg_subst_nosim; eauto ].
  all: eapply hrs_ival_set_si_ok_subst_nosim; eauto.
Qed.

Lemma exec_seq_imp_si_ok_subst_sim lseq: forall (hrs hrs_old: ristate),
  WHEN exec_seq_imp hrs hrs_old lseq ~> hrs' THEN forall ctx (sis: sistate) (si: fpasv)
  (SOK: sis_ok ctx sis)
  (ROK: ris_ok ctx hrs)
  (ROK_OLD: ris_ok ctx hrs_old)
  (RR_OLD: ris_refines ctx hrs_old sis)
  (SIM: si_mode hrs = true)
  (MEM_EQ: (ris_smem hrs) = (ris_smem hrs_old))
  (OK_SUBST: si_ok_subst sis si ctx)
  (REG_SUBST: reg_subst hrs sis si ctx),
  tr_sis_ok ctx sis (exec_seq lseq si).
Proof.
  induction lseq; wlp_simplify.
  - unfold tr_sis_ok. eauto.
  - exploit (hrs_ival_set_preserv_rok ctx hrs hrs_old a0); eauto; intros ROK'.
    exploit (hrs_ival_set_preserv_sim hrs hrs_old a0); eauto; intros SIM'.
    exploit (hrs_ival_set_preserv_smem hrs hrs_old a0); eauto; intros MEM_EQ'.
    eapply (H ctx sis (si_set a0 (iv_subst (ext si) b) si)); eauto.
    + rewrite <- MEM_EQ'; assumption.
    + eapply hrs_ival_set_si_ok_subst_sim; eauto.
    + eapply hrs_ival_set_reg_subst_sim; eauto.
Qed.

Lemma exec_seq_imp_reg_subst_nosim lseq: forall (hrs hrs_old: ristate),
  WHEN exec_seq_imp hrs hrs_old lseq ~> hrs' THEN forall ctx (sis: sistate) (si: fpasv)
  (ROK_OLD: ris_ok ctx hrs_old)
  (RR_OLD: ris_refines ctx hrs_old sis)
  (SIM: si_mode hrs = false)
  (MEM_EQ: (ris_smem hrs) = (ris_smem hrs_old))
  (SMEM: sis_smem sis = fSinit)
  (OK_SUBST: si_ok_subst sis si ctx)
  (REG_SUBST: reg_subst hrs sis si ctx)
  (RINIT: ris_input_init hrs = true)
  (OK_RREG: forall sv, In sv (ok_rsval hrs') -> eval_sval ctx sv <> None),
  reg_subst hrs' sis (exec_seq lseq si) ctx.
Proof.
  induction lseq; simpl; wlp_simplify.
  exploit (hrs_ival_set_preserv_rinit hrs hrs_old a0); eauto; intros RINIT'.
  exploit (hrs_ival_set_preserv_smem hrs hrs_old a0); eauto; intros MEM_EQ'.
  exploit (hrs_ival_set_preserv_sim hrs hrs_old a0); eauto; intros SIM'.
  eapply exec_seq_imp_incl in Hexta0 as INCL; eauto.
  eapply H; eauto;
  [ rewrite <- MEM_EQ'; assumption | idtac | eapply hrs_ival_set_reg_subst_nosim ; eauto ].
  all: eapply hrs_ival_set_si_ok_subst_nosim; eauto.
Qed.

Lemma exec_seq_imp_reg_subst_sim lseq: forall (hrs hrs_old: ristate),
  WHEN exec_seq_imp hrs hrs_old lseq ~> hrs' THEN forall ctx (sis: sistate) (si: fpasv)
  (SOK: sis_ok ctx sis)
  (ROK: ris_ok ctx hrs)
  (ROK_OLD: ris_ok ctx hrs_old)
  (RR_OLD: ris_refines ctx hrs_old sis)
  (MEM_EQ: (ris_smem hrs) = (ris_smem hrs_old))
  (OK_SUBST: si_ok_subst sis si ctx)
  (REG_SUBST: reg_subst hrs sis si ctx)
  (RINIT: ris_input_init hrs = true)
  (SIM: si_mode hrs = true),
  reg_subst hrs' sis (exec_seq lseq si) ctx.
Proof.
  induction lseq; simpl; wlp_simplify.
  inversion RR_OLD.
  exploit (hrs_ival_set_preserv_rok ctx hrs hrs_old a0); eauto; intros ROK'.
  exploit (hrs_ival_set_preserv_rinit hrs hrs_old a0); eauto; intros RINIT'.
  exploit (hrs_ival_set_preserv_sim hrs hrs_old a0); eauto; intros SIM'.
  exploit (hrs_ival_set_preserv_smem hrs hrs_old a0); eauto; intros MEM_EQ'.
  eapply H; eauto.
  - rewrite <- MEM_EQ'; assumption.
  - eapply hrs_ival_set_si_ok_subst_sim; eauto.
  - eapply hrs_ival_set_reg_subst_sim; eauto.
Qed.

(** ** Building a tree from the compact invariants representation, only on live variables *)
  
Fixpoint build_alive_imp (loutputs: list reg) (hrs: ristate): ?? (PTree.t sval) :=
  match loutputs with
  | nil => RET (PTree.empty sval)
  | r :: tl =>
      DO sreg <~ build_alive_imp tl hrs;;
      DO hsv <~ @hrs_sreg_get HCF hrs r;;
      RET (PTree.set r hsv sreg)
  end.

Lemma build_alive_imp_nofail loutputs sreg_subst: forall hrs r sv,
  WHEN build_alive_imp loutputs hrs ~> sreg THEN forall
  (BA: (sreg ! r = Some sv
       /\ (build_alive sreg_subst loutputs) ! r = None)
    \/ (sreg ! r = None
       /\ (build_alive sreg_subst loutputs) ! r = Some sv)),
  False.
Proof.
  induction loutputs; wlp_simplify.
  1,2: rewrite PTree.gEmpty in *; congruence.
  all:
    destruct (Pos.eq_dec a r); subst;
    try (try_simplify_someHyps; rewrite !PTree.gss in *; congruence);
    try (try_simplify_someHyps; rewrite !PTree.gso in *; eauto).
Qed.

Lemma build_alive_imp_sv_subst loutputs: forall hrs,
  WHEN build_alive_imp loutputs hrs ~> hrs' THEN forall ctx sis r sv1 sv2 lseq
  (GETR: hrs'!r = sv1)
  (REG_SUBST: reg_subst hrs sis (exec_seq lseq si_empty) ctx)
  (BA: (build_alive (ext (exec_seq lseq si_empty)) loutputs)!r = Some sv2),
  eval_osv ctx (hrs'!r) = eval_sval ctx (sv_subst (sis_smem sis) sis sv2).
Proof.
  induction loutputs; wlp_simplify.
  - rewrite PTree.gEmpty in BA; congruence.
  - destruct (Pos.eq_dec a r); subst.
    + rewrite PTree.gss in *; inv BA.
      rewrite <- REG_SUBST, <- H0; auto.
    + rewrite PTree.gso in *; auto.
      eapply IHloutputs; eauto.
Qed.

Lemma build_alive_imp_alive_eq lseq loutputs: forall (hrs: ristate) r,
  WHEN build_alive_imp loutputs hrs ~> hsreg THEN
  hsreg!r = None <-> (build_alive (ext (exec_seq lseq si_empty)) loutputs)!r = None.
Proof.
  induction loutputs; wlp_simplify;
  destruct (Pos.eq_dec a r); subst; intros.
  1,3: rewrite PTree.gss in *; congruence.
  all: rewrite PTree.gso in *; auto.
Qed.
Global Opaque build_alive_imp.

(** ** Verifying that the input state has [ris_input_init] set to [true] *)

Definition chk_input_init hrs: ?? unit :=
  if ris_input_init hrs then RET tt else FAILWITH "chk_input_init: input_init is false".

Definition chk_si_mode hrs: ?? unit :=
  if si_mode hrs then RET tt else FAILWITH "chk_si_mode: si_mode is false".

Lemma chk_si_mode_correct hrs:
  WHEN chk_si_mode hrs ~> _ THEN
  si_mode hrs = true.
Proof. wlp_simplify. Qed.
Global Opaque chk_si_mode.
Hint Resolve chk_si_mode_correct: wlp.

(** ** Combining trees of invariant (union) for jumptables *)

Definition most_defined_sv_imp (sv1 sv2: sval): ?? sval :=
  DO b <~ phys_eq sv1 sv2;;
  if b then RET sv1
  else FAILWITH "most_defined_sv_imp: no most defined symbolic value".

Lemma most_defined_sv_imp_determ: forall sv1 sv2,
  WHEN most_defined_sv_imp sv1 sv2 ~> sv3 THEN
  WHEN most_defined_sv_imp sv1 sv2 ~> sv3' THEN
  sv3 = sv3'.
Proof.
  unfold most_defined_sv_imp; wlp_simplify.
Qed.
Hint Resolve most_defined_sv_imp_determ: core.

Lemma most_defined_sv_imp_eq_and: forall a1 a2 : sval,
  WHEN PTree.f_imp_mostdef most_defined_sv_imp (Some a1) (Some a2) ~> oa
  THEN Some a1 = oa /\ Some a2 = oa.
Proof.
  wlp_simplify.
Qed.
Hint Resolve most_defined_sv_imp_eq_and: core.

Definition tr_hrs_single (hrs: ristate) (csi: csasv): ?? ristate :=
  DO hrs' <~ exec_seq_imp hrs hrs csi.(aseq);;
  DO sreg <~ build_alive_imp (Regset.elements csi.(outputs)) hrs';;
  RET (ris_sreg_set hrs' sreg).

Definition tr_sreg_union (sreg1 sreg2: PTree.t sval): ?? PTree.t sval :=
  PTree.combine_imp_mostdef most_defined_sv_imp sreg1 sreg2.

(** ** Updating an hashed refined state by applying the invariant
       NOTE that unlike in theory, here we use the same function for
       every final value.
*)

Fixpoint tr_hrs_rec (hrs: ristate) (lcsi: list csasv) {struct lcsi}: ?? ristate :=
  match lcsi with
  | nil => DO hrs' <~ tr_hrs_single hrs csi_empty;; RET (ris_input_false hrs')
  | csi :: nil => DO hrs' <~ tr_hrs_single hrs csi;; RET (ris_input_false hrs')
  | csi :: lcsi' =>
      DO hrs_r <~ tr_hrs_rec hrs lcsi';;
      DO hrs_l <~ tr_hrs_single hrs csi;;
      DO sreg <~ tr_sreg_union (hrs_l.(ris_sreg)) (hrs_r.(ris_sreg));;
      let ok_rsval_u := hrs_l.(ok_rsval) ++ hrs_r.(ok_rsval) in
      RET (ris_sreg_ok_set hrs_r sreg ok_rsval_u)
  end.

Definition tr_hrs (hrs: ristate) (lcsi: list csasv): ?? ristate :=
  DO _ <~ chk_input_init hrs;;
  DO _ <~ chk_si_mode hrs;;
  DO hrs' <~ tr_hrs_rec hrs lcsi;;
  RET hrs'.

Lemma sis_history_ok_equiv ctx (csix: invariants):
  WHEN exec_seq_imp ris_empty ris_empty (aseq (history csix)) ~> hrsH THEN forall
  (RINIT: ris_input_init hrsH = true)
  (MEM_EQ: ris_smem hrsH = fSinit),
  sis_ok ctx (sis_history csix)
  <-> ris_ok ctx hrsH.
Proof.
  unfold ris_empty, sis_history, tr_sis; wlp_simplify; inv H; simpl in *.
  - constructor; [ rewrite MEM_EQ |]; auto. intros sv HIN.
    destruct OK_PRE as (_ & PRE1 & PRE2).
    wlp_exploit exec_seq_imp_in; eauto with sempty.
    intros (sv' & ESVEQ & HIN_SI). rewrite ESVEQ.
    intros CONTRA; eapply PRE2; eauto.
  - assert (SIOK: si_ok ctx (history csix)).
    { apply si_ok_subst_sis_empty_si_ok.
      eapply exec_seq_imp_si_ok_subst_nosim; eauto with sempty. }
    repeat constructor; auto; simpl; try congruence.
    + intros r sv HINV; inv HINV. simpl; congruence.
    + intros sv HIN; autorewrite with sempty. apply SIOK; auto.
    + intros r sv; autodestruct; intros GETR HINV; inv HINV; simpl; try congruence.
      apply si_ok_sreg in SIOK as OK_SREG; eauto. autorewrite with sempty.
      eapply OK_SREG; eauto.
Qed.

Lemma sis_history_refines ctx (csix: invariants):
  WHEN tr_hrs_single ris_empty (history csix) ~> hrsH THEN
  ris_refines ctx hrsH (sis_history csix)
  /\ ris_input_init hrsH = true
  /\ ris_smem hrsH = fSinit
  /\ si_mode hrsH = false.
Proof.
  unfold ris_empty; wlp_xsimplify ltac:(eauto with wlp).
  eapply exec_seq_imp_preserv_rinit in Hexta as RINIT.
  eapply exec_seq_imp_preserv_sim_smem in Hexta as SIM_MEM_EQ.
  destruct SIM_MEM_EQ as (SIM & MEM_EQ).
  simpl in *; split; [| repeat split; auto ].
  constructor; simpl in *; auto.
  - eapply sis_history_ok_equiv in Hexta. rewrite Hexta; auto.
    unfold ris_sreg_set; split; intros ROK; inv ROK; constructor; simpl in *; auto.
  - rewrite <- MEM_EQ; simpl; congruence.
  - intros; unfold siof, si_apply, ris_sreg_get; simpl.
    rewrite <- RINIT. split; repeat autodestruct.
  - intros ROK r; inv ROK; simpl in *; unfold siof, si_apply, ris_sreg_get, ext; simpl.
    rewrite <- RINIT. repeat autodestruct.
    2,3: wlp_exploit build_alive_imp_nofail.
    intros BA_OUT GETR.
    exploit build_alive_imp_sv_subst; unfold reg_subst; eauto.
    + intros;
      eapply (exec_seq_imp_reg_subst_nosim (aseq (history csix)) ris_empty); eauto with sempty.
    + intros HSUBST_EMPTY. rewrite <- GETR, HSUBST_EMPTY.
      unfold eval_osv; autorewrite with sempty using auto.
Qed.

Lemma sis_invariants_refines ctx (csix: invariants) hrsH:
  WHEN tr_hrs_single hrsH (glue csix) ~> hrsHG THEN forall
  (RR: ris_refines ctx hrsH (sis_history csix))
  (RINIT: ris_input_init hrsH = true)
  (MEM_EQ: ris_smem hrsH = fSinit)
  (SIM: si_mode hrsH = false),
  ris_refines ctx hrsHG (sis_invariants csix)
  /\ ris_smem hrsHG = fSinit
  /\ ris_input_init hrsHG = true
  /\ incl (ok_rsval hrsH) (ok_rsval hrsHG)
  /\ (forall r, (ris_sreg hrsHG) ! r = None <-> (glue csix r) = None).
Proof.
  wlp_xsimplify ltac:(eauto with wlp).
  apply exec_seq_imp_preserv_rinit in Hexta as RINIT'; simpl in RINIT'; auto.
  apply exec_seq_imp_preserv_smem in Hexta as MEM_EQ'.
  apply exec_seq_imp_incl in Hexta as INCL.
  intros; split; [| repeat split; auto ].
  2: rewrite <- MEM_EQ'; assumption.
  2: rewrite <- RINIT'; assumption.
  inversion RR. unfold sis_invariants, sis_history in *.
  constructor; simpl in *; auto.
  - split; intros OK; inversion OK.
    + apply sis_invariants_ok_sis_history in OK as SOK_H. apply OK_EQUIV in SOK_H as ROK.
      constructor; simpl in *; [ rewrite <- MEM_EQ', MEM_EQ |]; auto. intros sv HIN.
      destruct OK_PRE as ((_ & PRE1 & PRE2) & PRE3 & PRE4).
      destruct (in_dec sveq_dec sv (ok_rsval hrsH)); [ inv ROK; eauto |].
      wlp_exploit exec_seq_imp_in; eauto with sempty.
      simpl; intros (sv' & ESVEQ & HIN'); rewrite ESVEQ; auto.
    + apply ris_sreg_set_incl_ok with (ris':=hrsH) in OK; auto.
      apply OK_EQUIV in OK as SOK; inversion SOK.
      destruct OK_PRE as (_ & PRE1 & PRE2).
      assert (OK_SUBST: si_ok_subst (sis_history csix) (exec_seq (aseq csix) si_empty) ctx)
      by (eapply exec_seq_imp_si_ok_subst_nosim; eauto with sempty;
        unfold si_ok_subst; simpl; contradiction).
      repeat constructor; auto; simpl.
      eapply si_ok_subst_invariants_sreg; eauto.
  - rewrite <- MEM_EQ', MEM_EQ; simpl; congruence.
  - intros ROK r; unfold ris_sreg_set, ris_sreg_get in *; simpl in *.
    split; repeat autodestruct.
  - intros ROK r; unfold ris_sreg_set, ris_sreg_get, siof, si_apply; simpl in *.
    repeat autodestruct. 2,3: wlp_exploit build_alive_imp_nofail.
    apply ris_sreg_set_incl_ok with (ris':=hrsH) in ROK as ROK'; auto.
    intros BA GETR. unfold eval_osv; exploit build_alive_imp_sv_subst; eauto.
    instantiate (2:=sis_history csix).
    + apply ris_ok_set_sreg in ROK; inv ROK.
      eapply (exec_seq_imp_reg_subst_nosim (aseq csix) hrsH hrsH); eauto with sempty.
    + unfold sis_history, tr_sis, siof, si_apply; simpl; rewrite GETR; auto.
  - destruct (glue csix r) eqn:CONTRA; auto.
    revert CONTRA; unfold siof, si_apply; simpl.
    wlp_exploit build_alive_imp_nofail.
  - destruct (exta0!r) eqn:CONTRA; auto.
    revert CONTRA; unfold siof, si_apply; simpl.
    wlp_exploit build_alive_imp_nofail.
Qed.

Lemma sis_source_refines ctx csix hrsH hrsHG
  (RR1: ris_refines ctx hrsH (sis_history csix))
  (RR2: ris_refines ctx hrsHG (sis_invariants csix))
  (MEM_EQ1: ris_smem hrsHG = fSinit)
  (MEM_EQ2: ris_smem hrsHG = ris_smem hrsH)
  (RINIT_EQ: ris_input_init hrsHG = ris_input_init hrsH)
  (INCL: incl (ok_rsval hrsH) (ok_rsval hrsHG))
  :ris_refines ctx (ris_sreg_set hrsHG (ris_sreg hrsH)) (sis_source csix).
Proof.
  unfold sis_history, sis_invariants, sis_source.
  inversion RR1; inversion RR2; constructor; simpl in *.
  - split; intros OK; inversion OK.
    + apply sis_source_ok_sis_history in OK as SOKh.
      apply sis_source_ok_sis_invariants in OK as SOKinvs; auto.
      apply OK_EQUIV0 in SOKinvs. apply ris_ok_set_sreg; assumption.
    + apply ris_ok_set_sreg in OK. apply OK_EQUIV0 in OK.
      apply sis_invariants_ok_sis_source; auto.
  - rewrite MEM_EQ1; simpl; congruence.
  - intros ROK r; unfold ris_sreg_set, ris_sreg_get in *; simpl in *.
    apply ris_sreg_set_incl_ok with (ris':=hrsH) in ROK; auto.
    rewrite RINIT_EQ, ALIVE_EQ; auto. reflexivity.
  - intros ROK r; unfold ris_sreg_set, ris_sreg_get in *; simpl in *.
    apply ris_sreg_set_incl_ok with (ris':=hrsH) in ROK; auto.
    rewrite RINIT_EQ, REG_EQ; auto.
Qed.

Lemma sis_target_refines ctx csix hrsH hrsHG
  (RR1: ris_refines ctx hrsH (sis_history csix))
  (RR2: ris_refines ctx hrsHG (sis_invariants csix))
  (MEM_EQ1: ris_smem hrsHG = fSinit)
  (MEM_EQ2: ris_smem hrsHG = ris_smem hrsH)
  (RINIT_EQ: ris_input_init hrsHG = ris_input_init hrsH)
  (INCL: incl (ok_rsval hrsH) (ok_rsval hrsHG))
  (GLUE_ALIVE: forall r, (ris_sreg hrsHG) ! r = None <-> (glue csix r) = None)
  :ris_refines ctx (ris_input_false hrsHG) (sis_target csix).
Proof.
  unfold sis_history, sis_invariants, sis_source.
  inversion RR1; inversion RR2; constructor; simpl in *.
  - split; intros OK; inversion OK.
    + apply sis_target_ok_sis_history in OK as SOKh.
      apply sis_target_ok_sis_invariants in OK as SOKinvs; auto.
      apply OK_EQUIV0 in SOKinvs. rewrite <- ris_input_false_ok; assumption.
    + apply ris_input_false_ok in OK. apply OK_EQUIV0 in OK.
      apply sis_invariants_ok_sis_target; auto.
  - rewrite MEM_EQ1; simpl; congruence.
  - intros ROK r; unfold ris_sreg_set, ris_sreg_get in *; simpl in *.
    apply ris_input_false_ok in ROK; auto. split; repeat autodestruct;
    rewrite GLUE_ALIVE || rewrite <- GLUE_ALIVE; congruence.
  - intros ROK r; unfold ris_sreg_set, ris_sreg_get in *; simpl in *.
    apply ris_input_false_ok in ROK; auto. repeat autodestruct.
    2,3: rewrite GLUE_ALIVE || rewrite <- GLUE_ALIVE; congruence.
    intros CSIR GETR; specialize (REG_EQ0 ROK r).
    rewrite CSIR, GETR in REG_EQ0; auto.
Qed.

(** Grouping together properties about the consistency
    of invariants application on a symbolic internal state. *)
Record tr_hrs_coherent ctx (hrs0 hrs: ristate) (sis: sistate) (si: fpasv) : Prop := {
  ALIVE: forall r : positive, hrs r = None -> si r = None;
  REG: forall r sv1 sv2, hrs r = Some sv1 -> si r = Some sv2 ->
       eval_sval ctx sv1 = eval_sval ctx (sv_subst (sis_smem sis) sis sv2);
  TRSOK: tr_sis_ok ctx sis si;
  RR: ris_refines ctx hrs (tr_sis sis si false);
  OKINCL: incl (ok_rsval hrs0) (ok_rsval hrs) }.

Theorem tr_hrs_single_correct hrs csi:
  WHEN tr_hrs_single hrs csi ~> hrs' THEN forall ctx sis
  (RINIT: ris_input_init hrs = true)
  (SIM: si_mode hrs = true)
  (SOK: sis_ok ctx sis)
  (RR: ris_refines ctx hrs sis),
  tr_hrs_coherent ctx hrs (ris_input_false hrs') sis csi.
Proof.
  wlp_simplify; inversion SOK; inversion RR0; subst.
  apply OK_EQUIV in SOK as ROK. unfold ris_input_false, ris_sreg_set.
  assert (TRSOK: tr_sis_ok ctx sis csi) by
  (eapply exec_seq_imp_si_ok_subst_sim; unfold si_ok_subst; eauto with sempty).
  econstructor; simpl; auto.
  - unfold ris_sreg_get, si_apply; simpl.
    intros r; autodestruct; intros GETR _.
    exploit build_alive_imp_alive_eq; intuition eauto.
  - unfold ris_sreg_get, si_apply; simpl; intros r sv1 sv2; autodestruct; intros.
    inv H; fold (eval_osv ctx (Some sv1)). rewrite <- EQ.
    eapply build_alive_imp_sv_subst; eauto.
    eapply exec_seq_imp_reg_subst_sim; eauto with sempty.
  - exploit exec_seq_imp_preserv_sim_smem; eauto; intros (SIM_OUT & MEM_EQ_OUT).
    constructor.
    + set (hrs' := {| ris_smem := exta;
                      ris_input_init := false;
                      si_mode := si_mode exta;
                      ok_rsval := ok_rsval exta;
                      ris_sreg := exta0 |}).
      apply exec_seq_imp_incl_rev in Hexta as INCL; auto.
      apply INCL in SIM; inv ROK.
      assert(OK_TR_HRS: ris_ok ctx hrs' <-> ris_ok ctx hrs) by
      (split; intro OK; inv OK; simpl in *; econstructor; eauto; simpl;
      rewrite <- MEM_EQ_OUT; auto).
      rewrite OK_TR_HRS, <- OK_EQUIV.
      apply ok_tr_sis; eauto.
    + simpl; rewrite <- MEM_EQ_OUT; auto.
    + intros; unfold ris_sreg_get, tr_sis, siof.
      exploit (build_alive_imp_alive_eq (aseq csi) (Regset.elements (outputs csi))); eauto; simpl.
      replace (SOME sv <- exta0 ! r IN Some sv) with (exta0 ! r) by autodestruct.
      intros ALIVE_EQ'; rewrite ALIVE_EQ'.
      split; autodestruct; intros BA1 BA2.
      exploit build_alive_nofail; eauto;
      contradiction.
    + intros; unfold ris_sreg_get, tr_sis, siof; simpl.
      replace (SOME sv <- exta0 ! r IN Some sv) with (exta0 ! r) by autodestruct.
      unfold eval_osv. repeat autodestruct; unfold si_apply; simpl.
      2,3: wlp_exploit build_alive_imp_nofail.
      intros BA_OUT GETR.
      exploit build_alive_imp_sv_subst; unfold reg_subst; eauto.
      * intros; eapply (exec_seq_imp_reg_subst_sim (aseq csi)); eauto with sempty; intros sv; auto.
      * intros HSUBST_EMPTY. fold (eval_osv ctx (Some s)).
        rewrite <- GETR, HSUBST_EMPTY; reflexivity.
  - eapply exec_seq_imp_incl; eauto.
Qed.
Global Opaque tr_hrs_single.

Lemma ris_ok_tr_update_app ctx rsvalsL rsvalsR hrs sreg:
  ris_ok ctx (ris_sreg_ok_set hrs sreg rsvalsL) ->
  ris_ok ctx (ris_sreg_ok_set hrs sreg rsvalsR) ->
  ris_ok ctx (ris_sreg_ok_set hrs sreg (rsvalsL ++ rsvalsR)).
Proof.
  unfold ris_sreg_ok_set; intros ROKL ROKR.
  inv ROKL; inv ROKR; constructor; auto; simpl; intros sv HIN.
  apply in_app_or in HIN; destruct HIN; auto.
Qed.

Lemma combine_tr_sis_ok_preserv ctx sis siL siR si
  (UNION2SI: union2_si ctx sis siL siR = Some si)
  (TRSIS_R : tr_sis_ok ctx sis siR)
  (TRSIS_L : tr_sis_ok ctx sis siL)
  :tr_sis_ok ctx sis si.
Proof.
  unfold tr_sis_ok, si_ok_subst, union2_si in *.
  destruct (union2_si_gen ctx sis siL siR); simpl in *; subst.
  destruct y as [FPAOK FPAREG].
  induction (fpa_reg siL) using PTree.tree_ind; induction (fpa_reg siR) using PTree.tree_ind; simpl.
  all:
    intros SOK sv HIN; rewrite FPAOK in HIN;
    apply in_app_or in HIN; destruct HIN; auto.
Qed.

Lemma combine_sis_ris_none ctx (siL siR si: fpasv) hrsL hrsR sis r
  (RINIT_R: ris_input_init hrsR = false)
  (ROKR: ris_ok ctx hrsR)
  (ROKL: ris_ok ctx (ris_input_false hrsL))
  (ALIVE_EQ_R: ris_ok ctx hrsR ->
    forall r, hrsR r = None <-> tr_sis sis siR false r = None)
  (ALIVE_EQ_L: ris_ok ctx (ris_input_false hrsL) ->
    forall r, (ris_input_false hrsL) r = None <-> tr_sis sis siL false r = None)
  (UNION2SI: union2_si ctx sis siL siR = Some si)
  :WHEN PTree.combine_imp_mostdef most_defined_sv_imp (ris_sreg hrsL) (ris_sreg hrsR) ~> sreg THEN
  si r = None ->
  sreg ! r = None.
Proof.
  unfold union2_si in UNION2SI. destruct (union2_si_gen ctx sis siL siR); simpl in *; subst.
  destruct y as [_ COMBINE].
  wlp_simplify; exploit PTree.gcombine_mostdef_none_rev; eauto; intros (HNL & HNR).
  assert (TRHNR: tr_sis sis siR false r = None) by (simpl; unfold si_apply; rewrite HNR; auto).
  assert (TRHNL: tr_sis sis siL false r = None) by (simpl; unfold si_apply; rewrite HNL; auto).
  apply H0 in TRHNR; apply H1 in TRHNL.
  replace (ris_input_false hrsL r) with ((ris_sreg hrsL)!r) in TRHNL by
  ( unfold ris_sreg_get, ris_input_false; simpl; autodestruct).
  replace (hrsR r) with ((ris_sreg hrsR)!r) in TRHNR
  by (unfold ris_sreg_get; rewrite RINIT_R; autodestruct).
  exploit PTree.gcombine_imp_mostdef_none; eauto.
Qed.

Lemma combine_ris_sis_none ctx sis (siL siR si: fpasv) (hrsR hrsL: ristate) r
  (RINIT_R: ris_input_init hrsR = false)
  (ALIVE_R : forall r : positive, hrsR r = None -> siR r = None)
  (ALIVE_L : forall r : positive, ris_input_false hrsL r = None -> siL r = None)
  (UNION2SI: union2_si ctx sis siL siR = Some si)
  :WHEN PTree.combine_imp_mostdef most_defined_sv_imp (ris_sreg hrsL) (ris_sreg hrsR) ~> sreg THEN
  sreg ! r = None ->
  si r = None.
Proof.
  wlp_simplify. exploit PTree.gcombine_imp_mostdef_none_rev; eauto; intros (HNL & HNR).
  specialize ALIVE_R with r; specialize ALIVE_L with r; unfold si_apply in *.
  replace (hrsR r) with ((ris_sreg hrsR)!r) in ALIVE_R by
  (unfold ris_sreg_get; rewrite HNR, RINIT_R; auto).
  replace (ris_input_false hrsL r) with ((ris_sreg hrsL)!r) in ALIVE_L by
  ( unfold ris_sreg_get, ris_input_false; simpl; rewrite HNL; auto).
  apply ALIVE_R in HNR; apply ALIVE_L in HNL.
  unfold union2_si in UNION2SI. destruct (union2_si_gen ctx sis siL siR); simpl in *; subst.
  destruct y as [_ COMBINE].
  exploit PTree.gcombine_mostdef; eauto; rewrite HNL, HNR; simpl; auto.
Qed.

Lemma combine_imp_ok_exists_r (hrsR: ristate) (siR: fpasv) r s
  (RINIT_R: ris_input_init hrsR = false)
  (SIR: siR ! r = Some s)
  (ALIVE_R: forall r, hrsR r = None -> siR r = None)
  :exists sv, hrsR r = Some sv.
Proof.
  unfold ris_sreg_get. autodestruct. eauto.
  intros. exploit ALIVE_R; unfold ris_sreg_get; rewrite RINIT_R.
  - rewrite EQ; auto.
  - unfold si_apply; congruence. 
Qed.

Lemma combine_imp_ok_exists_l hrsL (siL: fpasv) r s
  (SIR: siL r = Some s)
  (ALIVE_L: forall r, ris_input_false hrsL r = None -> siL r = None)
  :exists sv, ris_input_false hrsL r = Some sv.
Proof.
  unfold ris_sreg_get, ris_input_false in *; simpl in *.
  destruct ((ris_sreg hrsL)!r) eqn:REM.
  - exists s0; auto.
  - specialize ALIVE_L with r. rewrite REM in ALIVE_L.
    exploit ALIVE_L; auto. congruence.
Qed.

Lemma combine_imp_eval_eq ctx (siL siR si: fpasv) (hrs0 hrsL hrsR: ristate) sis r s1 s2
  (SIR: si ! r = Some s1)
  (RINIT_R: ris_input_init hrsR = false)
  (TRCR: tr_hrs_coherent ctx hrs0 hrsR sis siR)
  (TRCL: tr_hrs_coherent ctx hrs0 (ris_input_false hrsL) sis siL)
  (UNION2SI : union2_si ctx sis siL siR = Some si)
  :WHEN PTree.combine_imp_mostdef most_defined_sv_imp (ris_sreg hrsL) (ris_sreg hrsR) ~> sreg THEN
  forall (SRR: sreg ! r = Some s2),
  eval_sval ctx s2 = eval_sval ctx (sv_subst (sis_smem sis) sis s1).
Proof.
  inv TRCR; inv TRCL.
  unfold union2_si in UNION2SI. destruct (union2_si_gen ctx sis siL siR); simpl in *; subst.
  destruct y as [_ COMBINE].
  wlp_simplify. exploit PTree.gcombine_mostdef_ok; eauto.
  unfold PTree.f_mostdef, most_defined_sv. repeat autodestruct; intros; inv H.
  1,3:
    exploit (combine_imp_ok_exists_r hrsR siR); eauto; intros (svR & HSR);
    assert (HSRT: (ris_sreg hrsR)!r=Some svR) by
    ( generalize HSR; clear HSR; unfold ris_sreg_get; simpl; rewrite RINIT_R; autodestruct);
    eapply REG0; eauto; exploit PTree.gcombine_imp_mostdef_or_ok; eauto;
    intros; subst; auto.
  exploit (combine_imp_ok_exists_l hrsL siL); eauto; intros (svL & HSL);
  assert (HSLT: (ris_sreg hrsL)!r=Some svL) by
  (generalize HSL; clear HSL; unfold ris_sreg_get, ris_input_false; simpl; autodestruct);
  eapply REG1; eauto; exploit PTree.gcombine_imp_mostdef_or_ok; eauto;
  intros; subst; auto.
Qed.

Theorem tr_sreg_union_correct ctx (siL siR si: fpasv) (hrs0 hrsL hrsR: ristate) sis
  (SOK: sis_ok ctx sis)
  (RINIT_R: ris_input_init hrsR = false)
  (TRCR: tr_hrs_coherent ctx hrs0 hrsR sis siR)
  (TRCL: tr_hrs_coherent ctx hrs0 (ris_input_false hrsL) sis siL)
  (UNION2SI: union2_si ctx sis siL siR = Some si)
  :WHEN tr_sreg_union (ris_sreg hrsL) (ris_sreg hrsR) ~> sreg THEN
  tr_hrs_coherent ctx hrs0 (ris_sreg_ok_set hrsR sreg (ok_rsval hrsL ++ ok_rsval hrsR)) sis si.
Proof.
  inversion TRCR; inversion TRCL.
  eapply combine_tr_sis_ok_preserv in UNION2SI as TRSIS; eauto.
  unfold tr_sreg_union; wlp_simplify; inversion RR0; subst; inversion RR1; subst.
  apply (ok_tr_sis ctx sis siR false) in SOK as SOKR; auto.
  apply (ok_tr_sis ctx sis siL false) in SOK as SOKL; auto.
  apply OK_EQUIV in SOKR as ROKR.
  apply OK_EQUIV0 in SOKL as ROKL.
  constructor; auto.
  - intros; eapply combine_ris_sis_none; eauto.
    generalize H; clear H; unfold ris_sreg_ok_set, ris_sreg_get; simpl.
    rewrite RINIT_R; autodestruct.
  - intros; eapply combine_imp_eval_eq; eauto.
    generalize H; clear H; unfold ris_sreg_ok_set, ris_sreg_get; simpl.
    rewrite RINIT_R; autodestruct.
  - constructor.
    + split; [| rewrite ok_tr_sis; auto ]. intros TRSOK.
      rewrite OK_EQUIV in SOKR; rewrite OK_EQUIV0 in SOKL; inv SOKR; inv SOKL.
      apply ris_ok_tr_update_app; econstructor; simpl; eauto.
    + simpl; intros ROKLR; apply MEM_EQ.
      rewrite <- OK_EQUIV, ok_tr_sis; auto.
    + intros _.
      split; unfold ris_sreg_set, ris_sreg_get; simpl;
      rewrite RINIT_R; autodestruct; intros SIR _.
      * exploit combine_ris_sis_none; eauto.
        intros; rewrite H; reflexivity.
      * autodestruct; exploit combine_sis_ris_none; eauto; congruence.  
    + intros ROKLR.
      unfold ris_sreg_set, ris_sreg_get; simpl;
      rewrite RINIT_R; intros; repeat autodestruct.
      * unfold eval_osv.
        intros. eapply combine_imp_eval_eq; eauto.
      * intros; exploit combine_sis_ris_none; eauto; congruence.
      * intros; exploit combine_ris_sis_none; eauto; congruence.
  - unfold ris_sreg_ok_set; unfold ris_input_false in OKINCL1; simpl in *.
    apply incl_appl; auto.
Qed.

Theorem tr_sreg_union_nofail ctx (siL siR si: fpasv) (hrs0 hrsL hrsR: ristate) sis
  :WHEN tr_sreg_union (ris_sreg hrsL) (ris_sreg hrsR) ~> sreg THEN forall
  (SOK: sis_ok ctx sis)
  (RINIT_R: ris_input_init hrsR = false)
  (TRCR: tr_hrs_coherent ctx hrs0 hrsR sis siR)
  (TRCL: tr_hrs_coherent ctx hrs0 (ris_input_false hrsL) sis siL)
  (UNION2SI: union2_si ctx sis siL siR = None),
  False.
Proof.
  unfold tr_sreg_union, union2_si; wlp_simplify.
  destruct (union2_si_gen _ _ _ _); simpl in *; subst.
  exploit PTree.fcombine_mostdef; eauto; intros.
  unfold most_defined_sv, symbolic_eq.
  inv TRCL; inv TRCR. unfold si_apply in *.
  destruct (ris_input_false hrsL i) eqn:EQSIL; [| apply ALIVE0 in EQSIL; congruence ].
  destruct (hrsR i) eqn:EQSIR; [| apply ALIVE1 in EQSIR; congruence ].
  exploit PTree.gcombine_imp_mostdef_and_ok; eauto.
  - revert EQSIL; revert EQSIR; unfold ris_input_false, ris_sreg_get.
    simpl; repeat autodestruct; intros. inv EQSIL; inv EQSIR. intuition eauto.
  - intros SIEQ; subst. erewrite <- REG0; eauto. erewrite <- REG1; eauto.
    autodestruct. destruct (Values.Val.eq v v) eqn: EQV; simpl; congruence.
Qed.
Global Opaque tr_sreg_union.

Lemma tr_hrs_coherent_preserved ctx: forall hrs0 hrs sis si,
  tr_hrs_coherent (bcctx1 ctx) hrs0 hrs sis si
  <-> tr_hrs_coherent (bcctx2 ctx) hrs0 hrs sis si.
Proof.
  split; constructor; inv H; auto;
  unfold tr_sis_ok, si_ok_subst in *; intros;
  try (rewrite <- !sis_ok_preserved in * || rewrite -> !sis_ok_preserved in *);
  try (rewrite <- !eval_sval_preserved in * || rewrite -> !eval_sval_preserved in *);
  eauto.
  all: rewrite <- ris_refines_preserved || rewrite -> ris_refines_preserved; auto.
Qed.

Lemma refines_tr_sis_fpa_eq ctx hrs sis si csi sinit:
  fpa_eq (siof csi) si ->
  ris_refines ctx hrs (tr_sis sis csi sinit) -> ris_refines ctx hrs (tr_sis sis si sinit).
Proof.
  intros FPAEQ RR; inv FPAEQ; inv RR; constructor; auto.
  + rewrite <- OK_EQUIV. unfold tr_sis.
      split; intros TRSOK; inv TRSOK; constructor; simpl in *; auto.
      2,4: unfold si_apply in *; rewrite <- H0 in *; eauto.
      all:
        split; intuition auto; specialize H4 with sv;
        rewrite H in H4 || rewrite <- H in H4; auto.
  + intros ROK r; rewrite ALIVE_EQ; auto.
    split; intros TRS; inv TRS; simpl;
    unfold si_apply; rewrite H0; auto.
  + intros ROK r; rewrite REG_EQ; auto; simpl.
    unfold si_apply; rewrite H0; auto.
Qed.

Definition ctx_switch_prop ctx1 ctx2 :=
  ctx1 = ctx2 \/ (exists ctx, (ctx1 = bcctx1 ctx) /\ (ctx2 = bcctx2 ctx)).

Ltac destruct_ctx_sw hctx := destruct hctx as [CTXEQ|[ctx [HCTX1 HCTX2]]]; subst; auto.

Lemma tr_hrs_coherent_csi_si_empty ctx hrs0 hrs sis:
  tr_hrs_coherent ctx hrs0 hrs sis csi_empty ->
  tr_hrs_coherent ctx hrs0 hrs sis si_empty.
Proof.
  intros TRC; inv TRC; constructor; auto.
  inv RR0; constructor; auto.
  rewrite <- OK_EQUIV; split; intros TRSOK; inv TRSOK; constructor; auto.
Qed.

Lemma tr_hrs_coherent_csi_si ctx hrs0 hrs sis (csi: csasv) (si: fpasv)
  (FPAEQ: fpa_eq (siof csi) si)
  (SOK: sis_ok ctx sis)
  (TRC: tr_hrs_coherent ctx hrs0 hrs sis csi)
  :tr_hrs_coherent ctx hrs0 hrs sis si.
Proof.
  inversion FPAEQ; inv TRC; constructor; auto.
  - unfold si_apply in *; rewrite <- H0; auto.
  - unfold si_apply in *; rewrite <- H0; auto.
  - unfold tr_sis_ok, si_ok_subst in *. intros _ sv. rewrite <- H; auto.
  - eapply refines_tr_sis_fpa_eq; eauto.
Qed.
Hint Resolve tr_hrs_coherent_csi_si_empty tr_hrs_coherent_csi_si: core.

Theorem tr_hrs_single_correct_ctx csi: forall ctx1 ctx2 si hrs sis
  (FPAEQ: fpa_eq (siof csi) si)
  (RINIT: ris_input_init hrs = true)
  (SIM: si_mode hrs = true)
  (SOK: sis_ok ctx1 sis)
  (RR: ris_refines ctx1 hrs sis)
  (HCTX: ctx_switch_prop ctx1 ctx2),
  WHEN tr_hrs_single hrs csi ~> hrs' THEN
  tr_hrs_coherent ctx2 hrs (ris_input_false hrs') sis si.
Proof.
  intros; wlp_simplify.
  exploit tr_hrs_single_correct; eauto; intuition.
  destruct_ctx_sw HCTX; [| rewrite <- tr_hrs_coherent_preserved; auto ]; eauto.
Qed.

Theorem tr_hrs_rec_correct lcsi: forall ctx1 ctx2 si hrs sis
  (UNION: union_si ctx2 sis lcsi = Some si)
  (RINIT: ris_input_init hrs = true)
  (SIM: si_mode hrs = true)
  (SOK: sis_ok ctx1 sis)
  (RR: ris_refines ctx1 hrs sis)
  (HCTX: ctx_switch_prop ctx1 ctx2),
  WHEN tr_hrs_rec hrs lcsi ~> hrs' THEN
  tr_hrs_coherent ctx2 hrs hrs' sis si
  /\ ris_input_init hrs' = false.
Proof.
  induction lcsi; intros; inv UNION; simpl.
  - wlp_xsimplify ltac:(eauto with wlp); intros.
    exploit tr_hrs_single_correct; eauto; intuition.
    destruct_ctx_sw HCTX; rewrite <- tr_hrs_coherent_preserved; auto.
  - destruct lcsi.
    + wlp_simplify; exploit tr_hrs_single_correct_ctx; eauto.
      exploit (union_si_single ctx2 sis a si); eauto.
    + generalize H0; clear H0; autodestruct; intros UNION COMBINE.
      wlp_simplify. exploit tr_hrs_single_correct; eauto; intros TRCL.
      destruct_ctx_sw HCTX.
      * exploit (tr_sreg_union_correct ctx2 a f si hrs exta0 exta sis); eauto.
      * rewrite sis_ok_preserved in SOK.
        exploit (tr_sreg_union_correct (bcctx2 ctx) a f si hrs exta0 exta sis); eauto.
        rewrite <- tr_hrs_coherent_preserved; auto.
Qed.

Lemma tr_hrs_rec_nofail lcsi: forall ctx1 ctx2 sis hrs,
  WHEN tr_hrs_rec hrs lcsi ~> _ THEN forall
  (RINIT: ris_input_init hrs = true)
  (SIM: si_mode hrs = true)
  (SOK: sis_ok ctx1 sis)
  (RR: ris_refines ctx1 hrs sis)
  (HCTX: ctx_switch_prop ctx1 ctx2),
  union_si ctx2 sis lcsi <> None.
Proof.
  induction lcsi; intros.
  - wlp_simplify; inv H.
  - simpl. destruct lcsi.
    + wlp_simplify. unfold union2_si in H.
      destruct (union2_si_gen _ _ _ _); simpl in *; subst.
      exploit PTree.fcombine_mostdef; eauto; intros.
      rewrite PTree.gEmpty in H0; discriminate.
    + Local Opaque tr_hrs_rec. wlp_simplify. Local Transparent tr_hrs_rec.
      exploit H; eauto; clear H.
      destruct (SOME si2 <- union_si _ sis lcsi IN union2_si _ sis c si2) eqn:UNION; auto.
      exploit tr_hrs_rec_correct; eauto; [ simpl; eauto |]. intros (TRC_U & RINIT_U).
      eapply tr_hrs_single_correct in Hexta0; eauto.
      destruct_ctx_sw HCTX.
      * wlp_exploit (tr_sreg_union_nofail ctx2).
      * wlp_exploit (tr_sreg_union_nofail (bcctx2 ctx));
        rewrite <- sis_ok_preserved || rewrite <- tr_hrs_coherent_preserved; auto.
Qed.
Hint Resolve tr_hrs_rec_correct: wlp.

(** ** Applying invariant for a hashed refined state *)

Definition tr_sfv hrs (gm_select: node -> csasv) sfv: ?? ristate :=
  match sfv with
  | Sgoto pc => tr_hrs hrs [gm_select pc]
  | Scall sig svid args res pc =>
      if test_clobberable (gm_select pc) res then
        tr_hrs hrs [(csi_remove res (gm_select pc))]
      else FAILWITH "ri_sfv: Scall res not clobberable"
  | Sbuiltin ef args bres pc =>
      match reg_builtin_res bres with
      | Some r =>
          if test_clobberable (gm_select pc) r then
            tr_hrs hrs [(csi_remove r (gm_select pc))]
          else FAILWITH "ri_sfv: Sbuiltin res not clobberable"
      | None =>
          if test_csifreem (gm_select pc) then tr_hrs hrs [gm_select pc]
          else FAILWITH "ri_sfv: Sbuiltin memory dependent"
      end
  | Stailcall sig svid args => tr_hrs hrs []
  | Sreturn osv => tr_hrs hrs []
  | Sjumptable sv lpc => tr_hrs hrs (List.map (fun pc => gm_select pc) lpc)
  end.

(** Grouping together properties about the consistency
    of invariants application on a symbolic general state. *)
Record tr_hst_coherent ctx1 ctx2 input_init (trss ss: sstate) (rst0 rst: rstate) sfv := {
  TRSS: trss = ss;
  TRSS_OK: trss_ok ctx1 ss;
  REF: rst_refines input_init ctx2 rst ss;
  SYMB: exists sis, symb_exec_ok ctx2 ss sis sfv;
  OKIN: rst_ok_in ctx1 rst0 rst }.

Lemma tr_sfv_correct_aux ctx1 ctx2 sis hrs0 hrs si sfv1 sfv2
  (SOK : sis_ok ctx1 sis)
  (RFV : rfv_refines ctx1 sfv1 sfv2)
  (TRC: tr_hrs_coherent ctx2 (ris_si_mode_true hrs0) hrs sis si)
  (RINIT : ris_input_init hrs = false)
  (HCTX: ctx_switch_prop ctx1 ctx2)
  :exists ss : sstate, tr_hst_coherent ctx1 ctx2 false
    (Sfinal (tr_sis sis si false) sfv2) ss
    (Rfinal hrs0 sfv1) (Rfinal hrs sfv1) sfv2.
Proof.
  unfold symb_exec_ok. inv TRC.
  eexists; repeat (split; eauto).
  - do 3 econstructor; simpl; auto.
    apply ok_tr_sis; auto. unfold tr_sis_ok, si_ok_subst in *.
    destruct_ctx_sw HCTX. intros; rewrite eval_sval_preserved.
    apply TRSOK0; auto. apply sis_ok_preserved; auto.
  - constructor; auto; intros.
    destruct_ctx_sw HCTX. apply rfv_refines_preserved; auto.
  - do 2 econstructor; simpl; auto.
    apply ok_tr_sis; auto.
    destruct_ctx_sw HCTX. apply sis_ok_preserved; auto.
Qed.
Hint Resolve tr_sfv_correct_aux: core.
Hint Resolve fpa_eq_refl fpa_eq_csi_si_empty: core.

Lemma tr_sfv_correct ctx1 ctx2 gm_select ss1E hrs1_E rfv sis1 sfv1
  (SOUT : get_soutcome ctx1 ss1E = Some {| _sis := sis1; _sfv := sfv1 |})
  (SOK : sis_ok ctx1 sis1)
  (REF1E : rst_refines true ctx1 (Rfinal (ris_si_mode_true hrs1_E) rfv) ss1E)
  (HCTX: ctx_switch_prop ctx1 ctx2)
  :WHEN tr_sfv (ris_si_mode_true hrs1_E) gm_select rfv ~> hrs1_EI THEN
  exists ss1EI : sstate, tr_hst_coherent ctx1 ctx2 false
    (tr_sstate ctx2 (fun pc => gm_select pc) ss1E) ss1EI
    (Rfinal hrs1_E rfv) (Rfinal hrs1_EI rfv) sfv1.
Proof.
  inv REF1E; inversion RIS; rewrite <- OK_EQUIV in RFV.
  simpl in *; inv SOUT.
  exploit RFV; auto; clear RFV; intros RFV.
  unfold tr_sfv; destruct rfv; wlp_ssimplify ltac:(intuition eauto with wlp);
  inversion RFV; subst; simpl; repeat autodestruct.
  1-5,8:
    intros; eapply tr_hrs_single_correct_ctx in Hexta0; eauto; try_simplify_someHyps.
  - intros; eapply tr_hrs_rec_correct in Hexta0; intuition eauto.
  - wlp_exploit tr_hrs_rec_nofail.
Qed.
Global Opaque tr_sfv.
Hint Resolve tr_sfv_correct: wlp.

Fixpoint tr_hstate (gm_select: node -> csasv) hst: ?? rstate :=
  match hst with
  | Rfinal hrs sfv =>
      DO hrs' <~ tr_sfv (ris_si_mode_true hrs) gm_select sfv;;
      RET (Rfinal hrs' sfv)
  | Rcond cond args ifso ifnot =>
      DO ifso' <~ tr_hstate gm_select ifso;;
      DO ifnot' <~ tr_hstate gm_select ifnot;;
      RET (Rcond cond args ifso' ifnot')
  | Rabort => RET Rabort
  end.

Theorem tr_hstate_correct hst1_E ctx1 ctx2: forall gm_select ss1E sis1 sfv1
  (SOUT: get_soutcome ctx1 ss1E = Some {| _sis := sis1; _sfv := sfv1 |})
  (SOK : sis_ok ctx1 sis1)
  (REF1E: rst_refines true ctx1 hst1_E ss1E)
  (HCTX: ctx_switch_prop ctx1 ctx2),
  WHEN tr_hstate gm_select hst1_E ~> hst1_EI THEN
  exists ss1EI, tr_hst_coherent ctx1 ctx2 false
    (tr_sstate ctx2 (fun pc => gm_select pc) ss1E) ss1EI hst1_E hst1_EI sfv1.
Proof.
  unfold tr_hstate; induction hst1_E; wlp_simplify.
  - eapply tr_sfv_correct in Hexta; eauto.
    inv REF1E. constructor; auto.
    + apply ris_refines_preserv_sim; auto.
    + rewrite ris_ok_preserv_sim; auto.
  - inversion REF1E; subst; inv SOUT.
    generalize H0; clear H0; repeat autodestruct; intros; subst;
    [ exploit IHhst1_E1; eauto | exploit IHhst1_E2; eauto ]; intros (ss1EI & THC);
    inv THC; destruct SYMB0 as (sis1' & SYMB); simpl in *.
    all:
      eexists; repeat split; simpl; eauto;
      unfold trss_ok, symb_exec_ok in *; simpl.
    3,7: exists sis1'.
    1,3,4,6: destruct_ctx_sw HCTX; simplify_ctx; rewrite EQ0; eauto.
    2,4: eapply Okincond; eauto; rewrite RCOND; congruence.
    all:
      destruct_ctx_sw HCTX; econstructor; simplify_ctx; auto;
      try rewrite RCOND, EQ0; auto; try congruence.
  - inv REF1E; try_simplify_someHyps; congruence.
Qed.
Global Opaque tr_hstate.

End SymbolicInvariants.
