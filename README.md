# Chamois CompCert reference version for Gourdin Léo's PhD thesis

**/!\ This version is froze to serve as the reference branch described in my PhD thesis.
The latest development version of the Verimag's Chamois fork is available [here](https://gricad-gitlab.univ-grenoble-alpes.fr/certicompil/Chamois-CompCert).**

A fork of the formally verified C compiler by [Verimag](https://www-verimag.imag.fr) (in French Alps).
See the [documentation](https://yukit.frama.io/compcert-chamois-gl-thesis/) of this frozen version for the RISC-V backend,
and the [documentation](https://certicompil.gricad-pages.univ-grenoble-alpes.fr/Chamois-CompCert) of Chamois CompCert for the KVX backend.
