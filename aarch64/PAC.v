(* *************************************************************)
(*                                                             *)
(*             The Compcert verified compiler                  *)
(*                                                             *)
(*           David Monniaux     CNRS, VERIMAG                  *)
(*                                                             *)
(*  Copyright VERIMAG. All rights reserved.                    *)
(*  This file is distributed under the terms of the INRIA      *)
(*  Non-Commercial License Agreement.                          *)
(*                                                             *)
(* *************************************************************)

Require Import Values.
Require Import AST.
Require Linear.

Inductive pac_kind := PAC_NONE | PAC_KEYA.

Axiom choose_pac_kind : Linear.function -> pac_kind.

(** Pointer authentication *)
Axiom pointer_auth_encode_keyA :
  forall (ptr modifier : val), val.
Axiom pointer_auth_decode_keyA :
  forall (ptr modifier : val), val.
Axiom pointer_auth_idem_keyA:
  forall ptr modifier,
    (pointer_auth_decode_keyA
       (pointer_auth_encode_keyA ptr modifier) modifier) =
      ptr.    
Axiom pointer_auth_encode_undef_keyA :
  forall (ptr modifier : val),
    pointer_auth_encode_keyA ptr modifier = Vundef ->
    ptr = Vundef \/ modifier = Vundef.
Axiom pointer_auth_Tptr_keyA :
  forall (ptr modifier : val),
    Val.has_type ptr Tptr -> Val.has_type modifier Tptr ->
    Val.has_type (pointer_auth_encode_keyA ptr modifier) Tptr.

Definition pointer_auth_encode kind : val -> val -> val :=
  match kind with
  | PAC_NONE => fun ptr modifier => ptr
  | PAC_KEYA => pointer_auth_encode_keyA
  end.

(*
Definition pointer_auth_decode kind : val -> val -> val :=
  match kind with
  | PAC_NONE => fun ptr modifier => ptr
  | PAC_KEYA => pointer_auth_decode_keyA
  end.

Lemma pointer_auth_idem:
  forall kind ptr modifier,
    (pointer_auth_decode kind
       (pointer_auth_encode kind ptr modifier) modifier) =
      ptr.
Proof.
  destruct kind; cbn; intros.
  reflexivity.
  apply pointer_auth_idem_keyA.
Qed.

Lemma pointer_auth_encode_undef :
  forall kind (ptr modifier : val),
    pointer_auth_encode kind ptr modifier = Vundef ->
    ptr = Vundef \/ modifier = Vundef.
Proof.
  destruct kind; cbn; intros.
  { left. assumption. }
  apply pointer_auth_encode_undef_keyA.
  assumption.
Qed.
 *)

Lemma pointer_auth_Tptr :
  forall kind (ptr modifier : val),
    Val.has_type ptr Tptr -> Val.has_type modifier Tptr ->
    Val.has_type (pointer_auth_encode kind ptr modifier) Tptr.
Proof.
  destruct kind; cbn; intros.
  assumption.
  apply pointer_auth_Tptr_keyA; auto.
Qed.
