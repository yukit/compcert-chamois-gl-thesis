(* *************************************************************)
(*                                                             *)
(*             The Compcert verified compiler                  *)
(*                                                             *)
(*           Léo Gourdin        VERIMAG, UGA                   *)
(*                                                             *)
(*  Copyright VERIMAG. All rights reserved.                    *)
(*  This file is distributed under the terms of the INRIA      *)
(*  Non-Commercial License Agreement.                          *)
(*                                                             *)
(* *************************************************************)

Require Import Coqlib Floats Values Memory.
Require Import Integers.
Require Import Op Registers.
Require Import BTL_SEtheory.
Require Import BTL_SEsimuref.

Import SvalNotations.

(** Target op simplifications using "fake" values *)

Definition rewrite_ops (RRULES: rrules_set) (op: operation) (lsv: list sval)
  : option sval :=
  None.

Definition rewrite_cbranches (RRULES: rrules_set) (prev: ristate)
  (cond: condition) (args: list reg) : option (condition * list_sval) :=
  None.
