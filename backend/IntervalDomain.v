Require Import Coqlib Values Zbits Integers Lattice.
Require Eqdep_dec.

Module Interval <: SEMILATTICE_WITHOUT_BOTTOM.

Record interval :=
  mkinterval { itv_lo : Z.t;
               itv_hi : Z.t;
               itv_ok : itv_lo <=? itv_hi = true }.

Lemma itv_eq_bounds :
  forall a b,
    a.(itv_lo) = b.(itv_lo) ->
    a.(itv_hi) = b.(itv_hi) ->
    a = b.
Proof.
  intros.
  destruct a as [aulo auhi auok].
  destruct b as [bulo buhi buok].
  cbn in *.
  subst. f_equal.
  apply Eqdep_dec.UIP_dec.
  decide equality.
Qed.

Definition t := interval.

Definition eq (a b : t) :=
  a.(itv_lo) = b.(itv_lo) /\
    a.(itv_hi) = b.(itv_hi).

Lemma eq_refl: forall x, eq x x.
Proof.
  unfold eq. auto.
Qed.

Lemma eq_sym: forall x y, eq x y -> eq y x.
Proof.
  unfold eq. intuition auto.
Qed.

Lemma eq_trans: forall x y z, eq x y -> eq y z -> eq x z.
Proof.
  unfold eq. intuition lia.
Qed.

Definition beq (a b : t) :=
  (a.(itv_lo) =? b.(itv_lo)) && (a.(itv_hi) =? b.(itv_hi)).

Lemma beq_correct: forall x y, beq x y = true -> eq x y.
Proof.
  unfold beq, eq. intuition lia.
Qed.


Definition ge (a b : t) :=
  a.(itv_lo) <= b.(itv_lo) /\
  a.(itv_hi) >= b.(itv_hi).

Lemma ge_refl: forall x y, eq x y -> ge x y.
Proof.
  unfold eq, ge. intuition lia.
Qed.

Lemma ge_trans: forall x y z, ge x y -> ge y z -> ge x z.
Proof.
  unfold ge. intuition lia.
Qed.

Program Definition lub (a b : t) :=
  {| itv_lo := Z.min a.(itv_lo) b.(itv_lo);
     itv_hi := Z.max a.(itv_hi) b.(itv_hi);
    itv_ok := _ |}.
Obligation 1.
Proof.
  destruct a. destruct b. cbn. lia.
Qed.

Lemma ge_lub_left: forall x y, ge (lub x y) x.
Proof.
  unfold ge, lub. cbn.
  intuition lia.
Qed.

Lemma ge_lub_right: forall x y, ge (lub x y) y.
Proof.
  unfold ge, lub. cbn.
  intuition lia.
Qed.

Lemma lub_least:
  forall r p q, ge r p -> ge r q -> ge r (lub p q).
Proof.
  unfold ge. intros.
  destruct r. destruct p. destruct q.
  cbn in *. intuition lia.
Qed.

Definition zmatch z itv :=
  itv.(itv_lo) <= z <= itv.(itv_hi).

Definition ge_sem a b :=
  forall z, (zmatch z b) -> (zmatch z a).

Lemma ge_sem_eqv : forall a b,
    ge a b <-> ge_sem a b.
Proof.
  unfold ge, ge_sem, zmatch.
  intros. destruct a. destruct b. cbn.
  split.
  - intuition lia.
  - intro ZZ.
    split.
    + apply ZZ. lia.
    + apply Z.le_ge. apply ZZ. lia.
Qed.

Lemma ge_zmatch:
  forall a b z, ge a b -> zmatch z b -> zmatch z a.
Proof.
  intros. rewrite ge_sem_eqv in H.
  unfold ge_sem in H.
  auto.
Qed.

End Interval.
