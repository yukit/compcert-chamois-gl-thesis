(* *********************************************************************)
(*                                                                     *)
(*              The Compcert verified compiler                         *)
(*                                                                     *)
(*          David Monniaux, CNRS (Verimag), 2023                       *)
(*                                                                     *)
(* *********************************************************************)

(* This pass recognizes tail calls made to the same function (tail recursion) and replaces them with an unconditional branch to the top of the function, after placing parameters into the correct registers. This is faster, since it does not incur register restoration, stack frame deallocation, stack frame allocation and register saving as would a normal tail call.

It must be run after the tail call pass. *)

Require Import Coqlib Wfsimpl Maps Errors Integers Values.
Require Import AST Linking Globalenvs.
Require Import Op Registers RTL.

(* The parameter values are first copied to temporary registers starting at max_reg_function *)
Fixpoint copy_to_temps srcregs tmpreg pc (prec_code : code) : (node * code) :=
  match srcregs with
  | nil => (pc, prec_code)
  | h::t =>
      copy_to_temps t (Pos.succ tmpreg) (Pos.succ pc)
        (PTree.set pc (Iop Omove (h::nil) tmpreg (Pos.succ pc)) prec_code)
  end.

(* Then they are copied from these temporary registers to the registers used for parameters *)
Fixpoint copy_from_temps dstregs tmpreg pc (prec_code : code) : (node * code):=
  match dstregs with
  | nil => (pc, prec_code)
  | h::t =>
      copy_from_temps t (Pos.succ tmpreg) (Pos.succ pc)
        (PTree.set pc (Iop Omove (tmpreg::nil) h (Pos.succ pc)) prec_code)
  end.

(* This builds the loop back sequence in ascending pc order :
- copy to the temporary registers
- copy from the temporary registers
- jump to function entrypoint

We reverse the parameter values and registers so that the first parameter is the last register written to, which reflects the semantics of the function call. Normally this should not matter, since all parameter registers should be distinct, but this would entail defensively checking that it is the case and arguing about non aliasing. We instead just take the parameters in the right order. *)

Definition build_jump start tmpreg srcregs dstregs pc0
           (code0 : code): (node * code) :=
  let (pc1, code1) := copy_to_temps (List.rev srcregs) tmpreg pc0 code0 in
  let (pc2, code2) := copy_from_temps (List.rev dstregs) tmpreg pc1 code1 in
  ((Pos.succ pc2), (PTree.set pc2 (Inop start) code2)).

(* If the instruction is a tail call to the procedure with the same identifier as the current one, and the number of arguments is the same as that of the function parameters (again, this should always be the case but we defensively check for it), replace this call by an unconditional jump to the loop back sequence explained above. *)
Definition process_instr id start params tmpreg new_pc new_code pc instr :=
  match instr with
  | Itailcall sig (inr id') srcregs =>
      if (Pos.eq_dec id id') &&
           (Nat.eq_dec (List.length srcregs) (List.length params))
      then build_jump start tmpreg srcregs params new_pc
                      (PTree.set pc (Inop new_pc) new_code)
      else (new_pc, new_code)
  | _ => (new_pc, new_code)
  end.

(* I thought it a bit dangerous to add new instructions at nonexistent PC values if somehow existing code could jump to these instructions: this would turn undefined behavior (branching to instructions that don't exist) into the execution of a tail recursion call sequence. So we check that all successors of the function are within the instructions of the function. This should not be necessary, though. *)
Definition successors_inside f :=
  let limit := max_pc_function f in
  List.forallb (fun (pc_instr : node*instruction) =>
             let (pc, instr) := pc_instr in
             List.forallb (fun x => Pos.leb x limit)
                          (successors_instr instr))
               (PTree.elements (fn_code f)).

(* Process the function. The new instructions and temporary registers are placed above all instructions and registers inside the function. *)
Definition transf_function' (id: ident) (f: function) :=
      (let start := fn_entrypoint f in
       let tmpreg := Pos.succ (RTL.max_reg_function f) in
       let params := fn_params f in
       let code' := snd
                      (PTree.fold
                         (fun (new_pc_code : node * code) (pc : node) instr =>
                            let (new_pc, new_code) := new_pc_code in
                            process_instr id start params tmpreg new_pc new_code pc instr)
                         (fn_code f)
                         ((Pos.succ (RTL.max_pc_function f)), (fn_code f))) in
       OK (mkfunction f.(fn_sig)
                          params
                          f.(fn_stacksize)
                              code'
                        start)).
  
Definition transf_function (ge: Genv.t fundef unit) (id : ident) (f : function) :=
  if option_eq_dec eq_fundef (find_function ge (inr id) (Regmap.init Vundef)) (Some (Internal f))
  then
    if successors_inside f
    then transf_function' id f
    else Error (msg "Tailrec: successor points outside")
  else Error (msg "Tailrec: functions don't match; report and pass -fno-tailrec until fixed").
    
Definition transf_fundef (ge: Genv.t fundef unit) (id: ident) (fd: fundef) : Errors.res fundef :=
  AST.transf_partial_fundef (transf_function ge id) fd.

Definition transf_program (p: program): Errors.res program :=
  AST.transform_partial_program2 (transf_fundef (Genv.globalenv p)) (fun i v => OK v) p.
